class FoulMenageriePortalBolt : MaelsphereProjectile {
	Default {
		Speed 20;

		SeeSound "FoulMenagerie/Fire";
		DeathSound "FoulMenagerie/Explode";
		Obituary "%o was ripped apart by %k's Portal Bolt!";

		SchismProjectile.SummonType "WraamonVerdant";
		SchismProjectile.SummonStance 0;
		SchismProjectile.SummonFlags 0;
		SchismProjectile.SummonAngleOffset 0;
	}

	States {
		Death:
			FMEN E 4 bright {
				invoker.isDead = true;
			}
			FMEN F 4 bright SummonMinion();
			FMEN GHIJ 4 bright;
			Stop;
	}
}

class FoulMenageriePortalBoltUnstable : FoulMenageriePortalBolt {
	Default {
		SeeSound "FoulMenagerie/Fire";
		DeathSound "FoulMenagerie/Explode";
		Obituary "%o was ripped asunder by %k's Portal Bolt!";

		MaelsphereProjectile.Instability 0.5;
		MaelsphereProjectile.WeaveX 16;
		MaelsphereProjectile.WeaveY 4;
	}
}