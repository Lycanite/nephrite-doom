class DruidBowDimensionArrow : DruidBowLightningArrow {
	Default {
		SeeSound "DruidBow/Dimension/Fire";
		DeathSound "DruidBow/Dimension/Explode";
	}
	
	States {
		Spawn:
			DBWD A 6 bright;
			Loop;
			
		Death:
			DBWD C 0 bright A_SpawnItemEx("UOakPhaseStrike", 0, 0);
			DBWD CDEFG 4 bright {SchismAttacks.Explode(invoker, invoker.target, 6, 50, 0, 1, false, false, "Electric");}
			Stop;
	}
}

class DruidBowDimensionArrowFar : DruidBowDimensionArrow {
	Default {
		Speed 25;
		Gravity 0.1;
		
		SeeSound "DruidBow/Dimension/FireFar";
	}
}

class DruidBowDimensionArrowCharged : DruidBowDimensionArrowFar {
	Default {
		SeeSound "DruidBow/Dimension/FireCharged";
		DeathSound "DruidBow/Dimension/Explode";
	}
	
	States {
		Spawn:
			DBWD B 2 bright A_SpawnItemEx("DruidBowDimensionArrowTrail",random(-2,-8),random(8,-8),random(8,-8),0,0,frandom(0,0.6),0,SXF_NOCHECKPOSITION|SXF_CLIENTSIDE);
			Loop;
			
		Death:
			DBWD M 0 bright {
				A_SpawnItemEx("UOakPhaseStrike", 0, 0);
				A_SpawnItemEx("UOakPhaseStrike", 120);
				A_SpawnItemEx("UOakPhaseStrike", -120);
				A_SpawnItemEx("UOakPhaseStrike", 0, 120);
				A_SpawnItemEx("UOakPhaseStrike", 0, -120);
				A_SpawnItemEx("UOakPhaseStrike", 90, 90);
				A_SpawnItemEx("UOakPhaseStrike", -90, 90);
				A_SpawnItemEx("UOakPhaseStrike", 90, -90);
				A_SpawnItemEx("UOakPhaseStrike", -90, -90);
			}
			DBWD MNOPQR 4 bright {SchismAttacks.Explode(invoker, invoker.target, 6, 50, 0, 1, false, false, "Electric");}
			Stop;
	}
}

class DruidBowDimensionArrowTrail : Actor  {
	Default  {
		RenderStyle "Add";
		Alpha 0.5;
		
		+FORCEXYBILLBOARD
		+NOINTERACTION
	}
	
	States {
		Spawn:
			DBWD H 0 ThrustThingZ(0, Random(5, 10), 0, 0);
			DBWD H 0 ThrustThing(Random(0, 255), Random(0, 10), 0, 0);
		Active:
			DBWD HIJKL 1 bright A_FadeOut(0.05);
			Loop;
	}
}