class NoxiousDartProjectile : SchismProjectile {
	Default {
		Radius 4;
		Height 4;
		Scale 1;
		Speed 25;
		FastSpeed 30;
		DamageFunction 4;
		Alpha 1;
		RenderStyle "Translucent";
		
		DamageType "Poison";
		
		SeeSound "Projectile/NoxiousDart/Fire";
		DeathSound "Projectile/NoxiousDart/Explode";

		SchismProjectile.ParticleName "Poison";
		SchismProjectile.ParticleCount 1;
		SchismProjectile.StatusEffectClass "SchismPoisonEffect";
		SchismProjectile.StatusEffectDuration 5 * 35;
		SchismProjectile.StatusEffectStrength 2;

		+RIPPER;
	}
	
	States {
		Spawn:
			NPND ABCD 4;
			Loop;
			
		Death:
			NPND EFGHIJKL 4;
			Stop;
	}
}