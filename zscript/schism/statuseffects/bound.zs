class SchismBoundEffect : SchismStatusEffect {
	Actor boundTarget;

	Default {
		PowerUp.Duration -10;
		SchismStatusEffect.ParticleName "Arcane";
	}
	
	override void DoEffect() {
		super.DoEffect();
		if (!self.owner || self.owner.bBoss) {
			return;
		}

		if (self.boundTarget) {
			self.owner.Thrust(10, self.owner.AngleTo(self.boundTarget));
		}
		else if (self.effectTics % TICRATE == 0) {
			BlockThingsIterator iter = BlockThingsIterator.Create(self.owner, 300, false);
			while (iter.Next()) {
				Actor target = iter.thing;
				if (!target || target == self.owner || !self.owner.CheckSight(target) || !target.bIsMonster || target.bCorpse) {
					continue;
				}
				if (!SchismInteraction.CanHarmTarget(self.effectInflictor, target)) {
					continue;
				}
				self.boundTarget = target;
				break;
			}
		}
	}
}