class WhirlwindCharge : Ammo {
	Default {
		//$Category "Ammunition"
		Tag "Whirlwind Charge";
		Scale 0.5; 
		
		Inventory.PickupMessage "You gained a Whirlwind Charge!";
		Inventory.Amount 20;
		Inventory.MaxAmount 200;
		Ammo.BackpackAmount 300;
		Ammo.BackpackMaxAmount 300;
		
		Inventory.Icon "AHARA0";
		Inventory.PickupSound "Haruhi/Ammo/Whirlwind";
		Inventory.RestrictedTo "Haruhi";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			AHAR ABC 4 bright;
			Loop;
	}
}

class WhirlwindChargeLarge : WhirlwindCharge {
	Default {
		//$Category "Ammunition"
		Tag "Large Whirlwind Charge";
		
		Scale 0.35;
		Inventory.PickupMessage "You gained a large Whirlwind Charge!";
		Inventory.Amount 100;
		
		RenderStyle "Normal";
	}

	States {
		Spawn:
			AHAR DEF 4 bright;
			Loop;
	}
}

class GlacialCharge : Ammo {
	Default {
		//$Category "Ammunition"
		Tag "Glacial Charge";
		Scale 0.5; 
		
		Inventory.PickupMessage "You gained a Glacial Charge!";
		Inventory.Amount 20;
		Inventory.MaxAmount 200;
		Ammo.BackpackAmount 300;
		Ammo.BackpackMaxAmount 300;
		
		Inventory.Icon "AHARG0";
		Inventory.PickupSound "Haruhi/Ammo/Glacial";
		Inventory.RestrictedTo "Haruhi";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			AHAR GHI 4 bright;
			Loop;
	}
}

class GlacialChargeLarge : GlacialCharge {
	Default {
		//$Category "Ammunition"
		Tag "Large Glacial Charge";
		
		Scale 0.35;
		Inventory.PickupMessage "You gained a large Glacial Charge!";
		Inventory.Amount 100;
		
		RenderStyle "Normal";
	}

	States {
		Spawn:
			AHAR JKL 4 bright;
			Loop;
	}
}

class CelestialCharge : Ammo {
	Default {
		//$Category "Ammunition"
		Tag "Celestial Charge";
		Scale 0.5; 
		
		Inventory.PickupMessage "You gained a Celestial Charge!";
		Inventory.Amount 20;
		Inventory.MaxAmount 200;
		Ammo.BackpackAmount 300;
		Ammo.BackpackMaxAmount 300;
		
		Inventory.Icon "AHARM0";
		Inventory.PickupSound "Haruhi/Ammo/Celestial";
		Inventory.RestrictedTo "Haruhi";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			AHAR MNO 4 bright;
			Loop;
	}
}

class CelestialChargeLarge : CelestialCharge {
	Default {
		//$Category "Ammunition"
		Tag "Large Celestial Charge";
		
		Scale 0.35;
		Inventory.PickupMessage "You gained a large Celestial Charge!";
		Inventory.Amount 100;
		
		RenderStyle "Normal";
	}

	States {
		Spawn:
			AHAR PQR 4 bright;
			Loop;
	}
}

class PranaEnergy : Ammo {
	Default {
		//$Category "Ammunition"
		Tag "Prana";
		
		Scale 0.5;
		Inventory.PickupMessage "You gained Prana!";
		Inventory.Amount 20;
		Inventory.MaxAmount 200;
		Ammo.BackpackAmount 0;
		Ammo.BackpackMaxAmount 200;
		
		Inventory.Icon "AHARS0";
		Inventory.PickupSound "Haruhi/Ammo";
		Inventory.RestrictedTo "Haruhi";
		
		RenderStyle "Add";
		
		+FLOATBOB;
		+INVENTORY.IGNORESKILL;
	}

	States {
		Spawn:
			AHAR STU 4 bright;
			Loop;
	}
}