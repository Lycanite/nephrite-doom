class ArgusScroll : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Foul Menagerie can now summon an Argus at the cost of Havoc!";
		
		Inventory.Icon "REISA0";
		Inventory.PickupSound "Argus/See";
		Inventory.RestrictedTo "Reilodos", "ReilodosHavocSerpent";
		WeaponUpgrade.ExtraAmmoClass "ChaosEnergy";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			REIS A 1 bright;
			Loop;
	}
}

class FoulMenagerieArgusbolt : FoulMenageriePortalBolt {
	Default {
		Radius 16;
		Height 25;
		Scale 1.5;
		DamageFunction random(20, 30);

		SchismProjectile.SummonType "ArgusVerdant";
		SchismProjectile.SummonFlags SXF_NOCHECKPOSITION;
		
		SeeSound "FoulMenagerie/Fire/Argus";
		DeathSound "FoulMenagerie/Explode";
		Obituary "%o was ripped asunder by %k's Argus Bolt!";
	}
	
	override void SpawnParticles() {
		switch (Random(0, 2)) {
			case 1:
				self.SpawnParticle("Chaos", TICRATE, 1, (FRandom(-4, 4), 0, FRandom(-4, 4)), Random(0, 359), Random(-45, 45));
				break;
			case 2:
				self.SpawnParticle("Maelfire", TICRATE, 1, (FRandom(-4, 4), 0, FRandom(-4, 4)), Random(0, 359), Random(-45, 45));
				break;
			default:
				self.SpawnParticle("ChaosSpark", 8, 3, (FRandom(-4, 4), 0, FRandom(-4, 4)), Random(0, 359), Random(-45, 45));
		}
	}
	
	States {
		Spawn:
			FMEN A 2 bright;
			FME2 A 2 bright;
			FMEN B 2 bright;
			FME2 B 2 bright;
			FMEN C 2 bright;
			FME2 C 2 bright;
			FMEN D 2 bright;
			FME2 D 2 bright;
			Loop;
			
		Death:
			FMEN E 2 bright {
				invoker.isDead = true;
			}
			FME2 E 2 bright;
			FMEN F 2 bright SummonMinion();
			FME2 F 2 bright;
			FMEN G 2 bright;
			FME2 G 2 bright;
			FMEN H 2 bright;
			FME2 H 2 bright;
			FMEN I 2 bright;
			FME2 I 2 bright;
			FMEN J 2 bright;
			FME2 J 2 bright;
			Stop;
	}
}
