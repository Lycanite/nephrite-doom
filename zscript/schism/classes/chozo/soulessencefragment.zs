

class SoulEssenceFragment : HomingEffectProjectile {
	Default {
		Scale 0.25;
		Radius 16;
		Height 10;
		Speed 20;
		Gravity 0;
		DamageFunction 0;
		Alpha 1;
		Renderstyle "Add";
		
		SeeSound "Chozo/Soul/Drop";
		DeathSound "Chozo/Soul/Pickup";
		
		PROJECTILE;
		+RANDOMIZE;
		+SEEKERMISSILE;
		+RIPPER;
		+NOCLIP;
	}
	
	override void OnImpact() {
		followTarget.A_GiveInventory("SoulEssence", 1);
		A_StartSound("SoulEssence/Explode");
		super.OnImpact();
	}
	
	States {
		Spawn:
			ACHZ STUV 1 {
				return A_HomeToTarget();
			}
			Loop;
			
		Death:
			ACHZ T 4;
			Stop;
	}
}