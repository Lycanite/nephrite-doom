class Ossifex : MinionCreature {
	Default {
		Tag "Ossifex";
		
		Health 250;
		Radius 10;
		Height 50;
		Speed 20;
		Scale 1;
		Gravity 1;
		PainChance 120;
		Damage 1;
		BloodColor "44 00 00";
		
		MinionCreature.SummonTime 60 * 35;
		
		SeeSound "Ossifex/See";
		PainSound "Ossifex/Pain";
		DeathSound "Ossifex/Death";
		ActiveSound "Ossifex/See";
		MeleeSound "Ossifex/Melee";
		Obituary "%o was decapitated by an Ossifex!";
		HitObituary "%o was decapitated by an Ossifex!";

		+MinionCreature.TAUNT;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		if (Health <= 0) {
			return;
		}
	}
	
	action void A_OssifexAttack() {
		invoker.A_CustomMeleeAttack(random(10, 30), "Ossifex/Melee");
	}
	
	action void A_BoneBurst() {
		for (int i = 0; i < 20; i += 20) {
			invoker.A_SpawnItemEx("SchismGoreBone", 0, 0, 10, random(-6, 6), random(-6, 6), random(10, 40), i, SXF_CLIENTSIDE);
		}
	}
	
	States {
		Spawn:
			OSFX C 4 {
				return A_MinionIdle();
			}
			Loop;
			
		Guard:
			OSFX C 4 {
				return A_MinionGuard();
			}
			Loop;
			
		Follow:
			OSFX ABCD 4 {
				return A_FollowMaster();
			}
			Loop;
			
		See:
			OSFX ABCD 4 {
				return A_MinionChase();
			}
			Loop;
			
		Melee:
			OSFX EF 4 A_FaceTarget();
			OSFX G 4 A_OssifexAttack();
			Goto See;
			
		Pain:
			OSFX H 6 A_Pain();
			Goto See;
			
		Pain.SelfDamage:
			Goto See;
			
		Death:
		XDeath:
			OSFX H 5;
			OSFX I 5 A_Scream();
			OSFX J 5;
			OSFX K 5 A_CreatureDie();
			OSFX LMN 5;
			OSFX O -1 A_BoneBurst();
			Stop;
	}
}
