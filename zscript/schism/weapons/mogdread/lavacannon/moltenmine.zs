class MineModule : WeaponUpgrade {
	Default {
		Scale 1;
		Inventory.PickupMessage "Your Lava Cannon can now fire proximity Molten Mines that will attach to walls and enemies!";
		
		Inventory.Icon "MLVCY0";
		Inventory.PickupSound "LavaCannon/Pickup";
		Inventory.RestrictedTo "Mogdread";
		WeaponUpgrade.ExtraAmmoClass "MoglavaCharge";
		
		RenderStyle "Normal";
	}

	States {
		Spawn:
			MLVC Y 1 bright;
			Loop;
	}
}

class LavaCannonMineProjectile : LavaCannonProjectile {
	bool charged;
	int offsetX;
	int offsetY;
	int offsetZ;
	Actor followTarget;
	
	int chargeTime;
	int expireTime;
	
	Default {
		Radius 5;
		Height 5;
		Scale 1;
		Speed 20;
		DamageFunction 2;
		Alpha 1;
		Renderstyle "Normal";
		
		SeeSound "LavaCannon/Fire/Mine";
		
		+EXPLODEONWATER;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		chargeTime = 35 * 3;
		expireTime = 35 * 30;
	}
	
	override bool CanCollideWith(Actor other, bool passive) {
		if (!passive && !self.followTarget && other != self.target)
		{
			self.setFollowTarget(other);
		}
		return super.CanCollideWith(other, passive);
	}
	
	virtual void setFollowTarget(Actor newTarget) {
		self.followTarget = newTarget;
		self.offsetX = self.pos.x - newTarget.pos.x;
		self.offsetY = self.pos.y - newTarget.pos.y;
		self.offsetZ = self.pos.z - newTarget.pos.z;
	}
	
	action state A_MineAttachUpdate() {
		if (invoker.followTarget)
		{
			return ResolveState("Death");
		}
		return null;
	}
	
	action state A_MineProximityUpdate(bool charged) {
		// Attachment
		invoker.gravity = 0;
		if (invoker.followTarget)
		{
			invoker.SetOrigin(invoker.followTarget.pos +(invoker.offsetX, invoker.offsetY, invoker.offsetZ), false);
		}
		
		// Charging
		if (!charged)
		{
			if (invoker.chargeTime-- <= 0)
			{
				A_StartSound("LavaCannon/Mine/Charged", CHAN_BODY);
				return ResolveState("Charged");
			}
		}
		
		// Expiring
		else if (invoker.expireTime-- <= 0)
		{
			invoker.charged = charged;
			return ResolveState("Detonate");
		}
		
		// Proximity
		BlockThingsIterator iter = BlockThingsIterator.Create(self, 50, false);
		while (iter.Next())
		{
			Actor targetActor = iter.thing;
			if (targetActor != invoker.followTarget && targetActor != invoker.target && targetActor.bIsMonster && !targetActor.bFriendly && targetActor.Health > 0 && CheckSight(targetActor))
			{
				invoker.charged = charged;
				return ResolveState("Detonate");
			}
		}
		
		return null;
	}
	
	action void A_MineDetonate() {
		int damage = 50;
		int radius = 100;
		if (invoker.charged) {
			damage = 500;
			radius = 400;
		}
		A_QuakeEx(2, 2, 2, 10, 0, radius, "world/quake", 0, 1, 1, 0, 0, 1, 1);
		SchismAttacks.Explode(invoker, invoker.target, damage, radius, 100);
		
		for (int i = 0; i < damage / 2; i++) {
			invoker.SpawnParticle(
				"Fire", 2 * TICRATE, 4,
				(FRandom(-8, 8), FRandom(-8, 8), FRandom(-8, -2)),
				Random(0, 359), Random(-10, 10), 1.7
			);
		}
	}
	
	States {
		Spawn:
			MLVB T 1 bright A_MineAttachUpdate();
			Loop;
			
		Death:
			MLVB T 1 bright {
				return A_MineProximityUpdate(false);
			}
			Loop;
			
		Charged:
			MLVB U 1 bright {
				return A_MineProximityUpdate(true);
			}
			Loop;
			
		Detonate:
			MLVB H 2 bright A_MineDetonate();
			MLVB IJKLMNOP 2 bright;
			Stop;
	}
}
