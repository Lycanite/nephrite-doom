class SchismLevel : Thinker {
	const MD_SIDE_FRONT = 0;
	const MD_SIDE_BACK  = 1;
	const MD_TEXTURE_UPPER = 0;
	const MD_TEXTURE_MIDDLE = 1;
	const MD_TEXTURE_LOWER = 2;

	TextureId[1024] floorTextures;
	TextureId[1024] ceilingTextures;
	
	SchismLevel Init() {
		ChangeStatNum(STAT_STATIC);
		return self;
	}
	
	static SchismLevel Get() {
		ThinkerIterator it = ThinkerIterator.Create("SchismLevel", STAT_STATIC);
		SchismLevel p = SchismLevel(it.Next());
		if (p == null) {
			p = new("SchismLevel").Init();
		}
		return p;
	}

	/** 
	 * Replaces a sector floor and/or ceiling texture with another, useful for tagging all sectors together and doing a texture replace.
	 * @param sectorTag The tag of the sector to replace the textures of.
	 * @param targetTextureName The name of the texture to replace.
	 * @param replacementTextureName The name of the texture to replace with.
	 * @param floor If true the floor will be replaced.
	 * @param ceiling If true the ceiling will be replaced.
	 * @param lines If true all line textures of the sector will be replaced.
	 */
	static void ReplaceSectorTextures(int sectorTag, string targetTextureName, string replacementTextureName, bool floor, bool ceiling, bool lines) {
		TextureId targetTexture = TexMan.CheckForTexture(targetTextureName);
		TextureId replacementTexture = TexMan.CheckForTexture(replacementTextureName);
		SchismLevel schismLevel = SchismLevel.Get();
		SectorTagIterator sectorIter = Level.CreateSectorTagIterator(sectorTag);
		int i = 0;
		while ((i = sectorIter.Next()) > 0) {
			if (floor && Level.sectors[i].GetTexture(sector.floor) == targetTexture) {
				Level.sectors[i].SetTexture(sector.floor, replacementTexture);
			}
			if (ceiling && Level.sectors[i].GetTexture(sector.ceiling) == targetTexture) {
				Level.sectors[i].SetTexture(sector.ceiling, replacementTexture);
			}
			if (lines) {
				for (int j = 0; j < Level.sectors[i].lines.Size(); j++) {
					if (Level.sectors[i].lines[j].sidedef[MD_SIDE_FRONT]) {
						if (Level.sectors[i].lines[j].sidedef[MD_SIDE_FRONT].GetTexture(MD_TEXTURE_UPPER) == targetTexture) {
							Level.sectors[i].lines[j].sidedef[MD_SIDE_FRONT].SetTexture(MD_TEXTURE_UPPER, replacementTexture);
						}
						if (Level.sectors[i].lines[j].sidedef[MD_SIDE_FRONT].GetTexture(MD_TEXTURE_MIDDLE) == targetTexture) {
							Level.sectors[i].lines[j].sidedef[MD_SIDE_FRONT].SetTexture(MD_TEXTURE_MIDDLE, replacementTexture);
						}
						if (Level.sectors[i].lines[j].sidedef[MD_SIDE_FRONT].GetTexture(MD_TEXTURE_LOWER) == targetTexture) {
							Level.sectors[i].lines[j].sidedef[MD_SIDE_FRONT].SetTexture(MD_TEXTURE_LOWER, replacementTexture);
						}
					}
					if (Level.sectors[i].lines[j].sidedef[MD_SIDE_BACK]) {
						if (Level.sectors[i].lines[j].sidedef[MD_SIDE_BACK].GetTexture(MD_TEXTURE_UPPER) == targetTexture) {
							Level.sectors[i].lines[j].sidedef[MD_SIDE_BACK].SetTexture(MD_TEXTURE_UPPER, replacementTexture);
						}
						if (Level.sectors[i].lines[j].sidedef[MD_SIDE_BACK].GetTexture(MD_TEXTURE_MIDDLE) == targetTexture) {
							Level.sectors[i].lines[j].sidedef[MD_SIDE_BACK].SetTexture(MD_TEXTURE_MIDDLE, replacementTexture);
						}
						if (Level.sectors[i].lines[j].sidedef[MD_SIDE_BACK].GetTexture(MD_TEXTURE_LOWER) == targetTexture) {
							Level.sectors[i].lines[j].sidedef[MD_SIDE_BACK].SetTexture(MD_TEXTURE_LOWER, replacementTexture);
						}
					}
				}
			}
		}
	}

	/** 
	 * Snapshots the floor texture of the provided sector tag for use with RevertFloorTexture.
	 * @param sectorTag The tag of the sector floor texture to snapshot.
	 */
	static void SnapshotFloorTexture(int sectorTag) {
		SchismLevel schismLevel = SchismLevel.Get();
		SectorTagIterator sectorIter = Level.CreateSectorTagIterator(sectorTag);
		int i = 0;
		while ((i = sectorIter.Next()) > 0) {
			schismLevel.floorTextures[sectorTag] = Level.sectors[i].GetTexture(sector.floor);
		}
	}

	/** 
	 * Reverts the floor texture of the provided sector tag, should be set initially with SnapshotFloorTexture.
	 * @param sectorTag The tag of the sector floor texture to revert.
	 */
	static void RevertFloorTexture(int sectorTag) {
		TextureId texture = SchismLevel.Get().floorTextures[sectorTag];
		SectorTagIterator sectorIter = Level.CreateSectorTagIterator(sectorTag);
		int i = 0;
		while ((i = sectorIter.Next()) > 0) {
			Level.sectors[i].SetTexture(sector.floor, texture);
		}
	}

	/** 
	 * Snapshots the ceiling texture of the provided sector tag for use with RevertCeilingTexture.
	 * @param sectorTag The tag of the sector ceiling texture to snapshot.
	 */
	static void SnapshotCeilingTexture(int sectorTag) {
		SchismLevel schismLevel = SchismLevel.Get();
		SectorTagIterator sectorIter = Level.CreateSectorTagIterator(sectorTag);
		int i = 0;
		while ((i = sectorIter.Next()) > 0) {
			schismLevel.ceilingTextures[sectorTag] = Level.sectors[i].GetTexture(sector.ceiling);
		}
	}

	/** 
	 * Reverts the ceiling texture of the provided sector tag, should be set initially with SnapshotCeilingTexture.
	 * @param sectorTag The tag of the sector ceiling texture to revert.
	 */
	static void RevertCeilingTexture(int sectorTag) {
		TextureId texture = SchismLevel.Get().ceilingTextures[sectorTag];
		SectorTagIterator sectorIter = Level.CreateSectorTagIterator(sectorTag);
		int i = 0;
		while ((i = sectorIter.Next()) > 0) {
			Level.sectors[i].SetTexture(sector.ceiling, texture);
		}
	}

	/** 
	 * Revives all trees in the tag sectors.
	 * @param sectorTag The tag of the sector to heal trees in.
	 */
	static void ReviveSectorTrees(int sectorTag) {
		ThinkerIterator treeIterator = ThinkerIterator.Create("SchismTree");
		SchismTree tree;
		while (tree = SchismTree(treeIterator.Next())) {
			for (int i = 0; i < tree.CurSector.CountTags(); i++) {
				if (tree.CurSector.GetTag(i) == sectorTag) {
					tree.ReviveTree();
				}
			}
		}
	}

	/** 
	 * Overrides the season of trees in the tagged sector.
	 * @param sectorTag The tag of the sector to set the season of trees in.
	 * @param season The season to set. 0 = Auto, 1 = Spring, 2 = Summer, 3 = Autumn, 4 = Winter
	 */
	static void SeasonSectorTrees(int sectorTag, int season) {
		ThinkerIterator treeIterator = ThinkerIterator.Create("SchismTree");
		SchismTree tree;
		while (tree = SchismTree(treeIterator.Next())) {
			for (int i = 0; i < tree.CurSector.CountTags(); i++) {
				if (tree.CurSector.GetTag(i) == sectorTag) {
					tree.SetSeason(season);
				}
			}
		}
	}
}
