class Aglebemu : Creature {
	Default {
		Tag "Aglebemu";
		
		Health 200;
		Radius 20;
		Height 56;
		Speed 24;
		FloatSpeed 14;
		Scale 0.6;
		PainChance 250;
		Damage 2;
		MeleeRange 200;
		BloodColor "ff 00 00";

		DamageFactor "Poison", 0.1;

		+JUMPDOWN;

		+Creature.SWIMMER;
		
		SeeSound "Aglebemu/See";
		ActiveSound "Aglebemu/Idle";
		PainSound "Aglebemu/Pain";
		MeleeSound "Aglebemu/Melee";
		DeathSound "Aglebemu/Death";
		Obituary "%o was tongued in and swallowed whole by an Aglebemu!";
		HitObituary "%o was tongued in and swallowed whole by an Aglebemu!";
	}

	action void A_AglebemuAttack() {
		A_CreatureMelee(2, 6, "PoisonBubble", "Poison");
		if (invoker.target && FRandom(0, 1) >= 0.75) {
			invoker.target.Thrust(12, invoker.target.AngleTo(invoker));
			invoker.target.AddZ(12, false);
		}
	}
	
	States {
		Spawn:
			AGLE AABBCCBB 4 A_Look();
			Loop;
			
		See:
			AGLE DEFG 4 {
				if (FRandom(0, 1) >= 0.75) {
					A_Leap(Random(10, 20));
				}
				A_Chase();
			}
			Loop;
			
		Melee:
			AGLE HI 8 A_FaceTarget();
			AGLE J 8 A_AglebemuAttack();
			AGLE K 8;
			Goto See;
			
		Pain:
			AGLE L 6;
			AGLE M 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			AGLE L 5;
			AGLE M 5 A_Scream();
			AGLE NO 5;
			AGLE P 5 A_NoBlocking();
			AGLE QRSTUVW 5;
			AGLE X -1;
			Stop;
	}
}
