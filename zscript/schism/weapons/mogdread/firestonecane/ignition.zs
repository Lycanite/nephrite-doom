class IgnitionGem : WeaponPassive {
	Default {
		Scale 1;
		Inventory.PickupMessage "Your Firestone Cane can be used to ignite yourself in a flurry or vengeful flames!";
		
		Inventory.Icon "FSCNU0";
		Inventory.PickupSound "FirestoneCane/Pickup";
		Inventory.RestrictedTo "Mogdread";
		WeaponUpgrade.ExtraAmmoClass "FirestoneCharge";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			FSCN U 1 bright;
			Loop;
	}
}

class FirestoneCaneIgnition : SchismImmolateDebuff {
	Default {
		SchismStatusEffect.EffectDamage 1;
		SchismStatusEffect.EffectSpeedFactor 1.1;
		SchismStatusEffect.ParticleOffsetZ -40;
	}
	
	override void RefreshEffect(Actor effectInflictor, Actor effectSource, int effectTime, Class<SchismStatusEffect> spread, int strength) {
		super.RefreshEffect(effectInflictor, effectSource, effectTime, spread, strength);
		
		self.effectRadius = 100 * self.strength;
		self.effectSpeedFactor = 1.1;

		self.particleScale = 0.25 * self.strength;
		self.particleCount = self.strength;
		self.particleInterval = Min(Round(Float(TICRATE) / self.strength), 1);
		self.particleOffsetRandom = self.effectRadius;

		if (self.strength > 1) {
			self.effectSpeedFactor = 1.0 + (0.25 * (self.strength - 1)); // 25% and 50% speed boosts.
			self.particleCount = self.strength * 5;
		}
	}
}
