class AnzuOssifex : Creature {
	Default {
		Tag "Anzu Ossifex";
		
		Health 300;
		Radius 20;
		Height 56;
		Speed 10;
		Scale 1;
		Gravity 1;
		PainChance 100;
		Damage 1;
		MeleeThreshold 196;
		BloodColor "AA 22 00";
		
		SeeSound "AnzuOssifex/See";
		PainSound "AnzuOssifex/Pain";
		DeathSound "AnzuOssifex/Death";
		ActiveSound "AnzuOssifex/See";
		MeleeSound "AnzuOssifex/Melee";
		Obituary "%o couldn't dodge an Anzu Ossifex!";
		HitObituary "%o was punched into next wee by an Anzu Ossifex!";

		+MISSILEMORE;
	}
	
	States {
		Spawn:
			AZOX AB 10 A_Look();
			Loop;
			
		See:
			AZOX AABBCCDDEEFF 4 A_Chase();
			Loop;
			
		Melee:
			AZOX D 0 A_FaceTarget();
			AZOX D 6 A_SkelWhoosh();
			AZOX E 6 A_FaceTarget();
			AZOX F 6 A_CreatureMelee(6, 60, "Quake", "Quake");
			Goto See;
			
		Missile:
			AZOX F 0 bright A_FaceTarget();
			AZOX F 10 bright A_FaceTarget();
			AZOX G 10 A_SkelMissile();
			AZOX G 10 A_FaceTarget();
			Goto See;
			
		Pain:
			AZOX H 5;
			AZOX H 5 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			AZOX HM 7;
			AZOX N 7 A_Scream();
			AZOX O 7 A_CreatureDie();
			AZOX P 7;
			AZOX Q -1;
			Stop;
	}
}
