class NecronomiconBoneSplintProjectile : SoulEssenceProjectile {
	Default {
		Radius 3;
		Height 5;
		Scale 0.6;
		Speed 60;
		DamageFunction 6;
		Alpha 1;
		RenderStyle "Normal";
		BounceType "Grenade";
		BounceCount 2;
		
		SoulEssenceProjectile.SoulEssenceMin 3;
		SoulEssenceProjectile.SoulEssenceMin 6;
		
		SeeSound "Necronomicon/Fire/BoneSplint";
		DeathSound "Necronomicon/Explode/BoneSplint";
		
		+NODAMAGETHRUST;
	}
	
	States {
		Spawn:
			NCR1 ABCDEFGH 1;
			Loop;
			
		Death:
			NCR1 H 4;
			Stop;
	}
}

class NecronomiconBonePitProjectile : SchismProjectile {
	Default {
		Radius 16;
		Height 10;
		Scale 1;
		Speed 60;
		DamageFunction 10;
		Alpha 1;
		Renderstyle "Normal";
		
		SeeSound "Necronomicon/Fire/BonePit";
		DeathSound "Necronomicon/Explode/BonePit";
		
		+DONTSPLASH;
		+NODAMAGETHRUST;
		+PAINLESS;
		+FORCERADIUSDMG;
	}
	
	States {
		Spawn:
		Death:
			NCR1 I 2 {
				A_StartSound("Necronomicon/Fire/BonePit");
				SchismAttacks.Explode(invoker, invoker.target, 2, 100);
			}
			NCR1 JKLMNO 2 {SchismAttacks.Explode(invoker, invoker.target, 2, 100);}
			NCR1 OOOOOOO 4 {SchismAttacks.Explode(invoker, invoker.target, 2, 150);}
			NCR1 OOOOOOO 4 {SchismAttacks.Explode(invoker, invoker.target, 2, 100);}
			NCR1 O 4 {
				A_Scream();
				SchismAttacks.Explode(invoker, invoker.target, 2, 10);
			}
			NCR1 NMLKJI 4 {SchismAttacks.Explode(invoker, invoker.target, 2, 10);}
			Stop;
	}
}