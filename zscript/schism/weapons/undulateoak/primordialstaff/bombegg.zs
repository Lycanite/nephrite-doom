class BombEgg : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Primordial Staff can now lay a massive Egg Bomb full to the brim with explosive magic!";
		
		Inventory.Icon "WCCKX0";
		Inventory.PickupSound "PrimordialStaff/Pickup";
		Inventory.RestrictedTo "UndulateOak", "UndulateOakCockatrice";
		WeaponUpgrade.ExtraAmmoClass "LifeSeed";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			WCCK W 1 bright;
			Loop;
	}
}

class PrimordialStaffBomb : EctoplasmProjectile {
	Default {
		Radius 16;
		Height 10;
		Scale 1;
		Speed 12;
		DamageFunction 20;
		Gravity 0.125;
		Alpha 1;
		Renderstyle "Normal";
		Projectile;
		
		SeeSound "PrimordialStaff/Bomb";
		DeathSound "PrimordialStaff/Bomb/Explode";
		
		+RANDOMIZE;
		-NOGRAVITY;
		+EXPLODEONWATER;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		ectoplasmMin = 3;
		ectoplasmMax = 5;
	}
	
	States {
		Spawn:
			WCEG FGHI 4 bright;
			Loop;
			
		Death:
			WCEG J 4 bright {
				Gravity = 0;
				SchismAttacks.Explode(invoker, invoker.target, 10, 200, 10);
				A_QuakeEx(2, 2, 2, 10, 0, 500, "world/quake", 0, 1, 1, 0, 0, 1, 1);
				for (int i = 0; i < 360; i += 10) {
					A_SpawnProjectile("PrimordialStaffProjectile", 0, 0, i, CMF_ABSOLUTEPITCH, -25);
				}
			}
			WCEG KLMNO 4;
			TNT1 A 16;
			Stop;
	}
}
