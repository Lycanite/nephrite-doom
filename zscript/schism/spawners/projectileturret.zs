class SchismProjectileTurret : Actor {
	int turretTics;
	string user_projectileClass;

	Default {
		Tag "Projectile Turret";
		Gravity 0;
		Scale 1;
		
		+NOBLOCKMAP;
		+NOCLIP;
		+DONTSPLASH;
		+DONTBLAST;
		+NOTELEPORT;
		+NOGRAVITY;
	}

	override void Activate(Actor activator) {
		super.Activate(activator);
		self.bDormant = false;
	}

	override void Deactivate(Actor deactivator) {
		super.Deactivate(deactivator);
		self.bDormant = true;
	}
	
	/** 
	 * Fires this turret's projectile.
	 * @arg 0 The speed of the projectile.
	 * @arg 1 The rate of the projectile (delay in tics).
	 */
	action void A_ProjectileTurret() {
		if (invoker.bDormant) {
			return;
		}
		if (!invoker.user_projectileClass) {
			A_Log("No Projectile Class set for Turret!");
			return;
		}
		double projectileSpeed = invoker.args[0];
		int rate = invoker.args[1] > 0 ? invoker.args[1] : 60;
		if (invoker.turretTics++ % rate == 0) {
			bool spawned;
			Actor projectileActor;
			[spawned, projectileActor] = A_SpawnItemEx(
				invoker.user_projectileClass,
				0, 0, 0,
				cos(invoker.pitch) * projectileSpeed, 0, -(sin(invoker.pitch) * projectileSpeed),
				0, SXF_NOCHECKPOSITION
			);
			if (projectileActor) {
				projectileActor.scale.x *= invoker.scale.x;
				projectileActor.scale.y *= invoker.scale.y;
				projectileActor.A_SetSize(projectileActor.radius * invoker.scale.x, projectileActor.height * invoker.scale.y);
			}
		}
	}
	
	States {
		Spawn:
			TNT1 A 1 NoDelay A_ProjectileTurret();
			Loop;
	}
}