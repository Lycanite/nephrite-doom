class SchismShrub : VariantDecoration {
	Default {
		Tag "Shrub";
		
		Radius 24;
		Height 100;
		ProjectilePassHeight -16;

        VariantDecoration.Variants 1;
		
		+SOLID;
	}
}

class SchismShrubSaguaro : SchismShrub {
	Default {
		Tag "Saguaro Cactus";
        VariantDecoration.Variants 8;
	}
	
	States {
		Variant.1:
			SH00 A -1;
			Stop;
		Variant.2:
			SH00 B -1;
			Stop;
		Variant.3:
			SH00 C -1;
			Stop;
		Variant.4:
			SH00 D -1;
			Stop;
		Variant.5:
			SH00 E -1;
			Stop;
		Variant.6:
			SH00 F -1;
			Stop;
		Variant.7:
			SH00 G -1;
			Stop;
		Variant.8:
			SH00 H -1;
			Stop;
	}
}

class SchismShrubCholla : SchismShrub {
	Default {
		Tag "Cholla Cactus";
        VariantDecoration.Variants 4;
	}

	States {
		Variant.1:
			SH01 A -1;
			Stop;
		Variant.2:
			SH01 B -1;
			Stop;
		Variant.3:
			SH01 C -1;
			Stop;
		Variant.4:
			SH01 D -1;
			Stop;
	}
}

class SchismShrubHolodum : SchismShrub {
	Default {
		Tag "Holodum Shrub";
        VariantDecoration.Variants 2;
	}
	
	States {
		Variant.1:
			SH02 A -1;
			Stop;
		Variant.2:
			SH02 B -1;
			Stop;
	}
}

class SchismShrubBoxwood : SchismShrub {
	Default {
		Tag "Boxwood Shrub";
        VariantDecoration.Variants 3;
	}
	
	States {
		Variant.1:
			SH03 A -1;
			Stop;
		Variant.2:
			SH03 B -1;
			Stop;
		Variant.3:
			SH03 C -1;
			Stop;
	}
}

class SchismShrubHolly : SchismShrub {
	Default {
		Tag "Holly Shrub";
        VariantDecoration.Variants 3;
	}
	
	States {
		Variant.1:
			SH04 A -1;
			Stop;
		Variant.2:
			SH04 B -1;
			Stop;
		Variant.3:
			SH04 C -1;
			Stop;
	}
}

class SchismShrubRotundifolia : SchismShrub {
	Default {
		Tag "Rotundifolia Shrub";
	}
	
	States {
		Variant.1:
			SH05 A -1;
			Stop;
		Variant.2:
			SH05 B -1;
			Stop;
	}
}

class SchismShrubCypress : SchismShrub {
	Default {
		Tag "Cypress Shrub";
	}
	
	States {
		Variant.1:
			SH06 A -1;
			Stop;
		Variant.2:
			SH06 B -1;
			Stop;
	}
}

class SchismShrubZinnia : SchismShrub {
	Default {
		Tag "Zinnia Shrub";
	}
	
	States {
		Variant.1:
			SH07 A -1;
			Stop;
		Variant.2:
			SH07 B -1;
			Stop;
	}
}