class SchismChillDebuff : SchismStatusEffect {
	double speedOriginal;
	
	Default {
		PowerUp.Duration -10;
		
		SchismStatusEffect.Detrimental true;
		SchismStatusEffect.ParticleInterval 3;
		SchismStatusEffect.ParticleName "Frost";
	}
	
	override string GetFollowEffectClass() {
		return "SchismChillFollowEffect";
	}
	
	override double GetSpeedFactor() {
		if (!self.strength) {
			return 1;
		}
		if (PlayerPawn(self.owner)) {
			return 0.6 / self.strength;
		}
		return 0.3 / self.strength;
	}
	
	override void InitEffect() {
		super.InitEffect();
		if (!self.owner) {
			self.EffectInvalid = true;
			return;
		}
		
		speedOriginal = self.owner.speed;
		self.owner.speed = owner.speed * 0.5;
	}
	
	override void EndEffect() {
		super.EndEffect();
		if (self.EffectInvalid || !self.owner) {
			return;
		}
		
		self.owner.speed = self.speedOriginal;
	}
	
	override void OnOwnerDamaged(int damage, string damageType, actor inflictor) {
		super.OnOwnerDamaged(damage, damageType, inflictor);
		if (damageType == "Fire" || damageType == "Lava") {
			self.ChillBurst();
		}
	}
	
	bool ChillBurst(bool soak = true, int damageOverride = 0) {
		bool success = false;
		SchismChillFollowEffect chillProjectile = SchismChillFollowEffect(self.followEffect);
		if (chillProjectile && owner && owner.Health > 0) {
			success = chillProjectile.ChillBurst(effectInflictor, damageOverride);
		}
		
		// Remove Chill and Inflict Soak:
		if (soak) {
			Actor chilledTarget = self.owner;
			if (!chilledTarget) {
				return success;
			}
			self.RemoveEffect();
			SchismStatusEffects.InflictEffect("SchismSoakDebuff", self.effectInflictor, self.effectInflictor, chilledTarget, 15 * 35);
		}
		
		return success;
	}
}

class SchismChillFollowEffect : SchismFollowEffect {
	Default {
		SeeSound "Debuff/Chill";
		DeathSound "Debuff/Chill";
		Obituary "%o was frozen by %k's frost magic!";
		
		SchismProjectile.GenerateSpecialAmmo false;
	}
	
	bool ChillBurst(Actor triggerActor, int damageOverride = 0) {
		Actor effectTarget = self.GetFollowTarget();
		effectTarget.A_StartSound("Debuff/Chill/Burst");
		effectTarget.A_QuakeEx(2, 2, 2, 10, 0, 500, "world/quake", 0, 1, 1, 0, 0, 1, 1);

		int damage = 20;
		if (damageOverride != 0) {
			damage = damageOverride;
		}
		SchismAttacks.Explode(self, triggerActor, damage, 100, 0, 1, false, false);

		for (int i = 0; i < 200; i++) {
			self.SpawnParticleFromActor(
				effectTarget, "Frost", 2 * TICRATE, 4,
				(FRandom(-8, 8), FRandom(-8, 8), (effectTarget.height / 2) + FRandom(-8, -2)),
				Random(0, 359), Random(0, 359), 1
			);
			self.SpawnParticleFromActor(
				effectTarget, "Snowflake", 2 * TICRATE, 8,
				(FRandom(-8, 8), FRandom(-8, 8), (effectTarget.height / 2) + FRandom(-8, -2)),
				Random(0, 359), Random(0, 359), 1
			);
		}
		return true;
	}
}