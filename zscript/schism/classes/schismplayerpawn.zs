class SchismPlayerPawn : PlayerPawn {
	mixin SpawnParticleMixin;

	bool user_clearTeleportAbilities;
	
	string baseName;
	string hexenClass;
	int updateTic;
	double oldVelZ;
	
	bool beaconing;
	int beaconTime;
	int beaconSummonCount;
	int beaconSummonablePlayerAmount;
	int beaconTeleportCooldown;
	
	int abilityCooldownMax;
	int abilityCooldown;
	int abilityBCooldownMax;
	int abilityBCooldown;
	bool passiveEnabled;
	int comboPoints;
	
	array<MinionCreature> minions;
	
	vector3 teleportRecallPos;
	double teleportRecallAngle;
	int teleportRecallTime;
	vector3 teleportRecallSwapPos;
	actor teleportRecallSwapTarget;
	
	SchismPlayerInfo schismPlayerInfo;

	property BaseName: baseName;
	property HexenClass: hexenClass;
	property AbilityCooldownMax: abilityCooldownMax;
	property AbilityBCooldownMax: abilityBCooldownMax;

	Default {
		Speed 1;
		Health 100;
		Radius 16;
		Height 56;
		Mass 100;
		Scale 0.7;
		PainChance 255;
		RadiusDamageFactor 0.25;

		SchismPlayerPawn.AbilityCooldownMax TICRATE;
		SchismPlayerPawn.AbilityBCooldownMax TICRATE;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.updateTic = 0;

		self.beaconing = false;
		self.beaconTime = 0;
		self.beaconSummonCount = 0;
		self.beaconSummonablePlayerAmount = 0;
		self.beaconTeleportCooldown = 0;

		self.abilityCooldown = abilityCooldownMax;
		self.abilityBCooldown = abilityBCooldownMax;

		self.passiveEnabled = true;
	}
	
	override void Travelled() {
		super.Travelled();
		self.teleportRecallTime = 0; // Remove teleport recall on map change.
	}
	
	// Override to add an extra check for morphing to prevent a crash! https://github.com/coelckers/gzdoom/blob/master/wadsrc/static/zscript/actors/player/player.zs#L440
	override void TickPSprites() {
		let player = self.player;
		let pspr = player.psprites;
		while (pspr) {
			if ((pspr.Caller == null ||
				(pspr.Caller is "Inventory" && Inventory(pspr.Caller).Owner != pspr.Owner.mo) ||
				(pspr.Caller is "Weapon" && pspr.Caller != pspr.Owner.ReadyWeapon))
			) {
				pspr.Destroy();
			} else {
				pspr.Tick();
			}

			pspr = pspr.Next;
		}

		if ((health > 0) ||(player.ReadyWeapon != null && !player.ReadyWeapon.bNoDeathInput)) {
			if (player.ReadyWeapon == null) {
				if (player.PendingWeapon != WP_NOCHANGE) {
					player.mo.BringUpWeapon();
				}
			}
			else if (self.player) { // Added a check to see if player is still set (which is is not when morphing).
				self.CheckWeaponChange();
				if (player.WeaponState &(WF_WEAPONREADY | WF_WEAPONREADYALT)) {
					self.CheckWeaponFire();
				}
				self.CheckWeaponButtons();
			}
		}
	}

	// Override to add an extra check for player.mo to prevent a random crash in multiplayer. https://github.com/ZDoom/gzdoom/blob/master/wadsrc/static/zscript/actors/player/player.zs#L1051
	override bool CheckFrozen() {
		if (!self.player || !self.player.mo) {
			return false;
		}
		return super.CheckFrozen();
	}

	// Override to add an extra check for player.mo to prevent a random crash in multiplayer. https://github.com/ZDoom/gzdoom/blob/master/wadsrc/static/zscript/actors/player/player.zs#L729
	override void DeathThink() {
		if (!self.player || !self.player.mo) {
			return;
		}
		super.DeathThink();
	}

	// Override to add an extra check for player.mo to prevent a random crash in multiplayer. https://github.com/ZDoom/gzdoom/blob/master/wadsrc/static/zscript/actors/player/player.zs#L1026
	override void CheckCheats() {
		if (!self.player) {
			return;
		}
		super.CheckCheats();
	}
	
	/** 
	 * Gets an ammo class for the provided category and size.
	 * @param category The id of the category to get an ammo class for.
	 * @param size The id of the ammo size. 0 = Small, 1 = Large
	 * @return Returns the ammo class or an empty string if the category or size is invalid.
	 */
	virtual clearscope string GetAmmoClass(int category, int size) {
		return "";
	}
	
	/** 
	 * Gets a weapon class for the provided category and index.
	 * Note: Mode based weapons have their first upgrade initially, in this case index 1 (first upgrade) will be invalid for these weapons.
	 * @param category The id of the category to a get weapon class for.
	 * @param index The index of the weapon/upgrade/passive in the category. 0 = Base Weapon, 1-4 Weapon Upgrades, 5 Weapon Passive
	 * @return Returns the weapon class or an empty string if the category or index is invalid.
	 */
	virtual clearscope class<Inventory> GetWeaponClass(int category, int index) {
		return "";
	}
	
	/** 
	 * Gets the sigil item class, used for activating weapon ultimates.
	 * @return Returns the sigil item class.
	 */
	virtual clearscope class<Inventory> GetSigilClass() {
		return "";
	}

	/** 
	 * Populates weapons into the provided weapon spawner as weapon entries.
	 * @param category The id of the category to populate weapon entries for.
	 * @param weaponEntries The weapon entry array to add weapons entries to.
	 * @return Returns true on success or false if the weapon category is invalid.
	 */
	virtual bool PopulateWeaponSpawns(int category, Array<WeaponEntry> weaponEntries) {
		switch(category) {
			case 0: // Bonus (Chainsaw) - Passives
				self.CreateWeaponEntry(self.GetWeaponClass(0, 5), category, weaponEntries, 3, self.GetWeaponClass(0, 0));
				self.CreateWeaponEntry(self.GetWeaponClass(1, 5), category, weaponEntries, 3, self.GetWeaponClass(1, 0));
				self.CreateWeaponEntry(self.GetWeaponClass(2, 5), category, weaponEntries, 3, self.GetWeaponClass(2, 0));
				return true;

			case 1: // Primary (Pistol) - First (Starter) Weapon
				self.CreateWeaponEntry(self.GetWeaponClass(0, 0), category, weaponEntries);
				return true;

			case 2: // Secondary (Shotgun) - Second Weapon and Upgrades 1
				self.CreateWeaponEntry(self.GetWeaponClass(1, 0), category, weaponEntries);

				self.CreateWeaponEntry(self.GetWeaponClass(0, 1), category, weaponEntries, 1, self.GetWeaponClass(0, 0));
				self.CreateWeaponEntry(self.GetWeaponClass(1, 1), category, weaponEntries, 1, self.GetWeaponClass(1, 0));
				self.CreateWeaponEntry(self.GetWeaponClass(2, 1), category, weaponEntries, 1, self.GetWeaponClass(2, 0));
				return true;

			case 3: // Tertiary (Chaingun) - Third Weapon and Upgrades 2
				self.CreateWeaponEntry(self.GetWeaponClass(2, 0), category, weaponEntries);

				self.CreateWeaponEntry(self.GetWeaponClass(0, 2), category, weaponEntries, 1, self.GetWeaponClass(0, 0));
				self.CreateWeaponEntry(self.GetWeaponClass(1, 2), category, weaponEntries, 1, self.GetWeaponClass(1, 0));
				self.CreateWeaponEntry(self.GetWeaponClass(2, 2), category, weaponEntries, 1, self.GetWeaponClass(2, 0));
				return true;

			case 4: // Powerful (Rocket Launcher) - Upgrades 3, Passives, Novelty Weapons (Disabled)
			case 5: // Special (Plasma Rifle)
				self.CreateWeaponEntry(self.GetWeaponClass(0, 3), category, weaponEntries, 1, self.GetWeaponClass(0, 0));
				self.CreateWeaponEntry(self.GetWeaponClass(1, 3), category, weaponEntries, 1, self.GetWeaponClass(1, 0));
				self.CreateWeaponEntry(self.GetWeaponClass(2, 3), category, weaponEntries, 1, self.GetWeaponClass(2, 0));

				self.CreateWeaponEntry(self.GetWeaponClass(0, 5), category, weaponEntries, 3, self.GetWeaponClass(0, 0));
				self.CreateWeaponEntry(self.GetWeaponClass(1, 5), category, weaponEntries, 3, self.GetWeaponClass(1, 0));
				self.CreateWeaponEntry(self.GetWeaponClass(2, 5), category, weaponEntries, 3, self.GetWeaponClass(2, 0));

				// self.CreateWeaponEntry(self.GetWeaponClass(3, 0), category, weaponEntries); // Novelty Weapons (Barons Claw, etc)
				// self.CreateWeaponEntry(self.GetWeaponClass(3, 1), category, weaponEntries);
				// self.CreateWeaponEntry(self.GetWeaponClass(3, 2), category, weaponEntries);

				return true;

			case 6: // Ultimate (BFG9000) - Upgrades 4 - Should also be spawning Sigils
				self.CreateWeaponEntry(self.GetWeaponClass(0, 4), category, weaponEntries, 1, self.GetWeaponClass(0, 0));
				self.CreateWeaponEntry(self.GetWeaponClass(1, 4), category, weaponEntries, 1, self.GetWeaponClass(1, 0));
				self.CreateWeaponEntry(self.GetWeaponClass(2, 4), category, weaponEntries, 1, self.GetWeaponClass(2, 0));
				self.CreateWeaponEntry(self.GetSigilClass(), category, weaponEntries, 1);
				return true;

			default:
				return false;
		}
	}
	
	/** 
	 * Creates a weapon entry used to determine weapon spawns for the provided class and category.
	 * @param weaponClass The weapon/upgrade/passive class the entry is for.
	 * @param category The weapon category the entry is for.
	 * @param weaponEntries The weapon entry array to add weapons entries to.
	 * @param itemCount The maximum inventory count for this entry to be considered fully collected. (Usually 1, 3 for passives). Defaults to 1.
	 * @param dependantClass An optional class of item that the player needs to have in order to make use of the entry weapon (the base weapon for upgrades/passives, etc).
	 * @return Returns the newly created weapon entry or null if the class is invalid.
	 */
	WeaponEntry CreateWeaponEntry(class<Inventory> weaponClass, int category, Array<WeaponEntry> weaponEntries, int itemCount = 1, class<Inventory> dependantClass = "") {
		if (!weaponClass) {
			return null;
		}
		WeaponEntry weaponEntry = New("WeaponEntry").Init();
		weaponEntry.weaponClass = weaponClass;
		weaponEntry.weaponCategory = category;
		weaponEntry.playerNum = self.PlayerNumber();
		weaponEntry.itemCount = itemCount;
		weaponEntry.dependantClass = dependantClass;
		weaponEntries.Push(weaponEntry);
		return weaponEntry;
	}
	
	/** 
	 * Determines the category of the currently equipped weapon, used by HUD, etc.
	 * @return The category id of the currently equipped weapon if valid, -1 otherwise.
	 */
	clearscope int GetActiveWeaponCategory() {
		if (!self.player || !playeringame[self.PlayerNumber()] || !self.player.ReadyWeapon) {
			return -1;
		}
		string activeWeaponClass = self.player.ReadyWeapon.GetClassName();
		for (int categoryId = 0; categoryId <= 2; categoryId++) {
			if (activeWeaponClass == self.GetWeaponClass(categoryId, 0)) {
				return categoryId;
			}
		}
		return -1;
	}
	
	/** 
	 * Determines if the provided weapon category has the provided upgrade id.
	 * @param category The id of the category to a check upgrades for.
	 * @param upgrade The id of the upgrade to check for, should start from 1 as 0 will check for the base weapon itself.
	 * @return True if this player has the upgrade, false otherwise.
	 */
	clearscope bool HasWeaponUpgrade(int category, int upgrade) {
		class<Inventory> upgradeClass = self.GetWeaponClass(category, upgrade);
		if (upgradeClass) {
			return CountInv(upgradeClass);
		}
		return false;
	}
	
	/** 
	 * Gets the passive upgrade level of the provided weapon category.
	 * @param category The id of the category to get the passive level of.
	 * @return The passive effect level of the provided weapon category.
	 */
	virtual clearscope int GetWeaponPassiveLevel(int category) {
		return 0;
	}
	
	
	// On Tick:
	override void Tick() {
		// Voodoo Doll:
		if (!self.player || !self.player.mo || self.player.mo != self) {
			super.Tick();
			return;
		}
		
		self.UpdateTic++;
		self.oldVelZ = vel.z;
		
		// Schism Player Info;
		if (!self.schismPlayerInfo) {
			self.schismPlayerInfo = SchismPlayerInfo.Get();
		}
		
		// Beacon:
		if (self.beaconing) {
			if (--self.beaconTime <= 0 || self.beaconSummonCount >= self.beaconSummonablePlayerAmount || self.health <= 0) {
				self.BeaconStop();
			} else {
				self.OnBeaconTick();
			}
		}
		if (self.beaconTeleportCooldown > 0) {
			self.beaconTeleportCooldown--;
		}
		
		// Ability Cooldowns:
		if (self.abilityCooldown > 0) {
			self.abilityCooldown--;
		}
		if (self.abilityBCooldown > 0) {
			self.abilityBCooldown--;
		}
		
		// Temporary Teleport Recall:
		if (self.teleportRecallTime > 0) {
			if (--self.teleportRecallTime == 0) {
				self.SetOrigin(self.teleportRecallPos, true);
				if (self.teleportRecallSwapTarget) {
					self.teleportRecallSwapTarget.SetOrigin(self.teleportRecallSwapPos, true);
				}
			}
		}
		
		super.Tick();
	}
	
	
	// On Damage:
	override int DamageMobj(Actor inflictor, Actor source, int damage, Name damageType, int flags, double angle) {
		Actor sourceMaster = source;

		// Get true source through projectiles and minions:
		int sourceIter = 0;
		while (sourceMaster != null && sourceIter < 10) {
			if (sourceMaster == self) {
				return 0;
			}
			sourceMaster = sourceMaster.master;
			sourceIter++;
		}

		// Reduce PVP Damage:
		if (PlayerPawn(sourceMaster)) {
			damage *= 0.1; // 10% PVP Damage Scale
		}

		// Minion Shields:
		if (self.schismPlayerInfo) {
			MinionCreature shieldMinion = self.schismPlayerInfo.GetMinion(self.PlayerNumber(), "", -1, true);
			if (shieldMinion) {
				shieldMinion.DamageMobj(inflictor, source, damage, damageType, flags, angle);
				return 0;
			}

		}

		return super.DamageMobj(inflictor, source, damage, damageType, flags, angle);
	}
	

	// Status Effect Immunity Check: (Called by SchismStatusEffects.IfnlictEffect)
	virtual bool CanApplyEffect(class<Inventory> effectClass) {
		return true;
	}
	
	
	/**
	 * Called when this player damages another target, triggered by an event listener.
	 * @param damageTarget The actor being damaged by the player.
	 * @param damageDealt The amount of damage being dealt.
	 * @param damageType The type of the damage dealt.
	 * @param inflictor The direct cause of the damage (could be a projectile, etc).
	 */
	virtual void OnDamageDealtFrom(Actor damageTarget, double damageDealt, Name damageType, Actor inflictor) {}
	
	
	// Secret Sound:
	override bool OnGiveSecret(bool printmsg, bool playsound) {
		self.A_StartSound("*secret", CHAN_BODY);
		return super.OnGiveSecret(printmsg, playsound);
	}
	
	
	// Door Locked Sound:
	virtual void OnLinePreActivated(Line ActivatedLine, int ActivationType, bool ShouldActivate) {
		if (ActivationType != SPAC_Use && ActivationType != SPAC_UseThrough && ActivationType != SPAC_UseBack) {
			return;
		}
		
		// Door Locked Raise:
		if (ActivatedLine.special == 13 && ActivatedLine.args[3] != 0) {
			OnKeyNeeded(ActivatedLine.args[3]);
			return;
		}
		// Door Animated:
		if (ActivatedLine.special == 14 && ActivatedLine.args[3] != 0) {
			OnKeyNeeded(ActivatedLine.args[3]);
			return;
		}
		// Script Locked Exec:
		if (ActivatedLine.special == 83 && ActivatedLine.args[4] != 0) {
			OnKeyNeeded(ActivatedLine.args[4]);
			return;
		}
		// Script Locked Exec(Door Message):
		if (ActivatedLine.special == 85 && ActivatedLine.args[4] != 0) {
			OnKeyNeeded(ActivatedLine.args[4]);
			return;
		}
		// Door Generic:
		if (ActivatedLine.special == 202 && ActivatedLine.args[4] != 0) {
			OnKeyNeeded(ActivatedLine.args[4]);
			return;
		}
	}
	
	virtual void OnKeyNeeded(int neededKeyNumber) {
		self.A_StartSound("*keyfail", CHAN_BODY);
	}
	
	
	// Taunting:
	virtual void Taunt() {
		self.A_StartSound("*taunt", CHAN_BODY);
	}
	
	
	// Special Abilities:
	virtual bool SchismSpecial(int specialId) {
		if (specialId == 1) {
			if (self.abilityCooldown > 0) {
				return false;
			}
			self.abilityCooldown = self.abilityCooldownMax;
		} else if (specialId == 2) {
			if (self.abilityBCooldown > 0) {
				return false;
			}
			self.abilityBCooldown = self.abilityBCooldownMax;
		}
		return true;
	}
	
	
	// Toggle Passive:
	virtual void SchismPassive() {
		if (self.passiveEnabled) {
			self.A_StartSound("Class/Passive/Disable", CHAN_BODY);
		} else {
			self.A_StartSound("Class/Passive/Enable", CHAN_BODY);
		}
		self.passiveEnabled = !self.passiveEnabled;
	}
	
	
	// Beaconing:
	virtual void SchismBeacon() {
		if (!beaconing) {
			self.BeaconStart();
		}
		else {
			self.BeaconStop();
		}
	}
	
	virtual void BeaconStart() {
		if (!self.player || !playeringame[self.PlayerNumber()]) {
			return;
		}
		
		self.beaconTime = 10 * 35;
		self.beaconSummonCount = 0;
		self.beaconSummonablePlayerAmount = 0;
		self.beaconing = true;
		
		for (int playerNum = 0; playerNum < players.Size(); playerNum++) {
			if (!playeringame[playerNum]) {
				continue;
			}
			PlayerInfo targetplayerInfo = players[playerNum];
			SchismPlayerPawn schismPlayer = SchismPlayerPawn(targetplayerInfo.mo);
			if (schismPlayer && schismPlayer != self) {
				self.beaconSummonablePlayerAmount++;
				schismPlayer.A_Print(self.player.GetUserName() .. " is beaconing for allies to teleport to them!", 6);
			}
		}
	}
	
	virtual void BeaconStop() {
		self.beaconing = false;
		self.A_StopSound(CHAN_7);
	}
	
	virtual void OnBeaconTick() {
		self.A_StartSound("*beacon", CHAN_7, CHANF_LOOPING|CHAN_NOSTOP, 1.0, ATTN_NONE);
	}
	
	virtual void OnBeaconSummonPlayer() {
		self.beaconSummonCount++;
	}
	
	
	// Beacon Teleporting:
	virtual void SchismTeleport() {
		self.BeaconStop();
		
		if (beaconTeleportCooldown > 0) {
			self.A_StartSound("*usefail", CHAN_BODY);
			self.A_Print("Your beacon teleport is on cooldown!");
			return;
		}
		
		bool beaconingPlayerFound = false;
		for (int playerNum = 0; playerNum < players.Size(); playerNum++) {
			if (!playeringame[playerNum]) {
				continue;
			}
			PlayerInfo playerInfoInstance = players[playerNum];
			SchismPlayerPawn schismPlayer = SchismPlayerPawn(playerInfoInstance.mo);
			if (schismPlayer && schismPlayer.beaconing) {
				beaconingPlayerFound = true;
				self.TeleportToPlayer(schismPlayer);
				break;
			}
		}
		if (!beaconingPlayerFound) {
			self.A_StartSound("*usefail", CHAN_BODY);
			self.A_Print("No beaconing players to teleport to!");
		}
	}
	
	virtual void TeleportToPlayer(SchismPlayerPawn schismPlayer) {
		self.tracer = schismPlayer;
		if (A_Warp(AAPTR_TRACER, -(Radius + schismPlayer.Radius + 1), 0, 0, 0, 0, "Spawn") != null) {
			self.OnTeleportToBeaconSuccess();
			schismPlayer.OnBeaconSummonPlayer();
		} else {
			self.OnTeleportToBeaconFail();
		}
	}
	
	virtual void OnTeleportToBeaconSuccess() {
		self.A_SpawnItem("TeleportFog");
		self.A_StartSound("*teleport", CHAN_5);
		self.beaconTeleportCooldown = 10 * 35;
	}
	
	virtual void OnTeleportToBeaconFail() {
		self.A_StartSound("*usefail", CHAN_BODY);
		self.A_Print("\caNo safe place behind ally to teleport to.");
	}
	
	
	// Temporary Teleport Recall:
	virtual void StartTeleportRecall(int recallTime, vector3 swapPos =(0, 0, 0), Actor swapTarget = null) {
		self.teleportRecallPos = self.pos;
		self.teleportRecallAngle = self.angle;
		self.teleportRecallTime = recallTime;
		self.teleportRecallSwapPos = swapPos;
		self.teleportRecallSwapTarget = swapTarget;
	}
	
	
	// Change Class:
	virtual void ChangeClass(int classNumber) {
		string newClass = "Reilodos";
		if (classNumber == 1) {
			newClass = "UndulateOak";
		}
		else if (classNumber == 2) {
			newClass = "Mogdread";
		}
		else if (classNumber == 3) {
			newClass = "Chozo";
		}
		else if (classNumber == 4) {
			newClass = "Haruhi";
		}
		self.A_Morph(newClass, 0x7FFFFFFF, MRF_FULLHEALTH|MRF_WHENINVULNERABLE|MRF_NEWTIDBEHAVIOUR);
	}
}
