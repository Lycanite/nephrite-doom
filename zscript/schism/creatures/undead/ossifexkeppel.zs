class OssifexKeppel : Ossifex {
	Default {
		Tag "Keppel Ossifex";
	}
	
	States {
		Spawn:
			OSFK C 4 {
				return A_MinionIdle();
			}
			Loop;
			
		Guard:
			OSFK C 4 {
				return A_MinionGuard();
			}
			Loop;
			
		Follow:
			OSFK ABCD 4 {
				return A_FollowMaster();
			}
			Loop;
			
		See:
			OSFK ABCD 4 {
				return A_MinionChase();
			}
			Loop;
			
		Melee:
			OSFK EF 4 A_FaceTarget();
			OSFK G 4 A_OssifexAttack();
			Goto See;
			
		Pain:
			OSFK H 6 A_Pain();
			Goto See;
			
		Pain.SelfDamage:
			Goto See;
			
		Death:
		XDeath:
			OSFK H 5;
			OSFK I 5 A_Scream();
			OSFK J 5;
			OSFK K 5 A_CreatureDie();
			OSFK LMN 5;
			OSFK O -1 A_BoneBurst();
			Stop;
	}
}
