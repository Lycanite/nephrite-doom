class SchismRock : VariantDecoration {
	Default {
		Tag "Rock";
		
		Radius 20;
		Height 40;
		ProjectilePassHeight -8;
		
		VariantDecoration.Variants 1;
		
		+SOLID;
	}
}

class SchismRockSmall : SchismRock {
	Default {
		Tag "Small Rock";
		Radius 8;
		Height 16;
	}
}

class SchismRockChert : SchismRock {
	Default {
		Tag "Chert Rock";
		VariantDecoration.Variants 4;
	}
	
	States {
		Variant.1:
			NRK1 A -1;
			Stop;
		Variant.2:
			NRK1 B -1;
			Stop;
		Variant.3:
			NRK1 C -1;
			Stop;
		Variant.4:
			NRK1 D -1;
			Stop;
	}
}

class SchismRockChertSmall : SchismRockSmall {
	Default {
		Tag "Small Chert Rock";
		VariantDecoration.Variants 2;
	}
	
	States {
		Variant.1:
			NRK1 E -1;
			Stop;
		Variant.2:
			NRK1 F -1;
			Stop;
	}
}

class SchismRockSandstone : SchismRock {
	Default {
		Tag "Sandstone Rock";
		VariantDecoration.Variants 4;
	}
	
	States {
		Variant.1:
			NRK2 A -1;
			Stop;
		Variant.2:
			NRK2 B -1;
			Stop;
		Variant.3:
			NRK2 C -1;
			Stop;
		Variant.4:
			NRK2 D -1;
			Stop;
	}
}

class SchismRockSandstoneSmall : SchismRockSmall {
	Default {
		Tag "Small Sandstone Rock";
		VariantDecoration.Variants 2;
	}
	
	States {
		Variant.1:
			NRK2 E -1;
			Stop;
		Variant.2:
			NRK2 F -1;
			Stop;
	}
}

class SchismRockSlate : SchismRock {
	Default {
		Tag "Slate Rock";
		VariantDecoration.Variants 4;
	}
	
	States {
		Variant.1:
			NRK3 A -1;
			Stop;
		Variant.2:
			NRK3 B -1;
			Stop;
		Variant.3:
			NRK3 C -1;
			Stop;
		Variant.4:
			NRK3 D -1;
			Stop;
	}
}

class SchismRockSlateSmall : SchismRockSmall {
	Default {
		Tag "Small Slate Rock";
		VariantDecoration.Variants 2;
	}
	
	States {
		Variant.1:
			NRK3 E -1;
			Stop;
		Variant.2:
			NRK3 F -1;
			Stop;
	}
}

class SchismRockTuff : SchismRock {
	Default {
		Tag "Tuff Rock";
		VariantDecoration.Variants 4;
	}
	
	States {
		Variant.1:
			NRK4 A -1;
			Stop;
		Variant.2:
			NRK4 B -1;
			Stop;
		Variant.3:
			NRK4 C -1;
			Stop;
		Variant.4:
			NRK4 D -1;
			Stop;
	}
}

class SchismRockTuffSmall : SchismRockSmall {
	Default {
		Tag "Small Tuff Rock";
		VariantDecoration.Variants 2;
	}
	
	States {
		Variant.1:
			NRK4 E -1;
			Stop;
		Variant.2:
			NRK4 F -1;
			Stop;
	}
}

class SchismRockSchist : SchismRock {
	Default {
		Tag "Schist Rock";
		VariantDecoration.Variants 4;
	}
	
	States {
		Variant.1:
			NRK5 A -1;
			Stop;
		Variant.2:
			NRK5 B -1;
			Stop;
		Variant.3:
			NRK5 C -1;
			Stop;
		Variant.4:
			NRK5 D -1;
			Stop;
	}
}

class SchismRockSchistSmall : SchismRockSmall {
	Default {
		Tag "Small Schist Rock";
		VariantDecoration.Variants 2;
	}
	
	States {
		Variant.1:
			NRK5 E -1;
			Stop;
		Variant.2:
			NRK5 F -1;
			Stop;
	}
}

class SchismRockScoria : SchismRock {
	Default {
		Tag "Scoria Rock";
		VariantDecoration.Variants 4;
	}
	
	States {
		Variant.1:
			NRK6 A -1;
			Stop;
		Variant.2:
			NRK6 B -1;
			Stop;
		Variant.3:
			NRK6 C -1;
			Stop;
		Variant.4:
			NRK6 D -1;
			Stop;
	}
}

class SchismRockScoriaSmall : SchismRockSmall {
	Default {
		Tag "Small Scoria Rock";
		VariantDecoration.Variants 2;
	}
	
	States {
		Variant.1:
			NRK6 E -1;
			Stop;
		Variant.2:
			NRK6 F -1;
			Stop;
	}
}

class SchismRockLimestone : SchismRock {
	Default {
		Tag "Limestone Rock";
		VariantDecoration.Variants 4;
	}
	
	States {
		Variant.1:
			NRK7 A -1;
			Stop;
		Variant.2:
			NRK7 B -1;
			Stop;
		Variant.3:
			NRK7 C -1;
			Stop;
		Variant.4:
			NRK7 D -1;
			Stop;
	}
}

class SchismRockLimestoneSmall : SchismRockSmall {
	Default {
		Tag "Small Limestone Rock";
		VariantDecoration.Variants 2;
	}
	
	States {
		Variant.1:
			NRK7 E -1;
			Stop;
		Variant.2:
			NRK7 F -1;
			Stop;
	}
}

class SchismRockBasalt : SchismRock {
	Default {
		Tag "Basalt Rock";
		VariantDecoration.Variants 4;
	}
	
	States {
		Variant.1:
			NRK8 A -1;
			Stop;
		Variant.2:
			NRK8 B -1;
			Stop;
		Variant.3:
			NRK8 C -1;
			Stop;
		Variant.4:
			NRK8 D -1;
			Stop;
	}
}

class SchismRockBasaltSmall : SchismRockSmall {
	Default {
		Tag "Small Basalt Rock";
		VariantDecoration.Variants 2;
	}
	
	States {
		Variant.1:
			NRK8 E -1;
			Stop;
		Variant.2:
			NRK8 F -1;
			Stop;
	}
}

class SchismRockIce : SchismRock {
	Default {
		Tag "Ice Rock";
		VariantDecoration.Variants 4;
	}
	
	States {
		Variant.1:
			NRK9 A -1;
			Stop;
		Variant.2:
			NRK9 B -1;
			Stop;
		Variant.3:
			NRK9 C -1;
			Stop;
		Variant.4:
			NRK9 D -1;
			Stop;
	}
}

class SchismRockIceSmall : SchismRockSmall {
	Default {
		Tag "Small Ice Rock";
		VariantDecoration.Variants 2;
	}
	
	States {
		Variant.1:
			NRK9 E -1;
			Stop;
		Variant.2:
			NRK9 F -1;
			Stop;
	}
}

class SchismCrystalTallGreen : SchismRock {
	Default {
		Tag "Green Crystal Tall";
		
		Radius 20;
		Height 60;
	}
	
	States {
		Spawn:
			NCRS A -1;
			Stop;
	}
}

class SchismCrystalTallRed : SchismCrystalTallGreen {
	Default {
		Tag "Red Crystal Tall";
	}
	
	States {
		Spawn:
			NCRS B -1;
			Stop;
	}
}

class SchismCrystalTallBlue : SchismCrystalTallGreen {
	Default {
		Tag "Blue Crystal Tall";
	}
	
	States {
		Spawn:
			NCRS C -1;
			Stop;
	}
}

class SchismCrystalTallMagenta : SchismCrystalTallGreen {
	Default {
		Tag "Magenta Crystal Tall";
	}
	
	States {
		Spawn:
			NCRS D -1;
			Stop;
	}
}

class SchismCrystalTallYellow : SchismCrystalTallGreen {
	Default {
		Tag "Yellow Crystal Tall";
	}
	
	States {
		Spawn:
			NCRS E -1;
			Stop;
	}
}

class SchismCrystalTallTan : SchismCrystalTallGreen {
	Default {
		Tag "Tan Crystal Tall";
	}
	
	States {
		Spawn:
			NCRS F -1;
			Stop;
	}
}

class SchismCrystalTallBrown : SchismCrystalTallGreen {
	Default {
		Tag "Brown Crystal Tall";
	}
	
	States {
		Spawn:
			NCRS G -1;
			Stop;
	}
}

class SchismCrystalTallWhite : SchismCrystalTallGreen {
	Default {
		Tag "White Crystal Tall";
	}
	
	States {
		Spawn:
			NCRS H -1;
			Stop;
	}
}

class SchismCrystalTallBlack : SchismCrystalTallGreen {
	Default {
		Tag "Black Crystal Tall";
	}
	
	States {
		Spawn:
			NCRS I -1;
			Stop;
	}
}

class SchismCrystalGreen : SchismRock {
	Default {
		Tag "Green Crystal";
		
		Radius 14;
		Height 36;
	}
	
	States {
		Spawn:
			NCRS J -1;
			Stop;
	}
}

class SchismCrystalRed : SchismCrystalGreen {
	Default {
		Tag "Red Crystal";
	}
	
	States {
		Spawn:
			NCRS K -1;
			Stop;
	}
}

class SchismCrystalBlue : SchismCrystalGreen {
	Default {
		Tag "Blue Crystal";
	}
	
	States {
		Spawn:
			NCRS L -1;
			Stop;
	}
}

class SchismCrystalMagenta : SchismCrystalGreen {
	Default {
		Tag "Magenta Crystal";
	}
	
	States {
		Spawn:
			NCRS M -1;
			Stop;
	}
}

class SchismCrystalYellow : SchismCrystalGreen {
	Default {
		Tag "Yellow Crystal";
	}
	
	States {
		Spawn:
			NCRS N -1;
			Stop;
	}
}

class SchismCrystalTan : SchismCrystalGreen {
	Default {
		Tag "Tan Crystal";
	}
	
	States {
		Spawn:
			NCRS O -1;
			Stop;
	}
}

class SchismCrystalBrown : SchismCrystalGreen {
	Default {
		Tag "Brown Crystal";
	}
	
	States {
		Spawn:
			NCRS P -1;
			Stop;
	}
}

class SchismCrystalWhite : SchismCrystalGreen {
	Default {
		Tag "White Crystal";
	}
	
	States {
		Spawn:
			NCRS Q -1;
			Stop;
	}
}

class SchismCrystalBlack : SchismCrystalGreen {
	Default {
		Tag "Black Crystal";
	}
	
	States {
		Spawn:
			NCRS R -1;
			Stop;
	}
}

class SchismCrystalSmallGreen : SchismRock {
	Default {
		Tag "Green Crystal Small";
		
		Radius 14;
		Height 36;
	}
	
	States {
		Spawn:
			NCRS S -1;
			Stop;
	}
}

class SchismCrystalSmallRed : SchismCrystalSmallGreen {
	Default {
		Tag "Red Crystal Small";
	}
	
	States {
		Spawn:
			NCRS T -1;
			Stop;
	}
}

class SchismCrystalSmallBlue : SchismCrystalSmallGreen {
	Default {
		Tag "Blue Crystal Small";
	}
	
	States {
		Spawn:
			NCRS U -1;
			Stop;
	}
}

class SchismCrystalSmallMagenta : SchismCrystalSmallGreen {
	Default {
		Tag "Magenta Crystal Small";
	}
	
	States {
		Spawn:
			NCRS V -1;
			Stop;
	}
}

class SchismCrystalSmallYellow : SchismCrystalSmallGreen {
	Default {
		Tag "Yellow Crystal Small";
	}
	
	States {
		Spawn:
			NCRS W -1;
			Stop;
	}
}

class SchismCrystalSmallTan : SchismCrystalSmallGreen {
	Default {
		Tag "Tan Crystal Small";
	}
	
	States {
		Spawn:
			NCRS X -1;
			Stop;
	}
}

class SchismCrystalSmallBrown : SchismCrystalSmallGreen {
	Default {
		Tag "Brown Crystal Small";
	}
	
	States {
		Spawn:
			NCRS Y -1;
			Stop;
	}
}

class SchismCrystalSmallWhite : SchismCrystalSmallGreen {
	Default {
		Tag "White Crystal Small";
	}
	
	States {
		Spawn:
			NCRS Z -1;
			Stop;
	}
}

class SchismCrystalSmallBlack : SchismCrystalSmallGreen {
	Default {
		Tag "Black Crystal Small";
	}
	
	States {
		Spawn:
			NCRS [ -1;
			Stop;
	}
}

class SchismColourableTallCrystal : SchismCrystalTallWhite {
    string user_translation;
	
	Default {
		Tag "Colourable Tall Crystal";
	}
	
	override void PostBeginPlay()
    {
	    super.PostBeginPlay();
		if(!self.user_translation){
		A_Log("No translation set for Colourable Tall Crystal.");
		return;
		}
		
		A_SetTranslation(self.user_translation);
    }
}

class SchismColourableCrystal : SchismCrystalWhite {
    string user_translation;
	
	Default {
		Tag "Colourable Crystal";
	}
	
	override void PostBeginPlay()
    {
	    super.PostBeginPlay();
		if(!self.user_translation){
		A_Log("No translation set for Colourable Crystal.");
		return;
		}
		
		A_SetTranslation(self.user_translation);
    }
}

class SchismColourableSmallCrystal : SchismCrystalSmallWhite {
    string user_translation;
	
	Default {
		Tag "Colourable Small Crystal";
	}
	
	override void PostBeginPlay()
    {
	    super.PostBeginPlay();
		if(!self.user_translation){
		A_Log("No translation set for Colourable Small Crystal.");
		return;
		}
		
		A_SetTranslation(self.user_translation);
    }
}