extend class BloodGlobe {
	bool siphonLocked; // Use to prevent refire after killing a siphon target.

	virtual bool Siphon(Actor target = null) {
		if (self.siphonTarget) {
			target = self.siphonTarget;
		} else {
			self.siphonTarget = target;
		}
		if (!target || target.bCorpse || target.health <= 0) {
			self.A_ClearSiphon();
			return false;
		}

		self.siphonLocked = true;

		// Pain Stun:
		if (!target.bBoss && !SchismUtil.ActorInState(target, "Pain")) {
			target.SetStateLabel("Pain");
		}

		// Drain Health:
		target.DamageMobj(self.owner, self.owner, 4, "Fae", DMG_THRUSTLESS);
		self.owner.A_DamageSelf(-1, "None", DMSS_FOILINVUL|DMSS_NOFACTOR|DMSS_NOPROTECT);

		// Blood Animation:
		self.SpawnParticleFromActor(target, "Fae", TICRATE, 8, (0, 0, target.height / 2), Random(0, 360), Random(-45, 45), target.height / 40);

		// Float:
		if (!target.bBoss && target.mass <= 500) {
			vector3 floatPos = self.owner.Vec3Angle(self.owner.radius + 40, self.owner.angle, 20);
			target.SetOrigin(floatPos, true);
		}

		// Anima:
		self.owner.A_GiveInventory(self.ammoType2, 1);

		return true;
	}

	action void A_SiphonStart() {
		invoker.siphonLocked = false;
	}

	action state A_Siphon(bool useAmmo) {
		if (!A_CheckAmmo(2)) {
			return invoker.ResolveState("Ready");
		}
		if (useAmmo) {
			A_SpendAmmo(2);
		}

		if (!invoker.Siphon(invoker.siphonTarget) && useAmmo && !invoker.siphonLocked) {
			BloodGlobeSiphonProjectile siphonProjectile = BloodGlobeSiphonProjectile(
				A_FireProjectile("BloodGlobeSiphonProjectile", 0, false, 0, 0)
			);
			if (siphonProjectile) {
				siphonProjectile.bloodGlobe = invoker;
			}
		}

		return null;
	}

	action void A_ClearSiphon() {
		invoker.siphonTarget = null;
	}

	States {
		MainHand.AltFire:
			VSH1 K 0 {
				A_ClearBobbing();
				A_SiphonStart();
			}
			VSH1 KM 4 A_Light2();
		AltHold:
			VSH1 N 2 {
				return A_Siphon(true);
			}
			VSH1 NOOPPOO 2 {
				return A_Siphon(false);
			}
			VSH1 O 0 A_ReFire("AltHold");
			VSH1 O 0 {
				A_ClearSiphon();
				A_Light0();
			}
			Goto Ready;
	}
}

class BloodGlobeSiphonProjectile : SchismProjectile {
    BloodGlobe bloodGlobe;

	Default {
		Radius 4;
		Height 8;
		Scale 1;
		Speed 100;
		DamageFunction 1;
		Alpha 0.75;
		RenderStyle "Add";

        SchismProjectile.LifetimeMax TICRATE;
		
		SeeSound "BloodGlobe/Fire/Siphon";
		DeathSound "BloodGlobe/Explode";
		
		+NODAMAGETHRUST;
		+PAINLESS;
		+SEEKERMISSILE;
		+SCREENSEEKER;
		-THRUGHOST;
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);

        if (!specialDamage || !self.bloodGlobe) {
            return specialDamage;
        }

        // Set or Drain Siphon Target:
        self.bloodGlobe.Siphon(targetActor);
        return 0;
    }

    action state A_SiphonUpdate() {
        if (!invoker.bloodGlobe || invoker.bloodGlobe.siphonTarget) {
            return invoker.ResolveState("Death");
        }

        // Seek New Siphon Target:
        A_SeekerMissile(0, 20, SMF_LOOK|SMF_PRECISE);
        return null;
    }
	
	States {
		Spawn:
			VBLS AAAABBBBCCCCDDDD 1 {
                A_SiphonUpdate();
            }
			Loop;
			
		Death:
			VBLS AAAABBBBCCCCDDDD 1 A_FadeOut(0.05);
			Stop;
	}
}