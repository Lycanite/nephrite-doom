class TwisterTalisman : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Raijin's Battleaxe can now cast Reality Twister to knockback and displace your foes!";
		
		Inventory.Icon "RAJWC0";
		Inventory.PickupSound "RaijinsBattleaxe/Pickup";
		Inventory.RestrictedTo "UndulateOak", "UndulateOakCockatrice";
		WeaponUpgrade.ExtraAmmoClass "StormSphere";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			RAJW C 1 bright;
			Loop;
	}
}

class RaijinsBattleaxeTwisterProjectile : EctoplasmProjectile {
	Actor followTarget;
	int orbitRange;
	int orbitAngle;
	int orbitSpeed;
	int damageCooldown;
	
	Actor lastHit;
	
	Default {
		Radius 20;
		Height 64;
		Speed 15;
		Scale 0.35;
		DamageFunction 2;
		Gravity 0;
		Alpha 1;
		Renderstyle "Add";
		Projectile;
		
		SeeSound "";
		DeathSound "";
		
		BounceType "Hexen";

		SchismProjectile.KnockbackStrength 20;
		
		+RIPPER;
		+SKYEXPLODE;
		+BLOODLESSIMPACT;
		-BLOODSPLATTER;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		ectoplasmMin = 1;
		ectoplasmMax = 1;
		orbitSpeed = 6;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		
		if (self.followTarget) {
			int angle = self.orbitAngle += self.orbitSpeed;
			int offsetX = self.orbitRange * sin(angle);
			int offsetY = self.orbitRange * cos(angle);
			self.SetOrigin(self.followTarget.pos +(offsetX, offsetY, self.followTarget.height * 0.65), false);
			self.orbitRange += self.orbitSpeed;
		}
		
		if (!self.followTarget) {
			Destroy();
		}
		
		self.damageCooldown--;
		
		for (int i = 0; i < 2; i++) {
			A_SpawnItemEx("RaijinsBattleaxeDreadstormRain", random(-100, 100), random(-100, 100), random(20, 30), 0, 0, random(-10, -15), 0, SXF_CLIENTSIDE);
		}
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		if (self.damageCooldown > 0) {
			return 0;
		}
		self.damageCooldown = 5;
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		
		// Soak, Phase Teleswapping and Knockback:
		if (specialDamage > 0 && !targetActor.bFriendly && targetActor.health > 0 && !PlayerPawn(targetActor)) {
			if (targetActor.CountInv("SchismSoakDebuff") == 0) {
				SchismSoakDebuff soakDebuff = SchismSoakDebuff(targetActor.GiveInventoryType("SchismSoakDebuff"));
				if (soakDebuff) {
					soakDebuff.ApplyEffect(self.target, self);
				}
			}
			if (!targetActor.bBoss) {
				if (self.lastHit && self.lastHit.health > 0 && self.lastHit != targetActor) {
					Vector3 lastHitPos = self.lastHit.pos;
					self.lastHit.SetOrigin(targetActor.pos, false);
					self.lastHit.A_SpawnItemEx("OakBlink", 0, 0, 0);
					targetActor.SetOrigin(lastHitPos, false);
					targetActor.A_SpawnItemEx("OakBlink", 0, 0, 0);
				}
				self.lastHit = targetActor;
			}
		}
		
		return specialDamage;
	}
	
	virtual void setFollowTarget(Actor newTarget, int orbitOffset) {
		self.followTarget = newTarget;
		self.orbitRange = 0;
		self.orbitAngle = orbitOffset;
		if (newTarget) {
			self.orbitAngle += newTarget.angle;
		}
	}
	
	States {
		Spawn:
			RAJT ABCDEFGHIJK 4 bright;
		Death:
			Stop;
	}
}
