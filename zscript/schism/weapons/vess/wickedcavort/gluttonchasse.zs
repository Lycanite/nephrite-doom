class GluttonChasseScroll : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "You can now perform the Glutton Chasse to shift your enemies into an infight.";
		
		Inventory.Icon "VESWJ0";
		Inventory.PickupSound "WickedCavort/Pickup";
		Inventory.RestrictedTo "Vess";
		WeaponUpgrade.ExtraAmmoClass "LustDrupe";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			VESW J 1 bright;
			Loop;
	}
}

extend class WickedCavort {
	action state A_GluttonChasse() {
		if (!A_CheckSpecial(2) || !A_SpendAmmo(4)) {
			return ResolveState("Ready");
		}

		for (int i = 1; i <= 16; i++) {
			bool high = i % 2 != 0;
			Actor projectile = A_FireProjectile("GluttonChasseProjectile", i * 22.5, false, 0, high ? 64 : 0, FPF_NOAUTOAIM, -invoker.owner.pitch);
			GluttonChasseProjectile gluttonChasseProjectile = GluttonChasseProjectile(projectile);
			if (gluttonChasseProjectile) {
				gluttonChasseProjectile.wickedCavort = invoker;
				gluttonChasseProjectile.SetFollowTarget(invoker.owner, i * 22.5, high ? 64 : 0);
			}
		}

		return null;
	}

    States {
		MainHand.User2:
			VSH1 W 4 {
                A_ClearBobbing();
                A_Light2();
            }
			VSH1 X 4;
			VSH1 Y 4 A_GluttonChasse();
            VSH1 Z 4;
            VSH3 ABC 4;
			VSH3 C 0 A_MirrorHands();
			VSH3 C 0 A_Light0();
			Goto Ready;

		OffHand.User1:
			VSH3 DEF 4;
			VSH3 MNO 4;
			Loop;
    }
}

class GluttonChasseProjectile : WrathCrossProjectile {
	WickedCavort wickedCavort;

	Actor followTarget;
	float orbitRange;
	float orbitAngle;
	float orbitOffsetZ;
	int orbitSpeed;

	property OrbitSpeed: orbitSpeed;

	Default {
		Scale 0.5;
		Speed 16;
		Alpha 1;
		DamageFunction Random(2, 4);

		GluttonChasseProjectile.OrbitSpeed 8;
		
		SeeSound "WickedCavort/Fire/GluttonChasse";
		DeathSound "WickedCavort/Explode/PrideDosado";
		Obituary "%o were torn in %k's Glutton Chasse frenzy!";

		+RIPPER;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		
		// Orbit Follow Target:
		if (self.followTarget) {
			self.angle = self.orbitAngle += self.orbitSpeed;
			int offsetX = self.orbitRange * Sin(angle);
			int offsetY = self.orbitRange * Cos(angle);
			self.SetOrigin(self.followTarget.pos + (offsetX, offsetY, self.followTarget.height * 0.65 + self.orbitOffsetZ), false);
			self.orbitRange += self.orbitSpeed;
		}
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);

		if (specialDamage > 0 && !targetActor.bBoss) {
			// Target Scramble:
			if (self.wickedCavort) {
				if (targetActor.bIsMonster && targetActor.target == self.wickedCavort.owner && self.wickedCavort.lastHit != targetActor && SchismInteraction.CanHarmTarget(self.wickedCavort.owner, self.wickedCavort.lastHit)) {
					targetActor.target = self.wickedCavort.lastHit;
				}
				self.wickedCavort.lastHit = targetActor;
			}

			// Velocity:
			float boost = 0;
			if (self.wickedCavort) {
				boost = self.wickedCavort.GetPassiveLevel();
			}
			targetActor.vel.x += (self.orbitRange * Sin(angle) / 2) * boost;
			targetActor.vel.y += (self.orbitRange * Cos(angle) / 2) * boost;
		}

		return specialDamage;
	}
	
	virtual void SetFollowTarget(Actor newTarget, float orbitAngle, float orbitOffsetZ) {
		self.followTarget = newTarget;
		self.orbitRange = 0;
		self.orbitAngle = orbitAngle;
		self.orbitOffsetZ = orbitOffsetZ;
		if (newTarget) {
			self.orbitAngle += newTarget.angle;
		}
	}

	States {
		Spawn:
			VWC4 ABCDEFGDEFGDEFGHIJ 4 bright;
		Death:
			Stop;
	}
}