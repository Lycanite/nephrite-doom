class EctoTalisman : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Raijin's Battleaxe can now cast Ecto Strike to release an orbiting attack to displace and drain enemies!";
		
		Inventory.Icon "RAJWB0";
		Inventory.PickupSound "RaijinsBattleaxe/Pickup";
		Inventory.RestrictedTo "UndulateOak", "UndulateOakCockatrice";
		WeaponUpgrade.ExtraAmmoClass "StormSphere";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			RAJW B 1 bright;
			Loop;
	}
}

class RaijinsBattleaxeEctoProjectile : SchismStickyProjectile {
	RaijinsBattleaxe raijinsBattleaxe;
	
	Default {
		Radius 10;
		Height 7;
		Speed 15;
		Scale 0.25;
		DamageFunction 1;
		Gravity 0.25;
		Alpha 1;
		Renderstyle "Add";
		
		DamageType "Electric";
		
		SeeSound "";
		DeathSound "";
		
		-NOGRAVITY;
		+CANNOTPUSH;
		+NODAMAGETHRUST;
		-THRUGHOST;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		expireTime = 10 * 35;
	}
	
	action void LaserUpdate() {
		// Fire Lasers:
		if (invoker.raijinsBattleaxe)
		{
			for (int i = 0; i < invoker.raijinsBattleaxe.ectoProjectiles.Size(); i++)
			{
				RaijinsBattleaxeEctoProjectile targetProjectile = invoker.raijinsBattleaxe.ectoProjectiles[i];
				if (targetProjectile == invoker || !targetProjectile || invoker.Distance3D(targetProjectile) > 1000)
				{
					continue;
				}
				A_Face(targetProjectile, 0, 0);
				RaijinsBattleaxeEctoLaserProjectile projectileActor = RaijinsBattleaxeEctoLaserProjectile(A_SpawnProjectile("RaijinsBattleaxeEctoLaserProjectile", 0, 0, 0, CMF_AIMDIRECTION|CMF_TRACKOWNER , invoker.pitch));
				if (projectileActor)
				{
					projectileActor.target = target;
					projectileActor.stopTarget = targetProjectile;
				}
			}
		}
		
		// Area Damage:
		SchismAttacks.Explode(invoker, invoker.target, 1, 20, 0, 1, false, false, "Electric");
		A_SpawnItemEx("DruidBowDimensionArrowTrail", 0, 0, 0, 0, random(-4, 4), random(-4, 4), 0, SXF_CLIENTSIDE);
	}
	
	States {
		Spawn:
			RAJP LMNOPQRS 6 bright A_StartSound("RaijinsBattleaxe/Ecto", CHAN_BODY, CHANF_LOOPING);
			Loop;
			
		Death:
			RAJP T 1 bright {
				LaserUpdate();
				return AttachUpdate();
			}
			Loop;
			
		Detonate:
			RAJL C 2 bright Detonate();
			MLVB DEFGHIJKL 2 bright;
			Stop;
	}
}

class RaijinsBattleaxeEctoLaserProjectile : SchismProjectile {
	Actor stopTarget;
	
	Default {
		Radius 8;
		Height 5;
		Scale 0.5;
		Speed 30;
		Gravity 0;
		DamageFunction 1;
		Renderstyle "Add";
		
		DamageType "Electric";
		
		+RIPPER;
		+PAINLESS;
		+BLOODLESSIMPACT;
		-BLOODSPLATTER;
	}
	
	action state EctoLaserUpdate() {
		if (invoker.stopTarget && invoker.Distance3D(invoker.stopTarget) <= 20)
		{
			return ResolveState("InstantDeath");
		}
		return null;
	}
	
	States {
		Spawn:
			RAJP UUUUVVVVWWWWXXXX 1 bright {
				return EctoLaserUpdate();
			}
			Loop;
		
		Death:
			RAJL HIJKL 2 bright;
		InstantDeath:
			RAJL L 0;
			Stop;
	}
}
