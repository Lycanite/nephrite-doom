class IchorspitProjectile : SchismProjectile {
	Default {
		Radius 6;
		Height 6;
		Scale 1;
		Speed 12;
		FastSpeed 10;
		DamageFunction 4;
		Alpha 0.8;
		RenderStyle "Translucent";
		
		DamageType "Fae";
		
		SeeSound "Projectile/Ichorspit/Fire";
		DeathSound "Projectile/Ichorspit/Explode";
		Decal "BloodSplat";

		SchismProjectile.ParticleName "BloodDrip";
		SchismProjectile.ParticleScale 0.3;
		SchismProjectile.LifetimeMax 3 * TICRATE;
	}
	
	States {
		Spawn:
			NPIS AB 2;
			Loop;
			
		Death:
			NPIS CD 6;
			Stop;
	}
}