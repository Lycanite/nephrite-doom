class EbonCommand : ReilodosSpell {
	bool alternatingHands;
	int spellEffectLayerA;
	int spellEffectLayerB;
	int spellEffectFrame;
	
	ReilodosDoppelganger ganger;
	
	Default {
		Radius 20;
		Height 16;
		Scale 0.5;
		
		Inventory.PickupSound "EbonCommand/Pickup";
		Weapon.UpSound "EbonCommand/Up";
		Weapon.ReadySound "";
		SchismWeapon.FailSound "EbonCommand/Fire/Fail";
		
		Weapon.AmmoType1 "ShadowEnergy";
		
		SchismWeapon.PrimaryCost 1;
		SchismWeapon.SecondaryCost 10;
		SchismWeapon.SpecialACost 50;
		SchismWeapon.SpecialBCost 10;
		SchismWeapon.SpecialCCost 20;
		SchismWeapon.SpecialDCost 2;
		
		SchismWeapon.SpecialItemA "GrueScroll";
		SchismWeapon.SpecialItemB "HowlScroll";
		SchismWeapon.SpecialItemC "DarkDoppelgangerScroll";
		SchismWeapon.SpecialItemD "DarkDoppelgangerScroll";
		SchismWeapon.PassiveItem "ShadowcloakScroll";
		
		Inventory.PickupMessage "The dark shadow magics of the Ebon Command have been bestowed upon you!";
		Obituary "%o was consumed by the shadows of %k's Ebon Command!";
		SchismWeapon.InsufficientAmmoAMessage "\caNot enough Shadow Energy to cast this spell.";
		SchismWeapon.InsufficientAmmoBMessage "\caNot enough Havoc Energy to cast this spell.";
		SchismWeapon.NoSpecialAMessage "\caYou need a Grue Scroll to cast this spell.";
		SchismWeapon.NoSpecialBMessage "\caYou need a Lycan Howl Scroll to cast this spell.";
		SchismWeapon.NoSpecialCMessage "\caYou need a Dark Doppelganger Scroll to cast this spell.";
		SchismWeapon.NoSpecialDMessage "\caYou need a Shadow Games Scroll to cast this spell.";
		SchismWeapon.IdleBob 10;
		
		+WEAPON.NOALERT;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.spellEffectLayerA = -100;
		self.spellEffectLayerB = 100;
	}
	
	// Ready Effects:
	action void A_ReadyEffectStart() {
		invoker.spellEffectFrame = 0;
		A_Overlay(invoker.spellEffectLayerA, "SpellEffectA");
		A_Overlay(invoker.spellEffectLayerB, "SpellEffectB");
	}
	
	action void A_ReadyEffectLoop() {
		float spellEffectNormal = float(invoker.spellEffectFrame) / 23; // 24 frames
		if (spellEffectNormal > 0.5) {
			spellEffectNormal = 0.5 -(spellEffectNormal - 0.5);
		}
		spellEffectNormal *= 2;
		A_OverlayOffset(invoker.spellEffectLayerA, -50 +(50 * spellEffectNormal), 0); // Animate from -50 to 0
		A_OverlayOffset(invoker.spellEffectLayerB, -100 +(190 * spellEffectNormal), 0); // Animate from X-100 to X90
		invoker.spellEffectFrame++;
	}
	
	// Primary Shadow Shock:
	action state A_ShadowShockStart() {
		A_OverlayOffset(invoker.spellEffectLayerA, 0, 0);
		A_OverlayOffset(invoker.spellEffectLayerB, 0, 0);
		
		if (!invoker.alternatingHands) {
			A_SetOffHandState("OffHand.Fire.Right");
			return ResolveState("MainHand.Fire.Right");
		}
		else {
			A_SetOffHandState("OffHand.Fire.Left");
			return ResolveState("MainHand.Fire.Left");
		}
	}
	
	action void A_ShadowShock() {
		invoker.alternatingHands = !invoker.alternatingHands;
		A_Overlay(invoker.spellEffectLayerB, "SpellEffectStop");
		
		if (!A_SpendAmmo(1)) {
			return;
		}
		
		A_StartSound("EbonCommand/Fire", CHAN_WEAPON);
		for (int i = 0; i <= 10; i++) {
			A_FireProjectile("EbonCommandProjectile", random(-30, 30), false, random(-5, 5), random(-20, 20));
		}
	}
	
	// Secondary Lob Darklings:
	action void A_LobDarklings() {
		A_Overlay(invoker.spellEffectLayerB, "SpellEffectStop");
		
		if (!A_SpendAmmo(2)) {
			return;
		}
		
		A_StartSound("EbonCommand/LobDarklings", CHAN_WEAPON);
		A_FireProjectile("LobDarklingsProjectile", 0, false, 0, 0);
	}
	
	// Special 1 Grue:
	action state A_ShadowShockGrue() {
		invoker.alternatingHands = !invoker.alternatingHands;
		A_Overlay(invoker.spellEffectLayerB, "SpellEffectStop");
		
		if (!A_CheckSpecial(1) || !A_SpendAmmo(3)) {
			return ResolveState("Ready");
		}
		
		A_StartSound("EbonCommand/Fire/Burst", CHAN_WEAPON);
		for (int i = 0; i <= 10; i++) {
			A_FireProjectile("EbonCommandProjectile", random(-30, 30), false, random(-5, 5), random(-40, 40));
		}
		A_FireProjectile("EbonCommandProjectileGrue", 0, false, 0, 0);
		A_FireProjectile("EbonCommandProjectileGrue", -30, false, 0, 0);
		A_FireProjectile("EbonCommandProjectileGrue", 30, false, 0, 0);
		
		return null;
	}
	
	// Special 2 Lycan Howl:
	action state A_LycanHowl() {
		invoker.alternatingHands = !invoker.alternatingHands;
		A_Overlay(invoker.spellEffectLayerB, "SpellEffectStop");
		
		if (!A_CheckSpecial(2) || !A_SpendAmmo(4)) {
			return ResolveState("Ready");
		}
		
		A_StartSound("EbonCommand/Fire/Howl", CHAN_WEAPON);
		for (int i = 10; i < 360; i += 10) {
			A_FireProjectile("EbonCommandProjectileHowl", i, false);
		}
		
		return null;
	}
	
	// Special 3 Dark Doppelganger:
	action void A_DarkDoppelganger() {
		if (!A_CheckSpecial(3)) {
			return;
		}
		
		// Attempt to Spawn:
		bool spawned = false;
		Actor minionActor = null;
		[spawned, minionActor] = invoker.owner.A_SpawnItemEx("ReilodosDoppelganger", invoker.owner.radius + 24, 0, 0, 0, 0, 0, 0, SXF_NOCHECKPOSITION|SXF_NOPOINTERS);
		ReilodosDoppelganger newGanger = ReilodosDoppelganger(minionActor);
		if (newGanger) {
			newGanger.master = invoker.owner;
			newGanger.scale = invoker.owner.scale;
		}
		
		// Spawn Fail:
		if (!newGanger) {
			A_StartSound("EbonCommand/Fire/Fail", CHAN_WEAPON);
			A_Print("\caThe Doppelganger's spawn location is blocked.", 2);
			return;
		}
		
		// Spawn Success:
		A_StartSound("EbonCommand/Fire/Ganger", CHAN_WEAPON);
		A_SpendAmmo(5);
		if (invoker.ganger) {
			invoker.ganger.A_Die();
			invoker.ganger = null;
			invoker.tracer = null;
		}
		invoker.ganger = newGanger;
	}
	
	// Special 4 Shadow Games:
	action void A_ShadowGames() {
		if (!A_CheckSpecial(4)) {
			return;
		}
		if (!invoker.ganger) {
			A_StartSound("EbonCommand/Fire/Fail", CHAN_WEAPON);
			A_Print("\caYou need a Doppelganger to cast Shadow Games.", 2);
			return;
		}
		
		A_StartSound("EbonCommand/Fire/Games", CHAN_WEAPON);
		A_SpendAmmo(6);
		Vector3 ownerPos = invoker.owner.pos;
		Vector3 gangerPos = invoker.ganger.pos;
		invoker.owner.SetOrigin(gangerPos, true);
		invoker.ganger.SetOrigin(ownerPos, true);
	}
	
	// Ultimate Nox Promenade:
	action void A_NoxPromenade() {
		A_StartSound("EbonCommand/Fire/Nox", CHAN_WEAPON);
		NoxPromenadeProjectile noxPromenadeProjectile = NoxPromenadeProjectile(A_FireProjectile("NoxPromenadeProjectile", 0, false, 0, 0, FPF_NOAUTOAIM, -90));
		if (noxPromenadeProjectile) {
			noxPromenadeProjectile.ebonCommand = invoker;
		}
	}

	// Passive Shadow Cloak:
	action void A_ShadowCloak() {
		int passiveLevel = invoker.GetPassiveLevel();
		if (passiveLevel <= 0 || --invoker.passiveCooldown > 0) {
			return;
		}

		// Check movement and light level:
		if (invoker.owner.curSector.lightlevel > 64 && (Abs(invoker.owner.vel.x) > 1 || Abs(invoker.owner.vel.y) > 1 || Abs(invoker.owner.vel.z) > 1)) {
			return;
		}

		invoker.passiveCooldown = 2 * TICRATE;
		SchismStatusEffects.InflictEffect("SchismCloakEffect", invoker.owner, invoker.owner, invoker.owner, 3 * TICRATE, "", passiveLevel);
	}
	
	
	States {
		Spawn:
			REIB B 4 bright;
			Loop;
			
		MainHand.Select:
			REH1 W 1 A_Raise(12);
			Loop;
			
		MainHand.Deselect:
			REH1 W 1 A_Lower(12);
			Loop;
			
		MainHand.Ready:
			REH1 W 0 {
				A_ShadowCloak();
				A_ReadyEffectStart();
				return A_SchismWeaponReady();
			}
			REH1 W 1 {
				A_ShadowCloak();
				A_ReadyEffectLoop();
				return A_SchismWeaponReady();
			}
			REH1 W 4 { // Off Hand Catch
				A_ShadowCloak();
				A_ReadyEffectLoop();
				return A_SchismWeaponReady();
			}
			REH1 XXXX 1 {
				A_ShadowCloak();
				A_ReadyEffectLoop();
				return A_SchismWeaponReady();
			}
			REH1 YYYYYYY 1 {
				A_ShadowCloak();
				A_ReadyEffectLoop();
				return A_SchismWeaponReady();
			}
			REH1 Y 2 { // Main Hand Catch
				A_ShadowCloak();
				A_ReadyEffectLoop();
				return A_SchismWeaponReady();
			}
			REH1 XX 1 {
				A_ShadowCloak();
				A_ReadyEffectLoop();
				return A_SchismWeaponReady();
			}
			REH1 WWWWWWWW 1 {
				A_ShadowCloak();
				A_ReadyEffectLoop();
				return A_SchismWeaponReady();
			}
			Loop;
			
		MainHand.Fire:
			REH1 X 0 {
				A_ClearBobbing();
				return A_ShadowShockStart();
			}
			Goto Ready;
		MainHand.Fire.Right:
			REH1 Z 4;
			REH1 [ 4 A_ShadowShock();
			REH1 ZX 4;
			REH1 Z 0 {
				A_Overlay(invoker.spellEffectLayerB, "SpellEffectB");
				A_ReFire("Fire");
			}
			Goto Ready;
		MainHand.Fire.Left:
			REH1 X 4;
			REH1 Y 4 A_ShadowShock();
			REH1 XX 4;
			REH1 X 0 {
				A_Overlay(invoker.spellEffectLayerB, "SpellEffectB");
				A_ReFire("Fire");
			}
			Goto Ready;
			
		MainHand.Altfire:
			REH1 O 0 A_ClearBobbing();
			REH1 O 4;
			REH1 H 4 A_LobDarklings();
			REH1 IGO 4;
			Goto Ready;
			
		MainHand.User1:
			REH1 X 0 {
				A_ClearBobbing();
				A_OverlayOffset(invoker.spellEffectLayerA, 0, 0);
				A_OverlayOffset(invoker.spellEffectLayerB, 0, 0);
			}
			REH1 XZ 6;
			REH1 [ 6 {
				return A_ShadowShockGrue();
			}
			REH1 NRS 6;
			REH1 TUVTUV 2;
			REH1 N 6;
			Goto Ready;
			
		MainHand.User2:
			REH1 X 0 {
				A_ClearBobbing();
				A_OverlayOffset(invoker.spellEffectLayerA, 0, 0);
				A_OverlayOffset(invoker.spellEffectLayerB, 0, 0);
			}
			REH1 XZ 6;
			REH1 [ 6 {
				return A_LycanHowl();
			}
			REH1 NRS 6;
			REH1 TUVTUV 2;
			REH1 N 6;
			Goto Ready;
			
		MainHand.User3:
			REH1 X 0 {
				A_ClearBobbing();
				A_OverlayOffset(invoker.spellEffectLayerA, 0, 0);
				A_OverlayOffset(invoker.spellEffectLayerB, 0, 0);
			}
			REH1 XK 6;
			REH1 S 6 A_DarkDoppelganger();
			REH1 TW 6;
			Goto Ready;
			
		MainHand.User4:
			REH1 X 0 {
				A_ClearBobbing();
				A_OverlayOffset(invoker.spellEffectLayerA, 0, 0);
				A_OverlayOffset(invoker.spellEffectLayerB, 0, 0);
			}
			REH1 XL 4;
			REH1 M 4 A_ShadowGames();
			REH1 N 4;
			Goto Ready;
		
		MainHand.Ultimate:
			REH1 X 0 A_ClearBobbing();
			REH1 X 4 A_NoxPromenade();
			Goto Ready;
			
			
		OffHand.Select:
			REHL G 1;
			Loop;
		OffHand.Deselect:
			REHL G 1;
			Loop;
		OffHand.Ready:
			REHL F 1;
			REHL E 8;
			REHL F 6;
			REHL G 9;
			REHL G 3; // Off Hand Catch
			REHL F 1; // Off Hand Catch
			Loop;
		
		OffHand.Fire:
		OffHand.Fire.Right:
			REHL FGFF 4;
			Loop;
		OffHand.Fire.Left:
			REHL HIHF 4;
			Loop;
		
		OffHand.AltFire:
			REHL EFGF 4;
			Loop;
		
		OffHand.User1:
			REHL FHI 6;
			REHL I 0 A_OverlayFlags(10, PSPF_FLIP|PSPF_MIRROR, true);
			REH1 NRS 6;
			REH1 TUVTUV 2;
			REH1 N 6;
			REHL N 0 A_OverlayFlags(10, PSPF_FLIP|PSPF_MIRROR, false);
			Loop;
			
		OffHand.User2:
			REH1 W 0 A_OverlayFlags(10, PSPF_FLIP|PSPF_MIRROR, true);
			REH1 WX 4;
		OffHand.User2Hold:
			REH1 Y 4;
			Loop;
		OffHand.User2Finish:
			REH1 X 0 A_OverlayFlags(10, PSPF_FLIP|PSPF_MIRROR, true);
			REH1 XW 4;
			Goto Offhand.Ready;
		
		OffHand.User3:
			REH1 X 0 A_OverlayFlags(10, PSPF_FLIP|PSPF_MIRROR, true);
			REH1 XKSTW 6;
			REH1 W 0 A_OverlayFlags(10, PSPF_FLIP|PSPF_MIRROR, false);
			Loop;
		
		OffHand.User4:
			REH1 X 0 A_OverlayFlags(10, PSPF_FLIP|PSPF_MIRROR, true);
			REH1 XLMN 4;
			REH1 W 0 A_OverlayFlags(10, PSPF_FLIP|PSPF_MIRROR, false);
			Loop;
		
		OffHand.Ultimate:
			REH1 X 0 A_OverlayFlags(10, PSPF_FLIP|PSPF_MIRROR, true);
			REH1 X 4;
			Loop;
			
			
		SpellEffectA:
			REEC ABCDEFG 2 {
				A_OverlayFlags(invoker.spellEffectLayerA, PSPF_ALPHA|PSPF_RENDERSTYLE, true);
				A_OverlayAlpha(invoker.spellEffectLayerA, 1);
				A_OverlayRenderstyle(invoker.spellEffectLayerA, STYLE_SUBTRACT);
			}
			Loop;
		SpellEffectB:
			REEC HIJKL 2 {
				A_OverlayFlags(invoker.spellEffectLayerB, PSPF_ALPHA|PSPF_RENDERSTYLE, true);
				A_OverlayAlpha(invoker.spellEffectLayerB, 1);
				A_OverlayRenderstyle(invoker.spellEffectLayerB, STYLE_NORMAL);
			}
			Loop;
		SpellEffectStop:
			REEC A 0;
			Stop;
	}
}


#include "zscript/schism/weapons/reilodos/eboncommand/shadowshock.zs"
#include "zscript/schism/weapons/reilodos/eboncommand/lobdarklings.zs"
#include "zscript/schism/weapons/reilodos/eboncommand/summongrue.zs"
#include "zscript/schism/weapons/reilodos/eboncommand/lycanhowl.zs"
#include "zscript/schism/weapons/reilodos/eboncommand/darkdoppelganger.zs"
#include "zscript/schism/weapons/reilodos/eboncommand/shadowcloak.zs"
#include "zscript/schism/weapons/reilodos/eboncommand/noxpromenade.zs"
