class PhantomTalisman : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Raijin's Battleaxe can now cast hantom Strike to drain the very existence of your foes!";
		
		Inventory.Icon "RAJWA0";
		Inventory.PickupSound "RaijinsBattleaxe/Pickup";
		Inventory.RestrictedTo "UndulateOak", "UndulateOakCockatrice";
		WeaponUpgrade.ExtraAmmoClass "StormSphere";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			RAJW A 1 bright;
			Loop;
	}
}

class RaijinsBattleaxePhantomProjectile : RaijinsBattleaxeProjectile {
	Default {
		Height 5;
		Radius 5;
		Scale 0.5;
		
		SeeSound "RaijinsBattleaxe/Phantom";
		DeathSound "RaijinsBattleaxe/Phantom/Explode";
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		
		// Drain Health and Armor:
		if (specialDamage > 0 && !targetActor.bFriendly && target)
		{
			target.A_DamageSelf(-specialDamage, "None", DMSS_FOILINVUL|DMSS_NOFACTOR|DMSS_NOPROTECT);
			target.A_GiveInventory("ArmorBonus", floor(-float(specialDamage) / 2));
		}
		
		return specialDamage;
	}
	
	States {
		Spawn:
			RAJT LMNOPQ 4 bright;
			
		Death:
			RAJT R 4 bright {
				SchismAttacks.Explode(invoker, invoker.target, 8, 100);
			}
			RAJT STUVWXYZ[^] 2 bright;
			Stop;
	}
}
