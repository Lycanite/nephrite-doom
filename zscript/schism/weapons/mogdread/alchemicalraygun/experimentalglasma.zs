class ExperimentalGlasmaModule : WeaponUpgrade {
	Default {
		Scale 0.75;
		Inventory.PickupMessage "Your Alchemical Raygun can now dispatch glasma charges that will seek out immolated targets!";
		
		Inventory.Icon "ALCRY0";
		Inventory.PickupSound "AlchemicalRaygun/Pickup";
		Inventory.RestrictedTo "Mogdread";
		WeaponUpgrade.ExtraAmmoClass "LightCharge";
		
		RenderStyle "Normal";
	}

	States {
		Spawn:
			ALCR Y 1 bright;
			Loop;
	}
}

class AlchemicalRaygunGlasmaProjectile : AlchemicalRaygunProjectile {
	Default {
		Scale 0.75;
		Gravity 0.5;
		Alpha 0.75;
		
		DamageType "Nova";

		SchismProjectile.ParticleName "Nova";
		SchismProjectile.ParticleScale 0.25;
		
		SeeSound "AlchemicalRaygun/Fire/Glasma";

		-FORCERADIUSDMG;
		+SEEKERMISSILE;
		+SCREENSEEKER;
	}

	action state A_GlasmaUpdate() {
		// Seek new Tracer:
		if (!invoker.tracer && invoker.lifetime % TICRATE == 0) {
			invoker.tracer = invoker.FindNewTarget();
		}

		// Home on Tracer:
		A_SeekerMissile(0, 20, SMF_LOOK|SMF_PRECISE);
		if (invoker.lifetime > 5 * TICRATE) {
			return ResolveState("Death");
		}

		return null;
	}
	
	States {
		Spawn:
			ALCG ABABCBC 1 bright {
				return A_GlasmaUpdate();
			}
			Loop;
			
		Death:
			ALCG DCDEDEFEFGFGHGHIHIJIJKJKLKLMLMNMNONO 1 bright;
			Stop;
	}
}