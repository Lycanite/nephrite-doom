class BloodGlobe : VessSpell {
	BloodGlobeSphere sphere;
	BloodGlobeTarget sphereTarget;
	Actor siphonTarget;

	Default {
		Radius 20;
		Height 16;
		Scale 0.5;
		
		Inventory.PickupSound "BloodGlobe/Pickup";
		Weapon.UpSound "BloodGlobe/Up";
		Weapon.ReadySound "";
		SchismWeapon.FailSound "BloodGlobe/Fire/Fail";
		
		Weapon.AmmoType1 "BloodDrupe";
		
		SchismWeapon.PrimaryCost 1;
		SchismWeapon.SecondaryCost 1;
		SchismWeapon.SpecialACost 10;
		SchismWeapon.SpecialBCost 10;
		SchismWeapon.SpecialCCost 25;
		SchismWeapon.SpecialDCost 25;
		
		SchismWeapon.SpecialItemA "BloodfallsScroll";
		SchismWeapon.SpecialItemB "HyperbloodScroll";
		SchismWeapon.SpecialItemC "BlooddrinkerScroll";
		SchismWeapon.SpecialItemD "FleshbenderScroll";
		SchismWeapon.PassiveItem "SanguineScroll";
		
		Inventory.PickupMessage "The power to weild blood as a weapon is yours, how delightful!";
		Obituary "%o was drained into %k's Blood Globe!";
		SchismWeapon.InsufficientAmmoAMessage "\caNot enough Blood Drupes to cast this spell.";
		SchismWeapon.InsufficientAmmoBMessage "\caNot enough Anima to cast this spell.";
		SchismWeapon.NoSpecialAMessage "\caYou need a Bloodfalls Scroll to cast this spell.";
		SchismWeapon.NoSpecialBMessage "\caYou need a Hyperblood Scroll to cast this spell.";
		SchismWeapon.NoSpecialCMessage "\caYou need a Blooddrinker Scroll to cast this spell.";
		SchismWeapon.NoSpecialDMessage "\caYou need a Fleshbender Scroll to cast this spell.";

		SchismWeapon.IdleBob 30;
		DualHand.MirrorOffHand true;
	}

	virtual int AnimaLevel(int cost) {
		return Floor(float(self.owner.CountInv(self.ammoType2)) / cost);
	}

	virtual vector3 SphereTargetPos() {
		int anima = self.owner.CountInv(self.ammoType2);
		float offset = (float(anima) / 200) * 20;
		return self.owner.Vec3Angle(
			self.owner.radius + 15 + offset,
			self.owner.angle + 160 + (Sin(self.lifetime) * 40),
			self.owner.height + offset
		);
	}

	action void A_SpawnSphere() {
		if (!invoker.sphere) {
			bool spawned;
			Actor actor;
			[spawned, actor] = invoker.owner.A_SpawnItemEx("BloodGlobeSphere", -invoker.owner.radius - 15, invoker.owner.height);
			invoker.sphere = BloodGlobeSphere(actor);
			invoker.sphere.bloodGlobe = invoker;
			invoker.sphere.master = invoker.owner;
		}
		if (!invoker.sphereTarget) {
			bool spawned;
			Actor actor;
			[spawned, actor] = invoker.owner.A_SpawnItemEx("BloodGlobeTarget", -invoker.owner.radius - 15, invoker.owner.height);
			invoker.sphereTarget = BloodGlobeTarget(actor);
			invoker.sphereTarget.bloodGlobe = invoker;
			invoker.sphere.master = invoker.owner;
		}
		
	}

	action void A_DespawnSphere() {
		if (invoker.sphere) {
			invoker.sphere.Despawn();
			invoker.sphere = null;
		}
		if (invoker.sphereTarget) {
			invoker.sphereTarget.Destroy();
			invoker.sphereTarget = null;
		}
	}
	
	States {
		Spawn:
			VESW A 4 bright;
			Loop;
			
		MainHand.Select:
			VSH1 A 1 {
				A_SpawnSphere();
				A_Raise(12);
			}
			Loop;
			
		MainHand.Deselect:
			VSH1 A 1 {
				A_DespawnSphere();
				A_Lower(12);
			}
			Loop;
			
		MainHand.Ready:
			VSH1 AABBCCDDEEFF 3 {
				return A_SchismWeaponReady();
			}
			Loop;
		
		OffHand.Select:
		OffHand.Deselect:
		OffHand.Ready:
			VSH2 ABCDEF 5;
			Loop;
		
		OffHand.Fire:
		OffHand.User3:
		OffHand.Ultimate:
		OffHand.Reload:
			VSH2 GHIH 3;
			Loop;

		OffHand.AltFire:
		OffHand.User2:
		OffHand.User4:
			VSH2 JKLK 5;
			Loop;
	}
}

class BloodGlobeTarget : Actor {
	BloodGlobe bloodGlobe;
	bool locked;

	Default {
		+NOGRAVITY;
		+NOCLIP;
	}

	override void Tick() {
		super.Tick();

		if (!self.bloodGlobe) {
			self.Destroy();
			return;
		}

		if (!self.locked) {
			self.SetOrigin(self.bloodGlobe.SphereTargetPos(), false);
		}
	}

	States {
		Spawn:
			TNT1 A 1 nodelay;
			Loop;
	}
}

class BloodGlobeSphere : Actor {
	mixin SpawnParticleMixin;

	BloodGlobe bloodGlobe; // The controlling blood globe weapon instance.
	StateLabel specialAction; // A special action state overriding behaviour.

	Default {
		Height 20;
		Radius 10;
		Scale 0.1;
		Speed 2;
		Gravity 0;
		Alpha 0.8;
		RenderStyle "Translucent";

		+NOGRAVITY;
		+NOCLIP;
	}

	virtual void Despawn() {
		self.SetState(self.ResolveState("Death"));
	}

	virtual StateLabel GetAnimaState() {
		int anima = self.bloodGlobe.owner.CountInv(self.bloodGlobe.ammoType2);
		if (anima >= 150) {
			return "Max";
		} else if (anima >= 100) {
			return "Strong";
		} else if (anima >= 50) {
			return "Medium";
		}
		return "Weak";
	}

	action void A_FloatToTarget(float speed) {
		if (!invoker.bloodGlobe || !invoker.bloodGlobe.sphereTarget) {
			return;
		}
		double angle = invoker.AngleTo(invoker.bloodGlobe.sphereTarget);
		double pitch = invoker.PitchTo(invoker.bloodGlobe.sphereTarget);
		invoker.Vel3DFromAngle(speed, angle, pitch);
	}

	action state A_BloodGlobeUpdate(bool updateVisuals = true) {
		if (!invoker.bloodGlobe) {
			return invoker.ResolveState("Death");
		}
		if (invoker.specialAction) {
			return null;
		}

		// Follow:
		if (invoker.bloodGlobe.sphereTarget) {
			float distance = invoker.Distance3D(invoker.bloodGlobe.sphereTarget);
			float speed = Min(invoker.speed, distance);
			if (distance > 100) {
				speed += (distance - 100) / 8;
			}
			A_FloatToTarget(speed);
		}

		int anima = invoker.bloodGlobe.owner.CountInv(invoker.bloodGlobe.ammoType2);
		float scale = 0.1 + (float(anima) / 100 * 0.4);

		// Scale:
		if (updateVisuals) {
			A_SetScale(scale);
		}

		// Particles:
		invoker.SpawnParticle("Fae", TICRATE, 0.5,
			(Random(-20 * scale, 20 * scale), Random(-20 * scale, 20 * scale), Random(-20 * scale, 20 * scale)),
			Random(0, 360), Random(45, 90), scale / 2
		);

		// State:
		if (!updateVisuals) {
			return null;
		}
		StateLabel stateLabel = invoker.GetAnimaState();
		if (!SchismUtil.ActorInState(invoker, stateLabel)) {
			return ResolveState(stateLabel);
		}

		return null;
	}

	action void A_EndSpecial() {
		invoker.specialAction = null;
		if (invoker.bloodGlobe && invoker.bloodGlobe.sphereTarget) {
			invoker.bloodGlobe.sphereTarget.locked = false;
		}
	}

	action void A_BloodGlobeFade() {
		A_SetScale(invoker.scale.x * 0.01);
		A_FadeOut(0.01);
	}

	States {
		Spawn:
			VBLO A 0 nodelay A_BloodGlobeUpdate();
		Weak:
			VBLO AAAABBBBCCCCBBBB 1 A_BloodGlobeUpdate();
			Loop;
		Medium:
			VBLO DDDDEEEEFFFFEEEE 1 A_BloodGlobeUpdate();
			Loop;
		Strong:
			VBLO GGGGHHHHIIIIHHHH 1 A_BloodGlobeUpdate();
			Loop;
		Max:
			VBLO JJJJKKKKLLLLKKKK 1 A_BloodGlobeUpdate();
			Loop;
		Death:
			VBLO AAAABBBBCCCCDDDD 1 A_BloodGlobeFade();
			Loop;
	}
}

#include "zscript/schism/weapons/vess/bloodglobe/animashards.zs"
#include "zscript/schism/weapons/vess/bloodglobe/siphon.zs"
#include "zscript/schism/weapons/vess/bloodglobe/bloodfalls.zs"
#include "zscript/schism/weapons/vess/bloodglobe/hyperblood.zs"
#include "zscript/schism/weapons/vess/bloodglobe/blooddrinker.zs"
#include "zscript/schism/weapons/vess/bloodglobe/fleshbender.zs"
#include "zscript/schism/weapons/vess/bloodglobe/sanguine.zs"
#include "zscript/schism/weapons/vess/bloodglobe/fleshscape.zs"