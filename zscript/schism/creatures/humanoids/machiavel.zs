class Machiavel : Actor {
	Default {
		Health 20000;
		Radius 20;
		Height 60;
		Mass 1000;
		Scale 1.5;
		Speed 16;
		PainChance 0;

		MONSTER;
		+FLOORCLIP;
		+FLOAT;
		+DROPOFF;
		+MISSILEMORE;
		+NORADIUSDMG;
		
		+BOSS;
		+BOSSDEATH;
		+DONTMORPH;
		+NOFEAR;
		
		SeeSound "Machiavel/See";
		PainSound "Machiavel/Pain";
		DeathSound "Machiavel/Die";
		ActiveSound "Machiavel/Active";
		Obituary "%o was obliterated by Machiavel!";
		HitObituary "%o was obliterated by Machiavel!";
	}
	
	override void BeginPlay() {
		Super.BeginPlay ();
	}
	
	override void Tick() {
		if (Health > 0) {
			SpawnParticles ();
		}
		Super.Tick ();
	}
	
	action int GetPhase () {
		if (Health <= 4000) {
			return 5;
		}
		else if (Health <= 8000) {
			return 4;
		}
		else if (Health <= 12000) {
			return 3;
		}
		else if (Health <= 16000) {
			return 2;
		}
		return 1;
	}
	
	action void SpawnParticles () {
		for (int i = 0; i < GetPhase (); i++) {
			for (int j = 0; j < 8; j++) {
				A_SpawnItemEx ("MachiavelMenagerieTrail", random(-20, 20), random(-20, 20), random(80, 120), 0, 0, frandom(0, 0.6), 0, SXF_NOCHECKPOSITION);
			}
		}
	}
	
	action void LightningAttack () {
		for (int i = 0; i < GetPhase (); i++) {
			for (int j = 0; j < 24; j++) {
				int distanceX = random (-360, 360);
				int distanceY = random (-360, 360);
				A_SpawnItemEx ("MachiavelLightningStrike", distanceX, distanceY);
			}
		}
	}
	
	action void SummonAttack () {
		if (GetPhase () == 5) {
			A_SpawnProjectile ("MachiavelMenagerieProjectile5", 32, 0, 0);
			return;
		}
		if (GetPhase () == 4) {
			A_SpawnProjectile ("MachiavelMenagerieProjectile4", 32, 0, 0);
			return;
		}
		if (GetPhase () == 3) {
			A_SpawnProjectile ("MachiavelMenagerieProjectile3", 32, 0, 0);
			return;
		}
		if (GetPhase () == 2) {
			A_SpawnProjectile ("MachiavelMenagerieProjectile2", 32, 0, 0);
			return;
		}
		if (GetPhase () == 1) {
			A_SpawnProjectile ("MachiavelMenagerieProjectile1", 32, 0, 0);
			return;
		}
		A_SpawnProjectile ("MachiavelMenagerieProjectile", 32, 0, 0);
	}
	
	States {
		Spawn:
			MAVL ABCD 4 A_Look ();
			Loop;
			
		See:
			MAVL ABCD 4 A_Chase ();
			Loop;
			
		Missile:
			MAVL A 0 A_JumpIf (random (0.0, 1.0) <= 0.75, "MissileMenagerie");
			
		MissileLightning:
			MAVL E 64 {
				LightningAttack ();
			}
			
		MissileMenagerie:
			MAVL E 8;
			MAVL FFFF 8 {
				SummonAttack ();
			}
			Goto See;
			
		Pain:
			MAVL G 8 A_Pain ();
			Goto See;
			
		Pain.SelfDamage:
			Goto See;
			
		Death:
		XDeath:
			MAVL G 5 {
				A_Scream ();
				A_KillChildren ();
			}
			MAVL HGHG 5;
			MAVL H 5 LightningAttack ();
			MAVL GHGHGHGHGHHGHGHGHGHGHH 5;
			MAVL IJIJIJIJIJIJ 5;
			MAVL K 5 A_NoBlocking ();
			MAVL LKLKL 5;
			MAVL MNMNMNONO 5;
			MAVL P -1;
			Stop;
	}
}


// ========== Menagerie Projectile ==========
class MachiavelMenagerieProjectile : Actor {
	float instability;
	float weaveX;
	float weaveY;
	
	Default {
		Radius 16;
		Height 10;
		Speed 15;
		Gravity 0.25;
		Damage 4;
		Alpha 0.75;
		Renderstyle "Add";
		Scale 1;
		
		SeeSound "Machiavel/Menagerie/Fire";
		DeathSound "Machiavel/Menagerie/Explode";
		
		PROJECTILE;
		+RANDOMIZE;
	}
	
	override void BeginPlay() {
		instability = 0.0;
		weaveX = random (-8, 8);
		weaveY = random (0, -8);
		Super.BeginPlay ();
	}
	
	virtual void FoulSummon (void) {
		A_SpawnItem ("CreatureSpawnerSlinger", 0);
	}
	
	action void IncreaseInstability (float amount) {
		invoker.instability += amount;
	}
	
	action float GetWeaveX (void) {
		return invoker.weaveX * invoker.instability;
	}
	
	action float GetWeaveY (void) {
		return invoker.weaveY * invoker.instability;
	}
	
	States {
		Spawn:
			MAVP AAAABBBBCCCCDDDD 1 bright {
				A_SpawnItemEx ("MachiavelMenagerieTrail", random(-2, -8), random(8, -8), random(8, -8), 0, 0, frandom(0, 0.6), 0, SXF_NOCHECKPOSITION);
				A_Weave (3, 3, GetWeaveX (), GetWeaveY ());
				IncreaseInstability (0.025);
			}
			Loop;
			
		Death:
			MAVP E 4 bright;
			MAVP F 4 bright FoulSummon ();
			MAVP GHIJ 4 bright;
			Stop;
	}
}

class MachiavelMenagerieProjectile1 : MachiavelMenagerieProjectile {
	override void FoulSummon (void) {
		int summonRoll = random (0.0, 1.0);
		if (summonRoll >= 0.75) {
			A_SpawnItem ("MummyLeader", 0);
		}
		else {
			A_SpawnItem ("HereticImp", 0);
		}
	}
}

class MachiavelMenagerieProjectile2 : MachiavelMenagerieProjectile {
	override void FoulSummon (void) {
		int summonRoll = random (0.0, 1.0);
		if (summonRoll >= 0.75) {
			A_SpawnItem ("LostSoul", 0);
		}
		else {
			A_SpawnItem ("DoomImp", 0);
		}
	}
}

class MachiavelMenagerieProjectile3 : MachiavelMenagerieProjectile {
	override void FoulSummon (void) {
		int summonRoll = random (0.0, 1.0);
		if (summonRoll >= 0.75) {
			A_SpawnItem ("CreatureSpawnerFodder", 0);
		}
		else {
			A_SpawnItem ("CreatureSpawnerBrute", 0);
		}
	}
}

class MachiavelMenagerieProjectile4 : MachiavelMenagerieProjectile {
	override void FoulSummon (void) {
		int summonRoll = random (0.0, 1.0);
		if (summonRoll >= 0.95) {
			A_SpawnItem ("CreatureSpawnerGhast", 0);
		}
		else if (summonRoll >= 0.85) {
			A_SpawnItem ("CreatureSpawnerBaron", 0);
		}
		else if (summonRoll >= 0.6) {
			A_SpawnItem ("CreatureSpawnerLobber", 0);
		}
		else {
			A_SpawnItem ("CreatureSpawnerSlinger", 0);
		}
	}
}

class MachiavelMenagerieProjectile5 : MachiavelMenagerieProjectile {
	override void FoulSummon (void) {
		int summonRoll = random (0.0, 1.0);
		if (summonRoll >= 0.85) {
			A_SpawnItem ("CreatureSpawnerBrood", 0);
		}
		else if (summonRoll >= 0.6) {
			A_SpawnItem ("CreatureSpawnerBrute", 0);
		}
		else {
			A_SpawnItem ("CreatureSpawnerAssassin", 0);
		}
	}
}

class MachiavelMenagerieTrail : Actor  {
	Default  {
		RenderStyle "Add";
		Scale 0.5;
		Alpha 0.5;
		
		+NOINTERACTION
		+FORCEXYBILLBOARD
	}
	
	States {
		Spawn:
			MAVP KLMN 4;
			Stop;
	}
}


// ========== Lightning Projectile ==========
class MachiavelLightningStrike : Actor {
	Default {
		Radius 16;
		Height 10;
		Scale 1;
		Speed 15;
		FastSpeed 20;
		Damage 3;
		Alpha 1;
		Renderstyle "Add";
		
		DamageType "Electric";
		
		SeeSound "Machiavel/Lightning/Strike";
		DeathSound "Machiavel/Lightning/Strike";
		
		PROJECTILE;
		+RANDOMIZE;
		+FRIENDLY;
		+DONTSPLASH;
		+CEILINGHUGGER;
	}
	
	States {
		Spawn:
		Death:
			MAVP OPQROPQR 4 bright;
			MAVP OPQROPQROPQROPQROPQROPQRST 4 bright {
				A_SpawnProjectile ("MachiavelLightningBolt", 0, 0, 0, CMF_ABSOLUTEPITCH, 90);
			}
			Stop;
	}
}

class MachiavelLightningBolt : Actor {
	Default {
		Radius 16;
		Height 10;
		Scale 1;
		Speed 15;
		FastSpeed 20;
		Damage 1;
		Alpha 1;
		Renderstyle "Add";
		
		DamageType "Electric";
		
		SeeSound "Machiavel/Lightning/Strike";
		DeathSound "Machiavel/Lightning/Strike";
		
		PROJECTILE;
		+RANDOMIZE;
		+FRIENDLY;
		+DONTSPLASH;
	}
	
	States {
		Spawn:
		Death:
			MAVP STUVWXYZ 4 bright A_Explode (1, 20, 0);
			Stop;
	}
}