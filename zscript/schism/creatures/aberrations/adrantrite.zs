class AdranTrite : Trite {
	Default {
		Tag "Adran Trite";
		BloodColor "99 00 dd";

		SeeSound "AdranTrite/See";
		ActiveSound "AdranTrite/Idle";
		PainSound "AdranTrite/Pain";
		DeathSound "AdranTrite/Death";
		MeleeSound "AdranTrite/Melee";
		HitObituary "%o was nipped by an Adran Trite.";
	}
	
	States {
		Spawn:
			VTRT ABCB 6 A_Look();
			Loop;
			
		See:
			VTRT DEFE 4 {
				A_SetLeaping(false);
				A_StartSound("AdranTrite/Step", CHAN_7);
				A_Chase();
			}
			Loop;
			
		Melee:
			VTRT G 3 A_FaceTarget();
			VTRT H 3 {
				A_FaceTarget();
				A_Jump(192, 2);
				A_StartSound("AdranTrite/Hit");
			}
			VTRT I 6 A_CustomMeleeAttack(3);
			Goto See;
			
		Missile:
			VTRT G 0 A_Jump(128, "See");
			VTRT G 3 A_FaceTarget();
			VTRT H 10 {
				A_SetLeaping(true);
				A_StartSound("AdranTrite/Melee");
				A_SkullAttack();
				ThrustThingZ(0, 6, 0, 1);
			}
			VTRT H 0 A_SetLeaping(false);
			VTRT I 8 A_Stop();
			Goto See;
			
		Pain:
			VTRT JK 3;
			VTRT L 3 A_Pain();
			Goto See;
			
		Death:
			VTRT JKLM 3;
			VTRT N 3 A_Scream();
			VTRT O 3 A_CreatureDie();
			VTRT PQRSTUVWX 3;
			Stop;
	}
}