/**
 * Chaotically charges the afflicted allowing for the owner to cast Foul Burst on the target.
 * Detrimental when the inflictor is able to harm the owner of this effect.
 */
class SchismEntropicEffect : SchismStatusEffect {
	bool addedBurstSource;
	
	Default {
		PowerUp.Duration -10;
		SchismStatusEffect.ParticleName "ChaosSpark";
	}
	
	override void DoEffect() {
		super.DoEffect();
		
		if (!self.addedBurstSource) {
			FoulMenagerie foulMenagerie = FoulMenagerie(self.effectSource);
			if (foulMenagerie) {
				foulMenagerie.AddFoulBurstSource(self.owner);
				self.addedBurstSource = true;
			}
		}
	}
	
	override void EndEffect() {
		super.EndEffect();
		if (!self.owner) {
			return;
		}
		
		FoulMenagerie foulMenagerie = FoulMenagerie(self.effectSource);
		if (foulMenagerie) {
			foulMenagerie.RemoveFoulBurstSource(self.owner);
		}
	}
	
	override void RefreshEffect(Actor effectInflictor, Actor effectSource, int effectTime, Class<SchismStatusEffect> spread, int strength) {
		super.RefreshEffect(effectInflictor, effectSource, effectTime, spread, strength);
		self.detrimental = SchismInteraction.CanHarmTarget(effectInflictor, self.owner, false);
	}
}