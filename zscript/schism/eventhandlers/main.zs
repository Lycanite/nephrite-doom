class SchismMainEventHandler : StaticEventHandler {
	mixin SpawnParticleMixin;

	override void NetworkProcess(ConsoleEvent e) {
		super.NetworkProcess(e);
		
		if (!playeringame[e.Player]) {
			return;
		}
		SchismPlayerPawn playerPawn = SchismPlayerPawn(players[e.Player].mo);
		if (!playerPawn) {
			return;
		}
		
		if (playerPawn.health > 0) {
			if (e.name == "schism_taunt") {
				playerPawn.Taunt();
			}
			else if (e.name == "schism_special1") {
				playerPawn.SchismSpecial(1);
			}
			else if (e.name == "schism_special2") {
				playerPawn.SchismSpecial(2);
			}
			else if (e.name == "schism_passive") {
				playerPawn.SchismPassive();
			}
			else if (e.name == "schism_beacon") {
				playerPawn.SchismBeacon();
			}
			else if (e.name == "schism_teleport") {
				playerPawn.SchismTeleport();
			}
			else if (e.name == "schism_changeclass") {
				playerPawn.ChangeClass(e.args[0]);
			}
		}
	}
	
	override void WorldThingDamaged(WorldEvent e) {
		super.WorldThingDamaged(e);
		
		SchismPlayerPawn playerPawn = SchismPlayerPawn(e.DamageSource);
		if (playerPawn) {
			playerPawn.OnDamageDealtFrom(e.Thing, e.Damage, e.DamageType, e.Inflictor);
		}
		
		if (e.Damage > 0) {
			// Poison Effects:
			if (e.DamageType == "Poison") {
				for (int i = 0; i < 5; i++) {
					int particleRange = e.thing.radius / 4;
					int particleVert = e.thing.height / 4;
					self.SpawnParticleFromActor(
						e.thing,
						"PoisonBubble", TICRATE, 0.5,
						(
							FRandom(-particleRange, particleRange),
							FRandom(-particleRange, particleRange),
							FRandom(-particleVert, particleVert)
						),
						Random(0, 359), Random(-45, 45),
						0.5
					);
				}
			}
			
			// Status Effects:
			for (Inventory inventoryItem = e.thing.Inv; inventoryItem != NULL; inventoryItem = inventoryItem.Inv) {
				SchismStatusEffect statusEffect = SchismStatusEffect(inventoryItem);
				if (statusEffect) {
					statusEffect.OnOwnerDamaged(e.Damage, e.DamageType, e.Inflictor);
				}
			}
		}
	}
	
	override void WorldThingDied(WorldEvent e) {
		super.WorldThingDamaged(e);
		
		SchismProjectile projectile = SchismProjectile(e.inflictor);
		if (projectile) {
			projectile.OnKill(e.thing);
		}
	}
	
	override void WorldLinePreActivated(WorldEvent e) {
		super.WorldLinePreActivated(e);
		
		SchismPlayerPawn playerPawn = SchismPlayerPawn(e.Thing);
		if (!playerPawn) {
			return;
		}
		
		playerPawn.OnLinePreActivated(e.ActivatedLine, e.ActivationType, e.ShouldActivate);
		// https://github.com/coelckers/gzdoom/blob/8ff81c93e8ecf521e7d7a5be940eb6e4c8c53ab3/wadsrc/static/zscript/constants.txt#L1227
	}
}