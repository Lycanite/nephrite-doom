class SchismPandemicEffect : SchismStatusEffect {
	
	Default {
		PowerUp.Duration -10;
		
		SchismStatusEffect.Detrimental true;
		SchismStatusEffect.ParticleName "Fae";
		SchismStatusEffect.ParticleCount 1;
		SchismStatusEffect.ParticleSpeed 2;
		SchismStatusEffect.ParticleDuration TICRATE;
		SchismStatusEffect.ParticleColor 0xFF0099;
	}

	override void RefreshEffect(Actor effectInflictor, Actor effectSource, int effectTime, Class<SchismStatusEffect> spread, int strength) {
		super.RefreshEffect(effectInflictor, effectSource, effectTime, spread, strength);
		if (!self.owner) {
			return;
		}

		self.owner.target = null;
		self.owner.SetStateLabel("See");
	}

	override void DoEffect() {
		super.DoEffect();
		if (!self.owner || self.owner.Health <= 0 || self.owner.bCorpse) {
			return;
		}

		// Check Current Target Every Second:
		bool findTarget = false;
		if (self.effectTics % TICRATE == 0 && !self.IsPandemicTarget(self.owner.target)) {
			findTarget = true;
		}
		if (self.effectTics % (5 * TICRATE) == 0) {
			findTarget = true;
			if (self.owner.target && self.owner.target.bCorpse) {
				self.owner.target = null;
				self.owner.SetStateLabel("See");
			}
		}

		// Find New Pandemic Target:
		if (!findTarget) {
			return;
		}
		BlockThingsIterator iter = BlockThingsIterator.Create(self.owner, 1000, false);
		while (iter.Next()) {
			Actor target = iter.thing;
			if (!target || target == self.owner || !target.bIsMonster || target.bCorpse) {
				continue;
			}

			if (!self.IsPandemicTarget(target) || !self.owner.CheckSight(target)) {
				continue;
			}

			self.owner.target = target;
			self.owner.SetStateLabel("See");
			break;
		}
	}

	/**
	 * Returns true if the provided target actor is a valid pandemic attack target.
	 * @param target The actor to check.
	 * @return True if the target actor should be targetted by the afflicted.
	 */
	protected bool IsPandemicTarget(Actor target) {
		if (!target) {
			return false;
		}
		if (target.FindInventory("SchismPoisonEffect")) {
			return true;
		}
		if (target.FindInventory("SchismPlagueDebuff")) {
			return true;
		}
		if (target.FindInventory("SchismPandemicEffect")) {
			return true;
		}
		return false;
	}
}