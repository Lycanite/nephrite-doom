class SchismTreeBamboo : SchismTree {
	Default {
		Tag "Bamboo Tree";
		SchismTree.SpringVariants 2;
		SchismTree.SummerVariants 2;
		SchismTree.AutumnVariants 2;
		SchismTree.WinterVariants 2;
	}
	
	States {
		Spring.1:
			TR20 A -1;
			Stop;

		Spring.2:
			TR20 B -1;
			Stop;

		Summer.1:
			TR20 E -1;
			Stop;

		Summer.2:
			TR20 F -1;
			Stop;

		Autumn.1:
			TR20 I -1;
			Stop;

		Autumn.2:
			TR20 J -1;
			Stop;

		Winter.1:
			TD20 M -1;
			Stop;

		Winter.2:
			TD20 N -1;
			Stop;

		Death.Spring.1:
			TD20 A -1;
			Stop;

		Death.Spring.2:
			TD20 B -1;
			Stop;

		Death.Summer.1:
			TD20 E -1;
			Stop;

		Death.Summer.2:
			TD20 F -1;
			Stop;

		Death.Autumn.1:
			TD20 I -1;
			Stop;

		Death.Autumn.2:
			TD20 J -1;
			Stop;

		Death.Winter.1:
			TD20 M -1;
			Stop;

		Death.Winter.2:
			TD20 N -1;
			Stop;
	}
}