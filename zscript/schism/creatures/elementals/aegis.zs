class Aegis : MinionCreature {
	bool mode; // False = shields mode, true = swords mode.

	Default {
		Health 150;
		Radius 10;
		Height 70;
		Speed 20;
		FloatSpeed 10;
		Scale 0.65;
		Gravity 0;
		PainChance 120;
		Damage 1;
		BloodColor "FF FF AA";

		DamageFactor "Order", 0.1;

		MinionCreature.SummonTime 30 * 35;

		+FLOAT;
		+FLOATBOB;
		+MinionCreature.TAUNT;
		
		SeeSound "Aegis/See";
		PainSound "Aegis/Pain";
		DeathSound "Aegis/Death";
		ActiveSound "Aegis/Idle";
		MeleeSound "Aegis/Melee";
		Obituary "%o was sliced by a Aegis!";
		HitObituary "%o was sliced by a Aegis!";
	}

	override int DamageMobj(Actor inflictor, Actor source, int damage, Name mod, int flags, double angle) {
		// Reduced damage in shields mode:
		if (!self.mode) {
			damage *= 0.25;
		}
		return super.DamageMobj(inflictor, source, damage, mod, flags, angle);
	}

	action state A_AegisSee() {
		if (invoker.mode) {
			return ResolveState("See.Swords");
		}
		return null;
	}

	action state A_AegisChaseSwords() {
		A_Chase("Melee.Swords");
		if (FRandom(0, 1) > 0.95) {
			invoker.mode = false;
			return ResolveState("See.Shields");
		}
		return null;
	}

	action state A_AegisChaseShields() {
		A_Chase("Melee.Shields");
		if (FRandom(0, 1) > 0.95) {
			invoker.mode = true;
			return ResolveState("See.Swords");
		}
		return null;
	}
	
	action state A_AegisAttack() {
		// Shields Mode:
		if (!invoker.mode) {
			if (FRandom(0, 1) > 0.25) {
				invoker.mode = true;
				return ResolveState("Melee.Swords");
			}
			return null;
		}

		// Swords Mode:
		invoker.A_CreatureMelee(4, 10);
		return null;
	}
	
	action state A_AegisPain() {
		if (invoker.mode) {
			return ResolveState("Pain.Swords");
		}
		return null;
	}
	
	States {
		Spawn:
			AEGI ABCB 4 {
				A_Look();
			}
			Loop;
			
		See:
			AEGI A 0 A_AegisSee();
		See.Shields:
			AEGI ABCB 4 A_AegisChaseShields();
			Loop;
		See.Swords:
			AEGI DEF 4 A_Chase("Melee.Swords");
			AEGI E 4 A_AegisChaseSwords();
			Loop;
			
		Melee.Shields:
			AEGI A 4 A_AegisAttack();
			AEGI BCB 4;
			Goto See;
			
		Melee.Swords:
			AEGI DE 4 A_FaceTarget();
			AEGI FE 4 A_AegisAttack();
			Goto See;

		Pain:
			AEGI G 0 A_AegisPain();
		Pain.Shields:
			AEGI G 4 A_Pain();
			Goto See;
		Pain.Swords:
			AEGI H 10 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			AEGI I 5;
			AEGI J 5 A_Scream();
			AEGI K 5;
			AEGI L 5 A_CreatureDie();
			AEGI MNO 5;
			AEGI P -1;
			Stop;
	}
}