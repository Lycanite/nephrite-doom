class Neemlyph : MinionCreature {
	Default {
		Tag "Neemlyph";
		
		Health 100;
		Radius 10;
		Height 64;
		Speed 18;
		FloatSpeed 18;
		Scale 0.5;
		Gravity 0;
		PainChance 120;
		Damage 1;
		Alpha 1;
		BloodColor "ff 44 44";
		
		SeeSound "Neemlyph/See";
		PainSound "Neemlyph/Pain";
		DeathSound "Neemlyph/Death";
		ActiveSound "Neemlyph/Idle";
		MeleeSound "Neemlyph/Idle";
		Obituary "%o was enlightened by a Neemlyph!";
		HitObituary "%o was enlightened by a Neemlyph!";
		
		+FLOAT;
	}
	
	States {
		Spawn:
			NEEM ABCB 4 {
				A_Look();
				return A_StayWithMaster();
			}
			Loop;
			
		Follow:
			NEEM DEFE 4 {
				A_Look();
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			NEEM DEFE 4 {
				return A_MinionChase();
			}
			Loop;
		
		Missile:
			NEEM GH 4 A_FaceTarget();
			NEEM I 4 bright A_SpawnProjectile("HieroboltProjectile", 26);
			NEEM H 4 bright;
			Goto See;
			
		Pain:
			NEEM J 6 A_Pain();
			Goto See;
			
		Pain.SelfDamage:
			Goto See;
			
		Death:
		XDeath:
			NEEM J 5;
			NEEM K 5 A_Scream();
			NEEM L 5;
			NEEM M 5 A_CreatureDie();
			NEEM NO 5;
			NEEM P -1;
			Stop;
	}
}