class SanguineScroll : WeaponPassive {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Blood Globe grows more powerful!";
		
		Inventory.Icon "VESWH0";
		Inventory.PickupSound "BloodGlobe/Pickup";
		Inventory.RestrictedTo "Vess";
		WeaponUpgrade.ExtraAmmoClass "BloodDrupe";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			VESW H 1 bright;
			Loop;
	}
}

extend class BloodGlobeSphere {
	Array<BloodGlobeCluster> childSpheres;

	override void Tick() {
		super.Tick();
		if (!self.bloodGlobe || BloodGlobeCluster(self)) {
			return;
		}

		while (self.childSpheres.Size() < self.bloodGlobe.GetPassiveLevel()) {
			bool spawned;
			Actor actor;
			[spawned, actor] = self.A_SpawnItemEx("BloodGlobeCluster", 0, self.height * 0.5);
			BloodGlobeCluster childSphere = BloodGlobeCluster(actor);
			if (childSphere) {
				childSphere.master = self.bloodGlobe.owner;
				childSphere.bloodGlobe = self.bloodGlobe;
				childSphere.parentSphere = self;
				childSphere.clusterIndex = self.childSpheres.Size();
				self.childSpheres.Push(childSphere);
			} else {
				break;
			}
		}
	}
}

class BloodGlobeCluster : BloodGlobeSphere {
	BloodGlobeSphere parentSphere;
	int clusterIndex;

	Default {
		Height 10;
		Radius 5;
		Scale 0.05;
		Speed 2;
	}

	action state A_OrbitParentGlobe() {
		if (!invoker.parentSphere || !invoker.bloodGlobe) {
			return ResolveState("Death");
		}

		float scale = invoker.parentSphere.scale.x / 2;
		A_SetScale(scale);

		// Orbit:
		float angle = (invoker.clusterIndex * 120) + invoker.bloodGlobe.lifetime;
		int offsetX = 200 * scale * Sin(angle);
		int offsetY = 200 * scale * Cos(angle);
		int offsetZ = (invoker.parentSphere.height - invoker.height) * scale;
		invoker.SetOrigin(invoker.parentSphere.pos + (offsetX, offsetY, offsetZ), false);

		// Particles:
		invoker.SpawnParticle("Fae", TICRATE, 0.25,
			(Random(-20 * scale, 20 * scale), Random(-20 * scale, 20 * scale), Random(-20 * scale, 20 * scale)),
			Random(0, 360), Random(45, 90), scale / 2
		);

		// State:
		StateLabel stateLabel = invoker.parentSphere.GetAnimaState();
		if (invoker.parentSphere.specialAction == "Bloodfalls") {
			stateLabel = "Bloodfalls";
		} else if (invoker.parentSphere.specialAction == "Hyperblood") {
			stateLabel = "Hyperblood";
		} else if (invoker.parentSphere.specialAction == "Blooddrinker") {
			stateLabel = "Blooddrinker";
		} else if (invoker.parentSphere.specialAction == "Fleshbender") {
			stateLabel = "Fleshbender";
		}
		if (!SchismUtil.ActorInState(invoker, stateLabel)) {
			return ResolveState(stateLabel);
		}

		return null;
	}

	action state A_OrbitBloodfalls() {
		if (!invoker.parentSphere || !invoker.bloodGlobe) {
			return invoker.ResolveState("Death");
		}

		A_OrbitParentGlobe();

		if (invoker.parentSphere.specialAction != "Bloodfalls") {
			return invoker.ResolveState("Spawn");
		}

		for (int i = 0; i < invoker.parentSphere.bloodfallsLevel; i++) {
			Actor projectile = A_SpawnProjectile("BloodGlobeBloodfallsProjectile", 0, 0, Random(0, 360), CMF_AIMDIRECTION|CMF_TRACKOWNER, Random(0, 90));
			if (projectile && invoker.bloodGlobe) {
				projectile.target = invoker.bloodGlobe.owner;
				projectile.A_SetScale(invoker.scale.x);
			}
		}

		return null;
	}

	action state A_OrbitBlooddrinker() {
		if (!invoker.parentSphere || !invoker.bloodGlobe) {
			return invoker.ResolveState("Death");
		}

		A_OrbitParentGlobe();

		if (invoker.parentSphere.specialAction != "Blooddrinker") {
			return invoker.ResolveState("Spawn");
		}

		for (int i = 0; i < invoker.parentSphere.blooddrinkerLevel; i++) {
			Actor projectile = A_SpawnProjectile("BloodGlobeBlooddrinkerProjectile", 0, 0, Random(0, 360), CMF_AIMDIRECTION|CMF_TRACKOWNER, Random(0, 90));
			if (projectile && invoker.bloodGlobe) {
				projectile.target = invoker.bloodGlobe.owner;
				projectile.A_SetScale(invoker.scale.x);
			}
		}

		return null;
	}

	action void A_FleshbenderChild() {
		if (!invoker.parentSphere) {
			return;
		}
		invoker.fleshbenderLevel = invoker.parentSphere.fleshbenderLevel;
		BloodGlobeFleshTendril tendril = A_Fleshbender();
		if (tendril) {
			tendril.A_SetScale(tendril.scale.x * 0.75);
		}
	}

	States {
		Spawn:
			VBLO A 0 nodelay A_OrbitParentGlobe();
		Weak:
		Medium:
			VBLO AAAABBBBCCCCBBBB 1 A_OrbitParentGlobe();
			Loop;
		Strong:
			VBLO DDDDEEEEFFFFEEEE 1 A_OrbitParentGlobe();
			Loop;
		Max:
			VBLO GGGGHHHHIIIIHHHH 1 A_OrbitParentGlobe();
			Loop;
		Bloodfalls:
			VBLO MMMMNNNNOOOO 1 A_OrbitBloodfalls();
			Loop;
		Hyperblood:
			VBLO PPPPQQQQRRRRQQQQPPPP 1 A_OrbitParentGlobe();
			Goto Spawn;
		Blooddrinker:
			VBLO MMMMNNNNOOOOMMMMNNNNOOOO 1 A_OrbitBlooddrinker();
			Goto Spawn;
		Fleshbender:
			VBLO P 0 A_FleshbenderChild();
			VBLO PPPPQQQQRRRRQQQQPPPP 1 A_OrbitParentGlobe();
			Goto Spawn;
		Death:
			VBLO AAAABBBBCCCCDDDD 1 A_BloodGlobeFade();
			Loop;
	}
}