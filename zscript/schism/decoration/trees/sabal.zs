class SchismTreeSabal : SchismTree {
	Default {
		Tag "Sabal Tree";
		SchismTree.SpringVariants 2;
		SchismTree.SummerVariants 2;
		SchismTree.AutumnVariants 2;
		SchismTree.WinterVariants 2;
	}
	
	States {
		Spring.1:
			TR13 A -1;
			Stop;

		Spring.2:
			TR13 B -1;
			Stop;

		Summer.1:
			TR13 E -1;
			Stop;

		Summer.2:
			TR13 F -1;
			Stop;

		Autumn.1:
			TR13 I -1;
			Stop;

		Autumn.2:
			TR13 J -1;
			Stop;

		Winter.1:
			TD13 M -1;
			Stop;

		Winter.2:
			TD13 N -1;
			Stop;

		Death.Spring.1:
			TD13 A -1;
			Stop;

		Death.Spring.2:
			TD13 B -1;
			Stop;

		Death.Summer.1:
			TD13 E -1;
			Stop;

		Death.Summer.2:
			TD13 F -1;
			Stop;

		Death.Autumn.1:
			TD13 I -1;
			Stop;

		Death.Autumn.2:
			TD13 J -1;
			Stop;

		Death.Winter.1:
			TD13 M -1;
			Stop;

		Death.Winter.2:
			TD13 N -1;
			Stop;
	}
}