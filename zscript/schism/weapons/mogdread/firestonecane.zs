class FirestoneCane : SchismWeapon {
	Default {
		Radius 20;
		Height 16;
		Scale 0.5;
		
		Inventory.RestrictedTo "Mogdread";
		
		Inventory.PickupSound "FirestoneCane/Pickup";
		Weapon.UpSound "FirestoneCane/Up";
		Weapon.ReadySound "";
		SchismWeapon.FailSound "FirestoneCane/Fire/Fail";
		
		Weapon.AmmoType1 "FirestoneCharge";
		Weapon.AmmoType2 "InfernalCharge";
		
		SchismWeapon.PrimaryCost 1;
		SchismWeapon.SecondaryCost 2;
		SchismWeapon.SpecialACost 10;
		SchismWeapon.SpecialBCost 50;
		SchismWeapon.SpecialCCost 25;
		SchismWeapon.SpecialDCost 50;
		
		SchismWeapon.SpecialItemA "InfernoSpiralGem";
		SchismWeapon.SpecialItemB "FlameCombustionGem";
		SchismWeapon.SpecialItemC "DelphicNovaeGem";
		SchismWeapon.SpecialItemD "GlasmaShowerGem";
		SchismWeapon.PassiveItem "IgnitionGem";
		
		Inventory.PickupMessage "Oh very nice, this fine cane has a jewel infused with the power of a star!";
		Obituary "%o was toasted by Gentleman %k's Firestone Cane!";
		SchismWeapon.InsufficientAmmoAMessage "\caNot enough Firestone Charges to use this ability.";
		SchismWeapon.InsufficientAmmoBMessage "\caNot enough Infernal Charges to use this ability.";
		SchismWeapon.NoSpecialAMessage "\caYou need an Inferno Spiral Gem to use this ability.";
		SchismWeapon.NoSpecialBMessage "\caYou need a Flame Combustion Gem to use this ability.";
		SchismWeapon.NoSpecialCMessage "\caYou need a Delphic Novae Gem to use this ability.";
		SchismWeapon.NoSpecialDMessage "\caYou need a Glasma Shower Gem to use this ability.";

		SchismWeapon.IdleBob 20;
		
		+FLOATBOB;
		+NOGRAVITY;
		+INVENTORY.RESTRICTABSOLUTELY;
		+WEAPON.AMMO_OPTIONAL;
		+WEAPON.ALT_AMMO_OPTIONAL;
	}
	
	action void A_OnAttack() {
		A_ClearBobbing();
		A_OverlayOffset(0, 0, 32);
		A_Light2();
	}
	
	// Primary Fire Spray:
	action void A_FireSpray(bool useAmmo = false) {
		// Strong (Uses Ammo):
		if (A_CheckAmmo(1, 0, false)) {
			if (useAmmo) {
				A_SpendAmmo(1);
			}
			if (Random(0.0, 1.0) >= 0.95) {
				A_StartSound("FirestoneCane/Fire");
			}
			for (int i = 0; i <= 2; i++) {
				A_FireProjectile("FirestoneCaneProjectile", Random(-20, 20), false, 0, Random(-2, 2), FPF_NOAUTOAIM, Random(-4, 4));
			}
		}
		// Weak (Free, No Immolate):
		else {
			A_FireProjectile("FirestoneCaneProjectileWeak", Random(-20, 20), false, 0, Random(-2, 2), FPF_NOAUTOAIM, Random(-4, 4));
		}
	}
	
	// Secondary Firestorm:
	action void A_Firestorm(bool useAmmo = false) {
		if (useAmmo) {
			A_SpendAmmo(2);
		}
		if (Random(0.0, 1.0) >= 0.95) {
			A_StartSound("FirestoneCane/Fire/Storm");
		}
		for (int i = 0; i <= 16; i++) {
			A_FireProjectile("FirestoneCaneFirestormProjectile", Random(-180, 180), false, Random(-20, 20), -24, FPF_NOAUTOAIM, Random(-6, 6) - invoker.owner.pitch);
		}
		
		ThinkerIterator immolationIterator = ThinkerIterator.Create("SchismImmolateDebuff");
		SchismImmolateDebuff immolation;
		while (immolation = SchismImmolateDebuff(immolationIterator.Next())) {
			if (immolation.owner != invoker.owner && immolation.InflictedBy(invoker.owner)) {
				immolation.ImmolateFirestorm(invoker.owner);
			}
		}
	}
	
	// Special 1 Inferno Spiral:
	action state A_InfernoSpiral(bool useAmmo = false) {
		if (!A_CheckSpecial(1, false)) {
			return ResolveState("Ready");
		}
		if (useAmmo && !A_SpendAmmo(3)) {
			return ResolveState("Ready");
		}
		if (Random(0.0, 1.0) >= 0.95) {
			A_StartSound("FirestoneCane/Fire/Spiral");
		}
		A_FireProjectile("FirestoneCaneSpiralProjectile", 0, false, 0, -20, FPF_NOAUTOAIM, 0);
		return null;
	}
	
	// Special 2 Flame Combustion:
	action state A_FlameCombustion() {
		if (!A_CheckSpecial(2) || !A_CheckAmmo(4)) {
			return ResolveState("Ready");
		}
		
		ThinkerIterator immolationIterator = ThinkerIterator.Create("SchismImmolateDebuff");
		SchismImmolateDebuff immolation;
		bool anyCombustions = false;
		while (immolation = SchismImmolateDebuff(immolationIterator.Next())) {
			if (immolation.owner != invoker.owner && immolation.InflictedBy(invoker.owner)) {
				anyCombustions = immolation.ImmolateCombustion(invoker.owner);
			}
		}
		
		if (!anyCombustions) {
			return null;
		}
		
		A_SpendAmmo(4);
		for (int i = 0; i <= 32; i++) {
			invoker.SpawnParticle(
				"FireEmber", 16, 0.5,
				(0, 0, invoker.owner.height / 2),
				Random(0, 359), Random(-45, 45),
				FRandom(1, 1.6)
			);
		}
		
		return null;
	}
	
	// Special 3 Delphic Novae:
	action state A_DelphicNovae() {
		if (!A_CheckSpecial(3) || !A_SpendAmmo(5)) {
			return ResolveState("Ready");
		}

		A_StartSound("FirestoneCane/Fire/Delphic");
		for (int i = 1; i <= 16; i++) {
			bool high = i % 2 != 0;
			Actor projectile = A_FireProjectile("FirestoneCaneDelphicProjectile", i * 22.5, false, 0, high ? 64 : 0, FPF_NOAUTOAIM, -invoker.owner.pitch);
			FirestoneCaneDelphicProjectile delphicProjectile = FirestoneCaneDelphicProjectile(projectile);
			if (delphicProjectile) {
				delphicProjectile.SetFollowTarget(invoker.owner, i * 22.5, high ? 64 : 0);
			}
		}
		
		return null;
	}
	
	// Special 4 Glasma Shower:
	action state A_GlasmaShower() {
		if (!A_CheckSpecial(4) || !A_SpendAmmo(6)) {
			return ResolveState("Ready");
		}

		A_StartSound("FirestoneCane/Fire/Glasma");
		for (int i = 0; i < 8; i++) {
			A_FireProjectile("FirestoneCaneGlasmaProjectile", i * 45, false, 0, Random(-4, 4), FPF_NOAUTOAIM, -45 - invoker.owner.pitch);
		}

		return null;
	}
	
	// Ultimate Call MogDalek:
	action void A_CallMogDalek() {
		A_SpawnItemEx("MogDalek", 0, 0, 4, 0, 0, 50, 0, SXF_SETMASTER|SXF_NOCHECKPOSITION);
	}
	
	// Passive Ignition:
	action void A_Ignition() {
		int passiveLevel = invoker.GetPassiveLevel();
		if (passiveLevel <= 0 || invoker.passiveCooldown > 0) {
			return;
		}
		invoker.passiveCooldown = 1 * TICRATE;
		
		Actor targetActor = invoker.owner;
		bool spread = passiveLevel >= 3;
		SchismStatusEffects.InflictEffect("FirestoneCaneIgnition", invoker.owner, invoker.owner, invoker.owner, 2 * TICRATE, spread ? "SchismImmolateDebuff" : "", passiveLevel);
	}
	
	States {
		Spawn:
			FSCN P -1;
			Stop;
			
		Ready:
			FSCN ABCB 2 {
				A_Ignition();
				return A_SchismWeaponReady();
			}
			Loop;
			
		Deselect:
			FSCN A 1 A_Lower(12);
			Loop;
			
		Select:
			FSCN A 1 A_Raise(12);
			Loop;
			
		Fire:
			FSCN D 4 A_OnAttack();
			FSCN E 4;
		Hold:
			FSCN F 2 A_FireSpray(true);
			FSCN FGH 2 A_FireSpray(false);
			FSCN H 0 A_ReFire();
			FSCN E 4 A_Light0();
			FSCN F 4;
			goto Ready;
			
		AltFire:
			FSCN I 0 {
				return A_CheckAmmoJump(2, "Ready");
			}
		Firestorm:
			FSCN I 0 A_GiveInventory("FirestoneCaneStopMovement", 1);
			FSCN I 2 A_OnAttack();
			FSCN JK 2;
		AltHold:
			FSCN L 0 A_GiveInventory("FirestoneCaneStopMovement", 1);
			FSCN L 2 A_Firestorm(true);
			FSCN MNO 2 A_Firestorm(false);
			FSCN O 0 A_ReFire();
			FSCN L 0 A_GiveInventory("FirestoneCaneStopMovement", 1);
			FSCN LMNO 2 A_Firestorm(false);
			FSCN K 2 A_Light0();
			goto Ready;
		
		User1:
			FSCN D 4 A_OnAttack();
			FSCN E 4;
		User1Hold:
			FSCN L 2  {
				return A_InfernoSpiral(true);
			}
			FSCN MNOLMNO 2  {
				return A_InfernoSpiral(false);
			}
			FSCN E 4 A_Light0();
			FSCN F 4;
			goto Ready;
		
		User2:
			FSCN I 4 A_OnAttack();
			FSCN JK 4;
			FSCN L 4  {
				return A_FlameCombustion();
			}
			FSCN MNO 4;
			FSCN K 4 A_Light0();
			goto Ready;
		
		User3:
			FSCN I 2 A_OnAttack();
			FSCN JK 2;
			FSCN L 2  {
				return A_DelphicNovae();
			}
			FSCN MNO 2;
			FSCN K 2 A_Light0();
			goto Ready;
		
		User4:
			FSCN I 4 A_OnAttack();
			FSCN JK 4;
			FSCN L 4  {
				return A_GlasmaShower();
			}
			FSCN MNO 2;
			FSCN K 4 A_Light0();
			goto Ready;
		
		Ultimate:
			FSCN I 4 A_OnAttack();
			FSCN JK 4;
			FSCN L 4 A_CallMogDalek();
			FSCN MNO 2;
			FSCN K 4 A_Light0();
			goto Ready;
	}
}

class FirestoneCaneStopMovement : PowerSpeed {
	Default {
		Powerup.Duration 9;
		Powerup.Color  "FF BB 33", 0.2;
		Speed 0;
		Inventory.Icon "";
		
		+POWERSPEED.NOTRAIL;
		+INVENTORY.NOTELEPORTFREEZE;
	}
}

class FirestoneCaneProjectile : SchismProjectile {
	bool inflictImmolate;
	
	Default {
		Radius 4;
		Height 8;
		Scale 1;
		Speed 15;
		FastSpeed 20;
		DamageFunction 1;
		Alpha 0.5;
		RenderStyle "Add";
		
		DamageType "Fire";

		SchismProjectile.ParticleName "Fire";
		
		SeeSound "";
		DeathSound "FirestoneCane/Explode";
		
		-RIPPER;
		+NODAMAGETHRUST;
		+PAINLESS;
		-THRUGHOST;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.inflictImmolate = true;
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		if (specialDamage > 0 && self.inflictImmolate) {
			InflictEffect("SchismImmolateDebuff", targetActor, 10 * TICRATE);
		}
		return specialDamage;
	}
	
	override void SpawnParticles() {
		super.SpawnParticles();
		if (Random(0, 3) == 0) {
			self.SpawnParticle("FireEmber", 8, 3, (Random(-40, 40), 0, Random(-4, 4)), Random(0, 359), Random(-45, 45));
		}
	}
	
	States {
		Spawn:
			FSCF ABCDEFGHIJKLMNOP 4 bright;
			Stop;
			
		Death:
			Stop;
	}
}

class FirestoneCaneProjectileWeak : FirestoneCaneProjectile {
	Default {
		Scale 0.75;

		SchismProjectile.ParticleScale 0.5;
		SchismProjectile.ParticleInterval 4;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.inflictImmolate = false;
	}
}

class FirestoneCaneFirestormProjectile : FirestoneCaneProjectile {
	Default {
		Scale 0.25;

		SchismProjectile.ParticleScale 0.5;
		SchismProjectile.ParticleInterval 4;
	}
}


#include "zscript/schism/weapons/mogdread/firestonecane/infernospiral.zs"
#include "zscript/schism/weapons/mogdread/firestonecane/flamecombustion.zs"
#include "zscript/schism/weapons/mogdread/firestonecane/delphicnovae.zs"
#include "zscript/schism/weapons/mogdread/firestonecane/glasmashower.zs"
#include "zscript/schism/weapons/mogdread/firestonecane/ignition.zs"
