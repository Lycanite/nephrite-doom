class VoidSplintProjectile : SchismProjectile {
	VoidManipulation voidManipulation;
	int riftbolt;
	bool riftBoosted;

	Default {
		Scale 0.75;
		Radius 2;
		Height 2;
		Speed 100;
		Gravity 0;
		DamageFunction GetDamage();
		Alpha 1.0;
		RenderStyle "Translucent";
		
		DamageType "Void";

		SchismProjectile.ParticleName "Void";
		SchismProjectile.ParticleDuration 8;
		
		SeeSound "VoidManipulation/Projectile";
		DeathSound "VoidManipulation/Projectile/Explode";
		Obituary "%o was sent to the void via %k's Void Manipulation!";
		
		PROJECTILE;
		+RANDOMIZE;
		-THRUGHOST;
	}

	override int SpecialMissileHit(Actor target) {
		// Riftbolt Boost:
		if (!self.riftBoosted && self.riftbolt) {
			if (VoidManipulationRift(target) || VoidSpectre(target)) {
				target.A_StartSound("VoidManipulation/Projectile/Boost", CHAN_5);

				self.A_SetRenderStyle(1.0, STYLE_Add);
				self.scale.x = 1.5;
				self.scale.y = 1.5;
				self.particleScale = 2.0;
				self.particleSpeed = 4;

				if (self.riftbolt >= 2) {
					self.bRipper = true;
				}
				if (self.voidManipulation && self.riftbolt >= 3) {
					self.voidManipulation.FireRiftboltSplints(VoidManipulationRift(target));
				}

				self.riftBoosted = true;
				self.SetStateLabel("Empowered");
			}
		}

		// Go Through Minions:
		if (self.voidManipulation && target.master == self.voidManipulation.owner) {
			return 1;
		}
		
		return super.SpecialMissileHit(target);
	}
	
	override void Die(Actor source, Actor inflictor, int dmgflags) {
		if (self.riftBoosted && self.voidManipulation) {
			self.voidManipulation.lastBoostedHitPos = self.pos;
		}
		super.Die(source, inflictor, dmgflags);
	}
	
	override void OnDestroy() {
		if (self.riftBoosted && self.voidManipulation) {
			self.voidManipulation.lastBoostedHitPos = self.pos;
		}
		super.OnDestroy();
	}

	virtual int GetDamage() {
		// Riftbolt Damage Boost:
		if (self.riftBoosted) {
			return Random(20, 40);
		}
		return Random(5, 10);
	}
	
	States {
		Spawn:
			VDSP ABCD 4;
			Loop;
			
		Empowered:
			VDSP IJKL 4;
			Loop;
			
		Death:
			VDPT ABCDEFGHI 1;
			Stop;
	}
}