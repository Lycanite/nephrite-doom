class NetherWraith : MinionCreature {
	Default {
		Tag "Wraith";
		
		Health 100;
		Radius 16;
		Height 56;
		Mass 50;
		Speed 20;
		FloatSpeed 60;
		Scale 0.5;
		Gravity 0;
		PainChance 120;
		Damage 1;
		BloodColor "99 33 00";
		MeleeRange 88;

		DamageFactor "Nether", 0.1;

		MinionCreature.SummonTime 15 * 35;

		+FLOAT;
		+NOGRAVITY;
		+DONTFALL;
		
		SeeSound "NetherWraith/See";
		PainSound "NetherWraith/Pain";
		DeathSound "NetherWraith/Death";
		ActiveSound "NetherWraith/Idle";
		MeleeSound "NetherWraith/Melee";
		Obituary "%o was consumed by a Wraith!";
		HitObituary "%o was consumed by a Wraith!";
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		if (Health > 0) {
			self.SpawnParticle(
				"Nether", 16, 0.5,
				(0, 0, 0),
				Random(0, 359), Random(-20, 20),
				0.5
			);
		}
	}
	
	action void A_NetherWraithAttack() {
		invoker.A_CreatureMelee(5, 10, "Nether", "Nether");
		if (PlayerPawn(invoker.GetFinalMaster())) {
			invoker.A_Die();
		}
	}
	
	States {
		Spawn:
			NWRA ABCB 4 A_MinionIdle();
			Loop;
			
		See:
			NWRA DEF 4 A_MinionChase();
			NWRA E 4 {
				if (FRandom(0, 1) >= 0.75) {
					A_Leap(Random(15, 30), 0.1);
				}
				return A_MinionChase();
			}
			Loop;
			
		Melee:
			NWRA GH 4 A_FaceTarget();
			NWRA I 4 A_NetherWraithAttack();
			Goto See;
			
		Pain:
			NWRA J 6 A_Pain();
			Goto See;
			
		Pain.SelfDamage:
			Goto See;
			
		Death:
		XDeath:
			NWRA K 4;
			NWRA L 4 A_Scream();
			NWRA M 4 {
				SchismAttacks.Explode(invoker, invoker, 100, 100, 0, 1, false, false, "Nether");
			}
			NWRA N 4 A_CreatureDie();
			NWRA OPQRSTUVW 4;
			Stop;
	}
}