#!/bin/bash
set -e

wd=$(pwd)
filepath="${wd}/${1}"
npm run png-offset "$filepath" $2 $3 $4