class SchismTreeElm : SchismTree {
	Default {
		Tag "Elm Tree";
		SchismTree.SpringVariants 2;
		SchismTree.SummerVariants 2;
		SchismTree.AutumnVariants 2;
		SchismTree.WinterVariants 2;
	}
	
	States {
		Spring.1:
			TR19 A -1;
			Stop;

		Spring.2:
			TR19 B -1;
			Stop;

		Summer.1:
			TR19 E -1;
			Stop;

		Summer.2:
			TR19 F -1;
			Stop;

		Autumn.1:
			TR19 I -1;
			Stop;

		Autumn.2:
			TR19 J -1;
			Stop;

		Winter.1:
			TD19 M -1;
			Stop;

		Winter.2:
			TD19 N -1;
			Stop;

		Death.Spring.1:
			TD19 A -1;
			Stop;

		Death.Spring.2:
			TD19 B -1;
			Stop;

		Death.Summer.1:
			TD19 E -1;
			Stop;

		Death.Summer.2:
			TD19 F -1;
			Stop;

		Death.Autumn.1:
			TD19 I -1;
			Stop;

		Death.Autumn.2:
			TD19 J -1;
			Stop;

		Death.Winter.1:
			TD19 M -1;
			Stop;

		Death.Winter.2:
			TD19 N -1;
			Stop;
	}
}