class SchismInteraction : Thinker {
	/** 
	 * Gets the final master of the provided minion.
	 * @param minion The minion to get the final master of.
	 * @param iteration The recusrive iteration count, used to stop infinite loops in case actors refer to each other as a master in a circle.
	 */
	static Actor GetFinalMaster(Actor minion, int iteration = 0) {
		Actor finalMaster = minion;
		if (iteration > 10 || PlayerPawn(finalMaster)) {
			return finalMaster;
		}
		if (finalMaster.master) {
			finalMaster = GetFinalMaster(minion.master, iteration + 1);
		}
		return finalMaster;
	}

	/** 
	 * Determines if the provided attacker can harm (damage, debuff, etc) the provided target.
	 * @param attacker The attacking entity.
	 * @param target The target entity the attacker is checking if they can harm.
	 * @param hurtSelf If true, the attacker is allowed to hurt themselves if they are their own target. Defaults to false.
	 */
	static bool CanHarmTarget(Actor attacker, Actor target, bool hurtSelf = false) {
		if (!attacker || !target || !target.bShootable) {
			return false;
		}
		
		// Hurt Self:
		if (hurtSelf && attacker == target) {
			return true;
		}
		
		// Get Masters:
		Actor attacker = GetFinalMaster(attacker);
		Actor target = GetFinalMaster(target);
		
		// Same Master:
		if (attacker == target) {
			return false;
		}
		
		// Harming Players:
		if (PlayerPawn(target)) {
			// PVP:
			if (PlayerPawn(attacker) && GameInfo.GameType == 1) { // Coop
				return false;
			}
			
			// Friendly:
			if (attacker.bFriendly) {
				return false;
			}
		}

		// Infighting:
		if (!attacker.bFriendly && !target.bFriendly) {
			if (attacker.bDontHarmClass && target.GetClass() == attacker.GetClass()) {
				return false;
			}
			if (attacker.bDontHarmSpecies && target.species && target.species == attacker.species) {
				return false;
			}
		}
		
		return true;
	}
	
	/** 
	 * Determines if the provided helper can help (heal, buff, etc) the provided target.
	 * @param helper The helping entity.
	 * @param target The target entity the helper is checking if they can help.
	 * @param helpSelf If true, the helper is allowed to help themselves if they are their own target. Defaults to true.
	 */
	static bool CanHelpTarget(Actor helper, Actor target, bool helpSelf = true) {
		if (!helper || !target || !target.bShootable) {
			return false;
		}
		return !CanHarmTarget(helper, target, !helpSelf);
	}
}