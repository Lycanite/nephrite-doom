class MagmaProjectile : SchismProjectile {
	int burstTime;

	Default {
		Radius 16;
		Height 10;
		Scale 1;
		Speed 25;
		DamageFunction 10;
		Gravity 0.25;
		Alpha 1;
		Renderstyle "Add";
		DamageType "Fire";

		SchismProjectile.PoolTics 10 * TICRATE;
		SchismProjectile.PoolDamage 1;
		SchismProjectile.PoolDamageRadius 140;
		SchismProjectile.PoolClass "MagmaPool";
		SchismProjectile.PoolScale 1;
		
		SeeSound "LavaCannon/Fire";
		DeathSound "LavaCannon/Explode";
		
		-NOGRAVITY;
		+FORCERADIUSDMG;
		+PAINLESS;
		-THRUGHOST;
	}

	override void SpawnParticles() {
		super.SpawnParticles();

		if (self.burstTime > 0) {
			self.burstTime -= tics;
			self.SpawnParticle("Fire", TICRATE, 1, (0, 0, 0), Random(0, 359), Random(60, 90), 1.6);
		}
	}
	
	States {
		Spawn:
			MGMA ABCDEF 2 bright;
			Loop;
			
		Death:
			MGMA GHIJKLMNO 1 bright;
			MGMA P 1 bright A_QuakeEx(1, 1, 1, 10, 0, 100, "world/quake", 0, 1, 1, 0, 0, 1, 1);
		Pool:
			0000 P 1 A_PoolUpdate(0.04);
			0000 P 15;
			Loop;
	}
}

class MagmaPool : ProjectilePool {
	Default {
		Radius 16;
		Height 1;
		Scale 1;
		Gravity 0.25;
		Alpha 1;
		Renderstyle "Add";

		ProjectilePool.ScaleBase 1;
		ProjectilePool.RemainingTics 10 * TICRATE;

		ProjectilePool.particleName "Lava";
		ProjectilePool.particleCount 1;
		ProjectilePool.particleOffsetRandom 60;
		ProjectilePool.particleDuration TICRATE;
		ProjectilePool.particleSpeed 0.5;
		ProjectilePool.particlePitchMin -45;
		ProjectilePool.particlePitchMax 45;
	}
	
	States {
		Spawn:
			LVAP ABCDEFGHIJ 4 {
				return A_EffectUpdate(4);
			}
			Loop;
	}
}