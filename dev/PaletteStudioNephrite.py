bl_info = {
    "name": "Palette Studio (Schism)",
    "author": "Dieting Hippo, Lycanite",
    "version": (1, 2, 0),
    "blender": (2, 83, 0),
    "location": "Render > Palette",
    "support": "COMMUNITY", 
    "category": "Render",
    "description": "Render images with specified color palettes(.pal) out to 8 camera angles. Website: https://dietinghippo.itch.io/palette-studio Edited to allow for no palettes and to bulk render ranges of animation frames.",
    "wiki_url": "",
    "tracker_url": ""
}

import bpy
import bpy_extras
import csv
import os
import math

from bpy_extras.io_utils import ImportHelper
from bpy.types import Operator
from bpy.props import StringProperty
from mathutils import Euler

### Debug ###
ps_print_debug = False

def ps_dprint(string):
    if ps_print_debug:
        print(string)
    return    

### Enum ###

camera_angle_enum = [
    ("1", "1", "1 camera angle", 1),
    ("8", "8", "8 camera angles", 8),
    ("16", "16", "16 camera angles", 16)
]

### Getters ###
def get_bool_prop(props, name):
    try:
        value = props[name]
    except:
        value = True
    return value

def get_alpha(ps_props):
    try:
        use_alpha = ps_props['use_alpha']
    except:
        use_alpha = True
    return use_alpha

def get_alpha_threshold(ps_props):
    try:
        alpha_threshold = ps_props['alpha_threshold']
    except:
        alpha_threshold = 0.0
    return alpha_threshold

def get_num_cameras(ps_props):
    try:
        num_cameras = int(ps_props['num_cameras'])
    except:
        num_cameras = 8
    return num_cameras

def get_palette_path(ps_file):
    try:
        palette_path = ps_file['palette_file_path']
    except:
        palette_path = ""
    return palette_path

def get_palette_file(ps_file):
    try:
        palette_file = ps_file['palette_file']
    except:
        palette_file = ""
    return palette_file

def get_palette_indices(ps_file):
    try:
        palette_indices = ps_file['palette_indices']
    except:
        palette_indices = ""
    return palette_indices

def get_render_dir(ps_file):
    try:
        render_dir = ps_file['render_dir']
    except:
        render_dir = "//"
    return render_dir

def get_render_prefix(ps_file):
    try:
        render_prefix = ps_file['render_prefix']
    except:
        render_prefix = "PS Render"
    return render_prefix

### Methods ###

def camera_angle_char(camera_index):
    scene = bpy.context.scene
    ps_props = scene.palettestudio_fileprops
    
    camera_char = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G"]
    
    if get_num_cameras(ps_props) == 1 and camera_index == 0:
        return "0"
    else:
        return camera_char[camera_index]

def animation_char(frame_index, start_frame, enabled):
    if not enabled:
        return ""
    scene = bpy.context.scene
    ps_props = scene.palettestudio_fileprops
    
    animation_chars = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "[", "]", "^"]
    return animation_chars[frame_index - start_frame]

# Get closest color in subjects list to query input
def nearest_color( subjects, query ):
        return min( subjects, key = lambda subject: sum( (s - q) ** 2 for s, q in zip( subject, query ) ) )

#Read palette file (located in .blend directory)
def read_palette():
    # Get properties
    scene = bpy.context.scene
    ps_file = scene.palettestudio_fileprops
    ps_colors = scene.palettestudio_palettecolorlist
    
    # Get absolute path
    file_path = os.path.realpath(bpy.path.abspath(ps_file['palette_file_path']))
    
    # Exit and print log if file path is empty
    if file_path == "":        
        ps_dprint('File path empty.')
        return 
    
    # Open .pal file
    with open(file_path, 'r') as reader:
        # Read in header lines
        pal_header = reader.readline()  # JASC-PAL (Header)
        pal_version = reader.readline() # 0100     (Version)
        pal_colors = reader.readline()  # 256      (Color Amount, 16 or 256)
        
        # Read in color values
        lines = csv.reader(reader, delimiter=' ')
        
        # Clear existing palette color values
        ps_colors.clear()
        
        # Store color values in colors
        colors = []
        
        # Get RGB colors from palette
        for line in lines:
            colors = (*colors, (int(line[0]), int(line[1]), int(line[2]),),)
        
        # Store to global int vector list
        num_colors = 0
        for color in colors:
            colorProp = ps_colors.add()
            colorProp.color = color
            num_colors = num_colors + 1
        
        # Print palette conversion information
        ps_dprint("Converted palette: " + file_path)
        ps_dprint("Colors: " + str(num_colors))


def convert_palette(ref_pixel_arr):
    # Copy pixel array for improved read speeds
    pixel_arr = ref_pixel_arr[:]
    # Get number of pixels in array
    arr_len = int(len(pixel_arr)/4)
    # Init converted pixel array
    new_pixel_arr = [0.0] * len(pixel_arr)
    ps_dprint("Array Length: " + str(arr_len))
    
    ps_dprint("Start converting...")
    
    # Get properties
    scene = bpy.context.scene
    ps_colors = scene.palettestudio_palettecolorlist
    ps_props = scene.palettestudio_fileprops
    
    indices = []
    indices_str = ""
    if len(get_palette_indices(ps_props)) > 0:
        indices_str = [str(x) for x in get_palette_indices(ps_props).split(',')]
    
        for i in get_palette_indices(ps_props).split(','):
            if '-' not in i:
                indices.append(int(i))
            else:
                start, end = map(int, i.split('-'))
                indices+= range(start, end + 1)
    
    if len(indices) == 0:
        ps_dprint("No indices selected.")
    else:
        ps_dprint(indices)
    
    # Load colors from color property
    colors = []
    color_i = 0
    for color in ps_colors:
        if len(indices) == 0 or (len(indices) != 0 and color_i in indices):
            colors = (*colors, color.color,)
        color_i = color_i + 1
        
    # Initialize dict of known color matches
    known_colors = {}

    # Loop through each pixel
    for i in range(0, arr_len):
        # Get source color
        from_color = (int(pixel_arr[(i*4)+0]*255), int(pixel_arr[(i*4)+1]*255), int(pixel_arr[(i*4)+2]*255),)
        
        # If source color hasn't been converted, convert it
        if str(from_color) not in known_colors:
            # Get closest color
            new_color = nearest_color( colors, from_color )

            # Create known closest color
            to_color = ()
            for value in new_color:
                value = (value) / 255
                to_color = (*to_color, value,)
            to_color = (*to_color, 1.0,)
            known_colors[str(from_color)] = to_color
        
        # Store converted color in new pixel array
        new_pixel_arr[(i*4)+0], new_pixel_arr[(i*4)+1], new_pixel_arr[(i*4)+2], new_pixel_arr[(i*4)+3] = known_colors[str(from_color)]
        
        # Set alpha value based on source pixel
        if get_alpha(ps_props):
            if int(pixel_arr[(i*4)+3]) <= get_alpha_threshold(ps_props):
                new_pixel_arr[(i*4)+3] = 0
            else:
                new_pixel_arr[(i*4)+3] = 1    
        
    ps_dprint("Done converting.")
    return new_pixel_arr
    
def update_num_cameras(value):
    scene = bpy.context.scene
    ps_props = scene.palettestudio_fileprops
    ps_cameras = scene.palettestudio_cameralist
    ps_animations = scene.palettestudio_cameralist
    
    if 'PS Camera Rig' in bpy.data.objects:
        num_cameras = get_num_cameras(ps_props) 
        
        # Changing to 1 camera
        if num_cameras == 1:
            # Remove all cameras except the first
            for i in range(1, 16):
                ri = 16 - i
                angle_char = camera_angle_char(ri)
                camera_name = 'PS Camera ' + angle_char
                
                if camera_name in bpy.data.cameras:
                    bpy.data.cameras.remove(bpy.data.cameras[camera_name])
                    ps_cameras.remove(ri)
            
            bpy.data.cameras['PS Camera 1'].name = 'PS Camera 0'
            bpy.data.objects['PS Camera 1'].name = 'PS Camera 0'
            return
        
        if 'PS Camera 0' in bpy.data.cameras:
                bpy.data.cameras['PS Camera 0'].name = 'PS Camera 1'
                bpy.data.objects['PS Camera 0'].name = 'PS Camera 1'
        
        if num_cameras == 8:
            # Remove cameras 9 - G
            for i in range(0, 8):
                ri = 15 - i
                angle_char = camera_angle_char(ri)
                camera_name = 'PS Camera ' + angle_char
                
                if camera_name in bpy.data.cameras:
                    bpy.data.cameras.remove(bpy.data.cameras[camera_name])
                    ps_cameras.remove(ri)
                    
            # Add first eight cameras
            for i in range(0, 8):
                if 'PS Camera ' + camera_angle_char(i) not in bpy.data.cameras:
                    add_camera(i)
            return
        
        if num_cameras == 16:
            # Add all cameras
            for i in range(0, 16):
                if 'PS Camera ' + camera_angle_char(i) not in bpy.data.cameras:
                    add_camera(i)
            return

def drawPalettePreview():
    ps_props = bpy.context.scene.palettestudio_fileprops
    colors = bpy.context.scene.palettestudio_palettecolorlist
    color_num = len(colors)
    color_sqrt = int(math.ceil(math.sqrt(color_num)))
    
    if color_sqrt % 2 == 1:
        color_sqrt = color_sqrt #+ 1
    
    if 'Palette Preview' in bpy.data.images:
        bpy.data.images.remove(bpy.data.images['Palette Preview'])
        
    bpy.data.images.new(name = "Palette Preview", width = color_sqrt, height = color_sqrt)
    
    ps_props['palette_preview'] = bpy.data.images['Palette Preview']

    # Draw to image
    colors_drawn = 0
    for pixel_index in range(0, color_num):
        row = int(math.floor(pixel_index / color_sqrt))
        col = int(math.floor(pixel_index % color_sqrt))
        array_index = ((color_sqrt ** 2) - ((row * color_sqrt) + (color_sqrt - col))) * 4
        color = colors[pixel_index]
        r, g, b = color.color
        ps_props['palette_preview'].pixels[array_index + 0] = r / 255
        ps_props['palette_preview'].pixels[array_index + 1] = g / 255
        ps_props['palette_preview'].pixels[array_index + 2] = b / 255
        ps_props['palette_preview'].pixels[array_index + 3] = 1.0
        colors_drawn = colors_drawn + 1
        
        # Break out early if end of color list reached
        if colors_drawn >= color_num:
            break
        
def add_camera(camera_num):
    ps_props = bpy.context.scene.palettestudio_fileprops
    ps_cameras = bpy.context.scene.palettestudio_cameralist
    
    # Get camera rig
    camera_rig = bpy.data.objects['PS Camera Rig']
    
    # Get camera name
    angle_char = camera_angle_char(camera_num)
    camera_name = "PS Camera " + angle_char
    
    # Create camera
    camera = bpy.data.cameras.new(camera_name)
    camera.type = 'ORTHO'
    
    # Create camera object
    camera_obj = bpy.data.objects.new(camera_name, camera)
    bpy.context.scene.collection.objects.link(camera_obj)
    
    # Adjust location and rotation
    radius = 8
    camera_num_offset = camera_num
    
    if camera_num >= 8:
        camera_num_offset = camera_num_offset + 0.5
    
    camera_num_angle = camera_num_offset / 8
    angle = 2 * math.pi * camera_num_angle
    
    x = math.cos(angle) * radius
    y = math.sin(angle) * radius
    
    camera_obj.location = (y, -x, 0.0)
    camera_obj.rotation_euler = Euler((math.pi/2, 0.0, (math.pi/4)*camera_num_offset), 'XYZ')
    
    # Parent to camera rig
    camera_obj.parent = camera_rig
    
    # Add camera to pointer collection
    ps_camera = ps_cameras.add()
    ps_camera['camera'] = camera_obj
    ps_camera['enabled'] = True
    
    return

def rotate_light_rig_to_camera(camera_num):
    # Get camera rig
    light_rig = bpy.data.objects['PS Light Rig']
    
    camera_num_offset = camera_num
    
    if camera_num >= 8:
        camera_num_offset = camera_num_offset + 0.5
    
    light_rig.rotation_euler = Euler((0.0, 0.0, (math.pi/4)*camera_num_offset), 'XYZ')    

### Operators ###

class PaletteStudio_OT_OpenFileBrowser(Operator, ImportHelper):
    bl_idname = "palettestudio.open_filebrowser"
    bl_label = "Select Palette (.pal)"
    bl_description = "Select a Palette (.pal) file to obtain color information from. Accepted files are .pal with ASCII formatting"
    
    # Filter palette selector for .pal files
    filter_glob : StringProperty(
        default = '*.pal',
        options = {'HIDDEN'}
    )
    
    def execute(self, context):
        filename, extension = os.path.splitext(self.filepath)
        self.report(
            {'INFO'}, 'File path: %r' % 
            (self.filepath)
        )
        
        # Store filepath
        ps_props = context.scene.palettestudio_fileprops
        ps_props['palette_file_path'] = ""
        ps_props['palette_file_path'] = self.filepath
        
        # Store filename
        palette_name = bpy.path.basename(self.filepath)
        ps_props['palette_file'] = ""
        ps_props['palette_file'] = palette_name
        
        # Print results
        ps_dprint("Palette name: " + palette_name)
        ps_dprint("Filepath: " + self.filepath)
        ps_dprint("Filename: " + filename)
        ps_dprint("File ext: " + extension)
        
        # Read in new palette
        read_palette()
        
        ## Draw palette preview 
        drawPalettePreview()
        
        context.area.tag_redraw()
        
        return {'FINISHED'}
    
class PaletteStudio_OT_CreateCameraRig(Operator):
    bl_idname = "palettestudio.create_camera_rig"
    bl_label = "Create"
    bl_description = "Create a \"PS Camera Rig\" object and associated \"PS Camera\" cameras"
    
    def execute(self, context):
        ps_props = context.scene.palettestudio_fileprops
        ps_cameras = context.scene.palettestudio_cameralist
        
        # Don't generate new rig if one exists
        if 'PS Camera Rig' in bpy.data.objects:
            self.report(
                {'ERROR'}, "[Palette Studio] \"PS Camera Rig\" exists. Remove the existing rig by selecting \"Remove\" in the Camera Rig panel before adding another."
            )
            return {'CANCELLED'}
        
        # Don't generate new rig if cameras and camera data exist
        for i in range(0, get_num_cameras(ps_props)):
            if 'PS Camera ' + camera_angle_char(i) in bpy.context.scene.collection.objects:
                self.report(
                    {'ERROR'}, "[Palette Studio] \"PS Camera " + camera_angle_char(i) + "\" object exists. Select \"Remove\" in the Camera Rig panel, or delete \"PS Camera " + camera_angle_char(i) + "\" from Outliner > View Layer"
                )
                return {'CANCELLED'}
            if 'PS Camera ' + camera_angle_char(i) in bpy.data.cameras:
                self.report(
                    {'ERROR'}, "[Palette Studio] \"PS Camera " + camera_angle_char(i) + "\" data exists. Select \"Remove\" in the Camera Rig panel, or delete \"PS Camera " + camera_angle_char(i) + "\" from Outliner > Blender Files > Cameras"
                )
                return {'CANCELLED'}
        
        # Create empty to parent cameras to
        camera_rig = bpy.data.objects.new("PS Camera Rig", None)
        # Link to scene collection
        bpy.context.scene.collection.objects.link(camera_rig)
        # Set properties
        camera_rig.empty_display_size = 1
        camera_rig.empty_display_type = 'PLAIN_AXES'
        
        # Clear existing camera property information
        ps_cameras.clear()
        
        # Create cameras
        for i in range (0, get_num_cameras(ps_props)):
            add_camera(i)
        
        return {'FINISHED'}
    
class PaletteStudio_OT_RemoveCameraRig(Operator):    
    bl_idname = "palettestudio.remove_camera_rig"
    bl_label = "Remove"
    bl_description = "Remove a \"PS Camera Rig\" object and associated \"PS Camera\" cameras"
    
    def execute(self, context):
        ps_props = context.scene.palettestudio_fileprops
        ps_cameras = bpy.context.scene.palettestudio_cameralist
        
        # Remove camera data (and objects)
        for i in range(0, get_num_cameras(ps_props)):
            if 'PS Camera ' + camera_angle_char(i) in bpy.data.cameras:
                bpy.data.cameras.remove(bpy.data.cameras['PS Camera ' + camera_angle_char(i)])
        
        # Remove rig empty
        if 'PS Camera Rig' in bpy.data.objects:
            bpy.data.objects.remove(bpy.data.objects['PS Camera Rig'])
        
        # Clear camera list property
        ps_cameras.clear()
            
        return {'FINISHED'}
    
class PaletteStudio_OT_CreateLightRig(Operator):
    bl_idname = "palettestudio.create_light_rig"
    bl_label = "Create"
    bl_description = "Create a \"PS Light Rig\" object. Lights parented to the object will be rotated when \"Rotate to Camera\" is enabled"
    
    def execute(self, context):
        ps_props = context.scene.palettestudio_fileprops
        
        # Don't generate new rig if one exists
        if 'PS Light Rig' in bpy.data.objects:
            self.report(
                {'ERROR'}, "[Palette Studio] \"PS Light Rig\" exists. Remove the existing rig by selecting \"Remove\" in the Light Rig panel before adding another."
            )
            return {'CANCELLED'}
        
        # Create empty to parent lights to
        light_rig = bpy.data.objects.new("PS Light Rig", None)
        # Link to scene collection
        bpy.context.scene.collection.objects.link(light_rig)
        # Set properties
        light_rig.empty_display_size = 1
        light_rig.empty_display_type = 'PLAIN_AXES'
        
        return {'FINISHED'}
    
class PaletteStudio_OT_RemoveLightRig(Operator):    
    bl_idname = "palettestudio.remove_light_rig"
    bl_label = "Remove"
    bl_description = "Remove a \"PS Light Rig\" object. Lights parented to the object will be unparented"
    
    def execute(self, context):
        ps_props = context.scene.palettestudio_fileprops
        
        # Remove rig empty
        if 'PS Light Rig' in bpy.data.objects:
            bpy.data.objects.remove(bpy.data.objects['PS Light Rig'])
                
        return {'FINISHED'}
    
class PaletteStudio_OT_Render(Operator):
    bl_idname = "palettestudio.render"
    bl_label = "Render"
    bl_description = "Render the selected cameras in the palette colors selected. Rendered images will be placed in \"Out Dir\" with a filename of \"Prefix\" + Camera Number.\n\nex: Rendering Camera 8 with a prefix of \"TEST\" in \"//output/\" produces //output/TEST8.png"
    
    def execute(self, context):
        # Get file properties
        ps_props = context.scene.palettestudio_fileprops
        ps_cameras = context.scene.palettestudio_cameralist
        
        # Animation Frames:
        renderFrames = False
        if 'render_frames' in ps_props:
            renderFrames = ps_props['render_frames']
        startFrame = 0
        if 'start_frame' in ps_props:
            startFrame = ps_props['start_frame']
        endFrame = startFrame
        if 'end_frame' in ps_props and renderFrames:
            endFrame = ps_props['end_frame']
        if renderFrames and endFrame - startFrame > 29:
            self.report (
                {'ERROR'}, "[Palette Studio] Too many animation frames, only 29 frames per prefix is support (\"A\"-\"Z\", \"[\", \"]\" and \"^\")."
            )
            return {'CANCELLED'}

        if 'PS Camera Rig' not in bpy.data.objects:
            self.report (
                {'ERROR'}, "[Palette Studio] No \"PS Camera Rig\" found. Select \"Create\" in the Camera Rig panel to create a new Camera Rig."
            )
            return {'CANCELLED'}
        
        for frame in range(startFrame, endFrame + 1):
            # Set animation frame
            if renderFrames:
                bpy.context.scene.frame_set(frame)

            for i in range(0, get_num_cameras(ps_props)):
                ps_camera = ps_cameras[i]
                
                # Skip camera if not enabled
                if not ps_camera.enabled:
                    continue
                
                # Print render status to log
                print("Rendering PS Camera", camera_angle_char(i))
                
                # Skip and print warning if camera doesn't exist
                if not ps_camera['camera']:
                    self.report (
                        {'WARNING'}, "[Palette Studio] Camera \"PS Camera " + camera_angle_char(i) + "\" not found. Skipping render."
                    )
                    continue
                
                # Set scene camera
                bpy.context.scene.camera = ps_camera['camera']
                
                #Rotate lights if enabled
                if get_bool_prop(ps_props, 'rotate_light') and 'PS Light Rig' in bpy.data.objects:
                    rotate_light_rig_to_camera(i)
                
                # Render initial image
                ps_dprint("Rendering frame " + str(frame + 1) + "/" + str(endFrame - startFrame + 1) + "...")
                bpy.ops.render.render()
                
                # Stop render if no Viewer Node image was found.
                if 'Viewer Node' not in bpy.data.images:
                    self.report (
                        {'ERROR'}, "[Palette Studio] No \"Viewer Node\" image found. Add a \"Viewer\" node to the Scene Composition and connect an Image output."
                    )
                    return {'CANCELLED'}    
                
                # Remove previous render
                if 'PS Render' in bpy.data.images:
                    bpy.data.images.remove(bpy.data.images['PS Render'])
                
                # Render image from viewer node to render file
                render_path = bpy.path.abspath(get_render_dir(ps_props)) + get_render_prefix(ps_props) + animation_char(frame, startFrame, renderFrames) + camera_angle_char(i) + ".png"
                bpy.data.images['Viewer Node'].save_render(render_path)
                
                # Apply palette if set
                if 'palette_file' in ps_props:
                    render_image = bpy.data.images.load(render_path)
                    # Get array of pixels and convert to palette colors
                    render_image.name = 'PS Render'
                    ps_dprint("Converting image to palette...")
                    pixel_arr = render_image.pixels[:]
                    pixel_arr = convert_palette(pixel_arr)
                    render_image.pixels = pixel_arr[:]
                    ps_dprint("Finished converting.")
                
                    # Save image to file
                    render_image.save()
        
        # Reset light rig rotation
        if get_bool_prop(ps_props, 'rotate_light') and 'PS Light Rig' in bpy.data.objects:
                rotate_light_rig_to_camera(0)
            
        self.report(
            {'INFO'}, "[Palette Studio] Finished rendering."
        )

        return {'FINISHED'}   

### Panel ###

class PaletteStudio_PanelGroup:
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = 'Palette Studio'    

class PaletteStudio_Panel(PaletteStudio_PanelGroup, bpy.types.Panel):
    bl_idname = "PS_PT_select"
    bl_label = "Palette Studio"
    
    palette_file_path : bpy.props.StringProperty(
        name = "Palette File"
    )

    @classmethod
    def poll(cls, context):
        return True
    
    def draw_header(self, context):
        layout = self.layout

    def draw(self, context):
        pass

class PS_PT_PalettePanel(PaletteStudio_PanelGroup, bpy.types.Panel):
    bl_parent_id = "PS_PT_select"
    bl_label = "Palette"
    bl_order = 0
    
    def draw(self, context):
        layout = self.layout
        ps_props = context.scene.palettestudio_fileprops
        
        open_palette = layout.operator("palettestudio.open_filebrowser")
        palette_file_path = layout.prop(ps_props, 'palette_file_path')
        palette_file = layout.prop(ps_props, 'palette_file')
        
        if 'Palette Preview' in bpy.data.images:
            pal_prev = layout.template_ID_preview(ps_props,'palette_preview',hide_buttons=True)

        indices = layout.prop(ps_props, 'palette_indices')
            
class PS_PT_CameraRigPanel(PaletteStudio_PanelGroup, bpy.types.Panel):
    bl_parent_id = "PS_PT_select"
    bl_label = "Camera Rig"
    bl_order = 1
    
    def draw(self, context):
        layout = self.layout
        ps_props = context.scene.palettestudio_fileprops
        camera_list = context.scene.palettestudio_cameralist
        
        num_cameras = layout.prop(ps_props,'num_cameras')
        camera_rig_row = layout.row()
        camera_create_rig = camera_rig_row.operator("palettestudio.create_camera_rig")
        camera_clear_rig = camera_rig_row.operator("palettestudio.remove_camera_rig")
        
        camera_list_box = layout.box()
        num_cameras = len(camera_list)
        
        # Draw camera rows
        camera_list_row = None
        for camera_index in range(0, num_cameras):
            if camera_index % 4 == 0:
                camera_list_row = camera_list_box.row()
            
            camera = camera_list[camera_index]
            camera_list_row.prop(camera, 'enabled', text=camera_angle_char(camera_index))

class PS_PT_LightRigPanel(PaletteStudio_PanelGroup, bpy.types.Panel):
    bl_parent_id = "PS_PT_select"
    bl_label = "Light Rig"
    bl_order = 2
    
    def draw(self, context):
        layout = self.layout
        ps_props = context.scene.palettestudio_fileprops
        
        light_rig_row = layout.row()
        
        light_create_rig = light_rig_row.operator("palettestudio.create_light_rig")
        light_clear_rig = light_rig_row.operator("palettestudio.remove_light_rig")
        
        light_rotate = layout.prop(ps_props, 'rotate_light')

class PS_PT_AnimationsPanel(PaletteStudio_PanelGroup, bpy.types.Panel):
    bl_parent_id = "PS_PT_select"
    bl_label = "Animations"
    bl_order = 3
    
    def draw(self, context):
        layout = self.layout
        ps_props = context.scene.palettestudio_fileprops
        
        render_frames = layout.prop(ps_props, 'render_frames')

        animations_row = layout.row()
        start_frame = animations_row.prop(ps_props, 'start_frame')
        end_frame = animations_row.prop(ps_props, 'end_frame')
        
class PS_PT_RenderPanel(PaletteStudio_PanelGroup, bpy.types.Panel):
    bl_parent_id = "PS_PT_select"
    bl_label = "Render"
    bl_order = 4
    
    def draw(self, context):
        layout = self.layout
        ps_props = context.scene.palettestudio_fileprops
        
        render_alpha_row = layout.row()
        render_alpha = render_alpha_row.prop(ps_props, 'use_alpha')
        render_alpha_threshold = render_alpha_row.prop(ps_props, 'alpha_threshold')
        
        render_dir = layout.prop(ps_props, 'render_dir')
        render_prefix = layout.prop(ps_props, 'render_prefix')
        render_button = layout.operator("palettestudio.render")
    
### Properties ###

def PaletteStudio_GetPaletteFilePath(self):
    ps_file = bpy.context.scene.palettestudio_fileprops
    return get_palette_path(ps_file)

def PaletteStudio_GetPaletteFile(self):
    ps_file = bpy.context.scene.palettestudio_fileprops
    return get_palette_file(ps_file)

def PaletteStudio_UpdateNumCameras(self, value):
    update_num_cameras(value)
    return None
        
class PaletteStudio_FileProps(bpy.types.PropertyGroup):
    palette_file_path : bpy.props.StringProperty(
        name = "Path",
        subtype = 'NONE',
        get = PaletteStudio_GetPaletteFilePath
    )
    palette_file : bpy.props.StringProperty(
        name = "File",
        subtype = 'FILE_NAME',
        get = PaletteStudio_GetPaletteFile
    )
    palette_indices : bpy.props.StringProperty(
        name = "Palette Indices",
        description = "Specified palette indices to use when converting image palette. Indices start at 0 and can be specified individually or in a range, separated by a comma. Leave field blank to use full palette.\n\nex: \"0,3,9,11,12,13\" or \"0,3,9,11-13\"",
        subtype = 'NONE',
        default = ""
    )
    render_dir : bpy.props.StringProperty(
        name = "Out Dir",
        default = "//",
        subtype = 'DIR_PATH'
    )
    render_prefix : bpy.props.StringProperty(
        name = "Prefix",
        default = "PS Render",
        subtype = 'NONE'
    )
    alpha_threshold : bpy.props.FloatProperty(
        name = "Threshold",
        default = 0.0,
        min = 0.0,
        max = 1.0,
        soft_min = 0.0,
        soft_max = 1.0
    )
    use_alpha : bpy.props.BoolProperty(
        name = "Alpha",
        default = True
    )
    palette_preview : bpy.props.PointerProperty(
        name = "Palette Preview",
        type = bpy.types.Image  
    )    
    num_cameras : bpy.props.EnumProperty(
        items = camera_angle_enum,
        name = "Camera Angles",
        default = "8",    
        update = PaletteStudio_UpdateNumCameras      
    )
    rotate_light : bpy.props.BoolProperty(
        name = "Rotate to Camera",
        description = "Rotate the Light Rig to the active camera while rendering if true",
        default = True
    )

    render_frames : bpy.props.BoolProperty(
        name = "Render Animation Frames",
        default = False
    )
    start_frame : bpy.props.IntProperty(
        name = "Start",
        default = 0
    )
    end_frame : bpy.props.IntProperty(
        name = "End",
        default = 0
    )
    
class PaletteStudio_PaletteColorList(bpy.types.PropertyGroup):
    color : bpy.props.IntVectorProperty(
        name = "Palette Color",
        default = (0, 0, 0),
        min = 0,
        max = 255,
        soft_min = 0,
        soft_max = 255,
        options = {'HIDDEN'},
        subtype = 'COLOR',
        size = 3
    )

class PaletteStudio_CameraList(bpy.types.PropertyGroup):
    camera : bpy.props.PointerProperty(
        name = "PS Camera",
        type = bpy.types.Object
    )
    enabled : bpy.props.BoolProperty(
        name = "Enabled",
        default = True
    )
 
### Register ###
    
def register():
    bpy.utils.register_class(PaletteStudio_OT_OpenFileBrowser)
    bpy.utils.register_class(PaletteStudio_OT_CreateCameraRig)
    bpy.utils.register_class(PaletteStudio_OT_RemoveCameraRig)
    bpy.utils.register_class(PaletteStudio_OT_CreateLightRig)
    bpy.utils.register_class(PaletteStudio_OT_RemoveLightRig)
    bpy.utils.register_class(PaletteStudio_OT_Render)
    bpy.utils.register_class(PaletteStudio_FileProps)
    bpy.utils.register_class(PaletteStudio_PaletteColorList)
    bpy.utils.register_class(PaletteStudio_CameraList)
    bpy.utils.register_class(PaletteStudio_Panel)
    bpy.utils.register_class(PS_PT_PalettePanel)
    bpy.utils.register_class(PS_PT_CameraRigPanel)
    bpy.utils.register_class(PS_PT_LightRigPanel)
    bpy.utils.register_class(PS_PT_AnimationsPanel)
    bpy.utils.register_class(PS_PT_RenderPanel)
    bpy.types.Scene.palettestudio_fileprops = bpy.props.PointerProperty(type=PaletteStudio_FileProps)
    bpy.types.Scene.palettestudio_palettecolorlist = bpy.props.CollectionProperty(type=PaletteStudio_PaletteColorList)
    bpy.types.Scene.palettestudio_cameralist = bpy.props.CollectionProperty(type=PaletteStudio_CameraList)
    
def unregister():
    bpy.utils.unregister_class(PaletteStudio_OT_OpenFileBrowser)
    bpy.utils.unregister_class(PaletteStudio_OT_CreateCameraRig)
    bpy.utils.unregister_class(PaletteStudio_OT_RemoveCameraRig)
    bpy.utils.unregister_class(PaletteStudio_OT_CreateLightRig)
    bpy.utils.unregister_class(PaletteStudio_OT_RemoveLightRig)
    bpy.utils.unregister_class(PaletteStudio_OT_Render)
    bpy.utils.unregister_class(PaletteStudio_FileProps)
    bpy.utils.unregister_class(PaletteStudio_PaletteColorList)
    bpy.utils.unregister_class(PaletteStudio_CameraList)
    bpy.utils.unregister_class(PaletteStudio_Panel)
    bpy.utils.unregister_class(PS_PT_PalettePanel)
    bpy.utils.unregister_class(PS_PT_CameraRigPanel)
    bpy.utils.unregister_class(PS_PT_LightRigPanel)
    bpy.utils.unregister_class(PS_PT_AnimationsPanel)
    bpy.utils.unregister_class(PS_PT_RenderPanel)

if __name__ == "__main__":
    register()
