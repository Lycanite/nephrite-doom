class LichTotem : WeaponUltimate {
	Default {
		Scale 1;
		Inventory.PickupMessage "You have obtained a Lich Totem! With this you may activate a weapon's ultimate ability!";
		
		Inventory.Icon "ACHZ[0";
		Inventory.PickupSound "Chozo/Taunt";
		Inventory.RestrictedTo "Chozo";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}
	
	
	States {
		Spawn:
			ACHZ [ 1 bright;
			Loop;
	}
}
