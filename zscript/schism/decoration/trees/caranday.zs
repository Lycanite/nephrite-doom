class SchismTreeCaranday : SchismTree {
	Default {
		Tag "Caranday Tree";
		SchismTree.SpringVariants 2;
		SchismTree.SummerVariants 2;
		SchismTree.AutumnVariants 2;
		SchismTree.WinterVariants 2;
	}
	
	States {
		Spring.1:
			TR15 A -1;
			Stop;

		Spring.2:
			TR15 B -1;
			Stop;

		Summer.1:
			TR15 E -1;
			Stop;

		Summer.2:
			TR15 F -1;
			Stop;

		Autumn.1:
			TR15 I -1;
			Stop;

		Autumn.2:
			TR15 J -1;
			Stop;

		Winter.1:
			TD15 M -1;
			Stop;

		Winter.2:
			TD15 N -1;
			Stop;

		Death.Spring.1:
			TD15 A -1;
			Stop;

		Death.Spring.2:
			TD15 B -1;
			Stop;

		Death.Summer.1:
			TD15 E -1;
			Stop;

		Death.Summer.2:
			TD15 F -1;
			Stop;

		Death.Autumn.1:
			TD15 I -1;
			Stop;

		Death.Autumn.2:
			TD15 J -1;
			Stop;

		Death.Winter.1:
			TD15 M -1;
			Stop;

		Death.Winter.2:
			TD15 N -1;
			Stop;
	}
}