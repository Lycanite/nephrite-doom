class SchismCreatureSpawnDirectory : Thinker {
    Map<string, string> roleMappings;
    Map<string, CreatureSpawnRole> roles;
    Map<string, CreatureSpawnEntry> lastSpawnEntries;

    static SchismCreatureSpawnDirectory Get() {
		ThinkerIterator it = ThinkerIterator.Create("SchismCreatureSpawnDirectory", STAT_STATIC);
		SchismCreatureSpawnDirectory thinker = SchismCreatureSpawnDirectory(it.Next());
		if (!thinker) {
			thinker = New("SchismCreatureSpawnDirectory").Init();
		}
		return thinker;
	}

    SchismCreatureSpawnDirectory Init() {
		ChangeStatNum(STAT_STATIC);
		return self;
	}

    /**
     * Adds an actor class to a spawn role creating a new spawn entry.
     * @param creatureClass The actor class to add.
     * @param roleName The name of the role that the actor should spawn under.
     * @param collection The collection that the actor belongs to for cvar settings (doom, heretic, hexen, schism, etc).
     * @param replaceable If true, the actor will also become replaceable by the role (added to role mappings).
     * @param groupSize The number of actors to spawn, defaults to 1, additional actors are spawned on top with random velocity to spread them out.
     */
    virtual void AddSpawn(Class<Actor> creatureClass, string roleName, string collection, bool replaceable = false, int groupSize = 1) {
        // Get or Create Role:
        CreatureSpawnRole role = self.roles.Get(roleName);
        if (!role) {
            role = New("CreatureSpawnRole").Init();
            role.name = roleName;
            self.roles.Insert(roleName, role);
        }

        // Create and Add Entry:
        CreatureSpawnEntry entry = New("CreatureSpawnEntry").Init();
        entry.creatureClass = creatureClass;
        entry.collection = collection;
        entry.groupSize = groupSize;
        Cvar spawnerWeightCvar = Cvar.FindCvar("schism_spawners_" .. entry.collection);
        entry.weight = spawnerWeightCvar.GetInt();
        role.AddEntry(entry);

        // Add to Role as Replaceable:
        if (replaceable) {
            self.AddRoleMapping(creatureClass, roleName);
        }
    }

    /**
     * Sets the replacement role of the provided actor class. Call this outside of AddSpawn to only replace a creature instead of adding spawns alongside it.
     * @param creatureClass The class of the creature to assign a role to.
     */
    virtual void AddRoleMapping(Class<Actor> creatureClass, string roleName) {
        self.roleMappings.Insert(creatureClass.GetClassName(), roleName);
    }

    /**
     * Sets the replacement role of the provided actor class name. Call this outside of AddSpawn to only replace a creature instead of adding spawns alongside it.
     * @param creatureClassName The class name of the creature to assign a role to.
     */
    virtual void AddRoleMappingName(string creatureClassName, string roleName) {
        self.roleMappings.Insert(creatureClassName, roleName);
    }

    /**
     * Called when an actor is spawned into the world.
     * @param actor The newly spawned actor.
     */
    virtual void OnActorSpawn(Actor actor) {
        // Get Spawn Entry:
        if (!actor.bIsMonster) {
            return;
        }
        CreatureSpawnEntry spawnEntry = self.lastSpawnEntries.GetIfExists(actor.GetClass() .. "");
        self.lastSpawnEntries.Remove(actor.GetClass() .. "");
        if (!spawnEntry) {
            return;
        }

        // Spawn Group:
        for (int i = 1; i < spawnEntry.groupSize; i++) {
            bool spawned = false;
            Actor spawnedActor = null;
            [spawned, spawnedActor] = actor.A_SpawnItem(spawnEntry.creatureClass, 0, actor.height * (i - 1), false, true);
            if (!spawnedActor) {
                [spawned, spawnedActor] = actor.A_SpawnItem(spawnEntry.creatureClass, actor.radius * 2 * (i - 1), 0, false, true);
            }
            if (!spawnedActor) {
                [spawned, spawnedActor] = actor.A_SpawnItem(spawnEntry.creatureClass, -actor.radius * 2 * (i - 1), 0, false, true);
            }
            if (spawnedActor) {
                spawnedActor.Thrust(5, Random(0, 359));
            }
        }
    }

    /**
     * Gets a potential replacement actor class for the provided original class.
     * @param original The actor class to look for a replacement for.
     */
    virtual Class<Actor> GetReplacement(Class<Actor> original) {
        // Check Class:
        let defaults = GetDefaultByType(original);
        let creatureSpawnerDefaults = CreatureSpawner(defaults);
        if (!defaults.bIsMonster && !creatureSpawnerDefaults) {
            return null;
        }

        // Role Name:
        string roleName = self.roleMappings.GetIfExists(original.GetClassName());
        if (creatureSpawnerDefaults) {
            roleName = creatureSpawnerDefaults.spawnRole;
        }
        if (!roleName) {
            return null;
        }

        // Spawn Role:
        CreatureSpawnRole role = self.roles.GetIfExists(roleName);
        if (!role || !role.entries.Size()) {
            return null;
        }

        CreatureSpawnEntry spawnEntry = null;

        // Single Entry:
        if (role.entries.Size() == 1) {
            if (role.entries[0].weight <= 0) {
                return null;
            }
            spawnEntry = role.entries[0];
        }

        // Weighted Random Entry:
        else {
            int weightTarget = Random(1, role.weightMax);
            int weightCovered = 0;
            for (int i = 0; i < role.entries.Size(); i++) {
                if (role.entries[i].weight <= 0) {
                    continue;
                }
                int weightCheck = role.entries[i].weight + weightCovered;
                if (weightCheck >= weightTarget) {
                    spawnEntry = role.entries[i];
                    break;
                }
                weightCovered += role.entries[i].weight;
            }
            if (!spawnEntry) {
                console.printf("ERROR: Failed to get a creature to spawn within weight!");
            }
        }
        
        // Return Creature Class:
        if (!spawnEntry) {
            return null;
        }
        self.lastSpawnEntries.Insert(spawnEntry.creatureClass .. "", spawnEntry);
        return spawnEntry.creatureClass;
    }
}

class CreatureSpawnRole : Thinker {
    string name;
    Array<CreatureSpawnEntry> entries;
    int weightMax;

    CreatureSpawnRole Init() {
		ChangeStatNum(STAT_STATIC);
		return self;
	}

	virtual void AddEntry(CreatureSpawnEntry entry) {
        self.entries.Push(entry);
        if (entry.weight > 0) {
            self.weightMax += entry.weight;
        }
        // console.printf("Added creature spawn entry: " .. entry.creatureClass.GetClassName() .. " " .. self.name .. " " .. entry.collection .. " " .. entry.weight);
    }
}

class CreatureSpawnEntry : Thinker {
    Class<Actor> creatureClass;
    string collection;
    int groupSize;
    int weight; // The spawn weight of this entry, larger weights are more likely to be picked. 0- weights are disabled entirely.

    CreatureSpawnEntry Init() {
		ChangeStatNum(STAT_STATIC);
		return self;
	}
}