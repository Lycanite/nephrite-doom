class SchismTreeSycamore : SchismTree {
	Default {
		Tag "Sycamore Tree";
		SchismTree.SpringVariants 2;
		SchismTree.SummerVariants 2;
		SchismTree.AutumnVariants 2;
		SchismTree.WinterVariants 2;
	}
	
	States {
		Spring.1:
			TR12 A -1;
			Stop;

		Spring.2:
			TR12 B -1;
			Stop;

		Summer.1:
			TR12 E -1;
			Stop;

		Summer.2:
			TR12 F -1;
			Stop;

		Autumn.1:
			TR12 I -1;
			Stop;

		Autumn.2:
			TR12 J -1;
			Stop;

		Winter.1:
			TD12 M -1;
			Stop;

		Winter.2:
			TD12 N -1;
			Stop;

		Death.Spring.1:
			TD12 A -1;
			Stop;

		Death.Spring.2:
			TD12 B -1;
			Stop;

		Death.Summer.1:
			TD12 E -1;
			Stop;

		Death.Summer.2:
			TD12 F -1;
			Stop;

		Death.Autumn.1:
			TD12 I -1;
			Stop;

		Death.Autumn.2:
			TD12 J -1;
			Stop;

		Death.Winter.1:
			TD12 M -1;
			Stop;

		Death.Winter.2:
			TD12 N -1;
			Stop;
	}
}