class ToxicRocketProjectile : SchismProjectile {
	Default {
		Radius 11;
		Height 8;
		Scale 1;
		Speed 20;
		FastSpeed 25;
		Damage 15;
		PoisonDamage 30;
		Alpha 1;
		RenderStyle "Normal";
		
		DamageType "Poison";
		
		SeeSound "Projectile/ToxicRocket/Fire";
		DeathSound "Projectile/ToxicRocket/Explode";
		Decal "CacoDecal";

		SchismProjectile.ParticleName "Poison";
		SchismProjectile.ParticleCount 1;

		DontHurtShooter;
	}
	
	States {
		Spawn:
			NPTR A 2 bright;
			Loop;
			
		Death:
			NPTR A 1 bright A_SetRenderStyle(1, STYLE_ADD);
			NPTR B 1 bright A_Explode(60, 128, 0);
			NPTR CDEFGHIJKLMNOPQRSTU 1 bright;
			Stop;
	}
}