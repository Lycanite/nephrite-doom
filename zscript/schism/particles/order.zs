struct SchismOrderParticle {
	static void apply(out FSpawnParticleParams particleParams) {
		particleParams.texture = TexMan.CheckForTexture("particles/order/Order01.png");
		particleParams.style = STYLE_Add;
		particleParams.flags = SPF_ROLL|SPF_FULLBRIGHT;

		particleParams.accel = (0, 0, 0);
		particleParams.startRoll = 0;
		particleParams.rollVel = 0;
		particleParams.rollAcc = 0;

		particleParams.size = 16.0;
		particleParams.sizeStep = 0.5;
		particleParams.startAlpha = 0.8;
		particleParams.fadeStep = particleParams.startAlpha / particleParams.lifetime;
	}
}