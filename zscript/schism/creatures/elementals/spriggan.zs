class Spriggan : MinionCreature {
	Default {
		Tag "Spriggan";
		Species "Spriggan";
		
		Health 100;
		Radius 10;
		Height 50;
		Speed 15;
		FloatSpeed 15;
		Scale 0.5;
		Gravity 0;
		PainChance 120;
		Damage 1;
		Alpha 1;
		BloodColor "00 aa 00";
		MaxTargetRange 500;

		DamageFactor "Arbour", 0.1;
		
		SeeSound "Spriggan/See";
		PainSound "Spriggan/Pain";
		DeathSound "Spriggan/Death";
		ActiveSound "Spriggan/Idle";
		MeleeSound "Spriggan/Melee";
		Obituary "%o was leeched by a Spriggan!";
		HitObituary "%o was leeched by a Spriggan!";
		
		+FLOAT;
		+MTHRUSPECIES;
		+NOINFIGHTSPECIES;
		+Creature.HOVERBOB;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		self.SpawnParticle("Arbour", 8, 1, (FRandom(-8, 8), FRandom(-8, 8), FRandom(10, 40)), Random(45, 315), Random(-10, 10), 0.25);
		self.SpawnParticle("Leaf", 2 * 8, 1, (FRandom(-8, 8), FRandom(-8, 8), FRandom(10, 40)), Random(45, 315), Random(-10, 10), 0.5, 0x00AA00);
	}
	
	action void A_Burst() {
		for (int i = 0; i < 20; i += 20) {
			invoker.SpawnParticle("Arbour", 8, 3, (FRandom(-6, 6), FRandom(-6, 6), FRandom(10, 40)), i, Random(-10, 10));
			invoker.SpawnParticle("Leaf", 2 * 8, 4, (FRandom(-6, 6), FRandom(-6, 6), FRandom(10, 40)), i, Random(-10, 10), 1.0, 0x00AA00);
		}
	}

	action void A_LifeleechAttack(int damage) {
		A_StartSound("Projectile/Lifeleech/Fire", CHAN_WEAPON, CHANF_OVERLAP);
		A_CustomRailgun(damage, 0, "00 aa 00", "", RGF_SILENT|RGF_NOPIERCING, 1, Random(-8.0, 8.0), "LifeleechPuff", 0, 0, 5000, TICRATE, 1.0, 0.5, "None", invoker.height * 0.25, 270, 0, 3);
	}
	
	States {
		Spawn:
			SPIG AABBCCBB 4 {
				A_Look();
				return A_StayWithMaster();
			}
			Loop;
			
		Follow:
			SPIG DEFE 4 {
				A_Look();
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			SPIG DEFE 4 {
				return A_MinionChase();
			}
			Loop;
		
		Missile:
			SPIG H 20 A_FaceTarget();
			SPIG I 4 A_LifeleechAttack(0);
			SPIG J 4 A_LifeleechAttack(Random(0, 1));
			SPIG H 0 A_MonsterRefire(10, "See");
			Goto Missile + 1;
			
		Pain:
			SPIG K 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			SPIG L 5;
			SPIG M 5 A_Scream();
			SPIG N 5;
			SPIG O 5 A_CreatureDie();
			SPIG PQRSTUV 5;
			SPIG W 5 A_Burst();
		Fade:
			SPIG X 1 A_FadeOut(0.1);
			Loop;
	}
}

class LifeleechPuff : BulletPuff {
	Default {
		+MTHRUSPECIES;
	}

	States {
		Spawn:
			TNT1 A 4;
			Stop;
		Melee:
			TNT1 A 4;
			Stop;
	}
}