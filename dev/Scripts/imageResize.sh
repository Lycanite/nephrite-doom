#!/bin/bash
set -e

for file in *.png; do
    convert $file -resize $1 $file
done
