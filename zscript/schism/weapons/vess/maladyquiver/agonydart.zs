class AgonyDartScroll : WeaponPassive {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Agony Darts have been added to your Malady Quiver.";
		
		Inventory.Icon "VESWN0";
		Inventory.PickupSound "MaladyQuiver/Pickup";
		Inventory.RestrictedTo "Vess";
		WeaponUpgrade.ExtraAmmoClass "ToxicDrupe";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			VESW N 1 bright;
			Loop;
	}
}

extend class MaladyQuiver {
	action state A_AgonyDart() {
		if (!A_CheckSpecial(1) || !A_SpendAmmo(3)) {
			return ResolveState("Ready");
		}
        A_FireProjectile("AgonyDartProjectile", 0, false);
		return null;
	}

    States {
		MainHand.User1:
			VSH5 K 4 {
				A_ClearBobbing();
				A_Light2();
			}
			VSH5 L 4;
			VSH5 M 4 A_AgonyDart();
			VSH5 N 4;
			VSH5 OPQL 4;
			VSH5 C 0 A_MirrorHands();
			VSH5 C 0 A_Light0();
			Goto Ready;

		OffHand.User1:
			VSH5 ABCDEFGH 2;
			Loop;
    }
}

class AgonyDartProjectile : MaladyDartProjectile {
	Default {
        Scale 1;
		Radius 2;
		Height 2;
		Speed 50;
		FastSpeed 50;

		SchismProjectile.ParticleName "Anima";
		SchismProjectile.ParticleColor 0xF7FF0000;
		SchismProjectile.StatusEffectClass "SchismAstonishDebuff";
		SchismProjectile.StatusEffectDuration 10 * 35;
		SchismProjectile.StatusEffectStrength 1;
		
		SeeSound "MaladyQuiver/Fire/AgonyDart";
		DeathSound "MaladyQuiver/Explode/AgonyDart";
	}
	
	override SchismStatusEffect InflictEffect(Class<Inventory> effectName, Actor targetActor, int effectDuration, Class<SchismStatusEffect> effectSpread, int effectStrength) {
		super.InflictEffect(effectName, targetActor, effectDuration, effectSpread, effectStrength);
        SchismStatusEffect effect = SchismStatusEffect(SchismStatusEffects.InflictEffect("SchismWeaknessDebuff", self.target, self, targetActor, effectDuration, effectSpread, 2));
        if (effect) {
            effect.effectDamage = 1;
            effect.effectDamageType = "Nether";
            effect.particleName = "Nether";
        }
		return effect;
	}
	
	States {
		Spawn:
			VMQ1 ABCD 4;
			Loop;
			
		Death:
			VMQ1 EFGHIJKL 4;
			Stop;
	}
}