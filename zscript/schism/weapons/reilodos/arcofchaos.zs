class ArcOfChaos : MWeapLightning {
	Default {
		Weapon.SlotNumber 4;
		Inventory.PickupMessage "The Arc of Death spell imbued with Chaos!";
		Inventory.PickupSound "Reilodos/Taunt";
		Inventory.RestrictedTo "Reilodos", "ReilodosHavocSerpent";
		
		+INVENTORY.RESTRICTABSOLUTELY;
	}
	
	States {
		Ready:
			RELG AAAAA 1 Bright A_WeaponReady();
			RELG A 1 Bright A_LightningReady();
			RELG BBBBBB 1 Bright A_WeaponReady();
			RELG CCCCC 1 Bright A_WeaponReady();
			RELG C 1 Bright A_LightningReady();
			RELG BBBBBB 1 Bright A_WeaponReady();
			Loop;
			
		Deselect:
			RELG A 1 bright A_Lower(12);
			Loop;
			
		Select:
			RELG A 1 bright A_Raise(12);
			Loop;
			
		Fire:
			RELG DE 3 Bright;
			RELG F 4 Bright A_MLightningAttack("ArcOfChaosFloor", "ArcOfChaosCeiling");
			RELG G 4 Bright;
			RELG HI 3 Bright;
			RELG I 6 Bright Offset(0, 199);
			RELG C 2 Bright Offset(0, 55);
			RELG B 2 Bright Offset(0, 50);
			RELG B 2 Bright Offset(0, 45);
			RELG B 2 Bright Offset(0, 40);
			Goto Ready;
			
		Spawn:
			WMLG ABCDEFGH 4 bright;
			Loop;
	}
}


class ArcOfChaosCeiling : LightningCeiling {
	Default {
		MissileType "ArcOfChaosZap";
		Obituary "%o was struck by %k's Arc of Chaos!";
	}
	
	States {
		Spawn:
			AOC1 A 2 Bright A_LightningZap;
			AOC1 BCD 2 Bright A_LightningClip;
			Loop;
		Death:
			AOC2 A 2 Bright A_LightningRemove;
			AOC2 BCDEKLM 3 Bright;
			AOC2 N 35;
			AOC2 NO 3 Bright;
			AOC2 P 4 Bright;
			AOC2 QP 3 Bright;
			AOC2 Q 4 Bright;
			AOC2 P 3 Bright;
			AOC2 O 3 Bright;
			AOC2 P 3 Bright;
			AOC2 P 1 Bright A_HideThing;
			AOC2 N 1050;
			Stop;
	}
}


class ArcOfChaosFloor : LightningFloor {
	Default {
		MissileType "ArcOfChaosZap";
	}
	
	void A_LastZap() {
		Class<Actor> lightning = MissileName;
		if (lightning == NULL) lightning = "LightningZap";
		
		Actor mo = Spawn(lightning, self.Pos, ALLOW_REPLACE);
		if (mo)
		{
			mo.SetStateLabel("Death");
			mo.Vel.Z = 40;
			mo.SetDamage(0);
		}
	}
	
	States {
		Spawn:
			AOC1 E 2 Bright A_LightningZap;
			AOC1 FGH 2 Bright A_LightningClip;
			Loop;
		Death:
			AOC2 F 2 Bright A_LightningRemove;
			AOC2 GHIJKLM 3 Bright;
			AOC2 N 20;
			AOC2 NO 3 Bright;
			AOC2 P 4 Bright;
			AOC2 QP 3 Bright;
			AOC2 Q 4 Bright A_LastZap;
			AOC2 POP 3 Bright;
			AOC2 P 1 Bright A_HideThing;
			AOC2 N 1050;
			Stop;
	}
}


class ArcOfChaosZap : LightningZap {
	Default {
		Obituary "%o was struck by %k's Arc of Chaos!";
	}
	
	States {
		Spawn:
			AOC1 IJKLM 2 Bright A_ZapMimic;
			Loop;
		Death:
			AOC1 NOPQRSTU 2 Bright;
			Stop;
	}
}
