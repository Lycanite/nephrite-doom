class Sidhual : Hieromancer {
	Default {
		Tag "Sidhual";
		Health 5000;
		Radius 20;
		Height 120;
		Mass 100;
		Speed 8;
		FloatSpeed 4;
		Scale 1.5;
		Gravity 1;
		PainChance 10;
		DamageFunction 2;
		BloodColor "ff 00 00";

		+BOSS;
		+MISSILEMORE;
		
		SeeSound "Mirdun/See";
		PainSound "Mirdun/Pain";
		DeathSound "Mirdun/Death";
		ActiveSound "Mirdun/See";
		MeleeSound "Mirdun/Melee";
		Obituary "%o was put to justice by Sidhual!";
		HitObituary "%o was put to justice by Sidhual!";
	}
	
	States {
		Spawn:
			SIDH MN 8 A_Look();
			Loop;
			
		See:
			SIDH MNOP 4 A_Chase("Melee", "Missile");
			Loop;
			
		Melee:
		Missile:
			SIDH RS 4 A_FaceTarget();
			SIDH T 4 bright {
				if (Random(0, 1)) {
					A_SpawnProjectile("SidhualHieroboltProjectile", 92);
					A_SpawnProjectile("SidhualHieroboltProjectile", 64, 10);
					A_SpawnProjectile("SidhualHieroboltProjectile", 64, -10);
				} else {
					A_SpawnProjectile("WindProjectile", 64);
					A_SpawnProjectile("WindProjectile", 32, 10);
					A_SpawnProjectile("WindProjectile", 32, -10);
				}
			}
			SIDH T 4 bright {
				if (Random(0, 1)) {
					A_SpawnProjectile("SidhualHieroboltProjectile", 64);
					A_SpawnProjectile("SidhualHieroboltProjectile", 92, 10);
					A_SpawnProjectile("SidhualHieroboltProjectile", 92, -10);
				} else {
					A_SpawnProjectile("WindProjectile", 32);
					A_SpawnProjectile("WindProjectile", 64, 10);
					A_SpawnProjectile("WindProjectile", 64, -10);
				}
			}
			SIDH R 4;
			Goto See;
			
		Pain:
			SIDH Q 3;
			SIDH Q 3 A_Pain();
			SIDH T 4 bright {
				for (int i = 0; i < 8; i++) {
					A_SpawnProjectile("SidhualHieroboltProjectile", 92, 0, i * 45);
					A_SpawnProjectile("WindProjectile", 64, 0, i * 45);
				}
			}
			SIDH R 4;
			Goto See;
			
		Death:
		XDeath:
			SIDH G 5;
			SIDH H 5 A_Scream();
			SIDH I 5;
			SIDH J 5 A_NoBlocking();
			SIDH KL 5;
			Stop;
			
		Raise:
			SIDH ABCDEFG 5;
			Goto See;
	}
}

class SidhualHieroboltProjectile : HieroboltProjectile {
	Default {
		Speed 5;
		Scale 4;
	}
}