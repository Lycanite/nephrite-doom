class SchismAstonishDebuff : SchismStatusEffect {
	
	Default {
		PowerUp.Duration -10;
		
		SchismStatusEffect.Detrimental true;
		SchismStatusEffect.ParticleName "Nova";
		SchismStatusEffect.ParticleCount 4;
		SchismStatusEffect.ParticleSpeed 2;
		SchismStatusEffect.ParticleDuration TICRATE;
	}
	
	override double GetSpeedFactor() {
		// Periodically Stop Movement:
		return self.EffectInPhase() ? 0 : 1;
	}
	
	override void DoEffect() {
		super.DoEffect();
		
		if (!self.owner || self.owner.Health <= 0 || self.owner.bCorpse || self.owner.bBoss) {
			return;
		}
		
		// Periodic Pain State:
		if (self.EffectInPhase() && !SchismUtil.ActorInState(self.owner, "Pain")) {
			self.owner.SetStateLabel("Pain");
		}
	}

	virtual bool EffectInPhase() {
		return self.effectLifetime % TICRATE == 0;
	}
}
