class Malphas : Creature {	
	Default {
		Tag "Malphas";
		Health 100;
		Radius 31;
		Height 56;
		Mass 400;
		Speed 20;
		PainChance 120;
		
		SeeSound "Malphas/See";
		ActiveSound "Malphas/Idle";
		PainSound "Malphas/Pain";
		DeathSound "Malphas/Death";
		MeleeSound "Malphas/Melee";
		Obituary "%o was snatched by a naughty Malphas.";
		HitObituary "%o was snatched by a naughty Malphas.";
		
		+FLOAT;
		+NOGRAVITY;
		+CREATURE.HOVERBOB;
	}
	
	States {
		Spawn:
			MALP ABCD 10 A_Look();
			Loop;
			
		See:
			MALP ABC 3 A_Chase();
			MALP D 3 {
				A_Chase();
				A_StartSound("Malphas/Fly");
			}
			Loop;
			
		Melee:
			MALP E 5 A_FaceTarget();
			MALP F 5 bright A_CreatureMelee(20, 40, "Air", "Air");
			Goto See;
			
		Pain:
			MALP G 6 A_Pain();
			Goto See;
			
		Death:
			MALP G 8 ;
			MALP H 8 A_Scream();
			MALP IJ 8;
			MALP K 8 A_CreatureDie();
			MALP LM 8;
			MALP N -1;
			Stop;
	}
}