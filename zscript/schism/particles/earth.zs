struct SchismEarthParticle {
	static void apply(out FSpawnParticleParams particleParams) {
		particleParams.texture = TexMan.CheckForTexture("particles/earth/Earth01.png");
		particleParams.style = STYLE_Translucent;
		particleParams.flags = SPF_ROLL;

		particleParams.accel = (0, 0, 0);
		particleParams.startRoll = Random(0, 359);
		particleParams.rollVel = Random(-20, 20);
		particleParams.rollAcc = 0;

		particleParams.size = 16.0;
		particleParams.sizeStep = 1;
		particleParams.startAlpha = 1;
		particleParams.fadeStep = particleParams.startAlpha / particleParams.lifetime;
	}
}