class StunDartScroll : WeaponPassive {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Stun Darts have been added to your Malady Quiver.";
		
		Inventory.Icon "VESWO0";
		Inventory.PickupSound "MaladyQuiver/Pickup";
		Inventory.RestrictedTo "Vess";
		WeaponUpgrade.ExtraAmmoClass "ToxicDrupe";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			VESW O 1 bright;
			Loop;
	}
}

extend class MaladyQuiver {
	action state A_StunDart() {
		if (!A_CheckSpecial(2) || !A_SpendAmmo(4)) {
			return ResolveState("Ready");
		}
        A_FireProjectile("StunDartProjectile", 0, false);
		return null;
	}

    States {
		MainHand.User2:
			VSH5 K 4 {
				A_ClearBobbing();
				A_Light2();
			}
			VSH5 L 4;
			VSH5 M 4 A_StunDart();
			VSH5 N 4;
			VSH5 OPQL 4;
			VSH5 C 0 A_MirrorHands();
			VSH5 C 0 A_Light0();
			Goto Ready;

		OffHand.User2:
			VSH5 ABCDEFGH 2;
			Loop;
    }
}

class StunDartProjectile : MaladyDartProjectile {
	Default {
        Scale 1;
		Radius 2;
		Height 2;
		Speed 50;
		FastSpeed 50;
		
		SeeSound "MaladyQuiver/Fire/StunDart";
		DeathSound "MaladyQuiver/Explode/StunDart";

		SchismProjectile.ParticleName "Anima";
		SchismProjectile.ParticleColor 0xF7DDFF00;
		SchismProjectile.StatusEffectClass "SchismStunDebuff";
		SchismProjectile.StatusEffectDuration 10 * 35;
		SchismProjectile.StatusEffectStrength 1;
	}
	
	States {
		Spawn:
			VMQ2 ABCD 4;
			Loop;
			
		Death:
			VMQ2 EFGHIJKL 4;
			Stop;
	}
}