class GlacialNodachiProjectile : SchismProjectile {
	bool inflictChill;
	
	Default {
		Radius 16;
		Height 10;
		Scale 1;
		Speed 12;
		DamageFunction 6;
		Gravity 0;
		Alpha 0.8;
		Renderstyle "Add";
		
		DamageType "Ice";
		
		SeeSound "GlacialNodachi/Projectile";
		DeathSound "GlacialNodachi/Projectile/Explode";
		
		-THRUGHOST;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.inflictChill = true;
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		if (specialDamage > 0 && self.inflictChill)
		{
			InflictEffect("SchismChillDebuff", targetActor, 5 * 35);
		}
		return specialDamage;
	}
	
	override void SpawnParticles() {
		if (Random(0, 3)) {
			self.SpawnParticle("Frost", TICRATE, 1, (Random(-40, 40), 0, Random(-4, 4)), Random(0, 359), Random(-45, 45), 0.25);
		} else {
			self.SpawnParticle("Snowflake", 8, 3, (Random(-40, 40), 0, Random(-4, 4)), Random(0, 359), Random(-45, 45));
		}
	}
	
	States {
		Spawn:
			GNP1 ABCDEFABCDEF 2;
			
		Death:
			GNP1 A 2 {
				SchismAttacks.Explode(invoker, invoker.target, 4, 100, 0, 1, false, false, "Ice");
			}
			GNP1 BCDE 2 A_FadeOut(0.2);
			Stop;
	}
}

class GlacialNodachiFrigidProjectile : GlacialNodachiProjectile {
	Default {
		Scale 1;
		Speed 30;
		DamageFunction 5;
		
		SeeSound "GlacialNodachi/Projectile/Frigid";
		DeathSound "GlacialNodachi/Projectile/Frigid/Explode";
		
		+RIPPER;
	}
	
	override void SpawnParticles() {
		for (int i = 0; i < 2; i++) {
			self.SpawnParticle("Frost", TICRATE, 1, (FRandom(-40, 40), 0, FRandom(-4, 4)), Random(0, 359), Random(-45, 45), 0.5);
			self.SpawnParticle("Snowflake", 2 * TICRATE, 3, (FRandom(-40, 40), 0, FRandom(-4, 4)), Random(0, 359), Random(-45, 45));
		}
	}
	
	States {
		Spawn:
			GNP3 DEFGHIJKLMN 4;
			Stop;
			
		Death:
			Stop;
	}
}