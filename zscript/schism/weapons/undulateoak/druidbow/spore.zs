class DruidBowSporeArrow : DruidBowPoisonArrow {	
	Default {
		SeeSound "DruidBow/Spore/Fire";
		DeathSound "DruidBow/Spore/Explode";
	}
	
	override void BeginPlay() {
		super.BeginPlay();
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		
		if (specialDamage > 0 && SchismInteraction.CanHarmTarget(self.target, targetActor)) {
			InflictEffect("SchismStunDebuff", targetActor, 3 * 35);
		}
		
		return specialDamage;
	}
	
	States {
		Spawn:
			DBWS A 6;
			Loop;
			
		Death:
			DBWS CDEFG 4;
			Stop;
	}
}

class DruidBowSporeArrowFar : DruidBowSporeArrow {
	Default {
		Speed 25;
		Gravity 0.1;
		
		SeeSound "DruidBow/Spore/FireFar";
	}
}

class DruidBowSporeArrowCharged : DruidBowSporeArrowFar {	
	int poisonDurarionTics;
	int poisonIntervalTics;
	int poisonDamageAmount;
	int ticksUntilPoison;
	
	Default {		
		SeeSound "DruidBow/Spore/FireCharged";
		DeathSound "DruidBow/Spore/Explode";
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		
		self.poisonDurarionTics = 350;
		self.poisonIntervalTics = 10;
		self.poisonDamageAmount = 10;
		self.ticksUntilPoison = 10;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		if (!self.detonated) {
			PoisonEffects(128);
		}
		else {
			PoisonEffects();
			if (--ticksUntilPoison == 0) {
				ticksUntilPoison = poisonIntervalTics;
				PoisonGas();
			}
		}
	}
	
	virtual void PoisonEffects(int effectSize = 255) {
		int offsetMaxX = 0;
		int offsetMaxY = 0;
		int offsetMaxZ = 0;
		if (effectSize == 255) {
			offsetMaxX = random(-100, 100);
			offsetMaxY = random(-100, 100);
			offsetMaxZ = random(-100, 100);
		}
		self.SpawnParticle("Poison", TICRATE, 1, (Random(-4, 4), 0, Random(-4, 4)), Random(0, 359), Random(-45, 45));
		self.SpawnParticle("PoisonBubble", 2 * TICRATE, 1, (Random(-4, 4), 0, Random(-4, 4)), Random(0, 359), Random(-45, 45));
	}
	
	void PoisonGas() {
		BlockThingsIterator iter = BlockThingsIterator.Create(self, 100, false);
		while (iter.Next()) {
			Actor effectTarget = iter.thing;
			if (CheckSight(effectTarget) && SchismInteraction.CanHarmTarget(self.target, effectTarget)) {
				if (effectTarget.PoisonDurationReceived < poisonDurarionTics && effectTarget.PoisonDamageReceived <= poisonDamageAmount) {
					effectTarget.PoisonMobj(target, self, poisonDamageAmount, poisonDurarionTics, poisonIntervalTics, "poison");
				}
				if (!effectTarget.bBoss && !effectTarget.bFrightened) {
					InflictEffect("SchismStunDebuff", effectTarget, 3 * 35);
				}
			}
		}
	}
	
	States {
		Spawn:
			DBWS B 2 A_SpawnItemEx("DruidBowSporeArrowTrail",random(-2,-8),random(8,-8),random(8,-8),0,0,frandom(0,0.6),0,SXF_NOCHECKPOSITION|SXF_CLIENTSIDE);
			Loop;
			
		Death:
			DBWS M 4 {
				ectoplasmMin = 0;
				detonated = true;
			}
			DBWS NOPQR 4;
			TNT1 A 350;
			Stop;
	}
}

class DruidBowSporeArrowTrail : Actor  {
	Default  {
		Scale 4;
		RenderStyle "Add";
		Alpha 0.5;
		
		+NOINTERACTION
		+FORCEXYBILLBOARD
	}
	
	States {
		Spawn:
			DBWS H 0 ThrustThingZ(0, Random(5, 10), 0, 0);
			DBWS H 0 ThrustThing(Random(0, 255), Random(0, 10), 0, 0);
		Active:
			DBWS HIJKL 1 A_FadeOut(0.05);
			Loop;
	}
}