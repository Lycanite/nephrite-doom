uniform float timer;
vec4 Process(vec4 color)
{
    float intensity = 0.09;
    vec2 rippleCoord = gl_TexCoord[0].st;
	vec2 p = -1. + 2. * rippleCoord.xy - vec2(0, 1 * .001);
    float cLength = length(p);
    vec2 ripple = rippleCoord.xy / -1. +(p / cLength) * cos(cLength * 15.0 - timer * 8.0) * intensity;
    rippleCoord += ripple;

    vec2 warpCoord = gl_TexCoord[0].st;
    const float pi = 3.14159265358979323846;
    vec2 warp = vec2(0, 0);
    warp.y = sin(pi * 2.0 * (warpCoord.x + timer * 0.125)) * 0.1;
    warp.x = sin(pi * 2.0 * (warpCoord.y + timer * 0.125)) * 0.1;
    warpCoord += warp;

    return (getTexel(warpCoord) * (color * 0.5)) + (getTexel(warpCoord) * (color * 0.25)) + (getTexel(gl_TexCoord[0].st) * color);
}