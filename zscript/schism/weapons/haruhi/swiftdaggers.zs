class SwiftDaggers : DualHand {
	int primaryCostOffset;
	
	Default {
		Radius 20;
		Height 16;
		Scale 0.5;
		
		Inventory.RestrictedTo "Haruhi";
		
		Inventory.PickupSound "SwiftDaggers/Pickup";
		Weapon.UpSound "SwiftDaggers/Up";
		Weapon.ReadySound "";
		SchismWeapon.FailSound "SwiftDaggers/Fire/Fail";
		
		Weapon.AmmoType1 "WhirlwindCharge";
		Weapon.AmmoType2 "PranaEnergy";
		
		SchismWeapon.PrimaryCost 1;
		SchismWeapon.SecondaryCost 1;
		SchismWeapon.SpecialACost 10;
		SchismWeapon.SpecialBCost 25;
		SchismWeapon.SpecialCCost 25;
		SchismWeapon.SpecialDCost 50;
		
		SchismWeapon.SpecialItemA "EmpyrealSash";
		SchismWeapon.SpecialItemB "HarpieSash";
		SchismWeapon.SpecialItemC "QuetzSash";
		SchismWeapon.SpecialItemD "TornadoSash";
		SchismWeapon.PassiveItem "EmpyrealSash";
		// SchismWeapon.PassiveItem "WindingWindsSash";
		
		Inventory.PickupMessage "The empyrean Swift Daggers are yours to weild!";
		Obituary "%o was sliced and diced by %k's Swift Daggers!";
		SchismWeapon.InsufficientAmmoAMessage "\caNot enough Whirlwind Charges to use this ability.";
		SchismWeapon.InsufficientAmmoBMessage "\caNot enough Prana Energy to use this ability.";
		SchismWeapon.NoSpecialAMessage "\caYou need an Empyreal Sash to use this ability.";
		SchismWeapon.NoSpecialBMessage "\caYou need a Harpie Sash to use this ability.";
		SchismWeapon.NoSpecialCMessage "\caYou need a Quetz Sash to use this ability.";
		SchismWeapon.NoSpecialDMessage "\caYou need a Tornado Sash to use this ability.";

		SchismWeapon.IdleBob 30;

		+WEAPON.AMMO_OPTIONAL;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		mirrorOffHand = true;
		primaryCostOffset = 0;
	}
	
	
	// Primary Air Slash:
	action void A_AirSlash() {
		if (A_CheckAmmo(1, 0, false)) {
			if (invoker.primaryCostOffset-- <= 0)
			{
				A_SpendAmmo(1);
				invoker.primaryCostOffset = 2;
			}
			A_FireProjectile("SwiftDaggersProjectile", 0, false);
			A_FireProjectile("SwiftDaggersProjectile", 15, false, -20);
			A_FireProjectile("SwiftDaggersProjectile", -15, false, 20);
		}
		int meleeDamage = 10;
		if (CountInv("PowerStrength"))
		{
			meleeDamage = 20;
		}
		A_CustomPunch(meleeDamage, true, CPF_PULLIN|CPF_NOTURN, "BulletPuff", 96, 0, 0, "ArmorBonus", "SwiftDaggers/Melee", "SwiftDaggers/Miss");
	}
	
	
	// Secondary Whirling Dervish:
	action void A_WhirlingDervish() {
		if (!A_SpendAmmo(2)) {
			return;
		}
		A_FireProjectile("SwiftDaggersProjectile", 0, false);
		A_FireProjectile("SwiftDaggersProjectile", 15, false, -20);
		A_FireProjectile("SwiftDaggersProjectile", -15, false, 20);
		A_FireProjectile("SwiftDaggersWhirlingProjectile", 0, false);
		A_GiveInventory("HaruhiAirTime");
		A_ChangeVelocity(25, 0, 5, CVF_RELATIVE|CVF_REPLACE);
	}
	
	
	// Special 1 Empyreal Rending:
	action void A_EmpyrealRending() {
		if (!A_CheckSpecial(1) || !A_SpendAmmo(3)) {
			return;
		}
		for (int i = 10; i < 360; i += 10) {
			A_FireProjectile("SwiftDaggersEmpyrealProjectile", i, false);
		}
		A_GiveInventory("HaruhiAirTime");
		A_ChangeVelocity(0, 0, 25, CVF_RELATIVE|CVF_REPLACE);
	}
	
	
	// Special 2 Harpie Strike:
	action void A_HarpieStrike() {
		if (!A_CheckSpecial(2) || !A_SpendAmmo(4)) {
			return;
		}
		Actor projectile = A_FireProjectile("SwiftDaggersHarpieProjectile", 0, false);
		A_TakeInventory("HaruhiAirTime");
		if (projectile)
		{
			A_ChangeVelocity(projectile.vel.x, projectile.vel.y, projectile.vel.z, CVF_REPLACE);
		}
		A_GiveInventory("SwiftDaggersHarpieStrike");
	}
	
	
	// Special 3 Quetz Wingbeat:
	action void A_QuetzWingbeat() {
		if (!A_CheckSpecial(3) || !A_SpendAmmo(5)) {
			return;
		}
		A_FireProjectile("SwiftDaggersQuetzProjectile", 0, false);
		A_FireProjectile("SwiftDaggersQuetzProjectile", 15, false, -20);
		A_FireProjectile("SwiftDaggersQuetzProjectile", -15, false, 20);
		A_GiveInventory("HaruhiAirTime");
		A_ChangeVelocity(-25, 0, 20, CVF_RELATIVE|CVF_REPLACE);
	}
	
	
	// Special 4 Tornado:
	action void A_Tornado() {
		if (!A_CheckSpecial(4) || !A_SpendAmmo(6)) {
			return;
		}
		A_FireProjectile("SwiftDaggersTornado", 0, false);
	}
	
	
	// Ultimate Grand Hurricane:
	action void A_GrandHurricane() {
		A_FireProjectile("SwiftDaggersTornado", 0, false); // TODO Replace with new Hurricane starter.
	}
	
	
	States {
		Spawn:
			SWDG Z 4 bright;
			Loop;
			
		// Main Hand:
		MainHand.Select:
			SWDG A 1 A_Raise(12);
			Loop;
			
		MainHand.Deselect:
			SWDG A 1 A_Lower(12);
			Loop;
			
		MainHand.Ready:
			SWDG A 0 A_Jump(128, "MainHand.Ready.B");
		MainHand.Ready.A:
			SWDG AABBCCBB 3 {
				return A_SchismWeaponReady();
			}
			Loop;
		MainHand.Ready.B:
			SWDG DDEEFFEE 3 {
				return A_SchismWeaponReady();
			}
			Loop;
			
		MainHand.Fire:
			SWDG A 0 A_ClearBobbing();
			SWDG A 0 A_Jump(128, "MainHand.Fire.B");
			SWDG G 2;
			SWDG H 2 A_AirSlash();
			SWDG I 2;
			Goto Ready;
		MainHand.Fire.B:
			SWDG A 0 A_Jump(128, "MainHand.Fire.C");
			SWDG J 2;
			SWDG K 2 A_AirSlash();
			SWDG L 2;
			Goto Ready;
		MainHand.Fire.C:
			SWDG M 2;
			SWDG N 2 A_AirSlash();
			SWDG O 2;
			Goto Ready;
			
		MainHand.AltFire:
			SWDG A 0 A_ClearBobbing();
			SWDG S 4;
			SWDG T 4 A_WhirlingDervish();
			SWDG U 4;
			Goto Ready;
		
		MainHand.User1:
			SWDG A 0 A_ClearBobbing();
			SWDG P 4;
			SWDG Q 4 A_EmpyrealRending();
			SWDG R 4;
			Goto Ready;
			
		MainHand.User2:
			SWDG A 0 A_ClearBobbing();
			SWDG S 4;
			SWDG T 4 A_HarpieStrike();
			SWDG U 4;
			goto Ready;
		
		MainHand.User3:
			SWDG A 0 A_ClearBobbing();
			SWDG P 4;
			SWDG Q 4 A_QuetzWingbeat();
			SWDG R 4;
			goto Ready;
		
		MainHand.User4:
			SWDG A 0 A_ClearBobbing();
			SWDG P 4;
			SWDG Q 4 A_Tornado();
			SWDG R 4;
			Goto Ready;
		
		MainHand.Ultimate:
			SWDG A 0 A_ClearBobbing();
			SWDG P 4;
			SWDG Q 4 A_GrandHurricane();
			SWDG R 4;
			Goto Ready;
		
		
		// Off Hand:
		OffHand.Select:
		OffHand.Deselect:
			SWDG A 0 A_OverlayFlags(10, PSPF_FLIP|PSPF_MIRROR, true);
			SWDG A 1;
			Loop;
			
		OffHand.Ready:
			SWDG A 0 A_OverlayFlags(10, PSPF_FLIP|PSPF_MIRROR, true);
			SWDG A 0 A_Jump(128, "OffHand.Ready.B");
		OffHand.Ready.A:
			SWDG ABCB 6;
			Loop;
		OffHand.Ready.B:
			SWDG DEFE 6;
			Loop;
		
		OffHand.Fire:
			SWDG A 0 A_OverlayFlags(10, PSPF_FLIP|PSPF_MIRROR, true);
			SWDG A 0 A_Jump(128, "OffHand.Fire.B");
		OffHand.Fire.A:
			SWDG GHI 2;
			Loop;
		OffHand.Fire.B:
			SWDG A 0 A_Jump(128, "OffHand.Fire.C");
			SWDG JKL 2;
			Loop;
		OffHand.Fire.C:
			SWDG MNO 2;
			Loop;
		
		OffHand.AltFire:
		OffHand.User2:
			SWDG A 0 A_OverlayFlags(10, PSPF_FLIP|PSPF_MIRROR, true);
			SWDG STU 4;
			Loop;
			
		OffHand.User1:
		OffHand.User3:
		OffHand.User4:
		OffHand.Reload:
		OffHand.Ultimate:
			SWDG A 0 A_OverlayFlags(10, PSPF_FLIP|PSPF_MIRROR, true);
			SWDG PQR 6;
			Loop;
	}
}

#include "zscript/schism/weapons/haruhi/swiftdaggers/airslash.zs"
#include "zscript/schism/weapons/haruhi/swiftdaggers/empyreal.zs"
#include "zscript/schism/weapons/haruhi/swiftdaggers/harpie.zs"
#include "zscript/schism/weapons/haruhi/swiftdaggers/quetz.zs"
#include "zscript/schism/weapons/haruhi/swiftdaggers/tornado.zs"
