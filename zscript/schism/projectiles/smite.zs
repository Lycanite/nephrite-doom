class SmiteProjectile : SchismProjectile {
	Default {
		Radius 5;
		Height 7;
		Scale 0.5;
		Speed 15;
		DamageFunction 2;
		Alpha 1;
		RenderStyle "Add";
		DamageType "Aether";
		
		SchismProjectile.ParticleName "Aether";
		SchismProjectile.ParticleInterval 1;
		SchismProjectile.ParticleScale 2;
		
		SeeSound "Projectile/Smite/Fire";
		DeathSound "Projectile/Smite/Explode";
		
		+NODAMAGETHRUST;
		+RIPPER;
	}
	
	States {
		Spawn:
			NPSM AB 2 A_StartSound(invoker.seeSound, CHAN_AUTO, CHANF_LOOPING);
		SpawnLoop:
			NPSM CDEF 2 A_StartSound(invoker.seeSound, CHAN_AUTO, CHANF_LOOPING);
			Loop;
			
		Death:
			NPSM GH 4;
			Stop;
	}
}