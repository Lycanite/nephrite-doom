class WeaponUltimate : WeaponUpgrade {
	Default {
		Inventory.PickupMessage "You have found a weapon ultimate!";
		Inventory.Amount 1;
		Inventory.MaxAmount 3;
		WeaponUpgrade.ExtraAmmoAmount 0;
		
		RenderStyle "Normal";
		
		-COUNTSECRET;
		-INVENTORY.ALWAYSPICKUP;
		-INVENTORY.AUTOACTIVATE;
		+INVENTORY.INVBAR;
	}
	
	override bool Use(bool pickup) {
		PlayerPawn playerPawn = PlayerPawn(self.owner);
		if (!playerPawn) {
			return super.Use(pickup);
		}
		PlayerInfo playerInfo = players[playerPawn.PlayerNumber()];
		if (!playerInfo) {
			return super.Use(pickup);
		}
		SchismWeapon schismWeapon = SchismWeapon(playerInfo.readyWeapon);
		if (schismWeapon) {
			schismWeapon.ActivateUltimateAbility();
			return true;
		}
		return false;
	}

	States {
		Spawn:
			REIS A 1 bright;
			Loop;
	}
}
