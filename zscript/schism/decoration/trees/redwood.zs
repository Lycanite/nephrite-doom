class SchismTreeRedwood : SchismTree {
	Default {
		Tag "Redwood Tree";
		SchismTree.SpringVariants 2;
		SchismTree.SummerVariants 2;
		SchismTree.AutumnVariants 2;
		SchismTree.WinterVariants 2;
	}
	
	States {
		Spring.1:
			TR01 A -1;
			Stop;

		Spring.2:
			TR01 B -1;
			Stop;

		Summer.1:
			TR01 E -1;
			Stop;

		Summer.2:
			TR01 F -1;
			Stop;

		Autumn.1:
			TR01 I -1;
			Stop;

		Autumn.2:
			TR01 J -1;
			Stop;

		Winter.1:
			TR01 M -1;
			Stop;

		Winter.2:
			TR01 N -1;
			Stop;

		Death.Spring.1:
			TD01 A -1;
			Stop;

		Death.Spring.2:
			TD01 B -1;
			Stop;

		Death.Summer.1:
			TD01 E -1;
			Stop;

		Death.Summer.2:
			TD01 F -1;
			Stop;

		Death.Autumn.1:
			TD01 I -1;
			Stop;

		Death.Autumn.2:
			TD01 J -1;
			Stop;

		Death.Winter.1:
			TD01 M -1;
			Stop;

		Death.Winter.2:
			TD01 N -1;
			Stop;
	}
}