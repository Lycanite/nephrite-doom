class SchismPotA : Actor {
	Default {
		Tag "Pot A";
		
		Health 20;
		Radius 10;
		Height 32;
		
		+SOLID;
		+SHOOTABLE;
	}
	
	States {
		Spawn:
			NPOT A -1;
			Stop;
		
		Death:
			NPOT A 0;
			Stop;
	}
}

class SchismPotB : SchismPotA {
	Default {
		Tag "Pot B";
	}
	
	States {
		Spawn:
			NPOT B -1;
			Stop;
		
		Death:
			NPOT B 0;
			Stop;
	}
}

class SchismUrnGreen : SchismPotA {
	Default {
		Tag "Urn Green";
	}
	
	States {
		Spawn:
			NPOT C -1;
			Stop;
		
		Death:
			NPOT C 0;
			Stop;
	}
}

class SchismUrnGrey : SchismUrnGreen {
	Default {
		Tag "Urn Grey";
	}
	
	States {
		Spawn:
			NPOT D -1;
			Stop;
		
		Death:
			NPOT D 0;
			Stop;
	}
}

class SchismUrnBlack : SchismUrnGreen {
	Default {
		Tag "Urn Black";
	}
	
	States {
		Spawn:
			NPOT E -1;
			Stop;
		
		Death:
			NPOT E 0;
			Stop;
	}
}

class SchismUrnWhite : SchismUrnGreen {
	Default {
		Tag "Urn White";
	}
	
	States {
		Spawn:
			NPOT F -1;
			Stop;
		
		Death:
			NPOT F 0;
			Stop;
	}
}

class SchismUrnRed : SchismUrnGreen {
	Default {
		Tag "Urn Red";
	}
	
	States {
		Spawn:
			NPOT G -1;
			Stop;
		
		Death:
			NPOT G 0;
			Stop;
	}
}