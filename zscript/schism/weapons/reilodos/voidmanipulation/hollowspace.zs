class HollowSpaceProjectile : SchismProjectile {
	VoidManipulation voidManipulation;
	
	Default {
		Radius 10;
		Height 10;
		Speed 30;
		DamageFunction 20;
		Gravity 0;
		Alpha 1;
		Renderstyle "Add";
		Projectile;
		
		DamageType "Fire";
		
		SeeSound "VoidManipulation/Fire/Rift/Hollow";
		DeathSound "";
		
		+SKYEXPLODE;
	}
	
	action void A_CallStorm() {
		int stormSize = 3;
		int cloudSize = 200;
		for (int x = -(stormSize * cloudSize); x <= stormSize * cloudSize; x += cloudSize) {
			for (int y = -(stormSize * cloudSize); y <= stormSize * cloudSize; y += cloudSize) {
				A_SpawnItemEx("HollowSpaceCloud", x, y, 0, 0, 0, 0, 0, SXF_ISTRACER);
				HollowSpaceCloud cloud = HollowSpaceCloud(invoker.tracer);
				if (cloud) {
					cloud.voidManipulation = invoker.voidManipulation;
				}
				
				A_SpawnItemEx("HollowSpaceCloudFloor", x, y, 0, 0, 0, 0, 0, SXF_ISTRACER);
				cloud = HollowSpaceCloud(invoker.tracer);
				if (cloud) {
					cloud.voidManipulation = invoker.voidManipulation;
				}
			}
		}
	}
	
	States {
		Spawn:
		Death:
			VDPT A 1;
			VDPT A 1 A_CallStorm();
			Stop;
	}
}

class HollowSpaceCloud : SchismProjectile {
	int randomStart;
	VoidManipulation voidManipulation;
	
	Default {
		Radius 1;
		Height 1;
		Scale 2;
		Speed 15;
		DamageFunction 2;
		Alpha 0.25;
		Gravity 0;
		Renderstyle "Translucent";
		
		SeeSound "";
		DeathSound "";
		
		+DONTSPLASH;
		+SPAWNCEILING;
		+CEILINGHUGGER;
		+NODAMAGETHRUST;
		+PAINLESS;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		if (random(0, 3) == 0) {
			randomStart = random(0, 90);
		}
		else {
			randomStart = 0;
		}
	}
	
	action void A_HollowCloud() {
		A_StartSound("VoidManipulation/Fire/Rift/Hollow", CHAN_BODY, CHANF_LOOPING);
		if (!invoker.voidManipulation || invoker.randomStart <= 0) {
			return;
		}
		if (--invoker.randomStart == 0) {
			float z = GetZAt(0, 0, 0, GZF_3DRESTRICT|GZF_NO3DFLOOR);
			float ceilingZ = GetZAt(0, 0, 0, GZF_3DRESTRICT|GZF_NO3DFLOOR|GZF_CEILING);
			if (ceilingZ > z) {
				z = Random(z, ceilingZ);
			}
			Vector3 riftPos =(invoker.Pos.x + random(-100, 100), invoker.Pos.y + random(-100, 100), z);
			invoker.voidManipulation.CreateRift("VoidManipulationRiftHollow", riftPos, 0);
		}
	}
	
	States {
		Spawn:
			NCLV FGHIJABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDE 4 bright A_HollowCloud();
		Death:
			NCLV F 4 A_StopSound(CHAN_BODY);
		Death.Fade:
			NCLV GGGGHHHHIIIIJJJJ 1 A_FadeOut(0.1);
			Loop;
	}
}

class HollowSpaceCloudFloor : HollowSpaceCloud {
	Default {
		-CEILINGHUGGER;
		+FLOORHUGGER;
	}
}


class VoidManipulationRiftHollow : VoidManipulationRift {
	Default {
		Radius 32;
		Height 80;
		Scale 0.75;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		havocEnergy = 0;
		
		energy = 300;
		energyMax = 3020;
		energySustain = 300;
		sustained = true;
		pullRange = 500;
		sizeScale = 2;
		summonCountdown = 35;
		UpdateSize();
	}
}
