class SchismDelayDebuff : SchismStatusEffect {
	int lastTic;
	int extraTic;
	
	Default {
		Powerup.Duration -6;
		
		SchismStatusEffect.Detrimental true;
		SchismStatusEffect.ParticleName "SchismDelaySmall";
	}
	
	override double GetSpeedFactor() {
		return 0;
	}
	
	override void InitEffect() {
		super.InitEffect();
		if (!self.owner) {
			return;
		}
		
		self.lastTic = self.owner.Tics;
	}
	
	override void DoEffect() {
		super.DoEffect();
		
		if (!self.owner || self.owner.Health <= 0 || self.owner.bCorpse || self.owner.bBoss) {
			return;
		}
		
		if (self.extraTic-- > 0) {
			self.owner.Tics = self.lastTic;
		}
		else {
			self.lastTic = self.owner.Tics;
			self.extraTic = 2;
		}
	}
}
