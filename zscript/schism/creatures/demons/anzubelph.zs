class AnzuBelph : Belph {
	Default {
		Tag "Anzu Belph";

		Health 70;
		Mass 120;
		Speed 7;
		PainChance 210;

		SeeSound "AnzuBelph/See";
		ActiveSound "AnzuBelph/Idle";
		PainSound "AnzuBelph/Pain";
		DeathSound "AnzuBelph/Death";
		MeleeSound "AnzuBelph/Melee";
		Obituary "%o was shocked by an Anzu Belph.";
		HitObituary "An Anzu Belph had %o for lunch.";
	}
	
	states {
		Spawn:
			AZBL AB 10 A_Look();
			Loop;
		
		See:
			AZBL AABBCCDD 3 A_Chase();
			Loop;
		
		Melee:
			AZBL B 8 A_FaceTarget();
			AZBL C 1 A_StartSound("AnzuBelph/Melee");
			AZBL D 7 A_FaceTarget();
			AZBL D 6 A_CreatureMelee(3, 18, "Lightning", "Lightning", "SchismPoisonEffect", 5 * TICRATE);
			Goto See;
		
		Missile:
			AZBL CD 8 A_FaceTarget();
			AZBL E 6 A_SpawnProjectile("StaticShockProjectile", invoker.height / 2);
			Goto See;
		
		Pain:
			AZBL G 2;
			AZBL G 2 A_Pain();
			Goto See;
		
		Death:
		XDeath:
			AZBL I 8;
			AZBL J 8 A_Scream();
			AZBL K 6;
			AZBL L 6 A_CreatureDie();
			AZBL M -1;
			Stop;
		
		Raise:
			AZBL LKJI 8;
			Goto See;
	}
}