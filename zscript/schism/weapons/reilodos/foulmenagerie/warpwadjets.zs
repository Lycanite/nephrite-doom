class WarpWadjetsScroll : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Foul Menagerie can now unleash portal yeilding Warp Wadjets at the cost of Havoc!";
		
		Inventory.Icon "REISD0";
		Inventory.PickupSound "Wadjet/See";
		Inventory.RestrictedTo "Reilodos", "ReilodosHavocSerpent";
		WeaponUpgrade.ExtraAmmoClass "ChaosEnergy";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			REIS D 1 bright;
			Loop;
	}
}
