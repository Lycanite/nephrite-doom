class SchismWeaknessDebuff : SchismStatusEffect {
	Default {
		PowerUp.Duration -10;
		
		SchismStatusEffect.Detrimental true;
		SchismStatusEffect.ParticleName "Shadow";
	}
	
	override void ModifyDamage(int damage, Name damageType, out int newDamage, bool passive, Actor inflictor, Actor source, int flags) {
		super.ModifyDamage(damage, damageType, newDamage, passive, inflictor, source, flags);
		if (passive || damage <= 0) {
			return; // Active mode is for damage being dealt, passive is for damage being received.
		}
		newDamage = Max(1, ApplyDamageFactors(self.GetClass(), damageType, damage, int(double(damage) / Max(1, self.strength + 1))));
	}
}
