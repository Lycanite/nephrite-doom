class ChaosEnergy : Ammo {
	Default {
		//$Category "Ammunition"
		Tag "Chaos Energy";
		Scale 0.5; 
		
		Inventory.PickupMessage "You gain chaos energy!";
		Inventory.Amount 20;
		Inventory.MaxAmount 200;
		Ammo.BackpackAmount 300;
		Ammo.BackpackMaxAmount 300;
		
		Inventory.Icon "AREIA0";
		Inventory.PickupSound "Reilodos/Ammo/Chaos";
		Inventory.RestrictedTo "Reilodos", "ReilodosHavocSerpent";
		
		RenderStyle "Add";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			AREI ABC 4 bright;
			Loop;
	}
}


class ChaosEnergyLarge : ChaosEnergy {
	Default {
		//$Category "Ammunition"
		Tag "Large Chaos Energy";
		
		Scale 0.35;
		Inventory.PickupMessage "Ultimate chaos!";
		Inventory.Amount 100;
		
		RenderStyle "Normal";
	}

	States {
		Spawn:
			AREI DEF 4 bright;
			Loop;
	}
}


class ShadowEnergy : Ammo {
	Default {
		//$Category "Ammunition"
		Tag "Shadow Energy";
		Scale 0.35; 
		
		Inventory.PickupMessage "You gain shadow energy!";
		Inventory.Amount 20;
		Inventory.MaxAmount 200;
		Ammo.BackpackAmount 300;
		Ammo.BackpackMaxAmount 300;
		
		Inventory.Icon "AREIG0";
		Inventory.PickupSound "Reilodos/Ammo/Shadow";
		Inventory.RestrictedTo "Reilodos", "ReilodosHavocSerpent";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			AREI GHI 4 bright;
			Loop;
	}
}


class ShadowEnergyLarge : ShadowEnergy {
	Default {
		//$Category "Ammunition"
		Tag "Large Shadow Energy";
		
		Scale 0.25;
		Inventory.PickupMessage "Ultimate shadow!";
		Inventory.Amount 100;
	}

	States {
		Spawn:
			AREI JKL 4 bright;
			Loop;
	}
}


class VoidEnergy : Ammo {
	Default {
		//$Category "Ammunition"
		Tag "Void Energy";
		Scale 0.5; 
		
		Inventory.PickupMessage "You gain void energy!";
		Inventory.Amount 20;
		Inventory.MaxAmount 200;
		Ammo.BackpackAmount 300;
		Ammo.BackpackMaxAmount 300;
		
		Inventory.Icon "AREIM0";
		Inventory.PickupSound "Reilodos/Ammo/Void";
		Inventory.RestrictedTo "Reilodos", "ReilodosHavocSerpent";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			AREI MNO 4 bright;
			Loop;
	}
}


class VoidEnergyLarge : VoidEnergy {
	Default {
		//$Category "Ammunition"
		Tag "Large Void Energy";
		
		Scale 0.35;
		Inventory.PickupMessage "Endless void!";
		Inventory.Amount 100;
	}

	States {
		Spawn:
			AREI PQR 4 bright;
			Loop;
	}
}


class HavocEnergy : Ammo {
	Default {
		//$Category "Ammunition"
		Tag "HavocEnergy";
		
		Scale 0.5;
		Inventory.PickupMessage "You gain unstable havoc energy!";
		Inventory.Amount 20;
		Inventory.MaxAmount 200;
		Ammo.BackpackAmount 0;
		Ammo.BackpackMaxAmount 200;
		
		Inventory.Icon "AREIS0";
		Inventory.PickupSound "Reilodos/Ammo";
		Inventory.RestrictedTo "Reilodos", "ReilodosHavocSerpent";
		
		RenderStyle "Add";
		
		+FLOATBOB;
		+INVENTORY.IGNORESKILL;
	}

	States {
		Spawn:
			AREI STU 4 bright;
			Loop;
	}
}