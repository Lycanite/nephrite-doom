class AglebemuTadpole : Creature {
	Default {
		Tag "AglebemuTadpole";
		
		Health 100;
		Radius 20;
		Height 56;
		Speed 24;
		FloatSpeed 14;
		Scale 0.6;
		PainChance 250;
		Damage 2;
		MeleeRange 200;
		BloodColor "ff 00 00";

		DamageFactor "Poison", 0.1;

		+JUMPDOWN;

		-Creature.WALKER;
		+Creature.SWIMMER;
		
		SeeSound "AglebemuTadpole/See";
		ActiveSound "AglebemuTadpole/Idle";
		PainSound "AglebemuTadpole/Pain";
		MeleeSound "AglebemuTadpole/Melee";
		DeathSound "AglebemuTadpole/Death";
		Obituary "%o was tongued by an Aglebemu Tadpole!";
		HitObituary "%o was tongued by an Aglebemu Tadpole!";
	}
	
	States {
		Spawn:
			AGLT AABBCCBB 4 A_Look();
			Loop;
			
		See:
			AGLT DEFG 4 A_Chase();
			Loop;
			
		Melee:
			AGLT HI 8 A_FaceTarget();
			AGLT J 8 A_CreatureMelee(2, 6, "PoisonBubble", "Poison");
			AGLT K 8;
			Goto See;
			
		Pain:
			AGLT L 6;
			AGLT M 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			AGLT L 5;
			AGLT M 5 A_Scream();
			AGLT NO 5;
			AGLT P 5 A_NoBlocking();
			AGLT QRSTUVW 5;
			AGLT X -1;
			Stop;
	}
}
