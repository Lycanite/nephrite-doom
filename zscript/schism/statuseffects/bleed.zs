class SchismBleedDebuff : SchismStatusEffect {
	
	Default {
		PowerUp.Duration -10;
		
		SchismStatusEffect.Detrimental true;

		SchismStatusEffect.EffectInterval TICRATE;
		SchismStatusEffect.EffectDamage 4;
		SchismStatusEffect.EffectDamageType "Fae";
		SchismStatusEffect.EffectRadius 1000;

		SchismStatusEffect.ParticleName "Fae";
		SchismStatusEffect.ParticleCount 4;
		SchismStatusEffect.ParticleSpeed 2;
		SchismStatusEffect.ParticleDuration 1 * TICRATE;
	}
	
	override void DoEffect() {
		super.DoEffect();
		if (!self.owner || self.owner.Health <= 0 || self.owner.bCorpse) {
			return;
		}
		
		// Bleed Damage:
		float velTotal = Abs(self.owner.vel.x) + Abs(self.owner.vel.y) + Abs(self.owner.vel.z);
		int multiplier = Ceil(velTotal);
		if (PlayerPawn(self.Owner)) {
			multiplier = Ceil(multiplier / 4);
		}
		if (multiplier && self.effectTics % self.effectInterval == 0) {			
			// Anima Burst (Heal Inflictor's Allies):
			if (self.strength >= 2 && self.effectInflictor) {
				for (int i = 0; i < 40; i++) {
					self.SpawnParticleFromActor(
						self.owner, "Anima", 2 * TICRATE, 8,
						(0, 0, self.owner.height / 2),
						FRandom(0, 359), FRandom(-45, 45),
						self.particleScale, self.particleColor
					);
				}
				Array<Actor> actorsHealed;
				SchismAttacks.Explode(self.owner, self.effectInflictor, -(self.strength * 2 * multiplier), self.effectRadius, 0, 1, false, false, "Fae", actorsHealed);

				// Healing Particles:
				for (int healedIndex = 0; healedIndex < actorsHealed.Size(); healedIndex++) {
					Actor healedActor = actorsHealed[healedIndex];
					if (!healedActor || healedActor.bCorpse) {
						continue;
					}
					if (!healedActor.bIsMonster && !PlayerPawn(healedActor)) {
						continue;
					}
					for (int i = 0; i < 40; i++) {
						self.SpawnParticleFromActor(
							healedActor, "Anima", 2 * TICRATE, 8,
							(0, 0, healedActor.height / 2),
							FRandom(0, 359), FRandom(-45, 45),
							self.particleScale, self.particleColor
						);
					}
				}
			}

			// Bleed Damage:
			self.owner.DamageMobj(self.effectInflictor, self.effectSource, self.effectDamage * multiplier, self.effectDamageType, DMG_NO_ARMOR|DMG_THRUSTLESS);
		}
	}
}
