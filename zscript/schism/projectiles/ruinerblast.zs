class RuinerBlastProjectile : SchismProjectile {
	Default {
		Radius 10;
		Height 24;
		Scale 0.5;
		Speed 20;
		FastSpeed 25;
		Damage 20;
		Alpha 1;
		RenderStyle "Add";
		
		DamageType "Chaos";
		
		SeeSound "Projectile/RuinerBlast/Fire";
		DeathSound "Projectile/RuinerBlast/Explode";
		Decal "CacoScorch";

		SchismProjectile.ParticleName "ChaosFlare";
		SchismProjectile.ParticleCount 1;
	}
	
	States {
		Spawn:
			NPRB AB 4 bright;
			Loop;
			
		Death:
			NPRB CDEFGH 3 bright;
			Stop;
	}
}