class BoneArmorSigil : WeaponPassive {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Grim Scythe will now harvest the bones of your victims to form armor!";
		
		Inventory.Icon "GSCIF0";
		Inventory.PickupSound "GrimScythe/Pickup";
		Inventory.RestrictedTo "Chozo";
		WeaponUpgrade.ExtraAmmoClass "GrimRune";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			GSCI F 1 bright;
			Loop;
	}
}

class GrimScytheBoneArmor : HomingEffectProjectile {
	int armorAmount;

	Default {
		Scale 1;
		Radius 16;
		Height 10;
		Speed 20;
		Gravity 0;
		DamageFunction 0;
		Alpha 0.5;
		Renderstyle "Add";
		
		SeeSound "GrimScythe/BoneArmor";
		DeathSound "GrimScythe/BoneArmor";
		
		PROJECTILE;
		+RANDOMIZE;
		+SEEKERMISSILE;
		+RIPPER;
		+NOCLIP;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.armorAmount = 1;
	}
	
	override void OnImpact() {
		self.followTarget.A_GiveInventory("GrimScytheArmor", self.armorAmount);
		self.A_StartSound("GrimScythe/BoneArmor");
		super.OnImpact();
	}
	
	States {
		Spawn:
			BONE ABCDEFGH 1 {
				return A_HomeToTarget();
			}
			Loop;
			
		Death:
			BONE A 4;
			Stop;
	}
}

class GrimScytheArmor : BasicArmorBonus {
	Default {
		Armor.SavePercent 33.335;
		Armor.SaveAmount 1;
		Armor.MaxSaveAmount 200;

		+INVENTORY.ALWAYSPICKUP;
	}
}