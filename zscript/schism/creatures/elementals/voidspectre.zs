class VoidSpectre : MinionCreature {
	VoidManipulationRift homeRift;
	
	Default {
		Tag "Spectre";
		
		Health 200;
		Radius 10;
		Height 70;
		Speed 30;
		FloatSpeed 30;
		Scale 0.5;
		Gravity 0;
		PainChance 120;
		Damage 1;
		BloodColor "44 00 44";

		DamageFactor "Void", 0.1;

		MinionCreature.SummonTime 30 * 35;

		+FLOAT;
		
		SeeSound "VoidSpectre/See";
		PainSound "VoidSpectre/Pain";
		DeathSound "VoidSpectre/Death";
		ActiveSound "VoidSpectre/See";
		MeleeSound "VoidSpectre/Melee";
		Obituary "%o was consumed by a Void Spectre!";
		HitObituary "%o was consumed by a Void Spectre!";
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		if (self.health > 0) {
			self.SpawnParticle("Void", 8, 2, (FRandom(-8, 8), FRandom(-8, 8), FRandom(32, 40)), Random(0, 200));
		}
		if (self.homeRift && self.target && !self.target.bBoss) {
			self.target.Thrust(-self.homeRift.pullForce, self.homeRift.AngleTo(self.target));
		}
	}
	
	States {
		Spawn:
			SPTR ABCB 4 A_MinionIdle();
			Loop;
			
		See:
			SPTR ABCB 4 {
				A_Chase("Melee");
			}
			Loop;
			
		Melee:
			SPTR DE 4 A_FaceTarget();
			SPTR F 4 A_CreatureMelee(10, 30);
			SPTR E 4 A_FaceTarget();
			Goto See;
			
		Pain:
			SPTR G 6 A_Pain();
			Goto See;
			
		Pain.SelfDamage:
			Goto See;
			
		Death:
		XDeath:
			SPTR G 5;
			SPTR H 5 A_Scream();
			SPTR I 5;
			SPTR J 5 A_NoBlocking();
			SPTR KLMN 5;
			Stop;
	}
}