mixin class QueryPlayerInfoMixin {
	SchismPlayerInfo schismPlayerInfoCache;

    /** 
     * Gets the SchismPlayerInfo instance via a find or cache.
     */
	virtual SchismPlayerInfo GetSchismPlayerInfo() {
        if (!self.schismPlayerInfoCache) {
            self.schismPlayerInfoCache = SchismPlayerInfo.Get();
        }
        return self.schismPlayerInfoCache;
	}
}