class GreedyWaltzScroll : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "You can now perform the Greedy Waltz to consume the empowering cinders of enemy souls!";
		
		Inventory.Icon "VESWI0";
		Inventory.PickupSound "WickedCavort/Pickup";
		Inventory.RestrictedTo "Vess";
		WeaponUpgrade.ExtraAmmoClass "LustDrupe";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			VESW I 1 bright;
			Loop;
	}
}

extend class WickedCavort {
	action state A_GreedyWaltz() {
		if (!A_CheckSpecial(1) || !A_SpendAmmo(3)) {
			return ResolveState("Ready");
		}

        A_StartSound("WickedCavort/Fire/GreedyWaltz", CHAN_WEAPON);
		Array<Actor> projectiles;
		int radius = 2 + invoker.GetPassiveLevel();
        A_FireProjectileCross("GreedyWaltzProjectile", radius, 30, projectiles);
		for (int i = 0; i < projectiles.Size(); i++) {
			GreedyWaltzProjectile waltzProjectile = GreedyWaltzProjectile(projectiles[i]);
			if (waltzProjectile) {
				waltzProjectile.wickedCavort = invoker;
			}
		}
		return null;
	}

    States {
		MainHand.User1:
			VSH1 W 4 {
                A_ClearBobbing();
                A_Light2();
            }
			VSH1 X 4;
			VSH1 Y 4 A_GreedyWaltz();
            VSH1 Z 4;
            VSH3 ABC 4;
			VSH3 C 0 A_MirrorHands();
			VSH3 C 0 A_Light0();
			Goto Ready;

		OffHand.User1:
			VSH3 DEF 4;
			VSH3 MNO 4;
			Loop;
    }
}

class GreedyWaltzProjectile : WrathCrossProjectile {
	WickedCavort wickedCavort;

	Default {
		Radius 5;
		Height 7;
		Speed 10;
		Gravity 0;
		DamageFunction Random(2, 4);
		Alpha 0.5;
		RenderStyle "Add";
		Scale 1;
		
		SeeSound "WickedCavort/Fire/GreedyWaltz";
		DeathSound "WickedCavort/Explode/GreedyWaltz";
		Obituary "%o has their soul cremated by %k's Greedy Waltz!";

		SchismProjectile.LifetimeMax 2 * TICRATE;
		SchismProjectile.ParticleColor 0xDDAA00;
		
		+RIPPER;
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		if (specialDamage > 0 && self.wickedCavort) {
			self.wickedCavort.damageBoost = Min(1000, self.wickedCavort.damageBoost + 1);
		}
		return specialDamage;
	}
	
	States {
		Spawn:
			VWC3 ABCDE 4;
			Loop;
			
		Death:
			VWC3 EFGH 4;
			Stop;
	}
}