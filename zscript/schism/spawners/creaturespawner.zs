class CreatureSpawner : Actor { // Dummy actor that should be replaced with a creature.
    string spawnRole;
    
    property SpawnRole: spawnRole;

    States {
        Spawn:
			TNT1 A 1 NoDelay;
			Stop;
    }
}

const SPAWN_ROLE_GRUNT = "grunt"; // Weak, Ranged, Swarm
class CreatureSpawnerGrunt : CreatureSpawner {
    Default {
        CreatureSpawner.SpawnRole SPAWN_ROLE_GRUNT;
    }
}

const SPAWN_ROLE_BOUNCER = "bouncer"; // Low, Ranged
class CreatureSpawnerBouncer : CreatureSpawner {
    Default {
        CreatureSpawner.SpawnRole SPAWN_ROLE_BOUNCER;
    }
}

const SPAWN_ROLE_SNIPER = "sniper"; // Low, Long
class CreatureSpawnerSniper : CreatureSpawner {
    Default {
        CreatureSpawner.SpawnRole SPAWN_ROLE_SNIPER;
    }
}

const SPAWN_ROLE_SLINGER = "slinger"; // Low, Ranged
class CreatureSpawnerSlinger : CreatureSpawner {
    Default {
        CreatureSpawner.SpawnRole SPAWN_ROLE_SLINGER;
    }
}

const SPAWN_ROLE_LOBBER = "lobber"; // Mid, Ranged
class CreatureSpawnerLobber : CreatureSpawner {
    Default {
        CreatureSpawner.SpawnRole SPAWN_ROLE_LOBBER;
    }
}

const SPAWN_ROLE_BARON = "baron"; // High, Ranged
class CreatureSpawnerBaron : CreatureSpawner {
    Default {
        CreatureSpawner.SpawnRole SPAWN_ROLE_BARON;
    }
}

const SPAWN_ROLE_BRUTE = "brute"; // Low, Melee
class CreatureSpawnerBrute : CreatureSpawner {
    Default {
        CreatureSpawner.SpawnRole SPAWN_ROLE_BRUTE;
    }
}

const SPAWN_ROLE_ASSASSIN = "assassin"; // Low, Melee, Stealth
class CreatureSpawnerAssassin : CreatureSpawner {
    Default {
        CreatureSpawner.SpawnRole SPAWN_ROLE_ASSASSIN;
    }
}

const SPAWN_ROLE_FODDER = "fodder"; // Weak, Melee, Swarm
class CreatureSpawnerFodder : CreatureSpawner {
    Default {
        CreatureSpawner.SpawnRole SPAWN_ROLE_FODDER;
    }
}

const SPAWN_ROLE_JUGGERNAUGHT = "juggernaught"; // High, Melee
class CreatureSpawnerJuggernaught : CreatureSpawner {
    Default {
        CreatureSpawner.SpawnRole SPAWN_ROLE_JUGGERNAUGHT;
    }
}

const SPAWN_ROLE_SOUL = "soul"; // Low, Melee, Flying
class CreatureSpawnerSoul : CreatureSpawner {
    Default {
        CreatureSpawner.SpawnRole SPAWN_ROLE_SOUL;
    }
}

const SPAWN_ROLE_DART = "dart"; // Low, Ranged, Flying
class CreatureSpawnerDart : CreatureSpawner {
    Default {
        CreatureSpawner.SpawnRole SPAWN_ROLE_DART;
    }
}

const SPAWN_ROLE_GHAST = "ghast"; // Mid, Ranged, Flying
class CreatureSpawnerGhast : CreatureSpawner {
    Default {
        CreatureSpawner.SpawnRole SPAWN_ROLE_GHAST;
    }
}

const SPAWN_ROLE_BROOD = "brood"; // High, Ranged/Summoner, Flying
class CreatureSpawnerBrood : CreatureSpawner {
    Default {
        CreatureSpawner.SpawnRole SPAWN_ROLE_BROOD;
    }
}

const SPAWN_ROLE_TANK = "tank"; // High, Long
class CreatureSpawnerTank : CreatureSpawner {
    Default {
        CreatureSpawner.SpawnRole SPAWN_ROLE_TANK;
    }
}

const SPAWN_ROLE_CANNON = "cannon"; // High, Ranged/Splash
class CreatureSpawnerCannon : CreatureSpawner {
    Default {
        CreatureSpawner.SpawnRole SPAWN_ROLE_CANNON;
    }
}

const SPAWN_ROLE_SEEKER = "seeker"; // High, Homing
class CreatureSpawnerSeeker : CreatureSpawner {
    Default {
        CreatureSpawner.SpawnRole SPAWN_ROLE_SEEKER;
    }
}

const SPAWN_ROLE_CASTER = "caster"; // High, Special/Summon/Raise
class CreatureSpawnerCaster : CreatureSpawner {
    Default {
        CreatureSpawner.SpawnRole SPAWN_ROLE_CASTER;
    }
}

const SPAWN_ROLE_DESTROYER = "destroyer"; // Boss, Ranged/Splash
class CreatureSpawnerDestroyer : CreatureSpawner {
    Default {
        CreatureSpawner.SpawnRole SPAWN_ROLE_DESTROYER;
    }
}

const SPAWN_ROLE_MASTERMIND = "mastermind"; // Boss, Long
class CreatureSpawnerMastermind : CreatureSpawner {
    Default {
        CreatureSpawner.SpawnRole SPAWN_ROLE_MASTERMIND;
    }
}

const SPAWN_ROLE_URGHAST = "urghast"; // Boss, Ranged, Flying
class CreatureSpawnerUrghast : CreatureSpawner {
    Default {
        CreatureSpawner.SpawnRole SPAWN_ROLE_URGHAST;
    }
}