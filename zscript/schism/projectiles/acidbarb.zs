class AcidBarbProjectile : SchismProjectile {
	Default {
		Radius 8;
		Height 16;
		Scale 1;
		Speed 8;
		FastSpeed 10;
		Damage 4;
		PoisonDamage 32;
		Alpha 0.67;
		RenderStyle "Add";
		
		DamageType "Acid";
		
		SeeSound "Projectile/AcidBarb/Fire";
		DeathSound "Projectile/AcidBarb/Explode";

		SchismProjectile.ParticleName "Acid";
		SchismProjectile.ParticleCount 1;
		SchismProjectile.ParticleInterval 10;
		SchismProjectile.StatusEffectClass "SchismSoakDebuff";
		SchismProjectile.StatusEffectDuration 3 * TICRATE;
	}
	
	States {
		Spawn:
			NPAB AB 2 bright;
			Loop;
			
		Death:
			NPAB CDEF 4 bright;
			Stop;
	}
}