class SchismTreeMangrove : SchismTree {
	Default {
		Tag "Mangrove Tree";
		SchismTree.SpringVariants 2;
		SchismTree.SummerVariants 2;
		SchismTree.AutumnVariants 2;
		SchismTree.WinterVariants 2;
	}
	
	States {
		Spring.1:
			TR18 A -1;
			Stop;

		Spring.2:
			TR18 B -1;
			Stop;

		Summer.1:
			TR18 E -1;
			Stop;

		Summer.2:
			TR18 F -1;
			Stop;

		Autumn.1:
			TR18 I -1;
			Stop;

		Autumn.2:
			TR18 J -1;
			Stop;

		Winter.1:
			TD18 M -1;
			Stop;

		Winter.2:
			TD18 N -1;
			Stop;

		Death.Spring.1:
			TD18 A -1;
			Stop;

		Death.Spring.2:
			TD18 B -1;
			Stop;

		Death.Summer.1:
			TD18 E -1;
			Stop;

		Death.Summer.2:
			TD18 F -1;
			Stop;

		Death.Autumn.1:
			TD18 I -1;
			Stop;

		Death.Autumn.2:
			TD18 J -1;
			Stop;

		Death.Winter.1:
			TD18 M -1;
			Stop;

		Death.Winter.2:
			TD18 N -1;
			Stop;
	}
}