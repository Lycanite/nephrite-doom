class SchismLeaf : SchismSFXSolid {
	Default {
		Gravity 0.01;

		SchismSFX.ThrustLatMin -2;
		SchismSFX.ThrustLatMax 2;
		SchismSFX.ThrustLonMin 2;
		SchismSFX.ThrustLonMax 10;
		SchismSFX.ThrustVertMin -1;
		SchismSFX.ThrustVertMax 1;
		SchismSFX.FadeOut 0.01;
	}

	States {
		Spawn:
			NLF1 ABCDEFGHI 4 A_SFXUpdate(4);
			Loop;
	}
}

class SchismLeafDry : SchismLeaf {
	States {
		Spawn:
			NLF2 ABCDEFGHI 4 A_SFXUpdate(4);
			Loop;
	}
}

class SchismLeafSakura : SchismLeaf {
	States {
		Spawn:
			NLF3 ABCDEFGHI 4 A_SFXUpdate(4);
			Loop;
	}
}