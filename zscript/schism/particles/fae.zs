struct SchismFaeParticle {
	static void apply(out FSpawnParticleParams particleParams) {
		particleParams.texture = TexMan.CheckForTexture("particles/fae/Fae01.png");
		particleParams.style = STYLE_Translucent;
		particleParams.flags = SPF_ROLL;

		particleParams.accel = (0, 0, 0);
		particleParams.startRoll = Random(0, 359);
		particleParams.rollVel = Random(-10, 10);
		particleParams.rollAcc = 0;

		particleParams.size = 16.0;
		particleParams.sizeStep = 1;
		particleParams.startAlpha = 1;
		particleParams.fadeStep = particleParams.startAlpha / particleParams.lifetime;
	}
}

struct SchismAnimaParticle {
	static void apply(out FSpawnParticleParams particleParams) {
		particleParams.texture = TexMan.CheckForTexture("particles/fae/Anima01.png");
		particleParams.style = STYLE_Add;
		particleParams.flags = SPF_ROLL|SPF_FULLBRIGHT;

		particleParams.accel = (0, 0, 0);
		particleParams.startRoll = Random(0, 359);
		particleParams.rollVel = Random(-10, 10);
		particleParams.rollAcc = 0;

		particleParams.size = 16.0;
		particleParams.sizeStep = 1;
		particleParams.startAlpha = 0.8;
		particleParams.fadeStep = particleParams.startAlpha / particleParams.lifetime;
	}
}

struct SchismBloodDripParticle {
	static void apply(out FSpawnParticleParams particleParams) {
		particleParams.texture = TexMan.CheckForTexture("particles/fae/BloodDrip01.png");
		particleParams.style = STYLE_Translucent;
		particleParams.flags = SPF_ROLL;

		particleParams.accel = (0, 0, 0);
		particleParams.startRoll = 0;
		particleParams.rollVel = 0;
		particleParams.rollAcc = 0;

		particleParams.size = 8.0;
		particleParams.sizeStep = 0;
		particleParams.startAlpha = 1.0;
		particleParams.fadeStep = 0;
	}
}
