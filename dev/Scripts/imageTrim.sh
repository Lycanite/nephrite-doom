#!/bin/bash
set -e

for file in *.png; do
    convert $file -trim +repage $file
done