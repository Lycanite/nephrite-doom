class MinionCreature : Creature {
	mixin QueryPlayerInfoMixin;

	string minionCategory;
	int summonTime;
	double followStopDistance;
	float summonedAngle;
	int stance;
	bool minionShield;

	property MinionCategory: minionCategory; // The category of the minion, used by weapons and effects to interact with certain minions differently.
	property SummonTime: summonTime; // The duration in tics until this creature despawns when it has a master.
	property FollowStopDistance: followStopDistance; // When following a master, this is the instance until this creature will stop moving.
	property Stance: stance; // The current stance of the minion controlling its behaviour. 0 = Wander, 1 = Follow, 2 = Guard
	property MinionShield: minionShield; // If true, this minion will take damage in place of its master.

	uint MinionCreatureFlags;
	flagdef Taunt: MinionCreatureFlags, 0; // When enabled, this creature will force anything targeting its master to target it instead.

	bool teleportToMasterPending;
	bool permanentSummon;
	int checkMinionList;	
	Actor finalMaster;
	Class<Weapon> requireMasterWeapon;

	Default {
		MinionCreature.SummonTime 10 * 35;
		MinionCreature.FollowStopDistance 150;
		MinionCreature.Stance 0;

		-COUNTKILL;
		-MinionCreature.TAUNT;
	}

	virtual bool HasPlayerMaster() {
		return self.GetPlayerNumber() > -1;
	}
	
	virtual int GetPlayerNumber() {
		PlayerPawn playerMaster = PlayerPawn(self.GetFinalMaster());
		if (playerMaster) {
			return playerMaster.PlayerNumber();
		}
		return -1;
	}
	
	/**
	 * Gets the final master from a chain of masters always stopping at a player (in case players have a master set for some reason).
	 */
	virtual Actor GetFinalMaster() {
		if (!self.finalMaster) {
			if (self.master) {
				self.finalMaster = self.master;
				while (self.finalMaster.master && !PlayerPawn(self.finalMaster)) {
					self.finalMaster = self.finalMaster.master;
				}
			}
			if (PlayerPawn(self.finalMaster)) {
				self.SetFriendPlayer(players[PlayerPawn(self.finalMaster).PlayerNumber()]);
			}
		}
		if (!self.master) { // Recover lost master.
			self.master = self.finalMaster;
		}
		return self.finalMaster;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.permanentSummon = false;
		self.checkMinionList = 0;
		self.teleportToMasterPending = false;
	}

	override void PostBeginPlay() {
		self.summonedAngle = self.angle;

		// Minion Flags:
		if (PlayerPawn(self.GetFinalMaster())) {
			if (self.HasPlayerMaster()) {
				self.GetSchismPlayerInfo().AddMinion(self.GetPlayerNumber(), self);
			}
			
			if (!self.bFriendly) {
				self.bFriendly = true;
			}
			if (!self.bNeverRespawn) {
				self.bNeverRespawn = true;
			}
			if (!self.bDontCorpse) {
				self.bDontCorpse = true;
			}
		} else {
			if (!self.bCountKill) {
				self.bCountKill = true;
			}
		}

		super.PostBeginPlay();
	}
	
	override bool Used(Actor user) {
		bool isUsed = super.Used(user);
		if (self.master && user == self.master) {
			self.summonTime = 0;
		}
		return isUsed;
	}
	
	// Death and Destruction:
	override void Die(Actor source, Actor inflictor, int dmgflags) {
		self.RemoveFromMaster();
		super.Die(source, inflictor, dmgflags);
	}
	
	override void OnDestroy() {
		self.RemoveFromMaster();
		super.OnDestroy();
	}
	
	/** 
	 * Removes this minion from its (final player) master.
	 */
	virtual void RemoveFromMaster() {
		if (self.HasPlayerMaster()) {
			self.GetSchismPlayerInfo().RemoveMinion(self.GetPlayerNumber(), self);
		}
	}
	
	/** 
	 * Called by all minion actions that return a state, can be overidden to provide new states, etc.
	 */
	virtual state CheckMinionState() {
		return null;
	}
	
	// Minion Tick:
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		if (!self.GetFinalMaster()) {
			return;
		}
		
		// Teleport To Master:
		if (self.teleportToMasterPending) {
			double warpOffset =((Radius * 2) +(master.Radius * 2));
			if (A_Warp(AAPTR_MASTER, 0, warpOffset, 0, 0, WARPF_COPYVELOCITY, "Spawn") || A_Warp(AAPTR_MASTER, 0, -warpOffset, 0, 0, WARPF_COPYVELOCITY, "Spawn")) {
				self.target = self.master.target;
				self.teleportToMasterPending = false;
			}
		}
		
		// Despawn:
		if (!self.permanentSummon || !self.HasPlayerMaster()) {
			self.DecreaseSummonTime(1);
		}
		if (self.summonTime <= 0) {
			self.Despawn();
		}
		if (self.checkMinionList-- <= 0) {
			self.checkMinionList = 10 * 35;
			if (self.HasPlayerMaster() && !self.GetSchismPlayerInfo().IsMinion(self.GetPlayerNumber(), self)) {
				self.Despawn();
			}
		}
		if (self.requireMasterWeapon && self.GetFinalMaster()) {
			PlayerPawn playerPawn = PlayerPawn(self.GetFinalMaster());
			if (playerPawn) {
				PlayerInfo playerInfo = players[playerPawn.PlayerNumber()];
				if (playerInfo && !(playerInfo.readyWeapon is self.requireMasterWeapon)) {
					self.Despawn();
				}
			}
		}
		
		// Taunting:
		if (self.bTaunt && self.master && self.target) {
			Actor targetsTarget = self.target.target;
			if (targetsTarget == self.master) {
				target.target = self;
			}
		}
	}

    /**
	 * Despawns this minion sending it to its despawn (defaults to death) state.
	 */
	virtual void Despawn() {
		if (!SchismUtil.ActorInState(self, "Death") && !SchismUtil.ActorInState(self, "Despawn")) {
			self.SetStateLabel("Despawn");
		}
	}

	/**
	 * Called when this creature dies, can be overridden to add death logic.
	 */
	override void OnCreatureDie() {
		if (self.GetFinalMaster()) {
			self.Destroy();
		}
	}
	
	// Decrease Summon Time:
	action void DecreaseSummonTime(int amount) {
		invoker.summonTime -= amount;
	}
	
	// Teleport To Master(Pending):
	virtual void TeleportToMaster() {
		teleportToMasterPending = true;
	}
	
	// Should Chase Master (Following Movement):
	virtual bool ShouldChaseMaster() {
		if (!self.master) {
			return false;
		}
		if (self.Distance3D(self.master) <= self.followStopDistance) {
			return false;
		}
		return true;
	}
	
	// Minion Idle:
	action state A_MinionIdle() {
		state overrideState = invoker.CheckMinionState();
		if (overrideState) {
			return overrideState;
		}

		invoker.A_Look();
		if (!invoker.master) {
			return null;
		}
		if (invoker.stance == 0) {
			invoker.A_Wander();
			return null;
		}
		if (invoker.stance == 1) {
			return invoker.A_StayWithMaster();
		}
		return null;
	}
	
	// Minion Chase:
	action state A_MinionChase() {
		state overrideState = invoker.CheckMinionState();
		if (overrideState) {
			return overrideState;
		}
		
		if (invoker.master) {
			switch (invoker.stance) {
				case 1:
					if (!invoker.target) {
						return ResolveState("Follow");
					}
					break;
				case 2:
					return ResolveState("Guard");
			}
		}
		invoker.A_Chase();
		return null;
	}
	
	// Stay With Master:
	action state A_StayWithMaster() {
		state overrideState = invoker.CheckMinionState();
		if (overrideState) {
			return overrideState;
		}
		
		if (invoker.ShouldChaseMaster()) {
			return ResolveState("Follow");
		}
		return null;
	}
	
	// Follow Master:
	action state A_FollowMaster(double teleportDistance = 0) {
		state overrideState = invoker.CheckMinionState();
		if (overrideState) {
			return overrideState;
		}

		invoker.A_LookEx(LOF_NOJUMP);
		
		if (!invoker.ShouldChaseMaster()) {
			return ResolveState("Spawn");
		}
		
		if (teleportDistance && invoker.Distance3D(invoker.master) > teleportDistance) {
			invoker.TeleportToMaster();
		}
		
		invoker.target = invoker.master;
		invoker.A_Chase(null, null, CHF_NORANDOMTURN);
		return null;
	}
	
	// Minion Guard:
	action state A_MinionGuard() {
		state overrideState = invoker.CheckMinionState();
		if (overrideState) {
			return overrideState;
		}
		
		if (!invoker.master || invoker.stance != 2) {
			return ResolveState("Spawn");
		}
		if (invoker.target) {
			invoker.A_Chase("Melee", "Missile", CHF_DONTMOVE);
		} else {
			invoker.angle = invoker.summonedAngle;
		}
		return null;
	}
	
	// States:
	States {
		Guard:
			#### A 1 A_MinionGuard();
			Goto Spawn;
			
		Follow:
			#### A 1 A_FollowMaster();
			Goto Spawn;

		Despawn:
		Death:
			Stop;
	}
}
