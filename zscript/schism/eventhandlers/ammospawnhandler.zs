class SchismAmmoSpawnEventHandler : StaticEventHandler {
	SchismAmmoSpawnDirectory spawner;

	virtual SchismAmmoSpawnDirectory GetSpawner() {
		if (!self.spawner) {
			self.spawner = SchismAmmoSpawnDirectory.Get();
		}
		return self.spawner;
	}
	
	override void WorldThingSpawned(WorldEvent e) {
		super.WorldThingSpawned(e);
		
		if (!Inventory(e.thing) || Weapon(e.thing)) {
			return;
		}
		
		if (IsLargeAmmoASpawner(e.thing)) {
			self.GetSpawner().SpawnRandomAmmo(0, 1, e.thing);
		}
		else if (IsLargeAmmoBSpawner(e.thing)) {
			self.GetSpawner().SpawnRandomAmmo(1, 1, e.thing);
		}
		else if (IsLargeAmmoCSpawner(e.thing)) {
			self.GetSpawner().SpawnRandomAmmo(2, 1, e.thing);
		}
		else if (IsLargeAmmoAnySpawner(e.thing)) {
			self.GetSpawner().SpawnRandomAmmo(random(0, 2), 1, e.thing);
		}
		
		else if (IsSmallAmmoASpawner(e.thing)) {
			self.GetSpawner().SpawnRandomAmmo(0, 0, e.thing);
		}
		else if (IsSmallAmmoBSpawner(e.thing)) {
			self.GetSpawner().SpawnRandomAmmo(1, 0, e.thing);
		}
		else if (IsSmallAmmoCSpawner(e.thing)) {
			self.GetSpawner().SpawnRandomAmmo(2, 0, e.thing);
		}
		else if (IsSmallAmmoAnySpawner(e.thing)) {
			self.GetSpawner().SpawnRandomAmmo(random(0, 2), 0, e.thing);
		}
	}
	
	// Ammo A
	virtual bool IsSmallAmmoASpawner(Actor checkActor) {
		if (
			Clip(checkActor)
			|| GoldWandAmmo(checkActor)
			|| BlasterAmmo(checkActor)
			|| MaceAmmo(checkActor)
			|| ClipOfBullets(checkActor)
		) {
			return true;
		}
		
		String className = checkActor.GetClassName();
		className = className.MakeLower();
		return className.IndexOf("flare") >= 0 || className.IndexOf("bullet") >= 0;
	}
	
	virtual bool IsLargeAmmoASpawner(Actor checkActor) {
		if (
			ClipBox(checkActor)
			|| GoldWandHefty(checkActor)
			|| BlasterHefty(checkActor)
			|| MaceHefty(checkActor)
			|| Mana1(checkActor)
			|| Mana3(checkActor)
			|| BoxOfBullets(checkActor)
		) {
			return true;
		}
		
		if (!IsLargeAmmo(checkActor)) {
			return false;
		}
		
		String className = checkActor.GetClassName();
		className = className.MakeLower();
		return className.IndexOf("flare") >= 0 || className.IndexOf("bullet") >= 0;
	}
	
	// Ammo B
	virtual bool IsSmallAmmoBSpawner(Actor checkActor) {
		if (
			Shell(checkActor)
			|| CrossbowAmmo(checkActor)
		) {
			return true;
		}
		
		String className = checkActor.GetClassName();
		className = className.MakeLower();
		return className.IndexOf("tnt") >= 0 || className.IndexOf("plasma") >= 0;
	}
	
	virtual bool IsLargeAmmoBSpawner(Actor checkActor) {
		if (
			ShellBox(checkActor)
			|| CrossbowHefty(checkActor)
			|| Mana2(checkActor)
		) {
			return true;
		}
		
		if (!IsLargeAmmo(checkActor)) {
			return false;
		}
		
		String className = checkActor.GetClassName();
		className = className.MakeLower();
		return className.IndexOf("tnt") >= 0 || className.IndexOf("plasma") >= 0;
	}
	
	// Ammo C
	virtual bool IsSmallAmmoCSpawner(Actor checkActor) {
		if (
			Cell(checkActor)
			|| SkullRodAmmo(checkActor)
		) {
			return true;
		}
		
		String className = checkActor.GetClassName();
		className = className.MakeLower();
		return className.IndexOf("napalm") >= 0 || className.IndexOf("fuel can") >= 0 || className.IndexOf("tesla") >= 0 || className.IndexOf("spray") >= 0 || className.IndexOf("voodoo") >= 0;
	}
	
	virtual bool IsLargeAmmoCSpawner(Actor checkActor) {
		if (CellPack(checkActor)
			|| SkullRodHefty(checkActor)
			|| Mana3(checkActor)
		) {
			return true;
		}
		
		if (!IsLargeAmmo(checkActor)) {
			return false;
		}
		
		String className = checkActor.GetClassName();
		className = className.MakeLower();
		return className.IndexOf("napalm") >= 0 || className.IndexOf("fuel tank") >= 0 || className.IndexOf("tesla") >= 0 || className.IndexOf("spray") >= 0 || className.IndexOf("voodoo") >= 0;
	}
	
	
	// Ammo Any:
	virtual bool IsSmallAmmoAnySpawner(Actor checkActor) {
		String className = checkActor.GetClassName();
		className = className.MakeLower();
		return className.IndexOf("dehackedpickup") >= 0 || className.IndexOf("virus") >= 0 || className.IndexOf("heart") >= 0 || className.IndexOf("0gold") >= 0 || className.IndexOf("meat") >= 0 || className.IndexOf("ammo") >= 0 || className.IndexOf("clip") >= 0;
	}
	
	virtual bool IsLargeAmmoAnySpawner(Actor checkActor) {
		if (!IsLargeAmmo(checkActor)) {
			return false;
		}
		
		String className = checkActor.GetClassName();
		className = className.MakeLower();
		return className.IndexOf("sypth") >= 0 || className.IndexOf("virus") >= 0 || className.IndexOf("heart") >= 0 || className.IndexOf("0gold") >= 0 || className.IndexOf("meat") >= 0 || className.IndexOf("ammo") >= 0 || className.IndexOf("clip") >= 0;
	}
	
	
	// Is Large Ammo:
	virtual bool IsLargeAmmo(Actor checkActor) {
		Ammo ammo = Ammo(checkActor);
		if (!ammo) {
			return false;
		}
		return ammo && ammo.Amount > 10;
	}
}
