class SchismCloakEffect : SchismStatusEffect {
	bool originalNeverTarget;

	Default {
		PowerUp.Duration -10;
		SchismStatusEffect.ParticleName "Shadow";
	}
	
	override void ApplyEffect(Actor effectInflictor, Actor effectSource, int effectTime, Class<SchismStatusEffect> spread, int strength) {
		super.ApplyEffect(effectInflictor, effectSource, effectTime, spread, strength);
		if (!self.owner) {
			return;
		}

		// Owner Original Stats Tracking:
		self.originalNeverTarget = self.owner.bNeverTarget;
	}

	override void RefreshEffect(Actor effectInflictor, Actor effectSource, int effectTime, Class<SchismStatusEffect> spread, int strength) {
		super.RefreshEffect(effectInflictor, effectSource, effectTime, spread, strength);
		if (!self.owner) {
			return;
		}

		// Invisibility Power:
		SchismCloakEffectPower power = SchismCloakEffectPower(self.owner.GiveInventoryType("SchismCloakEffectPower"));
		if (power) {
			power.effectTics = self.effectTics * 4; // 4x to prevent blinking, etc. Gets removed on EndEffect.
		}

		// Never Target:
		if (self.strength >= 2) {
			self.owner.bNeverTarget = true;
		}

		// Clear Targets:
		if (self.strength >= 3) {
			ThinkerIterator iterator = ThinkerIterator.Create("Actor", STAT_DEFAULT);
			Actor actor;
			while (actor = Actor(iterator.next())) {
				if (actor.master != self.owner && actor.target == self.owner) {
					actor.target = null;
				}
			}
		}
	}
	
	override void EndEffect() {
		super.EndEffect();
		if (!self.owner) {
			return;
		}

		// Invisibility Power:
		self.owner.TakeInventory("SchismCloakEffectPower", 1);

		// Revert Owner Stats:
		self.owner.bNeverTarget = self.originalNeverTarget;
	}
}

class SchismCloakEffectPower : PowerInvisibility {
	Default {
		Inventory.MaxAmount 1;
		Powerup.Duration -40;
		Powerup.Strength 50; // Controls alpha.
		Powerup.Mode "Fuzzy";

		-Inventory.HUBPOWER;
		+Inventory.ALWAYSPICKUP;
		-Inventory.ADDITIVETIME;
		+INVENTORY.NOSCREENBLINK;
		+CANTSEEK;
		+SHADOW;
	}
}
