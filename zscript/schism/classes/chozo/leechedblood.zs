class LeechedBlood : HomingEffectProjectile {
	int healingAmount;

	Default {
		Scale 0.25;
		Radius 16;
		Height 10;
		Speed 20;
		Gravity 0;
		DamageFunction 0;
		Alpha 0.5;
		Renderstyle "Add";
		
		SeeSound "Chozo/LeechedBlood";
		DeathSound "Chozo/LeechedBlood";
		
		PROJECTILE;
		+RANDOMIZE;
		+SEEKERMISSILE;
		+RIPPER;
		+NOCLIP;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		healingAmount = 1;
	}
	
	override void OnImpact() {
		followTarget.A_DamageSelf(-healingAmount, "None", DMSS_FOILINVUL|DMSS_NOFACTOR|DMSS_NOPROTECT);
		A_StartSound("Chozo/LeechedBlood");
		super.OnImpact();
	}
	
	States {
		Spawn:
			ACHZ WXYZ 1 {
				return A_HomeToTarget();
			}
			Loop;
			
		Death:
			ACHZ Z 4;
			Stop;
	}
}