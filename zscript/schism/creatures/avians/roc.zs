class Roc : MinionCreature {
	Default {
		Tag "Roc";

		Health 250;
		Radius 26;
		Height 72;
		Speed 30;
		Scale 0.75;
		Gravity 0;
		PainChance 180;
		Damage 2;
		BloodColor "ff 00 00";

		MinionCreature.SummonTime 5 * 35;

		+FLOAT;
		
		SeeSound "Roc/See";
		PainSound "Roc/Pain";
		DeathSound "Roc/Death";
		ActiveSound "Roc/See";
		MeleeSound "Roc/Melee";
		Obituary "%o was plunged to their death by a Roc!";
		HitObituary "%o was plunged to their death by a Roc!";
	}

	action void A_RocAttack() {
		A_CreatureMelee(4);
		ThrustThingZ(0, 12, 0, 1);
		if (invoker.target) {
			invoker.target.AddZ(100, false);
		}
	}
	
	States {
		Spawn:
			ROCC AABBCCBB 4 {
				A_Look();
				return A_StayWithMaster();
			}
			Loop;
			
		Follow:
			ROCC DE 4 {
				A_Look();
				return A_FollowMaster(1000);
			}
			ROCC F 4 {
				A_StartSound("Roc/Fly");
				A_Look();
				return A_FollowMaster(1000);
			}
			ROCC E 4 {
				A_Look();
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			ROCC DEFE 4 {
				return A_MinionChase();
			}
			ROCC F 4 {
				A_StartSound("Roc/Fly");
				return A_MinionChase();
			}
			ROCC E 4 {
				return A_MinionChase();
			}
			Loop;
			
		Melee:
			ROCC G 8 A_FaceTarget();
			ROCC H 8 A_RocAttack();
			ROCC I 8;
			Goto See;
			
		Pain:
			ROCC J 6;
			ROCC J 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			ROCC KL 5;
			ROCC M 5 A_Scream();
			ROCC N 5;
			ROCC P 5 A_CreatureDie();
			ROCC P -1;
			Stop;
	}
}
