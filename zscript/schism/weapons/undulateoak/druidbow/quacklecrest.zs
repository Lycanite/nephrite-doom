class QuackleDruidCrest : WeaponPassive {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Druid Bow now has a hunting Quackle!";
		
		Inventory.Icon "DBW1G0";
		Inventory.PickupSound "DruidBow/Pickup";
		Inventory.RestrictedTo "UndulateOak", "UndulateOakCockatrice";
		WeaponUpgrade.ExtraAmmoClass "DruidArrow";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			DBW1 G 1 bright;
			Loop;
	}
}