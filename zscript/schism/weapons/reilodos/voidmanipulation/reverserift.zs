class ReverseRiftScroll : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Void Manipulation can now create a Reverse Rift at the cost of Havoc!";
		
		Inventory.Icon "REISK0";
		Inventory.PickupSound "VoidManipulation/Pickup";
		Inventory.RestrictedTo "Reilodos", "ReilodosHavocSerpent";
		WeaponUpgrade.ExtraAmmoClass "VoidEnergy";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			REIS K 1 bright;
			Loop;
	}
}


class VoidManipulationRiftReverse : VoidManipulationRift {
	Default {
		Alpha 1.0;

		RenderStyle "Add";

		VoidManipulationRift.LoopSound "VoidManipulation/VoidRift/Reverse";
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		energy = 4000;
		energyMax = 4000;
		energySustain = 4000;
		sustained = true;
		pullForce = 0;
		pullRange = 50;
		minionClass = "";
	}
	
	States {
		Spawn:
			VDRF ABCDEFGHIJKLMNOPQRSTUVWXY 1 bright {
				A_StartSound(invoker.loopSound, CHAN_BODY, CHANF_LOOPING);
				invoker.SpawnParticle("Void", 8, 3, (FRandom(-64, 64), FRandom(-64, 64), FRandom(-32, 32)), Random(0, 359), Random(-60, -10));
				return A_VoidRiftUpdate();
			}
			Loop;
			
		Death:
			VDRF ABCDEFGHIJKLMNOP 1 A_FadeOut(0.1);
			Stop;
	}
}


class ReverseRiftProjectile : TearRiftProjectile {
	Default {
		RenderStyle "Add";
	}
	
	Default {
		Radius 5;
		Height 7;
		Scale 1;
		
		DamageFunction random(1, 2);
		RenderStyle "Add";
		
		SeeSound "VoidManipulation/Fire/Rift/Reverse";
		DeathSound "VoidManipulation/Fire/Rift/Reverse";
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		riftClass = "VoidManipulationRiftReverse";
	}
	
	action void A_RiftProjectileUpdate() {
		if (invoker.height < 32) {
			invoker.height += 0.07;
		}
	}
	
	States {
		Spawn:
			VDPT ABCDEFGHI 1 A_RiftProjectileUpdate();
			Loop;
			
		Death:
			VDPT A 0 A_CreateRift();
		Expired:
			VDPT ABCDEFGHI 1 A_FadeOut(0.05);
			Loop;
	}
}
