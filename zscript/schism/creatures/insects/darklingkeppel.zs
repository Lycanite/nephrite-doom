class DarklingKeppel : Darkling {
	States {
		Spawn:
			DRKK CD 4 {
				A_SetLeaping(false);
				A_RandomWander();
				return A_DarklingLatchCheck();
			}
			Loop;
			
		See:
			DRKK CD 4 {
				A_SetLeaping(false);
				A_Chase();
				return A_DarklingLatchCheck();
			}
			Loop;
			
		Melee:
			DRKK E 4 A_FaceTarget();
			DRKK F 4 A_DarklingAttack();
			DRKK AB 4;
			Goto See;
			
		Missile:
			DRKK A 0 A_Jump(128, "See");
			DRKK F 3 A_FaceTarget();
			DRKK E 10 {
				A_SetLeaping(true);
				A_StartSound("Darkling/Melee");
				A_SkullAttack();
				ThrustThingZ(0, 6, 0, 1);
				for (int i = 0; i < 4; i++) {
					A_SpawnProjectile("FalseProjectile", 20, 0, 0, CMF_TRACKOWNER);
				}
			}
			DRKK F 8 {
				A_SetLeaping(false);
				A_Stop();
			}
			Goto See;
		
		Latching:
			DRKK H 4 {
				A_SetLeaping(false);
				return A_DarklingUnlatchCheck();
			}
			DRKK I 4 A_DarklingAttack();
			Loop;
			
		Pain:
			DRKK G 6 A_SetLeaping(false);
			DRKK B 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			DRKK J 5;
			DRKK K 5 A_Scream();
			DRKK L 5;
			DRKK M 5 A_NoBlocking();
			DRKK N 5;
		FadeOut:
			DRKK N 1 A_FadeOut(2);
			Loop;
	}
}
