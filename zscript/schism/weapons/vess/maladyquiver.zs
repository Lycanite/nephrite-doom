class MaladyQuiver : VessSpell {
	Default {
		Radius 20;
		Height 16;
		Scale 0.5;
		
		Inventory.PickupSound "MaladyQuiver/Pickup";
		Weapon.UpSound "MaladyQuiver/Up";
		Weapon.ReadySound "";
		SchismWeapon.FailSound "MaladyQuiver/Fire/Fail";
		
		Weapon.AmmoType1 "ToxicDrupe";
		
		SchismWeapon.PrimaryCost 1;
		SchismWeapon.SecondaryCost 1;
		SchismWeapon.SpecialACost 10;
		SchismWeapon.SpecialBCost 10;
		SchismWeapon.SpecialCCost 25;
		SchismWeapon.SpecialDCost 25;
		
		SchismWeapon.SpecialItemA "AgonyDartScroll";
		SchismWeapon.SpecialItemB "StunDartScroll";
		SchismWeapon.SpecialItemC "ScourgeDartScroll";
		SchismWeapon.SpecialItemD "PandemicDartScroll";
		SchismWeapon.PassiveItem "ContagionScroll";
		
		Inventory.PickupMessage "Deadly poisons seep through the pages of the Malady Quiver.";
		Obituary "%o died a slow and painful death to %k's Malady Quiver!";
		SchismWeapon.InsufficientAmmoAMessage "\caNot enough Toxic Drupes to cast this spell.";
		SchismWeapon.InsufficientAmmoBMessage "\caNot enough Anima to cast this spell.";
		SchismWeapon.NoSpecialAMessage "\caYou need an Agony Dart Scroll to cast this spell.";
		SchismWeapon.NoSpecialBMessage "\caYou need a Stun Dart Scroll to cast this spell.";
		SchismWeapon.NoSpecialCMessage "\caYou need a Scourge Dart Scroll to cast this spell.";
		SchismWeapon.NoSpecialDMessage "\caYou need a Pandemic Dart Scroll to cast this spell.";

		SchismWeapon.IdleBob 30;
		DualHand.MirrorOffHand true;
	}
	
	States {
		Spawn:
			VESW C 4 bright;
			Loop;
			
		MainHand.Select:
			VSH1 A 1 A_Raise(12);
			Loop;
			
		MainHand.Deselect:
			VSH1 A 1 A_Lower(12);
			Loop;
			
		MainHand.Ready:
			VSH1 AABBCCDDEEFF 3 {
				return A_SchismWeaponReady();
			}
			Loop;
			
		OffHand.Select:
		OffHand.Deselect:
		OffHand.Ready:
			VSH5 ABCDEFGH 5;
			Loop;
		
		OffHand.Ultimate:
			VSH5 NOPQ 3;
			Loop;
	}
}

#include "zscript/schism/weapons/vess/maladyquiver/maladydart.zs"
#include "zscript/schism/weapons/vess/maladyquiver/violentfever.zs"
#include "zscript/schism/weapons/vess/maladyquiver/agonydart.zs"
#include "zscript/schism/weapons/vess/maladyquiver/stundart.zs"
#include "zscript/schism/weapons/vess/maladyquiver/scourgedart.zs"
#include "zscript/schism/weapons/vess/maladyquiver/pandemicdart.zs"
#include "zscript/schism/weapons/vess/maladyquiver/contagion.zs"
#include "zscript/schism/weapons/vess/maladyquiver/godbreakerplague.zs"