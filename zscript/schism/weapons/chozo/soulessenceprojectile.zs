class SoulEssenceProjectile : SchismProjectile {
	int soulEssenceMin;
	int soulEssenceMax;

	GrimScythe grimScythe; // For Bone Armor passive.

	property SoulEssenceMin: soulEssenceMin;
	property SoulEssenceMax: soulEssenceMax;

	Default {
		SoulEssenceProjectile.SoulEssenceMin 1;
		SoulEssenceProjectile.SoulEssenceMin 3;

		-THRUGHOST;
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		if (self.soulEssenceMin > 0 && targetActor.bIsMonster && !targetActor.bFriendly) {
			if (specialDamage > 0 && targetActor.bIsMonster && !targetActor.bFriendly && !PlayerPawn(targetActor)) {
				for (int i = 0; i < random(self.soulEssenceMin, self.soulEssenceMax); i++) {
					ReapSoulEssence(targetActor);
				}
			}
		}
		return specialDamage;
	}
	
	virtual void ReapSoulEssence(Actor targetActor) {
		self.tracer = targetActor;

		// Soul Essence:
		Actor spawnedSoulEssence = A_SpawnProjectile("SoulEssenceFragment", 0, 0, 0, CMF_TRACKOWNER, 0, AAPTR_TRACER);
		if (spawnedSoulEssence) {
			spawnedSoulEssence.target = self.target;
			spawnedSoulEssence.tracer = targetActor;
			SoulEssenceFragment soulEssenceFragment = SoulEssenceFragment(spawnedSoulEssence);
			if (soulEssenceFragment) {
				soulEssenceFragment.followTarget = self.target;
			}
		}

		// Bone Armor (Grim Scythe Passive):
		if (self.grimScythe && self.grimScythe.GetPassiveLevel() > 0) {
			Actor spawnedBoneArmor = A_SpawnProjectile("GrimScytheBoneArmor", 0, 0, 0, CMF_TRACKOWNER, 0, AAPTR_TRACER);
			if (spawnedBoneArmor) {
				spawnedBoneArmor.target = self.target;
				spawnedBoneArmor.tracer = targetActor;
				GrimScytheBoneArmor grimScytheBoneArmor = GrimScytheBoneArmor(spawnedBoneArmor);
				if (grimScytheBoneArmor) {
					grimScytheBoneArmor.followTarget = self.target;
					grimScytheBoneArmor.armorAmount = self.grimScythe.GetPassiveLevel();
				}
			}
		}
	}
}