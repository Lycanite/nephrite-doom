class BloodDrupe : Ammo {
	Default {
		//$Category "Ammunition"
		Tag "Blood Drupe";
		Scale 0.5; 
		
		Inventory.PickupMessage "You gain a blood drupe!";
		Inventory.Amount 20;
		Inventory.MaxAmount 200;
		Ammo.BackpackAmount 300;
		Ammo.BackpackMaxAmount 300;
		
		Inventory.Icon "AVESA0";
		Inventory.PickupSound "Vess/Ammo/Blood";
		Inventory.RestrictedTo "Vess";
		
		RenderStyle "Add";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			AVES ABCB 4 bright;
			Loop;
	}
}


class BloodDrupeLarge : BloodDrupe {
	Default {
		//$Category "Ammunition"
		Tag "Large Blood Drupe";
		
		Scale 0.5;
		Inventory.PickupMessage "A gorged blood drupe!";
		Inventory.Amount 100;
		
		RenderStyle "Add";
	}

	States {
		Spawn:
			AVES DEFE 4 bright;
			Loop;
	}
}


class LustDrupe : Ammo {
	Default {
		//$Category "Ammunition"
		Tag "Lust Drupe";
		Scale 0.5; 
		
		Inventory.PickupMessage "You gain lust drupe!";
		Inventory.Amount 20;
		Inventory.MaxAmount 200;
		Ammo.BackpackAmount 300;
		Ammo.BackpackMaxAmount 300;
		
		Inventory.Icon "AVESG0";
		Inventory.PickupSound "Vess/Ammo/Lust";
		Inventory.RestrictedTo "Vess";
		
		RenderStyle "Add";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			AVES GHIH 4 bright;
			Loop;
	}
}


class LustDrupeLarge : LustDrupe {
	Default {
		//$Category "Ammunition"
		Tag "Large Lust Drupe";
		
		Scale 0.5;
		Inventory.PickupMessage "Unending lust!";
		Inventory.Amount 100;
		
		RenderStyle "Add";
	}

	States {
		Spawn:
			AVES JKLK 4 bright;
			Loop;
	}
}


class ToxicDrupe : Ammo {
	Default {
		//$Category "Ammunition"
		Tag "Toxic Drupe";
		Scale 0.5; 
		
		Inventory.PickupMessage "You gain toxic drupe!";
		Inventory.Amount 20;
		Inventory.MaxAmount 200;
		Ammo.BackpackAmount 300;
		Ammo.BackpackMaxAmount 300;
		
		Inventory.Icon "AVESM0";
		Inventory.PickupSound "Vess/Ammo/Toxic";
		Inventory.RestrictedTo "Vess";
		
		RenderStyle "Add";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			AVES MNON 4 bright;
			Loop;
	}
}


class ToxicDrupeLarge : ToxicDrupe {
	Default {
		//$Category "Ammunition"
		Tag "Large Toxic Drupe";
		
		Scale 0.5;
		Inventory.PickupMessage "Uncancellable toxicity!";
		Inventory.Amount 100;
	}

	States {
		Spawn:
			AVES PQRQ 4 bright;
			Loop;
	}
}


class Anima : Ammo {
	Default {
		//$Category "Ammunition"
		Tag "Anima";
		
		Scale 0.5;
		Inventory.PickupMessage "You gain a rush of anima!";
		Inventory.Amount 20;
		Inventory.MaxAmount 200;
		Ammo.BackpackAmount 0;
		Ammo.BackpackMaxAmount 200;
		
		Inventory.Icon "AVESS0";
		Inventory.PickupSound "Vess/Ammo";
		Inventory.RestrictedTo "Vess";
		
		RenderStyle "Add";
		
		+FLOATBOB;
		+INVENTORY.IGNORESKILL;
	}

	States {
		Spawn:
			AVES STU 4 bright;
			Loop;
	}
}