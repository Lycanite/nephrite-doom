class FormerPrivate : Creature {
	Default {
		Tag "Former Private";
		Scale 1;
		Health 20;
		Radius 20;
		Height 56;
		Mass 100;
		Speed 8;
		Gravity 1;
		PainChance 200;
		BloodColor "ff 00 00";
		Species "Former";

        DropItem "Clip";

		SeeSound "Former/See";
		PainSound "Former/Pain";
		DeathSound "Former/Death";
		ActiveSound "Former/See";
		AttackSound "FormerPrivate/Fire";
		Obituary "%o was shot by a Former Private!";

		+NOINFIGHTSPECIES;
    }

    States {
        Spawn:
			FPRV AB 10 A_Look();
			Loop;
			
		See:
			FPRV AABBCCDD 4 A_Chase();
			Loop;
			
		Missile:
			FPRV E 10 A_FaceTarget();
			FPRV F 8 A_CreatureHitscan(5, 15, 1);
            FPRV E 8;
			Goto See;
			
		Pain:
			FPRV G 3;
			FPRV G 3 A_Pain();
			Goto See;
			
		Death:
			FPRV H 0 A_Jump(128, "Death2");
			FPRV H 5;
			FPRV I 5 A_Scream();
			FPRV J 5 A_CreatureDie();
			FPRV K 5;
			FPRV L -1;
			Stop;
		Death2:
			FPRV H 5;
			FPRV I 5 A_Scream();
			FPRV J 5 A_CreatureDie();
			FPRV K 5;
			FPRV L -1;
			Stop;
			
		Raise:
			FPRV KJIH 5;
			Goto See;
    }
}