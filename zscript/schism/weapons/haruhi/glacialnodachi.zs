class GlacialNodachi : SchismWeapon {
	int primaryCostOffset;
	bool mirroredAttack;
	bool verticalIceSlice;
	
	Default {
		Radius 20;
		Height 16;
		Scale 0.5;
		
		Inventory.RestrictedTo "Haruhi";
		
		Inventory.PickupSound "GlacialNodachi/Pickup";
		Weapon.UpSound "GlacialNodachi/Up";
		Weapon.ReadySound "";
		SchismWeapon.FailSound "GlacialNodachi/Fire/Fail";
		
		Weapon.AmmoType1 "GlacialCharge";
		Weapon.AmmoType2 "PranaEnergy";
		
		SchismWeapon.PrimaryCost 1;
		SchismWeapon.SecondaryCost 1;
		SchismWeapon.SpecialACost 25;
		SchismWeapon.SpecialBCost 25;
		SchismWeapon.SpecialCCost 5;
		SchismWeapon.SpecialDCost 50;
		
		SchismWeapon.SpecialItemA "IcicleFractal";
		SchismWeapon.SpecialItemB "BlizzardFractal";
		SchismWeapon.SpecialItemC "ShatterFractal";
		SchismWeapon.SpecialItemD "EviscerateFractal";
		SchismWeapon.PassiveItem "IcicleFractal";
		// SchismWeapon.PassiveItem "ArcticFractal";
		
		Inventory.PickupMessage "The icy Glacial Nodachi is yours to weild!";
		Obituary "%o was crystalised by %k's Glacial Nodachi!";
		SchismWeapon.InsufficientAmmoAMessage "\caNot enough Glacial Charges to use this ability.";
		SchismWeapon.InsufficientAmmoBMessage "\caNot enough Prana Energy to use this ability.";
		SchismWeapon.NoSpecialAMessage "\caYou need an Icicle Fractal to use this ability.";
		SchismWeapon.NoSpecialBMessage "\caYou need a Blizzard Fractal to use this ability.";
		SchismWeapon.NoSpecialCMessage "\caYou need a Shatter Fractal to use this ability.";
		SchismWeapon.NoSpecialDMessage "\caYou need an Eviscerate Fractal to use this ability.";

		SchismWeapon.IdleBob 30;

		+WEAPON.AMMO_OPTIONAL;
	}
	
	action void A_OnMirroredAttack() {
		A_OverlayFlags(0, PSPF_FLIP|PSPF_MIRROR, invoker.mirroredAttack);
		invoker.mirroredAttack = !invoker.mirroredAttack;
	}
	
	
	// Primary Ice Slice:
	action state A_IceSliceAlterate() {
		if (invoker.verticalIceSlice) {
			invoker.verticalIceSlice = !invoker.verticalIceSlice;
			return ResolveState("Fire.Vertical");
		}
		invoker.verticalIceSlice = !invoker.verticalIceSlice;
		return null;
	}
	
	action void A_IceSlice() {
		if (CountInv(A_GetAmmoClass(1)) >= A_GetAmmoCost(1)) {
			if (invoker.primaryCostOffset-- <= 0) {
				A_SpendAmmo(1);
				invoker.primaryCostOffset = 2;
			}
			A_FireProjectile("GlacialNodachiProjectile", 0, false);
			A_FireProjectile("GlacialNodachiProjectile", 15, false, -20);
			A_FireProjectile("GlacialNodachiProjectile", -15, false, 20);
		}
		int meleeDamage = 10;
		if (CountInv("PowerStrength")) {
			meleeDamage = 20;
		}
		A_CustomPunch(meleeDamage, true, CPF_PULLIN|CPF_NOTURN, "BulletPuff", 96, 0, 0, "ArmorBonus", "GlacialNodachi/Melee", "GlacialNodachi/Miss");
	}
	
	
	// Secondary Frigid Blast:
	action void A_FrigidBlast() {
		if (!A_SpendAmmo(2)) {
			return;
		}
		A_FireProjectile("GlacialNodachiFrigidProjectile", 0, false, 0, 0, FPF_NOAUTOAIM, 0);
		A_FireProjectile("GlacialNodachiFrigidProjectile", 0, false, -20, -30, FPF_NOAUTOAIM, 0);
		A_FireProjectile("GlacialNodachiFrigidProjectile", 0, false, 20, -30, FPF_NOAUTOAIM, 0);
		A_FireProjectile("GlacialNodachiFrigidProjectile", 0, false, -30, 10, FPF_NOAUTOAIM, 0);
		A_FireProjectile("GlacialNodachiFrigidProjectile", 0, false, 30, 10, FPF_NOAUTOAIM, 0);
		A_FireProjectile("GlacialNodachiFrigidProjectile", 0, false, 0, 20, FPF_NOAUTOAIM, 0);
	}
	
	
	// Special 1 Icicle Valley:
	action void A_IcicleValley() {
		if (!A_CheckSpecial(1) || !A_SpendAmmo(3)) {
			return;
		}
		A_FireProjectile("GlacialNodachiIcicleSeedProjectile", 0, false);
	}
	
	
	// Special 2 Blizzard:
	action void A_Blizzard() {
		if (!A_CheckSpecial(2) || !A_SpendAmmo(4)) {
			return;
		}
		for (int i = 0; i < 360; i += 20) {
			A_FireProjectile("GlacialNodachiBlizzardProjectile", i, false);
		}
		A_GiveInventory("HaruhiStealth");
	}
	
	
	// Special 3 Shattering Strike:
	action void A_ShatteringStrike() {
		if (!A_CheckSpecial(3) || !A_SpendAmmo(5)) {
			return;
		}
		A_StartSound("GlacialNodachi/Shatter");
		for (int i = 0; i < 15; i++) {
			invoker.SpawnParticleFromActor(invoker.owner, "Frost", 2 * TICRATE, 1, (FRandom(-40, 40), FRandom(-40, 40), FRandom(20, 60)), Random(0, 359), Random(-20, 20));
			A_SpawnItemEx("IceChunk", Random(-40, 40), Random(-40, 40), Random(20, 60), 0, 0, 0, 0, SXF_CLIENTSIDE);
		}
		
		ThinkerIterator chillIterator = ThinkerIterator.Create("SchismChillDebuff");
		SchismChillDebuff chill;
		while (chill = SchismChillDebuff(chillIterator.Next())) {
			if (chill.owner && chill.owner.health > 0 && chill.InflictedBy(invoker.owner)) {
				chill.ChillBurst(false, 200);
			}
		}
	}
	
	
	// Special 4 Eviscerate: TODO Replace
	action state A_EviscerateOld(bool useAmmo = false) {
		if (useAmmo) {
			if (!A_CheckSpecial(4) || !A_SpendAmmo(6)) {
				return ResolveState("Ready");
			}
		}
		
		A_FireProjectile("GlacialNodachiEviscerateProjectile", 0, false);
		A_FireProjectile("GlacialNodachiEviscerateProjectile", 15, false, -20);
		A_FireProjectile("GlacialNodachiEviscerateProjectile", -15, false, 20);
		
		return null;
	}
	
	
	// Ultimate Eviscerate:
	action void A_Eviscerate() {
		A_FireProjectile("GlacialNodachiEviscerateProjectile", 0, false);
		A_FireProjectile("GlacialNodachiEviscerateProjectile", 15, false, -20);
		A_FireProjectile("GlacialNodachiEviscerateProjectile", -15, false, 20);
	}
	
	
	States {
		Spawn:
			GNOD Z 4 bright;
			Loop;
			
		Select:
			GNOD A 1 A_Raise(12);
			Loop;
			
		Deselect:
			GNOD A 1 A_Lower(12);
			Loop;
			
		Ready:
			GNOD AABBCCBB 3 {
				return A_SchismWeaponReady();
			}
			Loop;
			
		Fire:
			GNOD A 0 {
				A_ClearBobbing();
				return A_IceSliceAlterate();
			}
			GNOD A 0 A_OnMirroredAttack();
			GNOD DE 2;
			GNOD F 2 A_IceSlice();
			GNOD GH 2;
		Fire.Vertical:
			GNOD IJ 2;
			GNOD K 2 A_IceSlice();
			GNOD A 0 A_OnMirroredAttack();
			GNOD LM 2;
			Goto Ready;
			
		AltFire:
			GNOD A 0 A_ClearBobbing();
			GNOD NO 4;
			GNOD P 8 A_FrigidBlast();
			GNOD N 4;
			Goto Ready;
		
		User1:
			GNOD A 0 A_ClearBobbing();
			GNOD IJ 4;
			GNOD K 4 A_IcicleValley();
			GNOD LM 4;
			Goto Ready;
			
		User2:
			GNOD A 0 A_ClearBobbing();
			GNOD N 4;
			GNOD O 4 A_Blizzard();
			GNOD P 4;
			goto Ready;
		
		User3:
			GNOD NO 6;
			GNOD P 16 A_ShatteringStrike();
			GNOD N 6;
			goto Ready;
		
		User4:
			GNOD A 0 {
				A_ClearBobbing();
				A_OnMirroredAttack();
			}
			GNOD DE 2;
			GNOD F 2 {
				return A_EviscerateOld(true);
			}
			GNOD GH 2;
			GNOD A 0 A_OnMirroredAttack();
			GNOD IJ 2;
			GNOD K 2 A_EviscerateOld();
			GNOD LM 2;
			GNOD DE 2;
			GNOD F 2 A_EviscerateOld();
			GNOD GH 2;
			Goto Ready;
		
		Ultimate:
			GNOD A 0 {
				A_ClearBobbing();
				A_OnMirroredAttack();
			}
			GNOD DE 2;
			GNOD F 2 A_Eviscerate();
			GNOD GH 2;
			GNOD A 0 A_OnMirroredAttack();
			GNOD IJ 2;
			GNOD K 2 A_Eviscerate();
			GNOD LM 2;
			GNOD DE 2;
			GNOD F 2 A_Eviscerate();
			GNOD GH 2;
			Goto Ready;
	}
}

#include "zscript/schism/weapons/haruhi/glacialnodachi/iceslice.zs"
#include "zscript/schism/weapons/haruhi/glacialnodachi/icicle.zs"
#include "zscript/schism/weapons/haruhi/glacialnodachi/blizzard.zs"
#include "zscript/schism/weapons/haruhi/glacialnodachi/shatter.zs"
#include "zscript/schism/weapons/haruhi/glacialnodachi/eviscerate.zs"
