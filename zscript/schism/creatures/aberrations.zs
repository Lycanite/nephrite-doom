#include "zscript/schism/creatures/aberrations/tariaksuq.zs"

#include "zscript/schism/creatures/aberrations/trite.zs"
#include "zscript/schism/creatures/aberrations/adrantrite.zs"

#include "zscript/schism/creatures/aberrations/grigori.zs"

#include "zscript/schism/creatures/aberrations/grell.zs"
#include "zscript/schism/creatures/aberrations/nomadrum.zs"
#include "zscript/schism/creatures/aberrations/nomadrumverdant.zs"

#include "zscript/schism/creatures/aberrations/dologroth.zs"

#include "zscript/schism/creatures/aberrations/astaroth.zs"
#include "zscript/schism/creatures/aberrations/asakkuastaroth.zs"
#include "zscript/schism/creatures/aberrations/anzuastaroth.zs"
#include "zscript/schism/creatures/aberrations/adranastaroth.zs"

#include "zscript/schism/creatures/aberrations/asakkuasnarax.zs"
#include "zscript/schism/creatures/aberrations/anzuasnarax.zs"