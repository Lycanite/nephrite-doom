class Umibas : Creature {
	Default {
		Tag "Umibas";
		
		Health 1000;
		Radius 40;
		Height 64;
		Speed 14;
		Mass 1000;
		FloatSpeed 14;
		Scale 1;
		Gravity 1;
		PainChance 120;
		Damage 1;
		Alpha 1;
		BloodColor "ff 66 00";
		
		SeeSound "Umibas/See";
		PainSound "Umibas/Pain";
		DeathSound "Umibas/Death";
		ActiveSound "Umibas/Idle";
		MeleeSound "Umibas/Melee";
		Obituary "%o was devoured by a Umibas!";
		HitObituary "%o was devoured by a Umibas!";

		+DONTTHRUST;
		+MISSILEMORE;
	}
	
	States {
		Spawn:
			UMIB ABCB 6 A_Look();
			Loop;

		See:
			UMIB DEFGFE 6 A_Chase();
			Loop;

		Melee:
			UMIB I 4 A_FaceTarget();
			UMIB JK 6 A_CreatureMelee(20, 40);
			Goto See;

		Missile:
			UMIB HIJ 6 A_FaceTarget();
			UMIB K 6 {
				for (int i = 0; i < 8; i++) {
					A_SpawnProjectile("MagmaProjectile", 16, 0, i * 45, Random(-10, 10));
				}
			}
			UMIB I 6;
			Goto See;

		Pain:
			UMIB L 3;
			UMIB L 3 A_Pain();
			Goto See;

		Death:
		XDeath:
			UMIB LMN 6;
			UMIB O 5 A_Scream();
			UMIB P 5 A_CreatureDie();
			UMIB QRS 5;
			UMIB T -1;
			Stop;
		}
}