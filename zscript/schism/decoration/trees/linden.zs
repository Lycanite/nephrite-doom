class SchismTreeLinden : SchismTree {
	Default {
		Tag "Linden Tree";
		SchismTree.SpringVariants 2;
		SchismTree.SummerVariants 2;
		SchismTree.AutumnVariants 2;
		SchismTree.WinterVariants 2;
	}
	
	States {
		Spring.1:
			TR00 A -1;
			Stop;

		Spring.2:
			TR00 B -1;
			Stop;

		Summer.1:
			TR00 E -1;
			Stop;

		Summer.2:
			TR00 F -1;
			Stop;

		Autumn.1:
			TR00 I -1;
			Stop;

		Autumn.2:
			TR00 J -1;
			Stop;

		Winter.1:
			TD00 M -1;
			Stop;

		Winter.2:
			TD00 N -1;
			Stop;

		Death.Spring.1:
			TD00 A -1;
			Stop;

		Death.Spring.2:
			TD00 B -1;
			Stop;

		Death.Summer.1:
			TD00 E -1;
			Stop;

		Death.Summer.2:
			TD00 F -1;
			Stop;

		Death.Autumn.1:
			TD00 I -1;
			Stop;

		Death.Autumn.2:
			TD00 J -1;
			Stop;

		Death.Winter.1:
			TD00 M -1;
			Stop;

		Death.Winter.2:
			TD00 N -1;
			Stop;
	}
}