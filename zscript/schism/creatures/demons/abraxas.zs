class Abraxas : Creature {	
	Default {
		Tag "Abraxas";
		Health 4000;
		Scale 1.5;
		Radius 40;
		Height 110;
		Mass 1000;
		Speed 8;
		PainChance 20;
		
		SeeSound "Abraxas/See";
		ActiveSound "Abraxas/Idle";
		PainSound "Abraxas/Pain";
		MeleeSound "Abraxas/Melee";
		DeathSound "Abraxas/Death";
		Obituary "%o was no match for an Abraxas.";
		HitObituary "%o was ripped apart by an Abraxas.";
		
		+BOSS;
		+BOSSDEATH;
		+NORADIUSDMG;
		+DONTMORPH;
	}
	
	action state A_RandomMissile() {
		int randomValue = random(1, 255);
		if (randomValue >= 128) {
			return ResolveState("MissileCannon");
		}
		return ResolveState("MissileBall");
	}
	
	States {
		Spawn:
			ABRX AB 10 A_Look();
			Loop;
			
		See:
			ABRX AABBCCDD 3 A_Chase();
			Loop;
			
		Melee:
			ABRX BC 5 A_FaceTarget();
			ABRX D 5 bright A_CreatureMelee(20, 40, "Hellflame", "Nether");
			Goto See;
			
		Missile:
			ABRX B 0  A_RandomMissile();
			Goto See;
		MissileBall:
			ABRX EF 8 bright A_FaceTarget();
			ABRX G 3 bright {
				A_SpawnProjectile("HellfireballProjectile", 28, 0, 0, 0);
				A_SpawnProjectile("HellfireballProjectile", 28, 0, 4, 0);
				A_SpawnProjectile("HellfireballProjectile", 28, 0, -4, 0);
			}
			ABRX H 3 bright {
				if (random(1, 255) <= 64) {
					A_Jump(256, "MissileCannon");
				}
			}
			ABRX IJ 8 bright A_FaceTarget();
			ABRX K 3 bright {
				A_SpawnProjectile("HellfireballProjectile", 28, 0, 0, 0);
				A_SpawnProjectile("HellfireballProjectile", 28, 0, 4, 0);
				A_SpawnProjectile("HellfireballProjectile", 28, 0, -4, 0);
			}
			ABRX L 3 bright {
				if (random(1, 255) <= 64) {
					A_Jump(256, "MissileCannon");
				}
			}
			Goto See;
		MissileCannon:
			ABRX MN 8 bright A_FaceTarget();
			ABRX O 4 bright A_SpawnProjectile("VainHellCannonProjectile", 28, 0, 0, 0);
			ABRX P 4 bright;
			Goto See;
			
		Melee:
			ABRX EF 8 bright A_FaceTarget();
			ABRX G 3 bright A_CustomMeleeAttack(random(10,30));
			ABRX H 3 bright;
			Goto See;
			
		Pain:
			ABRX Q 2;
			ABRX Q 2 A_Pain();
			Goto See;
			
		Death:
		    ABRX R 6 bright;
			ABRX S 6 bright A_Scream();
			ABRX T 6 bright;
			ABRX U 6 bright A_NoBlocking();
			ABRX VW 6 bright;
			ABRX X 6 bright A_SpawnProjectile("HellfireSoul", 86, 0, 0, 2, 90);
			ABRX YZ 6 bright;
			ABRX [ 6 bright A_Fall();
			Stop;
	}
}