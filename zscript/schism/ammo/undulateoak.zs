class LifeSeed : Ammo {
	Default {
		//$Category "Ammunition"
		
		Tag "Life Seed";
		Scale 0.5;
		Inventory.PickupMessage "It's a Seed of Life, perfect for a hungry magic chicken!";
		Inventory.Amount 20;
		Inventory.MaxAmount 200;
		Ammo.BackpackAmount 300;
		Ammo.BackpackMaxAmount 300;
		
		Inventory.Icon "AUOKA0";
		Inventory.PickupSound "UndulateOak/Ammo";
		Inventory.RestrictedTo "UndulateOak", "UndulateOakCockatrice";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			AUOK ABC 4;
			Loop;
	}
}


class LifeSeedLarge : LifeSeed {
	Default {
		//$Category "Ammunition"
		
		Tag "Large Life Seed";
		Scale 0.5;
		Inventory.PickupMessage "A large Seed of Life full of Nature magic!";
		Inventory.Amount 250;
	}
	
	States {
		Spawn:
			AUOK DEFE 4;
			Loop;
	}
}


class DruidArrow : Ammo {
	Default {
		//$Category "Ammunition"
		
		Tag "Druid Arrows";
		Scale 0.5;
		Inventory.PickupMessage "A quiver full of ancient druid arrows!";
		Inventory.Amount 20;
		Inventory.MaxAmount 200;
		Ammo.BackpackAmount 300;
		Ammo.BackpackMaxAmount 300;
		
		Inventory.Icon "AUOKG0";
		Inventory.PickupSound "UndulateOak/Ammo";
		Inventory.RestrictedTo "UndulateOak", "UndulateOakCockatrice";
	}

	States {
		Spawn:
			AUOK GHIH 4;
			Loop;
	}
}


class DruidArrowLarge : DruidArrow {
	Default {
		//$Category "Ammunition"
		
		Tag "Large Druid Arrows";
		Scale 0.5;
		Inventory.PickupMessage "A large quiver overflowing with druid arrows!";
		Inventory.Amount 250;
	}
	
	States {
		Spawn:
			AUOK JKL 4;
			Loop;
	}
}


class StormSphere : Ammo {
	Default {
		//$Category "Ammunition"
		
		Tag "Storm Sphere";
		Scale 0.35;
		Inventory.PickupMessage "A mystical sphere that contains a small dimensional storm!";
		Inventory.Amount 20;
		Inventory.MaxAmount 200;
		Ammo.BackpackAmount 300;
		Ammo.BackpackMaxAmount 300;
		
		Inventory.Icon "AUOKM0";
		Inventory.PickupSound "UndulateOak/Ammo";
		Inventory.RestrictedTo "UndulateOak", "UndulateOakCockatrice";
	}

	States {
		Spawn:
			AUOK MNO 4;
			Loop;
	}
}


class StormSphereLarge : StormSphere {
	Default {
		//$Category "Ammunition"
		
		Tag "Large Storm Sphere";
		Scale 0.35;
		Inventory.PickupMessage "A mystical sphere that contains a large dimensional storm!";
		Inventory.Amount 250;
	}
	
	States {
		Spawn:
			AUOK PQR 4;
			Loop;
	}
}


class Ectoplasm : Ammo {
	Actor followTarget;
	
	Default {
		//$Category "Ammunition"
		
		Tag "Ectoplasm";
		Radius 64;
		Height 10;
		Scale 0.25;
		Gravity 1;
		
		Inventory.PickupMessage "Ectoplasm, powerful life essence.";
		Inventory.Amount 2;
		Inventory.MaxAmount 200;
		Ammo.BackpackAmount 0;
		Ammo.BackpackMaxAmount 200;
		
		Inventory.Icon "AUOKS0";
		Inventory.PickupSound "UndulateOak/Ectoplasm";
		Inventory.RestrictedTo "UndulateOak", "UndulateOakCockatrice";
		
		Renderstyle "Add";
		
		+FLOATBOB;
		+RANDOMIZE;
		+INVENTORY.ALWAYSPICKUP;
		+INVENTORY.IGNORESKILL;
	}
	
	override void Tick() {
		super.Tick();
		if (self.followTarget)
		{
			float distance = Distance3D(self.followTarget);
			if (distance <= 600)
			{
				float thrust =(1 -(distance / 600));
				self.Thrust(thrust, self.AngleTo(self.followTarget));
				self.AddZ(thrust * 0.25, false);
			}
		}
		else
		{
			self.alpha = 0;
		}
	}
	
	override void OnDestroy() {
		super.OnDestroy();
		if (self.followTarget)
		{
			UndulateOak oakTarget = UndulateOak(self.followTarget);
			if (oakTarget && oakTarget.activeEctoplasm > 0)
			{
				oakTarget.activeEctoplasm--;
			}
		}
	}
	
	action void SetFollowTarget(Actor targetActor) {
		invoker.followTarget = targetActor;
		UndulateOak oakTarget = UndulateOak(invoker.followTarget);
		if (oakTarget && oakTarget.activeEctoplasm < 100)
		{
			oakTarget.activeEctoplasm++;
		}
	}
	
	action void CheckEctoplasmCap() {
		UndulateOak oakTarget = UndulateOak(invoker.followTarget);
		if (oakTarget && oakTarget.activeEctoplasm >= 100)
		{
			invoker.alpha = 0;
		}
	}

	States {
		Spawn:
			AUOK ST 4 A_FadeOut(0.01);
			AUOK U 4 {
				CheckEctoplasmCap();
				A_FadeOut(0.01);
			}
			Loop;
	}
}