class LichScepterArmyProjectile : SchismProjectile {
	Default {
		Radius 10;
		Height 10;
		Speed 15;
		DamageFunction 20;
		Gravity 0;
		Alpha 1;
		Renderstyle "Add";
		Projectile;
		
		SeeSound "LichScepter/Fire/Army";
		DeathSound "";
		
		+SKYEXPLODE;
	}
	
	action void A_CallStorm() {
		int stormSize = 3;
		int cloudSize = 200;
		for (int x = -(stormSize * cloudSize); x <= stormSize * cloudSize; x += cloudSize) {
			for (int y = -(stormSize * cloudSize); y <= stormSize * cloudSize; y += cloudSize) {
				A_SpawnItemEx("LichScepterArmyCloud", x, y, 0, 0, 0, 0, 0);
			}
		}
	}
	
	States {
		Spawn:
			LSP1 ABCDEF 6 bright;
			Loop;
			
		Death:
			LSP1 G 4 bright A_CallStorm();
			LSP1 HIJKL 4 bright;
			Stop;
	}
}

class LichScepterArmyCloud : SchismProjectile {
	Default {
		Radius 1;
		Height 1;
		Scale 2;
		Speed 15;
		DamageFunction 2;
		Alpha 0.5;
		Gravity 0;
		
		SeeSound "";
		DeathSound "";
		
		+DONTSPLASH;
		+SPAWNCEILING;
		+CEILINGHUGGER;
		+NODAMAGETHRUST;
		+PAINLESS;
	}
	
	action void A_RaiseUndead() {
		if (Random(0, 60) == 60) {
			bool spawned;
			Actor projectile;
			[spawned, projectile] = A_SpawnItemEx("LichScepterArmySummon", random(-100, 100), random(-100, 100), 0, 0, 0, 0, 0);
			LichScepterArmySummon lichScepterArmySummon = LichScepterArmySummon(projectile);
			if (lichScepterArmySummon) {
				switch (Random(0, 3)) {
					case 1:
						lichScepterArmySummon.summonType = "Ossifex";
						break;
					case 2:
						lichScepterArmySummon.summonType = "GrimReaver";
						break;
					case 3:
						lichScepterArmySummon.summonType = "Ghoul";
						break;
				}
			}
		}
	}
	
	States {
		Spawn:
			NCLG FGHIJ 4 bright;
			NCLG ABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDE 4 bright A_RaiseUndead();
		Death:
			NCLG F 4 bright;
			NCLG GHIJ 4 bright;
			Stop;
	}
}

class LichScepterArmySummon : SchismProjectile {
	Default {
		Radius 16;
		Height 10;
		Scale 1;
		Speed 12;
		DamageFunction 0;
		Gravity 1;

		SchismProjectile.SummonType "Draugr";
		SchismProjectile.SummonStance 0; // Wander
		SchismProjectile.SummonFlags SXF_NOCHECKPOSITION;
		
		+RANDOMIZE;
		+MISSILE;
	}
	
	States {
		Spawn:
			BONE ABCDEFGH 4;
			Loop;
		Death:
			NRNP E 4 {
				invoker.SummonMinion();
				for (int i = 0; i < 20; i += 20) {
					invoker.A_SpawnItemEx("SchismGoreBone", 0, 0, 10, random(-6, 6), random(-6, 6), random(10, 40), i, SXF_CLIENTSIDE);
				}
			}
			Stop;
	}
}
