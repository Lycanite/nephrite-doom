class GapingRiftScroll : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Void Manipulation can now create a Gaping Rift at the cost of Havoc!";
		
		Inventory.Icon "REISI0";
		Inventory.PickupSound "VoidManipulation/Pickup";
		Inventory.RestrictedTo "Reilodos", "ReilodosHavocSerpent";
		WeaponUpgrade.ExtraAmmoClass "VoidEnergy";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			REIS I 1 bright;
			Loop;
	}
}


class VoidManipulationRiftGaping : VoidManipulationRift {
	Default {
		Radius 32;
		Height 80;
		Scale 1;

		VoidManipulationRift.LoopSound "VoidManipulation/VoidRift/Gaping";
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		havocEnergy = 0;
		
		energy = 300;
		energyMax = 300;
		energySustain = 300;
		sustained = true;
		pullRange = 500;
		sizeScale = 2;
		summonCountdown = 35;
		minionClass = "AdranAstaroth";
	}
}


class GapingRiftProjectile : TearRiftProjectile {
	Default {
		Radius 10;
		Height 14;
		Scale 2;
		
		DamageFunction random(1, 2);
		
		SeeSound "VoidManipulation/Fire/Rift/Gaping";
		DeathSound "VoidManipulation/Fire/Rift/Gaping";
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		riftClass = "VoidManipulationRiftGaping";
	}
	
	action void A_RiftProjectileUpdate() {
		if (invoker.height < 32)
		{
			invoker.height += 0.07;
		}
	}
	
	States {
		Spawn:
			VDPT ABCDEFGHI 1 A_RiftProjectileUpdate();
			Loop;
			
		Death:
			VDPT A 0 A_CreateRift();
		Expired:
			VDPT ABCDEFGHI 1 A_FadeOut(0.05);
			Loop;
	}
}
