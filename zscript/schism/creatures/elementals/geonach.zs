class Geonach : MinionCreature {
	Default {
		Health 200;
		Radius 10;
		Height 70;
		Speed 20;
		Mass 500;
		FloatSpeed 10;
		Scale 0.5;
		Gravity 0;
		PainChance 120;
		Damage 1;
		BloodColor "33 22 22";

		DamageFactor "Earth", 0.1;

		MinionCreature.SummonTime 30 * 35;

		+FLOAT;
		+FLOATBOB;
		+DONTTHRUST;
		+MinionCreature.TAUNT;
		
		SeeSound "Geonach/See";
		PainSound "Geonach/Pain";
		DeathSound "Geonach/Death";
		ActiveSound "Geonach/Idle";
		MeleeSound "Geonach/Melee";
		Obituary "%o was crushed by a Geonach!";
		HitObituary "%o was crushed by a Geonach!";
	}
	
	States {
		Spawn:
			GEON ABCB 4 A_Look();
			Loop;
			
		See:
			GEON DEFE 4 A_Chase("Melee");
			Loop;
			
		Melee:
			GEON GG 4 A_FaceTarget();
			GEON H 8 A_CreatureMelee(10, 20);
			GEON II 4 A_FaceTarget();
			GEON J 8 A_CreatureMelee(20, 30);
			Goto See;
			
		Pain:
			GEON K 6 A_Pain();
			Goto See;
			
		Pain.SelfDamage:
			Goto See;
			
		Death:
		XDeath:
			GEON K 5;
			GEON L 5 A_Scream();
			GEON M 5;
			GEON N 5 A_CreatureDie();
			GEON OPQ 5;
			GEON R -1;
			Stop;
	}
}