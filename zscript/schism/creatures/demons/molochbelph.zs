class MolochBelph : Belph {
	Default {
		Tag "Moloch Belph";
		
		SeeSound "MolochBelph/See";
		ActiveSound "MolochBelph/Idle";
		PainSound "MolochBelph/Pain";
		DeathSound "MolochBelph/Death";
		MeleeSound "MolochBelph/Melee";
		Obituary "%o was twisted by a Moloch Belph!";
		HitObituary "%o was contorted by a Moloch Belph!";
	}
	
	States {
		Spawn:
			MLBL AF 10 A_Look();
			Loop;
			
		See:
			MLBL BBCCDDEE 3 A_Chase();
			Loop;
			
		Melee:
			MLBL FG 8 A_FaceTarget();
			MLBL A 6 A_CreatureMelee(3, 24, "Void", "Void");
			Goto See;

		Missile:
			MLBL FG 8 A_FaceTarget();
			MLBL A 6 A_SpawnProjectile("TendrilboltProjectile", invoker.height / 2);
			Goto See;
				
		Pain:
			MLBL H 2;
			MLBL H 2 A_Pain();
			Goto See;
			
		Death:
			MLBL I 4 ;
			MLBL J 4 A_Scream();
			MLBL K 4;
			MLBL L 4 A_CreatureDie();
			MLBL M -1;
			Stop;
		
		XDeath:
			MLBL N 4 ;
			MLBL O 4 A_XScream();
			MLBL P 4;
			MLBL Q 4 A_CreatureDie();
			MLBL RS 4;
			MLBL T -1;
			Stop;
			
		Raise:
			MLBL MLKJI 8;
			Goto See;
	}
}