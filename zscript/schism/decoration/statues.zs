class SchismStatueEgyptianA : Actor {
	Default {
		Tag "Egyptian Statue A";
		
		Radius 24;
		Height 52;
		ProjectilePassHeight -16;
		
		+SOLID;
	}
	
	States {
		Spawn:
			NSTA A -1;
			Stop;
	}
}

class SchismStatueEgyptianB: SchismStatueEgyptianA {
	Default {
		Tag "Egyptian Statue B";
	}
	
	States {
		Spawn:
			NSTA B -1;
			Stop;
	}
}

class SchismStatueEgyptianC: SchismStatueEgyptianA {
	Default {
		Tag "Egyptian Statue C";
	}
	
	States {
		Spawn:
			NSTA C -1;
			Stop;
	}
}

class SchismStatueEgyptianD: SchismStatueEgyptianA {
	Default {
		Tag "Egyptian Statue D";
	}
	
	States {
		Spawn:
			NSTA D -1;
			Stop;
	}
}

class SchismStatueEgyptianE: SchismStatueEgyptianA {
	Default {
		Tag "Egyptian Statue E";
	}
	
	States {
		Spawn:
			NSTA E -1;
			Stop;
	}
}