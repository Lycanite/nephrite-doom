Class MaraApollyon : Creature {
    Default {
        Tag "Mara Apollyon";

        Health 700;
        Radius 20;
        Height 56;
        Mass 120;
        Speed 10;
        PainChance 24;
        Gravity 0.5;

        SeeSound "MaraApollyon/See";
        ActiveSound "MaraApollyon/Idle";
        PainSound "MaraApollyon/Pain";
        DeathSound "MaraApollyon/Death";
        Obituary "A Mara Apollyon smited %o for there was no place in life for such a fool.";
        HitObituary "A Mara Apollyon smited %o for there was no place in life for such a fool.";
    }

    States {
        Spawn:
            MAPL A 10 A_Look();
            Loop;

        See:
            MAPL A 0 { invoker.bHoverBob = true; }
			MAPL AAA 3 A_VileChase();
            Loop;

        Missile:
            MAPL B 0 bright { invoker.bHoverBob = false; }
            MAPL BBCCD 6 bright A_FaceTarget();
            MAPL DDDDD 6 bright A_SpawnProjectile("SmiteProjectile", 0, 0, 0, 0, 0);
            MAPL C 6 bright A_VileTarget();
            MAPL CBBCCDD 6 bright A_FaceTarget();
            MAPL D 6 A_VileAttack();
            MAPL C 20;
            Goto See;

        Heal:
            MAPL E 10 bright { invoker.bHoverBob = false; }
            MAPL CB 10 bright;
            Goto See;

        Pain:
            MAPL F 2;
            MAPL F 2 A_Pain();
            Goto See;

        Death:
        XDeath:
            MAPL G 6;
            MAPL H 6 A_Scream();
            MAPL IJK 6;
            MAPL L 6 A_CreatureDie();
            MAPL M -1;
            Stop;

        Raise:
            MAPL M -1;
            Stop;
    }
}