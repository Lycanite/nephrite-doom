extend class BloodGlobe {
	action state A_Animashards(bool useAmmo) {
		class<BloodGlobeAnimashardProjectile> projectileClass = "BloodGlobeAnimashardProjectile";
		if (!A_CheckAmmo(1, 0, false)) {
			projectileClass = "BloodGlobeAnimashardProjectileWeak";
		}
		if (useAmmo) {
			A_SpendAmmo(1);
		}

		if (invoker.lifetime % 4 == 0) {
			A_StartSound("Projectile/Animashard/Fire", CHAN_WEAPON, CHANF_OVERLAP);
		}

		int angleTime = TICRATE * 2;
		float angle = Cos(invoker.lifetime * 4) * 40;
		for (int i = 0; i <= 1; i++) {
			BloodGlobeAnimashardProjectile animashardProjectile = BloodGlobeAnimashardProjectile(
				A_FireProjectile(projectileClass, i ? angle : -angle, false, Random(-1, 1), Random(-2, 2))
			);
			if (animashardProjectile) {
				animashardProjectile.bloodGlobe = invoker;
			}
		}

		return null;
	}

	States {			
		MainHand.Fire:
			VSH1 G 0 A_ClearBobbing();
			VSH1 G 4 A_Light2();
		Hold:
			VSH1 H 1 {
				return A_Animashards(true);
			}
			VSH1 HIIJJJJ 1 {
				return A_Animashards(false);
			}
			VSH1 I 0 A_ReFire("Hold");
			VSH1 I 0 A_Light0();
			Goto Ready;
	}
}

class BloodGlobeAnimashardProjectile : AnimashardProjectile {
    BloodGlobe bloodGlobe;

	Default {
		SchismProjectile.LifetimeMax TICRATE;
		SchismProjectile.GiveAmmoClass "Anima";
		SchismProjectile.GiveAmmoMin 1;
		SchismProjectile.GiveAmmoMax 1;
		
		SeeSound "BloodGlobe/Fire";
		DeathSound "BloodGlobe/Explode";
	}

	override void GiveAmmo(Actor target, class<Inventory> ammoClass, int amount) {
		if (self.bloodGlobe) {
			super.GiveAmmo(self.bloodGlobe.owner, ammoClass, amount);
		}
	}
}

class BloodGlobeAnimashardProjectileWeak : BloodGlobeAnimashardProjectile {
	Default {
		DamageFunction 3;
        Alpha 0.5;

        SchismProjectile.LifetimeMax 18;
        SchismProjectile.ParticleInterval 16;
	}
}