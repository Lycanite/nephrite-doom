class VolcanoModule : WeaponUpgrade {
	Default {
		Scale 1;
		Inventory.PickupMessage "Your Lava Cannon can now launch Volcano devices that fill the area with lava!";
		
		Inventory.Icon "MLVCX0";
		Inventory.PickupSound "LavaCannon/Pickup";
		Inventory.RestrictedTo "Mogdread";
		WeaponUpgrade.ExtraAmmoClass "MoglavaCharge";
		
		RenderStyle "Normal";
	}

	States {
		Spawn:
			MLVC X 1 bright;
			Loop;
	}
}

class LavaCannonVolcanoProjectile : LavaCannonProjectile {
	Default {
		Radius 10;
		Height 10;
		Scale 1;
		Speed 20;
		DamageFunction 0;
		Alpha 1;
		Renderstyle "Normal";

		SchismProjectile.PoolTics 15 * TICRATE;
		
		SeeSound "LavaCannon/Fire/Volcano";
		
		+RIPPER;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
	}
	
	action state A_VolcanoUpdate(int tics) {
		for (int i = 0; i <= 3; i++) {
			LavaCannonEruptionProjectile projectileActor = LavaCannonEruptionProjectile(A_SpawnProjectile("LavaCannonEruptionProjectile", 4, Random(-6, 6), Random(-180, 180), CMF_AIMDIRECTION|CMF_TRACKOWNER, Random(-80, -45)));
			if (projectileActor) {
				projectileActor.target = target;
			}
		}
		
		invoker.poolTics -= tics;
		if (invoker.poolTics <= 0) {
			return ResolveState("Null");
		}
		return null;
	}
	
	States {
		Spawn:
			MLVB QRSR 4 bright;
			Loop;
			
		Death:
			MLVB QR 4 bright;
			MLVB S 4 bright {
				return A_VolcanoUpdate(16);
			}
			MLVB R 4 bright;
			Loop;
	}
}
