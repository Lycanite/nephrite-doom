class LaserModule : WeaponUpgrade {
	Default {
		Scale 0.75;
		Inventory.PickupMessage "Your Alchemical Raygun can now fire a Laser Clamp!";
		
		Inventory.Icon "ALCRX0";
		Inventory.PickupSound "AlchemicalRaygun/Pickup";
		Inventory.RestrictedTo "Mogdread";
		WeaponUpgrade.ExtraAmmoClass "LightCharge";
		
		RenderStyle "Normal";
	}

	States {
		Spawn:
			ALCR X 1 bright;
			Loop;
	}
}

class AlchemicalRaygunLaserProjectile : AlchemicalRaygunProjectile {
	int offsetX;
	int offsetY;
	int offsetZ;
	Actor followTarget;
	int expireTime;
	
	Default {
		Radius 10;
		Height 5;
		Scale 0.75;
		Speed 30;
		DamageFunction 2;
		Alpha 1;
		Renderstyle "Normal";
		
		SeeSound "AlchemicalRaygun/Fire/Laser";
		
		+EXPLODEONWATER;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		expireTime = 35 * 30;
	}
	
	override bool CanCollideWith(Actor other, bool passive) {
		if (!passive && !self.followTarget && other != self.target) {
			self.setFollowTarget(other);
		}
		return super.CanCollideWith(other, passive);
	}
	
	virtual void setFollowTarget(Actor newTarget) {
		self.followTarget = newTarget;
		self.offsetX = self.pos.x - newTarget.pos.x;
		self.offsetY = self.pos.y - newTarget.pos.y;
		self.offsetZ = self.pos.z - newTarget.pos.z;
	}
	
	action state A_AttachUpdate() {
		if (invoker.followTarget) {
			return ResolveState("Death");
		}
		return null;
	}
	
	action state A_LaserUpdate(bool charged) {
		// No Owner
		if (!target) {
			return ResolveState("Detonate");
		}
		master = target;
		
		// Attachment
		invoker.gravity = 0;
		if (invoker.followTarget) {
			invoker.SetOrigin(invoker.followTarget.pos + (invoker.offsetX, invoker.offsetY, invoker.offsetZ), false);
		}
		
		// Fire Laser:
		AlchemicalRaygunFlareProjectile projectileActor = AlchemicalRaygunFlareProjectile(A_SpawnProjectile("AlchemicalRaygunFlareProjectile", 0, 0, 180, CMF_AIMDIRECTION|CMF_TRACKOWNER , invoker.Pitch));
		if (projectileActor) {
			projectileActor.target = target;
		}
		
		// Expiring
		if (invoker.expireTime-- <= 0) {
			return ResolveState("Detonate");
		}
		
		return null;
	}
	
	action void A_Detonate() {
		int damage = 50;
		int radius = 100;
		A_QuakeEx(2, 2, 2, 10, 0, radius, "world/quake", 0, 1, 1, 0, 0, 1, 1);
		SchismAttacks.Explode(invoker, invoker.target, damage, radius, 100);
		
		for (int i = 0; i < damage / 2; i++) {
			invoker.SpawnParticle(
				"Fire", 2 * TICRATE, 4,
				(FRandom(-8, 8), FRandom(-8, 8), FRandom(-8, -2)),
				Random(0, 359), Random(-10, 10), 1.7
			);
		}
	}
	
	States {
		Spawn:
			ALCP P 1 bright A_AttachUpdate();
			Loop;
			
		Death:
			ALCP P 1 bright {
				return A_LaserUpdate(false);
			}
			Loop;
			
		Detonate:
			MLVB H 2 bright A_Detonate();
			MLVB IJKLMNOP 2 bright;
			Stop;
	}
}
