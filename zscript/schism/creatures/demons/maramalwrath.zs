class MaraMalwrath : Malwrath {
	Default {
		Tag "Mara Malwrath";
		Health 300;
		Speed 14;
		
		SeeSound "MaraMalwrath/See";
		ActiveSound "MaraMalwrath/Idle";
		PainSound "MaraMalwrath/Pain";
		DeathSound "MaraMalwrath/Death";
		MeleeSound "MaraMalwrath/Melee";
		Obituary "%o was smited by a Mara Malwrath.";
		HitObituary "%o was consumed by a Mara Malwrath.";
	}
		
	States {
		Spawn:
			MARA A 10 A_Look();
			Loop;
			
		See:
			MARA A 3 A_Chase();
			Loop;
			
		Melee:
			MARA EF 5 A_FaceTarget();
			MARA G 5 bright A_CreatureMelee(20, 40, "Aether", "Aether");
			Goto See;
			
		Missile:
			MARA E 5 {
				A_FaceTarget();
				A_StartSound(invoker.meleeSound);
			}
			MARA F 5 A_FaceTarget();
			MARA G 5 bright A_SpawnProjectile("GreedyBlastProjectile", invoker.height / 2);
			Goto See;
		
		Pain:
			MARA H 3;
			MARA H 3 A_Pain();
			MARA I 6;
			Goto See;
		
		Death:
			MARA I 4;
			MARA J 4 A_Scream();
			MARA KL 4;
			MARA M 4 A_CreatureDie();
			MARA NOPQR 4;
			MARA S -1 A_SetFloorClip();
			Stop;
			
		Raise:
			MARA R 4 A_UnSetFloorClip();
			MARA QPONMLKJI 4;
			Goto See;
	}
}