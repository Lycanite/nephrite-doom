class SchismFlightBuff : SchismStatusEffect {
	Default {
		PowerUp.Duration -10;
		SchismStatusEffect.ParticleName "Leaf";
		SchismStatusEffect.ParticleColor 0xF7000000;
	}
	
	override void InitEffect() {
		super.InitEffect();
		if (!self.owner) {
			self.effectInvalid = true;
			return;
		}
	}
	
	override void EndEffect() {
		super.EndEffect();
		if (self.effectInvalid || !self.owner) {
			return;
		}
		self.effectInvalid = true;
	}
	
	override void DoEffect() {
		super.DoEffect();
		self.particleColor = self.effectTics > TICRATE * 3 ? 0xF7000000 : 0x964B00;
		if (self.owner && !self.effectInvalid) {
			self.owner.GiveInventory("SchismFlightBuffPower", 1);
		}
	}
}

class SchismFlightBuffPower : PowerFlight {
	Default {
		PowerUp.Duration 3;
		Inventory.MaxAmount 1;
		-Inventory.HUBPOWER;
		+Inventory.ALWAYSPICKUP;
		-Inventory.ADDITIVETIME;
		+INVENTORY.NOSCREENBLINK;
	}
}