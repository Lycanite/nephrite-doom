class SchismStatusEffects : Thinker {
	/** 
	 * Inflicts a status effect by the inflictor onto the target, if they already have the effect then it is refreshed. Used by weapons, projectiles and monsters to inflict effects.
	 * @param effectClass The name of the effect class.
	 * @param effectInflictor The actor to set as the overall creator of the effect.
	 * @param effectSource The actor to set as the direct source of the effect, this could be a projectile, etc.
	 * @param effectTarget The actor that the effect is being applied to.
	 * @param effectTime The duration of the effect in tics. Defaults to 10 seconds.
	 * @param spread An optional status effect to spread to enemies within this effect's radius.
	 * @param strength The strength of the effect, varies depending on the type of effect, ignored by most effects. Defaults to 1.
	 */
	static Inventory InflictEffect(class<Inventory> effectClass, Actor effectInflictor, Actor effectSource, Actor effectTarget, int effectTime = 10 * TICRATE, Class<SchismStatusEffect> spread = "", int strength = 1) {
		if (!effectTarget || !effectSource) {
			return null;
		}

		// Immunity Checks:
		Creature creature = Creature(effectTarget);
		if (creature && !creature.CanApplyEffect(effectClass)) {
			return null;
		}
		SchismPlayerPawn schismPlayerPawn = SchismPlayerPawn(effectTarget);
		if (schismPlayerPawn && !schismPlayerPawn.CanApplyEffect(effectClass)) {
			return null;
		}

		// Apply New:
		if (effectTarget.CountInv(effectClass) == 0) {
			Inventory statusEffectInventory = effectTarget.GiveInventoryType(effectClass);
			SchismStatusEffect statusEffect = SchismStatusEffect(statusEffectInventory);
			if (statusEffect) {
				statusEffect.ApplyEffect(effectInflictor, effectSource, effectTime, spread, strength);
			}
			return statusEffectInventory;
		}

		// Refresh Existing:
		Inventory statusEffectInventory = effectTarget.FindInventory(effectClass);
		SchismStatusEffect statusEffect = SchismStatusEffect(statusEffectInventory);
		if (statusEffect) {
			statusEffect.RefreshEffect(effectInflictor, effectSource, effectTime, spread, strength);
		}
		return statusEffectInventory;
	}
}
