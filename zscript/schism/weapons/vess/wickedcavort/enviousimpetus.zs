class EnviousImpetusScroll : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "You can watch your enemies in Envy as they are bound together in an Impetus!";
		
		Inventory.Icon "VESWL0";
		Inventory.PickupSound "WickedCavort/Pickup";
		Inventory.RestrictedTo "Vess";
		WeaponUpgrade.ExtraAmmoClass "LustDrupe";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			VESW L 1 bright;
			Loop;
	}
}

extend class WickedCavort {
	action state A_EnviousImpetus() {
		if (!A_CheckSpecial(4) || !A_SpendAmmo(6)) {
			return ResolveState("Ready");
		}

        A_StartSound("WickedCavort/Fire/EnviousImpetus", CHAN_WEAPON);
        Actor projectileActor = A_FireProjectile("EnviousImpetusProjectile");
		SchismProjectile projectile = SchismProjectile(projectileActor);
		if (projectile) {
			projectile.statusEffectDuration = (5 + (invoker.GetPassiveLevel() * 5)) * TICRATE;
		}
		return null;
	}

    States {
		MainHand.User4:
			VSH1 Q 2 {
                A_ClearBobbing();
                A_Light2();
            }
			VSH1 R 2;
			VSH1 S 2 A_EnviousImpetus();
            VSH1 T 2;
            VSH1 UVR 2;
			VSH1 R 0 A_Light0();
			Goto Ready;

        OffHand.User4:
			VSH3 PQR 2;
			Loop;
    }
}

class EnviousImpetusProjectile : WrathCrossProjectile {
	Default {
		Radius 5;
		Height 7;
		Scale 0.5;
		Speed 20;
		Gravity 0.25;
		DamageFunction Random(10, 20);
		Alpha 1.0;
		RenderStyle "Add";

		BounceType "Hexen";
		BounceFactor 1;
		WallBounceFactor 1;
		BounceCount 3;

		SchismProjectile.LifetimeMax 0;

		SchismProjectile.ParticleName "Nether";
		SchismProjectile.ParticleColor 0x00FF00;
		SchismProjectile.StatusEffectClass "SchismBoundEffect";
		SchismProjectile.StatusEffectDuration 5 * TICRATE;
		SchismProjectile.StatusEffectStrength 1;
		
		SeeSound "WickedCavort/Fire/EnviousImpetus";
		DeathSound "WickedCavort/Explode/PrideDosado";
		Obituary "%o was soul broken %k's Envious Impetus!";
		
		-NOGRAVITY;
	}

	action void A_Fragment() {
		bool spawned;
		Actor projectileActor;
		[spawned, projectileActor] = invoker.A_SpawnItemEx("EnviousImpetusFragment", 0, 0, 0, 2, 4, 5);
		[spawned, projectileActor] = invoker.A_SpawnItemEx("EnviousImpetusFragment", 0, 0, 0, 2, -4, 5);
		if (projectileActor) {
			projectileActor.target = invoker.target;
		}
	}
	
	States {
		Spawn:
			VWC7 ABCDE 4;
		See:
			VWC7 FGHIJ 4;
			Loop;
			
		Death:
			VWC7 G 0 A_Fragment();
			VWC7 GHIJ 4;
			Stop;
	}
}

class EnviousImpetusFragment : EnviousImpetusProjectile {
	States {
		Spawn:
			VWC6 ABCDEFGH 4;
			Loop;
			
		Death:
			VWC6 FGHIJ 4;
			Stop;
	}
}