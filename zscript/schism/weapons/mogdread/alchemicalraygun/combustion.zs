class CombustionModule : WeaponUpgrade {
	Default {
		Scale 0.75;
		Inventory.PickupMessage "Your Alchemical Raygun can now fire an explosive beam!";
		
		Inventory.Icon "ALCRW0";
		Inventory.PickupSound "AlchemicalRaygun/Pickup";
		Inventory.RestrictedTo "Mogdread";
		WeaponUpgrade.ExtraAmmoClass "LightCharge";
		
		RenderStyle "Normal";
	}

	States {
		Spawn:
			ALCR W 1 bright;
			Loop;
	}
}

class AlchemicalRaygunCombustionProjectile : AlchemicalRaygunProjectile {
	bool exploded;
	
	Default {
		Scale 0.75;
		Gravity 0.5;
		
		SeeSound "AlchemicalRaygun/Fire/Combustion";
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		exploded = false;
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		if (specialDamage > 0 && !exploded) {
			exploded = true;			
			A_StartSound("FirestoneCane/Combustion");
			//A_QuakeEx(2, 2, 2, 10, 0, 500, "world/quake", 0, 1, 1, 0, 0, 1, 1);
			SchismAttacks.Explode(self, self.target, 4, 300);
			for (int i = 0; i <= 20; i++) {
				self.SpawnParticle(
					"Fire", 2 * TICRATE, 4,
					(FRandom(-8, 8), FRandom(-8, 8), FRandom(-8, -2)),
					Random(0, 359), Random(-10, 10), 1.7
				);
			}
		}
		return specialDamage;
	}
	
	States {
		Spawn:
			ALCC ABABCBC 1 bright;
			
		Death:
			ALCC DCDEDEFEFGFGHGHIHIJIJKJKLKLMLMNMNONO 1 bright;
			Stop;
	}
}
