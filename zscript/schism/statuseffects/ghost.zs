class SchismGhostEffect : SchismStatusEffect {
	bool originalGhost;
	bool originalThruActors;
	
	Default {
		PowerUp.Duration -10;
		SchismStatusEffect.ParticleName "Ghost";
	}
	
	override void InitEffect() {
		super.InitEffect();
		if (!self.owner) {
			self.EffectInvalid = true;
			return;
		}
		
		self.originalGhost = self.owner.bGhost;
		self.owner.bGhost = true;
		self.originalThruActors = self.owner.bThruActors;
		self.owner.bThruActors = true;
	}
	
	override void EndEffect() {
		super.EndEffect();
		if (self.EffectInvalid || !self.owner) {
			return;
		}
		self.owner.bGhost = self.originalGhost;
		self.owner.bThruActors = self.originalThruActors;
	}
}