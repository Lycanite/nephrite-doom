class FrostshardProjectile : SchismProjectile {
	Default {
		Radius 8;
		Height 8;
		Scale 0.25;
		Speed 20;
		FastSpeed 20;
		DamageFunction 4;
		Alpha 0.8;
		RenderStyle "Translucent";
		
		DamageType "Ice";
		SchismProjectile.ParticleName "Frost";
		SchismProjectile.ParticleCount 1;
		SchismProjectile.StatusEffectClass "SchismChillDebuff";
		SchismProjectile.StatusEffectDuration 5 * TICRATE;
		SchismProjectile.KnockbackStrength 0.5;
		
		SeeSound "GlacialNodachi/Projectile";
		DeathSound "GlacialNodachi/Projectile/Explode";
	}
	
	States {
		Spawn:
			PFSH ABCDEFGHIHGFEDCB 2;
			Loop;
			
		Death:
			PFSH JKLMNOPQRSTUVWXYZ 2;
			Stop;
	}
}