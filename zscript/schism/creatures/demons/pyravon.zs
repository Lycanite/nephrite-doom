class Pyravon : Creature {
	Default {
		Tag "Pyravon";
		Health 300;
		Radius 24;
		Height 48;
		Mass 200;
		Speed 12;
		PainChance 128;
		Gravity 0;
		
		SeeSound "Pyravon/See";
		PainSound "Pyravon/Pain";
		DeathSound "Pyravon/Death";
		ActiveSound "Pyravon/Idle";
		Obituary "%o got incinerated by a Pyravon.";
		HitObituary "%o got incinerated by a Pyravon.";
		
		+NOGRAVITY;
		+FLOAT;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		self.SpawnParticle("Fire", 8, 1, (FRandom(-8, 8), FRandom(-8, 8), FRandom(10, 40)), Random(45, 315), Random(-10, 10), 0.25);
		self.SpawnParticle("FireEmber", 2 * 8, 3, (FRandom(-8, 8), FRandom(-8, 8), FRandom(10, 40)), Random(45, 315), Random(-10, 10), 0.25);
	}
	
	States {
		Spawn:
			PYVN ABCDB 8 bright A_Look();
			Loop;
		
		See:
			PYVN A 0 bright A_StartSound("Pyravon/Fly", CHAN_5);
			PYVN ABCDB 2 bright A_Chase();
			Loop;
			
		Missile:
			PYVN CE 10 bright A_FaceTarget();
			PYVN F 10 bright A_SpawnProjectile("FireballProjectile", invoker.height / 2);
			PYVN D 10;
			PYVN B 10 bright A_MonsterRefire(80, "See");
			Goto Missile + 1;
			
		Pain: 
			PYVN G 3 bright;
			PYVN G 3 bright A_Pain();
			PYVN G 3 bright;
			Goto See;
			
		Death: 
			PYVN G 5 bright;
			PYVN H 5 bright A_Scream();
			PYVN IJK 5 bright;
			PYVN L 5 bright A_CreatureDie();
			PYVN M -1 A_SetFloorClip();
			Stop;
			
		Raise: 
			PYVN M 5 A_UnSetFloorClip();
			PYVN LKJIHG 5 bright;
			Goto See;
	}
}