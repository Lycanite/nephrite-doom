class NecronomiconFinalProjectile : SchismProjectile {
	EbonCommand ebonCommand;
	
	Default {
		Radius 10;
		Height 10;
		Speed 30;
		DamageFunction 20;
		Gravity 0;
		Alpha 1;
		Renderstyle "Add";
		Projectile;
		
		DamageType "Lava";
		
		SeeSound "Necronomicon/Fire/Final";
		DeathSound "";
		
		+SKYEXPLODE;
	}
	
	action void A_CallStorm() {
		int stormSize = 3;
		int cloudSize = 200;
		for (int x = -(stormSize * cloudSize); x <= stormSize * cloudSize; x += cloudSize) {
			for (int y = -(stormSize * cloudSize); y <= stormSize * cloudSize; y += cloudSize) {
				A_SpawnItemEx("NecronomiconFinalCloud", x, y, 0, 0, 0, 0, 0, SXF_TRANSFERPOINTERS|SXF_NOCHECKPOSITION);
				A_SpawnItemEx("NecronomiconFinalCloudFloor", x, y, 0, 0, 0, 0, 0, SXF_TRANSFERPOINTERS|SXF_NOCHECKPOSITION);
			}
		}
	}
	
	States {
		Spawn:
		Death:
			MLVB A 1;
			MLVB B 1 A_CallStorm();
			Stop;
	}
}

class NecronomiconFinalCloud : SchismProjectile {
	int randomStart;
	
	Default {
		Radius 1;
		Height 1;
		Scale 2;
		Speed 15;
		DamageFunction 2;
		Alpha 0.25;
		Gravity 0;
		Renderstyle "Translucent";
		
		SeeSound "";
		DeathSound "";
		
		+DONTSPLASH;
		+SPAWNCEILING;
		+CEILINGHUGGER;
		+NODAMAGETHRUST;
		+PAINLESS;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		randomStart = Random(0, TICRATE - 1);
	}
	
	action void A_FinalEruption() {
		A_StartSound("Necronomicon/Final", CHAN_BODY, CHANF_LOOPING);
		if (invoker.lifetime % TICRATE == invoker.randomStart) {
			if (invoker.bFloorHugger) {
				if (Random (0, 1)) {
					invoker.A_SpawnItemEx("NecronomiconBonePitProjectile", 0, 0, 0, 0, 0, 0, 0, SXF_TRANSFERPOINTERS|SXF_NOCHECKPOSITION);
				} else {
					invoker.A_SpawnItemEx("NecronomiconAetherLightningProjectile", 0, 0, 0, Random(-10, 10), Random(-10, 10), 0, 0, SXF_TRANSFERPOINTERS|SXF_NOCHECKPOSITION);
				}
			} else {
				if (Random (0, 1)) {
					invoker.A_SpawnItemEx("NecronomiconSoulfireProjectile", 0, 0, -10, 0, 0, -30, 0, SXF_TRANSFERPOINTERS|SXF_NOCHECKPOSITION);
				} else {
					invoker.A_SpawnItemEx("NecronomiconChainsOfIceProjectile", 0, 0, -10, 0, 0, -30, 0, SXF_TRANSFERPOINTERS|SXF_NOCHECKPOSITION);
				}
			}
		}
	}
	
	States {
		Spawn:
			NCLH FGHIJABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDE 4 A_FinalEruption();
			NCLH ABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDE 4 A_FinalEruption();
		Death:
			NCLH F 4 A_StopSound(CHAN_BODY);
		Death.Fade:
			NCLH GGGGHHHHIIIIJJJJ 1 A_FadeOut(0.1);
			Loop;
	}
}

class NecronomiconFinalCloudFloor : NecronomiconFinalCloud {
	Default {
		-CEILINGHUGGER;
		+FLOORHUGGER;
	}
}