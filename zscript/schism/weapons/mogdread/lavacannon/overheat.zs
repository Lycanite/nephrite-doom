class OverheatModule : WeaponUpgrade {
	Default {
		Scale 1;
		Inventory.PickupMessage "Your Lava Cannon can now Overheat and unleash a volatile super blast!";
		
		Inventory.Icon "MLVCV0";
		Inventory.PickupSound "LavaCannon/Pickup";
		Inventory.RestrictedTo "Mogdread";
		WeaponUpgrade.ExtraAmmoClass "MoglavaCharge";
		
		RenderStyle "Normal";
	}

	States {
		Spawn:
			MLVC V 1 bright;
			Loop;
	}
}

class LavaCannonOverheatProjectile : LavaCannonProjectile {
	Default {
		Radius 32;
		Height 20;
		Scale 2;
		Speed 10;
		DamageFunction 50;
		Gravity 0;
		Alpha 1;
		Renderstyle "Normal";
		
		SeeSound "LavaCannon/Fire/Overheat";
		
		+RIPPER;
	}
	
	action state A_OverheatProjectileUpdate(bool spawnLava) {
		A_QuakeEx(2, 2, 2, 10, 0, 500, "world/quake", 0, 1, 1, 0, 0, 1, 1);
		if (spawnLava) {
			A_SpawnItemEx("LavaCannonProjectile", 0, 0, 0, 0, -1, 0, 0, SXF_TRANSFERPOINTERS);
		}
		return null;
	}
	
	States {
		Spawn:
			MLVB A 2 bright A_OverheatProjectileUpdate(true);
			MLVB BC 2 bright A_OverheatProjectileUpdate(false);
			MLVB D 2 bright A_OverheatProjectileUpdate(true);
			MLVB EF 2 bright A_OverheatProjectileUpdate(false);
			Loop;
			
		Death:
			MLVB GHIJKLMNOP 1 bright;
			Stop;
	}
}
