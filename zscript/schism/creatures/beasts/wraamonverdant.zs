class WraamonVerdant : Wraamon {
	Default {
		Height 4;
		Radius 3;

		Tag "Verdant Wraamon";
	}
	
	States {
		Spawn:
			WRAV CDEF 4 {
				return A_MinionIdle();
			}
			Loop;
			
		Guard:
			WRAV CDEF 4 {
				return A_MinionGuard();
			}
			Loop;
			
		Follow:
			WRAV CDEF 4 {
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			WRAV CDEF 4 {
				return A_MinionChase();
			}
			Loop;
			
		Melee:
			WRAV A 4 A_FaceTarget;
			WRAV B 4 A_MinionHavocMeleeAttack(2);
			WRAV AB 4;
			Goto See;
			
		Missile:
			WRAV A 0 A_Jump(128, "See");
			WRAV G 3 A_FaceTarget();
			WRAV H 10 A_Leap(60);
			WRAV I 8 A_LeapAttack(4);
			Goto See;
			
		Pain:
			WRAV G 6;
			WRAV D 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			WRAV G 5;
			WRAV J 5 A_Scream();
			WRAV K 5;
			WRAV L 5 A_NoBlocking();
			WRAV M 5;
			WRAV NO 5;
			Stop;
	}
}
