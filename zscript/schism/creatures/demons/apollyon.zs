Class Apollyon : Creature {
    Default {
        Tag "Apollyon";

        Health 700;
        Radius 20;
        Height 56;
        Mass 120;
        Speed 10;
        PainChance 24;

        SeeSound "Apollyon/See";
        ActiveSound "Apollyon/Idle";
        PainSound "Apollyon/Pain";
        DeathSound "Apollyon/Death";
        Obituary "An Apollyon burned %o to a crisp.";
        HitObituary "An Apollyon burned %o to a crisp.";
    }

    States {
        Spawn:
            APOL AB 10 A_Look();
            Loop;

        See:
            APOL AABBCCDDEEFF 2 A_VileChase();
            Loop;

        Missile:
            APOL GHIJK 6 bright A_FaceTarget();
            APOL LLL 6 bright A_SpawnProjectile("FireballProjectile", invoker.height / 2, 0, 0, 0, 0);
            APOL K 6 bright A_VileTarget();
            APOL JIHGHIJK 6 bright A_FaceTarget();
            APOL L 6 A_VileAttack();
            APOL L 20;
            Goto See;

        Heal:
            APOL L 10 bright;
            Goto See;

        Pain:
            APOL M 2;
            APOL M 2 A_Pain();
            Goto See;

        Death:
            APOL N 6;
            APOL O 6 A_Scream();
            APOL PQR 6;
            APOL S 6 A_CreatureDie();
            APOL T -1;
            Stop;

        XDeath:
            APOL U 5;
            APOL V 5 A_XScream();
            APOL W 5;
            APOL X 5 A_CreatureDie();
            APOL YZ 5;
            APOL [] 5;
            APOL ^ -1;
            Stop;

        Raise:
            APOL ^ -1;
            Stop;
    }
}