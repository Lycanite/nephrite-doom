class VariantDecoration : Actor {
	int user_variant; // If non-zero, forces a variation.

	int variants;
	property Variants: variants; // The number of random variants.
	vector2 baseScale; // The scale captured before random scaling is applied.

	Default {
		Tag "Decoration";
		
		Radius 20;
		Height 40;
		ProjectilePassHeight -8;
		
		VariantDecoration.Variants 1;
	}

	override void PostBeginPlay() {
		super.PostBeginPlay();
		self.baseScale = self.scale;
	}

	action state A_RandomVariant() {
		// Scale Randomisation:
		float scale = SchismUtil.PositionalVarition(invoker.pos, 0.9, 1.1);
		invoker.scale.x = invoker.baseScale.x * scale;
		invoker.scale.y = invoker.baseScale.y * scale;

		// State Variation:
		if (!invoker.variants) {
			return null;
		}
		int stateVariation = invoker.user_variant ? invoker.user_variant : Round(SchismUtil.PositionalVarition(invoker.pos, 1, invoker.variants));
		switch (stateVariation) {
			case 1:
				return FindState("Variant.1");
			case 2:
				return FindState("Variant.2");
			case 3:
				return FindState("Variant.3");
			case 4:
				return FindState("Variant.4");
			case 5:
				return FindState("Variant.5");
			case 6:
				return FindState("Variant.6");
			case 7:
				return FindState("Variant.7");
			case 8:
				return FindState("Variant.8");
			case 9:
				return FindState("Variant.9");
		}
		return null;
	}
	
	States {
		Spawn:
			TNT1 A 0 NoDelay {
				return A_RandomVariant();
			}
			Stop;
	}
}

#include "zscript/schism/decoration/trees.zs"
#include "zscript/schism/decoration/stumps.zs"
#include "zscript/schism/decoration/shrubs.zs"
#include "zscript/schism/decoration/herbs.zs"
#include "zscript/schism/decoration/flowers.zs"
#include "zscript/schism/decoration/mushrooms.zs"
#include "zscript/schism/decoration/rocks.zs"
#include "zscript/schism/decoration/lights.zs"
#include "zscript/schism/decoration/statues.zs"
#include "zscript/schism/decoration/breakables.zs"
#include "zscript/schism/decoration/effects.zs"
#include "zscript/schism/decoration/hazards.zs"
#include "zscript/schism/decoration/poses.zs"