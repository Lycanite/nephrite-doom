class ReactionModule : WeaponUpgrade {
	Default {
		Scale 0.75;
		Inventory.PickupMessage "Your Alchemical Raygun can now fire a Reaction Flare!";
		
		Inventory.Icon "ALCRV0";
		Inventory.PickupSound "AlchemicalRaygun/Pickup";
		Inventory.RestrictedTo "Mogdread";
		WeaponUpgrade.ExtraAmmoClass "LightCharge";
		
		RenderStyle "Normal";
	}

	States {
		Spawn:
			ALCR V 1 bright;
			Loop;
	}
}

class AlchemicalRaygunReactionProjectile : AlchemicalRaygunClusterProjectile {
	Default {
		Scale 0.75;
		DamageFunction 10;
		
		SeeSound "AlchemicalRaygun/Fire/Reaction";
		
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		clusterProjectile = "AlchemicalRaygunReactionClusterProjectile";
	}
	
	States {
		Spawn:
			ALCC ABCB 4 bright;
			Loop;
		
		Death:
			ALCC DEFGHIJKLMNO 2 bright;
			Stop;
	}
}

class AlchemicalRaygunReactionClusterProjectile : AlchemicalRaygunClusterProjectile {
	Default {
		DamageFunction 1;
	}
}
