class SchismTreeChestnut : SchismTree {
	Default {
		Tag "Chestnut Tree";
		SchismTree.SpringVariants 2;
		SchismTree.SummerVariants 2;
		SchismTree.AutumnVariants 2;
		SchismTree.WinterVariants 2;
	}
	
	States {
		Spring.1:
			TR21 A -1;
			Stop;

		Spring.2:
			TR21 B -1;
			Stop;

		Summer.1:
			TR21 E -1;
			Stop;

		Summer.2:
			TR21 F -1;
			Stop;

		Autumn.1:
			TR21 I -1;
			Stop;

		Autumn.2:
			TR21 J -1;
			Stop;

		Winter.1:
			TD21 M -1;
			Stop;

		Winter.2:
			TD21 N -1;
			Stop;

		Death.Spring.1:
			TD21 A -1;
			Stop;

		Death.Spring.2:
			TD21 B -1;
			Stop;

		Death.Summer.1:
			TD21 E -1;
			Stop;

		Death.Summer.2:
			TD21 F -1;
			Stop;

		Death.Autumn.1:
			TD21 I -1;
			Stop;

		Death.Autumn.2:
			TD21 J -1;
			Stop;

		Death.Winter.1:
			TD21 M -1;
			Stop;

		Death.Winter.2:
			TD21 N -1;
			Stop;
	}
}