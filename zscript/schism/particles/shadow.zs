struct SchismShadowParticle {
	static void apply(out FSpawnParticleParams particleParams) {
		int frame = Random(1, 3);
		particleParams.texture = TexMan.CheckForTexture("particles/shadow/Shadow0" .. frame .. ".png");
		particleParams.style = STYLE_Subtract;
		particleParams.flags = SPF_ROLL;

		particleParams.accel = (0, 0, 0);
		particleParams.startRoll = Random(0, 359);
		particleParams.rollVel = Random(-10, 10);
		particleParams.rollAcc = 0;

		particleParams.size = 32.0;
		particleParams.sizeStep = 1;
		particleParams.startAlpha = 0.5;
		particleParams.fadeStep = particleParams.startAlpha / particleParams.lifetime;
	}
}

struct SchismShadowEmberParticle {
	static void apply(out FSpawnParticleParams particleParams) {
		particleParams.texture = TexMan.CheckForTexture("particles/shadow/ShadowEmber01.png");
		particleParams.style = STYLE_Subtract;
		particleParams.flags = SPF_ROLL;

		particleParams.accel = (0, 0, 0);
		particleParams.startRoll = Random(0, 359);
		particleParams.rollVel = Random(-5, 5);
		particleParams.rollAcc = 0;

		particleParams.size = 16.0;
		particleParams.sizeStep = 0;
		particleParams.startAlpha = 1.0;
		particleParams.fadeStep = particleParams.startAlpha / particleParams.lifetime;
	}
}