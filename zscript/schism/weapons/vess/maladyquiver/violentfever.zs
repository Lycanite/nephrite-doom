extend class MaladyQuiver {
    action state A_ViolentFever() {
		if (!A_CheckAmmo(2)) {
			return ResolveState("Ready");
		}
        
		ThinkerIterator poisonEffectIterator = ThinkerIterator.Create("SchismPoisonEffect");
		SchismPoisonEffect poisonEffect;
        bool anyFever = false;
		while (poisonEffect = SchismPoisonEffect(poisonEffectIterator.Next())) {
            if (poisonEffect.owner != invoker.owner && poisonEffect.InflictedBy(invoker.owner)) {
				poisonEffect.Fever(invoker.owner, 4);
                anyFever = true;
			}
		}

        if (anyFever) {
            A_SpendAmmo(2);
        }

        return null;
    }

    States {
		MainHand.AltFire:
            VSH1 K 0 A_ClearBobbing();
			VSH1 LMN 4 A_Light2();
			VSH5 R 4 A_Light2();
        AltHold:
			VSH5 S 4 {
                return A_ViolentFever();
            }
			VSH5 T 4;
			VSH5 U 4 A_ReFire("AltHold");
			VSH5 V 4 A_Light0();
			Goto Ready;

        OffHand.AltFire:
			VSH5 ABCDEFGH 2;
			Loop;
    }
}