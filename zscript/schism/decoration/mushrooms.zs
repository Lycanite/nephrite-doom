class SchismMushroom : Actor {
	Default {
		Tag "Mushroom";
		Scale 0.5;
		
		Radius 16;
		Height 16;
	}
}

class SchismMushroomAgaric : SchismMushroom {
	Default {
		Tag "Agaric Mushroom";
	}
	
	States {
		Spawn:
			NMSH A -1;
			Stop;
	}
}

class SchismMushroomWhitecap : SchismMushroom {
	Default {
		Tag "Whitecap Mushroom";
	}
	
	States {
		Spawn:
			NMSH B -1;
			Stop;
	}
}

class SchismMushroomMilkcap : SchismMushroom {
	Default {
		Tag "Milkcap Mushroom";
	}
	
	States {
		Spawn:
			NMSH C -1;
			Stop;
	}
}

class SchismMushroomWoodtuft : SchismMushroom {
	Default {
		Tag "Woodtuft Mushroom";
	}
	
	States {
		Spawn:
			NMSH D -1;
			Stop;
	}
}

class SchismMushroomFieldcap : SchismMushroom {
	Default {
		Tag "Fieldcap Mushroom";
	}
	
	States {
		Spawn:
			NMSH E -1;
			Stop;
	}
}

class SchismMushroomChestnut : SchismMushroom {
	Default {
		Tag "Chestnut Mushroom";
	}
	
	States {
		Spawn:
			NMSH F -1;
			Stop;
	}
}

class SchismMushroomWebcap : SchismMushroom {
	Default {
		Tag "Webcap Mushroom";
	}
	
	States {
		Spawn:
			NMSH G -1;
			Stop;
	}
}