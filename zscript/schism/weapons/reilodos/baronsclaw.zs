class BaronsClaw : ReilodosSpell {
	Default {
		Radius 20;
		Height 16;
		Scale 1;
		
		Weapon.AmmoUse 20;
		
		Weapon.UpSound "BaronsClaw/Pickup";
		Weapon.ReadySound "";
		Obituary "%o was incinerated by %k's invocation of the Hell Baron!";
		Inventory.PickupMessage "The demonic powers of the Hell Baron's Claw now surge through your very being!";
		Inventory.PickupSound "BaronsClawPickup";
		Inventory.RestrictedTo "Reilodos", "ReilodosHavocSerpent";
		
		+FLOATBOB;
		+NOGRAVITY;
		-WEAPON.ALT_AMMO_OPTIONAL;
	}
	
	States {
		Spawn:
			REBC I -1;
			Stop;
			
		MainHand.Select:
			REH1 A 1 A_Raise(12);
			Loop;
			
		MainHand.Deselect:
			REH1 A 1 A_Lower(12);
			Loop;
			
		MainHand.Ready:
			REH1 BCD 6 A_WeaponReady;
			Loop;
			
		MainHand.Fire:
			REH1 EF 4;
			REBC EF 6 A_Light2;
			REBC G 6 A_FireProjectile("BaronsClawProjectile");
			REBC H 6 A_Light0;
			Goto Ready;
			
		MainHand.AltFire:
			REH1 EF 4;
			REBC EF 4 A_Light2;
			REBC G 6 {
				A_TakeInventory("HavocEnergy", 10);
				A_SpawnItemEx("BaronsClawFire", 80);
				A_SpawnItemEx("BaronsClawFire", -80);
				A_SpawnItemEx("BaronsClawFire", 0, 80);
				A_SpawnItemEx("BaronsClawFire", 0, -80);
				A_SpawnItemEx("BaronsClawFire", 60, 60);
				A_SpawnItemEx("BaronsClawFire", -60, 60);
				A_SpawnItemEx("BaronsClawFire", 60, -60);
				A_SpawnItemEx("BaronsClawFire", -60, -60);
			}
			REBC H 6 A_Light0;
			Goto Ready;
			
		OffHand.Select:
		OffHand.Deselect:
		OffHand.Ready:
			REBC ABC 4;
			Loop;
		
		OffHand.Fire:
		OffHand.AltFire:
		OffHand.User1:
		OffHand.User2:
		OffHand.User3:
		OffHand.User4:
		OffHand.Reload:
			REBC D 4 bright;
			Loop;
	}
}

class BaronsClawProjectile : SchismProjectile {
	Default {
		Radius 16;
		Height 10;
		Scale 1;
		Speed 15;
		FastSpeed 20;
		DamageFunction 60;
		Alpha 1;
		Renderstyle "Add";
		
		SeeSound "BaronsClaw/Fire";
		DeathSound "BaronsClaw/Explode";
		Decal "BaronScorch";
	}
	
	States {
		Spawn:
			REBB AB 4 bright;
			Loop;
			
		Death:
			REBB CD 4 bright {SchismAttacks.Explode(invoker, invoker.target, 100, 150, 0, 1, false, false, "Fire");}
			REBB EFGH 4 bright;
			Stop;
	}
}

class BaronsClawFire : SchismProjectile {
	Default {
		Radius 16;
		Height 10;
		Scale 1;
		Speed 15;
		FastSpeed 20;
		DamageFunction 10;
		Alpha 1;
		Renderstyle "Add";
		
		DamageType "Fire";
		
		SeeSound "BaronsClaw/Fire";
		DeathSound "BaronsClaw/Fire";
		Decal "BaronScorch";
		
		+DONTSPLASH;
		+NODAMAGETHRUST;
		+PAINLESS;
		+FORCERADIUSDMG;
	}
	
	States {
		Spawn:
		Death:
			REBB IJKLMNO 4 bright {SchismAttacks.Explode(invoker, invoker.target, 2, 200);}
			REBB IJKLMNO 4 bright {SchismAttacks.Explode(invoker, invoker.target, 2, 150);}
			REBB IJKLMNO 4 bright {SchismAttacks.Explode(invoker, invoker.target, 2, 100);}
			REBB IJKLMNO 4 bright {SchismAttacks.Explode(invoker, invoker.target, 2, 50);}
			Stop;
	}
}