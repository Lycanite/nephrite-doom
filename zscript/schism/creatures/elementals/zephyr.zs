class Zephyr : MinionCreature {
	Default {
		Tag "Zephyr";
		
		Health 150;
		Radius 10;
		Height 50;
		Speed 15;
		FloatSpeed 15;
		Scale 0.5;
		Gravity 0;
		PainChance 120;
		Damage 1;
		Alpha 0.8;
		BloodColor "99 bb aa";

		DamageFactor "Air", 0.1;
		
		SeeSound "Zephyr/See";
		PainSound "Zephyr/Pain";
		DeathSound "Zephyr/Death";
		ActiveSound "Zephyr/Idle";
		MeleeSound "Zephyr/Melee";
		Obituary "%o was blown away by a Zephyr!";
		HitObituary "%o was blown away by a Zephyr!";
		
		+FLOAT;
	}
	
	States {
		Spawn:
			ZEPH AABBCCBB 4 {
				A_Look();
				return A_StayWithMaster();
			}
			Loop;
			
		Follow:
			ZEPH DEFE 4 {
				A_Look();
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			ZEPH DEFE 4 {
				return A_MinionChase();
			}
			Loop;
			
		Melee:
			ZEPH GH 4 A_FaceTarget();
			ZEPH I 8 {
				A_CreatureMelee(10, 30);
				invoker.Thrust(Random(-10, -4), invoker.angle);
			}
			Goto See;
		
		Missile:
			ZEPH GH 10 A_FaceTarget();
			ZEPH I 10 {
				A_SpawnProjectile("WindProjectile", 64);
				invoker.Thrust(Random(4, 10), invoker.angle);
			}
			Goto See;
			
		Pain:
			ZEPH J 6 A_Pain();
			Goto See;
			
		Pain.SelfDamage:
			Goto See;
			
		Death:
		XDeath:
			ZEPH J 5;
			ZEPH K 5 A_Scream();
			ZEPH L 5;
			ZEPH M 5 A_CreatureDie();
			ZEPH NO 5;
		Fade:
			ZEPH P 1 A_FadeOut(0.1);
			Loop;
	}
}