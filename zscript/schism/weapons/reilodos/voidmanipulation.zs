class VoidManipulation : ReilodosSpell {
	int splintSpeed;
	vector3 lastBoostedHitPos;

	Array<VoidManipulationRift> rifts;
	
	VoidManipulationRift targetRift;
	VoidManipulationRiftReverse reverseRift;
	VoidManipulationRiftNixian nixianRift;
	
	Default {
		Radius 20;
		Height 16;
		Scale 0.5;
		
		Inventory.PickupSound "VoidManipulation/Pickup";
		Weapon.UpSound "VoidManipulation/Up";
		Weapon.ReadySound "";
		SchismWeapon.FailSound "VoidManipulation/Fire/Fail";
		
		Weapon.AmmoType1 "VoidEnergy";
		
		SchismWeapon.PrimaryCost 1;
		SchismWeapon.SecondaryCost 1;
		SchismWeapon.SpecialACost 50;
		SchismWeapon.SpecialBCost 10;
		SchismWeapon.SpecialCCost 10;
		SchismWeapon.SpecialDCost 10;
		
		SchismWeapon.SpecialItemA "GapingRiftScroll";
		SchismWeapon.SpecialItemB "HungeringVengeanceScroll";
		SchismWeapon.SpecialItemC "ReverseRiftScroll";
		SchismWeapon.SpecialItemD "NixianRiftScroll";
		SchismWeapon.PassiveItem "RiftboltScroll";
		
		Inventory.PickupMessage "The reality collapsing abilities of Void Manipulation have been bestowed upon you!";
		Obituary "%o was banished into the void by %k's Void Manipulation!";
		SchismWeapon.InsufficientAmmoAMessage "\caNot enough Void Energy to cast this spell.";
		SchismWeapon.InsufficientAmmoBMessage "\caNot enough Havoc Energy to cast this spell.";
		SchismWeapon.NoSpecialAMessage "\caYou need a Gaping Rift Scroll to cast this spell.";
		SchismWeapon.NoSpecialBMessage "\caYou need a Hungering Vengeance Scroll to cast this spell.";
		SchismWeapon.NoSpecialCMessage "\caYou need a Reverse Rift Scroll to cast this spell.";
		SchismWeapon.NoSpecialDMessage "\caYou need a Nixian Rift Scroll to cast this spell.";

		SchismWeapon.IdleBob 30;
		DualHand.MirrorOffHand true;
	}
	
	virtual void AddRift(VoidManipulationRift rift) {
		if (!rift) {
			return;
		}
		rift.voidManipulation = self;
		rift.target = self.owner;
		self.rifts.Push(rift);
	}
	
	virtual void RemoveRift(VoidManipulationRift rift) {
		if (!rift) {
			return;
		}
		int riftIndex = self.rifts.Find(rift);
		if (riftIndex < self.rifts.Size()) {
			self.rifts.Delete(riftIndex, 1);
		}
	}
		
	// Create Void Rift:
	virtual void CreateRift(string riftClass, Vector3 position, int initialEnergy) {
		self.A_SpawnItemEx(riftClass, position.x - self.pos.x, position.y - self.pos.y, position.z - self.pos.z + 20, 0, 0, 0, 0, SXF_ABSOLUTEPOSITION|SXF_SETMASTER|SXF_NOPOINTERS|SXF_ISTRACER);
		VoidManipulationRift rift = VoidManipulationRift(self.tracer);
		if (!rift) {
			return;
		}
		self.AddRift(rift);
		
		if (rift.GetClassName() == "VoidManipulationRift") {
			self.targetRift = rift;
		}
		else if (VoidManipulationRiftReverse(rift)) {
			if (self.reverseRift) {
				self.reverseRift.Destroy();
			}
			self.reverseRift = VoidManipulationRiftReverse(rift);
		}
	}
	
	// On Void Rift Damage:
	virtual void OnRiftDamage(VoidManipulationRift rift, Actor riftTarget) {
		if (!rift.sustained || rift.pullForce <= 0 || !riftTarget) {
			return;
		}
		if (!riftTarget.bIsMonster && !PlayerPawn(riftTarget)) {
			return;
		}
		if (!riftTarget.bCorpse && rift.havocEnergy > 0) {
			self.owner.A_GiveInventory("HavocEnergy", rift.havocEnergy);
		}
		if (self.reverseRift && rift.Distance3D(riftTarget) <= 250 && rift.Distance3D(self.reverseRift) <= 2000) {
			Actor lastTracer = riftTarget.tracer;
			riftTarget.tracer = self.reverseRift;
			riftTarget.A_Warp(AAPTR_TRACER, Random(-riftTarget.radius, riftTarget.radius), Random(-riftTarget.radius, riftTarget.radius), 0, 0, 0);
			riftTarget.tracer = lastTracer;
		}
	}
	
	// Fire Riftbolt Splints:
	virtual void FireRiftboltSplints(Actor trigger) {
		int riftCount = self.rifts.Size();
		for (int i = 0; i < riftCount; i++) {
			VoidManipulationRift rift = self.rifts[i];
			if (rift && rift != trigger) {
				bool projectileSpawned;
				Actor projectile;
				[projectileSpawned, projectile] = rift.A_SpawnItemEx("VoidSplintProjectile", 0, 0, rift.height / 2, 0, 0, 0, self.owner.angle, SXF_ABSOLUTEANGLE|SXF_ABSOLUTEVELOCITY|SXF_MULTIPLYSPEED);
				VoidSplintProjectile voidSplintProjectile = VoidSplintProjectile(projectile);
				if (voidSplintProjectile) {
					voidSplintProjectile.Relaunch(self.lastBoostedHitPos);
					voidSplintProjectile.target = self.owner;
					voidSplintProjectile.voidManipulation = self;
					voidSplintProjectile.riftbolt = 0; // No infinite projectiles!
				}
			}
		}
	}
	
	// Primary Void Splint:
	action void A_VoidSplintStart() {
		invoker.splintSpeed = 0;
	}
	
	action state A_VoidSplint(int round) {
		if (round == 1) {
			if (!A_SpendAmmo(1)) {
				return ResolveState("Ready");
			}
			if (invoker.splintSpeed < 16) {
				invoker.splintSpeed++;
			}
		}
		if (round == 2 && invoker.splintSpeed < 3) {
			return null;
		}
		if (round == 3 && invoker.splintSpeed < 8) {
			return null;
		}
		if (round == 4 && invoker.splintSpeed < 16) {
			return null;
		}
		
		A_StartSound("VoidManipulation/Fire", CHAN_WEAPON);
		VoidSplintProjectile voidSplintProjectile = VoidSplintProjectile(A_FireProjectile("VoidSplintProjectile", Random(-1, 1), false, Random(-1, 1), Random(-2, 2)));
		if (voidSplintProjectile) {
			voidSplintProjectile.voidManipulation = invoker;
			voidSplintProjectile.riftbolt = invoker.GetPassiveLevel();
		}
		return null;
	}
	
	// Secondary Tear Rift:
	action void A_TearRiftStart() {
		invoker.targetRift = null;
	}
	
	action state A_TearRift(bool useAmmo) {
		if (invoker.targetRift && invoker.targetRift.sustained) {
			return ResolveState("RiftSustained");
		}
		
		if (useAmmo && !A_SpendAmmo(2)) {
			return ResolveState("Ready");
		}
		
		A_StartSound("VoidManipulation/Fire/Rift", CHAN_WEAPON, CHANF_LOOPING); // Stopped at the end of altfire.
		TearRiftProjectile tearRiftProjectile = TearRiftProjectile(A_FireProjectile("TearRiftProjectile", 0, false, 0, 0));
		if (tearRiftProjectile) {
			tearRiftProjectile.voidManipulation = invoker;
		}
		
		return null;
	}
	
	// Special 1 Gaping Rift:
	action void A_GapingRift() {
		if (!A_CheckSpecial(1) || !A_SpendAmmo(3)) {
			return;
		}
		
		TearRiftProjectile tearRiftProjectile = TearRiftProjectile(A_FireProjectile("GapingRiftProjectile", 0, false, 0, 0));
		if (tearRiftProjectile) {
			tearRiftProjectile.voidManipulation = invoker;
		}
	}
	
	// Special 2 Hungering Vengeance:
	action void A_HungeringVengeance() {
		if (!A_CheckSpecial(2) || !A_SpendAmmo(4)) {
			return;
		}
		
		A_StartSound("VoidManipulation/Fire/Hunger", CHAN_WEAPON);
		A_FireProjectile("HungeringVengeanceProjectile", 0, false, 0, 0);
	}
	
	// Special 3 Reverse Rift:
	action void A_ReverseRift() {
		if (!A_CheckSpecial(3) || !A_SpendAmmo(5)) {
			return;
		}
		
		TearRiftProjectile tearRiftProjectile = TearRiftProjectile(A_FireProjectile("ReverseRiftProjectile", 0, false, 0, 0));
		if (tearRiftProjectile) {
			tearRiftProjectile.voidManipulation = invoker;
		}
	}
	
	// Special 4 Nixian Rift:
	action void A_NixianRift() {
		if (!A_CheckSpecial(4) || !A_SpendAmmo(6)) {
			return;
		}
		
		// Teleport to Nixian Rift:
		if (invoker.nixianRift) {
			invoker.owner.tracer = invoker.nixianRift;
			invoker.owner.A_Warp(AAPTR_TRACER);
			invoker.owner.A_StartSound("Teleport/Void", CHAN_5);
			for (int i = 0; i < 16; i++) {
				A_FireProjectile("VoidSplintProjectile", 22.5 * i, false, 0, Random(-2, 2), 0, -invoker.owner.pitch);
			}
			return;
		}

		// Create Nixian Rift:
		bool portalSpawned;
		Actor nixianRift;
		[portalSpawned, nixianRift] = invoker.owner.A_SpawnItemEx("VoidManipulationRiftNixian", 0, 0, 0, 0, 0, 0, 0, SXF_SETMASTER|SXF_NOPOINTERS);
		invoker.nixianRift = VoidManipulationRiftNixian(nixianRift);
		if (invoker.nixianRift) {
			invoker.nixianRift.voidManipulation = invoker;
			invoker.AddRift(invoker.nixianRift);
			invoker.owner.A_StartSound("VoidManipulation/Fire/Nixian", CHAN_5);
		}
	}
	
	// Ultimate Hollow Space:
	action void A_HollowSpace() {
		A_StartSound("VoidManipulation/Fire/Hollow", CHAN_WEAPON, CHANF_LOOPING);
		HollowSpaceProjectile hollowSpaceProjectile = HollowSpaceProjectile(A_FireProjectile("HollowSpaceProjectile", 0, false, 0, 0, FPF_NOAUTOAIM, -90));
		if (hollowSpaceProjectile) {
			hollowSpaceProjectile.voidManipulation = invoker;
		}
	}
	
	States {
		Spawn:
			REIB C 4 bright;
			Loop;
			
		MainHand.Select:
			REH2 A 1 A_Raise(12);
			Loop;
			
		MainHand.Deselect:
			REH2 A 1 A_Lower(12);
			Loop;
			
		MainHand.Ready:
			REH2 AABBCCDD 3 {
				return A_SchismWeaponReady();
			}
			Loop;
			
		MainHand.Fire:
			REH1 A 0 A_ClearBobbing();
			REH2 I 4 {
				A_VoidSplintStart();
				A_Light2();
			}
		Hold:
			REH2 N 1 {
				return A_VoidSplint(1);
			}
			REH2 N 1  {
				return A_VoidSplint(4);
			}
			REH2 O 1  {
				return A_VoidSplint(3);
			}
			REH2 O 1  {
				return A_VoidSplint(4);
			}
			REH2 P 1  {
				return A_VoidSplint(2);
			}
			REH2 P 1  {
				return A_VoidSplint(4);
			}
			REH2 O 1  {
				return A_VoidSplint(3);
			}
			REH2 O 1  {
				return A_VoidSplint(4);
			}
			REH2 P 0 A_ReFire("Hold");
			REH2 P 0 A_Light0();
			Goto Ready;
			
		MainHand.AltFire:
			REH1 A 0 {
				A_ClearBobbing();
				A_TearRiftStart();
			}
			REH2 IE 4 A_Light2();
		AltHold:
			REH2 F 1 {
				return A_TearRift(true);
			}
			REH2 FFFGGGGHHHH 1 {
				return A_TearRift(false);
			}
			REH2 N 4 A_ReFire("AltHold");
			REH2 M 4 {
				A_StopSound(CHAN_WEAPON);
				A_Light0();
			}
			Goto Ready;
		RiftSustained:
			REH2 NOP 4 A_Light2();
			REH2 A 0 A_ReFire("MainHand.AltFire");
			REH2 A 0 A_Light0();
			Goto Ready;
		
		MainHand.User1:
			REH1 A 0 A_ClearBobbing();
			REH2 IL 4;
			REH2 P 4 A_GapingRift();
			REH2 NI 4 A_Light0();
			Goto Ready;
		
		MainHand.User2:
			REH1 A 0 A_ClearBobbing();
			REH2 IL 4;
			REH2 G 4 A_HungeringVengeance();
			REH2 HI 4 A_Light0();
			Goto Ready;
			
		MainHand.User3:
			REH1 A 0 A_ClearBobbing();
			REH2 IE 4 A_Light2();
		User3Hold:
			REH2 F 4 A_ReverseRift();
			REH2 GHN 4;
			REH2 M 4 A_Light0();
			Goto Ready;
		
		MainHand.User4:
			REH1 A 0 A_ClearBobbing();
			REH2 G 4;
			REH2 H 4 A_NixianRift();
			REH2 I 4 A_Light0();
			Goto Ready;
		
		MainHand.Ultimate:
			REH1 A 0 A_ClearBobbing();
			REH2 G 4;
			REH2 H 4 A_HollowSpace();
			REH2 I 4 A_Light0();
			Goto Ready;
			
			
		OffHand.Select:
		OffHand.Deselect:
		OffHand.Ready:
			REH2 I 0 A_OverlayFlags(10, PSPF_FLIP|PSPF_MIRROR, true);
			REH2 IJKLM 6;
			Loop;
		
		OffHand.Fire:
		OffHand.AltFire:
		OffHand.User1:
		OffHand.User2:
		OffHand.User3:
		OffHand.User4:
		OffHand.Ultimate:
		OffHand.Reload:
			REH2 I 0 A_OverlayFlags(10, PSPF_FLIP|PSPF_MIRROR, true);
			REH2 IJKLM 4;
			Loop;
	}
}


class VoidManipulationPuff : BulletPuff {
	Default {
		Scale 0.5;
		Alpha 0.5;
	}
	
	States {
		Spawn:
			VDPT ABCDEFGHI 1;
		Melee:
			VDPT ABCDEFGHI 1;
			Stop;
	}
}


#include "zscript/schism/weapons/reilodos/voidmanipulation/voidrift.zs"
#include "zscript/schism/weapons/reilodos/voidmanipulation/voidsplint.zs"
#include "zscript/schism/weapons/reilodos/voidmanipulation/tearrift.zs"
#include "zscript/schism/weapons/reilodos/voidmanipulation/gapingrift.zs"
#include "zscript/schism/weapons/reilodos/voidmanipulation/hungeringvengeance.zs"
#include "zscript/schism/weapons/reilodos/voidmanipulation/reverserift.zs"
#include "zscript/schism/weapons/reilodos/voidmanipulation/nixianrift.zs"
#include "zscript/schism/weapons/reilodos/voidmanipulation/hollowspace.zs"
#include "zscript/schism/weapons/reilodos/voidmanipulation/riftbolt.zs"
