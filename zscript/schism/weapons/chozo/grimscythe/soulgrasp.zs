class SoulGraspSigil : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Grim Scythe can now cast Soul Grasp at the cost of Soul Essence!";
		
		Inventory.Icon "GSCIC0";
		Inventory.PickupSound "GrimScythe/Pickup";
		Inventory.RestrictedTo "Chozo";
		WeaponUpgrade.ExtraAmmoClass "GrimRune";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			GSCI C 1 bright;
			Loop;
	}
}

class GrimScytheSoulGraspProjectile : SchismProjectile {
	Default {
		Radius 20;
		Height 10;
		Scale 1.5;
		Speed 60;
		DamageFunction 10;
		Alpha 0.75;
		RenderStyle "Add";
		
		SeeSound "GrimScythe/Fire/Grasp";
		DeathSound "GrimScythe/Explode/Grasp";
		
		+NODAMAGETHRUST;
		+PAINLESS;
		+SEEKERMISSILE;
		+SCREENSEEKER;
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		
		// Taunt and Grasp:
		if (specialDamage > 0) {
			targetActor.target = target;
			if (!targetActor.bBoss) {
				targetActor.A_Warp(AAPTR_TARGET, target.Radius + targetActor.radius, 0, 0, 0, WARPF_NOCHECKPOSITION);
			}
		}
		
		return specialDamage;
	}
	
	States {
		Spawn:
			GSCP OOOOPPPPQQQQ 1 A_SeekerMissile(0, 20, SMF_LOOK|SMF_PRECISE);
			Loop;
			
		Death:
			GSCP OPQRS 4;
			Stop;
	}
}
