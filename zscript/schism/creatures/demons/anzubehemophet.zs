class AnzuBehemophet : Behemophet {
	Default {
		Tag "AnzuBehemophet";
		Speed 7;
		PainChance 80;
		
		SeeSound "AnzuBehemophet/See";
		ActiveSound "AnzuBehemophet/Idle";
		PainSound "AnzuBehemophet/Pain";
		DeathSound "AnzuBehemophet/Death";
		Obituary "%o was annihilated by an Anzu Behemophet.";
	}
		
	States {
		Spawn:
			ANZU AB 10 A_Look();
			Loop;
			
		See:
			ANZU AABBCCDD 3 A_Chase();
			Loop;
		
		Melee:
		Missile:
			ANZU DE 8 A_FaceTarget();
			ANZU F 8 Bright A_SpawnProjectile("AnzuBehemophetRocket", 32);
			ANZU G 8;
			ANZU HIHI 4 Bright A_SPosAttackUseAtkSound();
			ANZU S 8;
			Goto See;
		
		Pain:
			ANZU J 4;
			ANZU J 4 A_Pain();
			Goto See;
		
		Death:
			ANZU J 8;
			ANZU K 8 A_Scream();
			ANZU L 8;
			ANZU M 8 A_CreatureDie();
			ANZU NOPQ 8;
			ANZU R -1 A_BossDeath();
			Stop;
			
		Raise:
			ANZU N 5;
			ANZU MLKJI 5;
			Goto See;
	}
}

class AnzuBehemophetRocket : Rocket {
	Default {
		Speed 10;
		Damage 10;
	}
}