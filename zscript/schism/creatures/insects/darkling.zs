class Darkling : MinionCreature {
	int wanderTicks;
	bool wandering;
	Actor latchTarget;
	float latchX;
	float latchY;
	
	Default {
		Health 20;
		Radius 3;
		Height 5;
		Speed 16;
		Scale 0.75;
		PainChance 250;
		Damage 2;
		BloodColor "00 ff 66";

		MinionCreature.SummonTime 10 * 35;

		+JUMPDOWN;
		
		SeeSound "Darkling/See";
		PainSound "Darkling/Pain";
		DeathSound "Darkling/Death";
		ActiveSound "Darkling/See";
		MeleeSound "Darkling/Melee";
		Obituary "%o was consumed by a Darkling!";
		HitObituary "%o was consumed by a Darkling!";
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		wanderTicks = 0;
		wandering = true;
		latchX = -10;
		latchY = 0;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		if (self.health > 0) {
			self.SpawnParticle("Shadow", 8, 1, (FRandom(-8, 8), FRandom(-8, 8), FRandom(10, 40)), Random(45, 315), Random(-10, 10), 0.25);
			if (self.latchTarget) {
				self.tracer = self.latchTarget;
				A_Warp(AAPTR_TRACER, latchTarget.Radius + self.latchX, latchTarget.Radius + self.latchY, self.latchTarget.Height / 2, 0, WARPF_ABSOLUTEOFFSET);
				if (self.latchTarget.health <= 0 || self.latchTarget != self.target) {
					self.latchTarget = null;
				}
			}
		}
	}
	
	action void A_RandomWander() {
		A_Look();
		if (invoker.wanderTicks++ >= 35) {
			invoker.wanderTicks = 0;
			invoker.wandering = !invoker.wandering;
		}
		if (invoker.wandering) {
			invoker.A_Wander();
		}
	}
	
	action void A_SetLeaping(bool isLeaping) {
		if (isLeaping) {
			invoker.gravity = 0.125;
		}
		else {
			invoker.gravity = 1;
		}
		invoker.bNODAMAGE = isLeaping;
		invoker.bNOPAIN = isLeaping;
	}
	
	action state A_DarklingLatchCheck() {
		if (invoker.latchTarget) {
			return ResolveState("Latching");
		}
		return null;
	}
	
	action state A_DarklingUnlatchCheck() {
		if (invoker.latchTarget) {
			return null;
		}
		return ResolveState("See");
	}
	
	action void A_DarklingAttack() {
		int darklingDamage = random(2, 4);
		invoker.A_CreatureMelee(darklingDamage);
		if (invoker.target && SchismInteraction.CanHarmTarget(invoker, invoker.target)) {
			invoker.latchX = random(-10, 10);
			invoker.latchY = random(-10, 10);
			invoker.latchTarget = invoker.target;
			SchismStatusEffects.InflictEffect("SchismWeaknessDebuff", invoker, invoker, invoker.target, 10 * 35);
		}
		invoker.A_GiveInventory("HavocEnergy", 1, AAPTR_MASTER);
		invoker.A_DamageSelf(-darklingDamage, "None", DMSS_FOILINVUL|DMSS_NOFACTOR|DMSS_NOPROTECT);
	}
	
	States {
		Spawn:
			DRKG CD 4 {
				A_SetLeaping(false);
				A_RandomWander();
				return A_DarklingLatchCheck();
			}
			Loop;
			
		See:
			DRKG CD 4 {
				A_SetLeaping(false);
				A_Chase();
				return A_DarklingLatchCheck();
			}
			Loop;
			
		Melee:
			DRKG E 4 A_FaceTarget();
			DRKG F 4 A_DarklingAttack();
			DRKG AB 4;
			Goto See;
			
		Missile:
			DRKG A 0 A_Jump(128, "See");
			DRKG F 3 A_FaceTarget();
			DRKG E 10 {
				A_SetLeaping(true);
				A_StartSound("Darkling/Melee");
				A_SkullAttack();
				ThrustThingZ(0, 6, 0, 1);
				for (int i = 0; i < 4; i++) {
					A_SpawnProjectile("FalseProjectile", 20, 0, 0, CMF_TRACKOWNER);
				}
			}
			DRKG F 8 {
				A_SetLeaping(false);
				A_Stop();
			}
			Goto See;
		
		Latching:
			DRKG H 4 {
				A_SetLeaping(false);
				return A_DarklingUnlatchCheck();
			}
			DRKG I 4 A_DarklingAttack();
			Loop;
			
		Pain:
			DRKG G 6 A_SetLeaping(false);
			DRKG B 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			DRKG J 5;
			DRKG K 5 A_Scream();
			DRKG L 5;
			DRKG M 5 A_NoBlocking();
			DRKG N 5;
		FadeOut:
			DRKG N 1 A_FadeOut(2);
			Loop;
	}
}
