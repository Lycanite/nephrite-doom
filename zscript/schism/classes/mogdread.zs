class Mogdread : SchismPlayerPawn {
	Default {
		Speed 1;
		Health 100;
		Radius 16;
		Height 56;
		Mass 100;
		Scale 0.8;
		PainChance 255;
		RadiusDamageFactor 0.25;

		DamageFactor "Fire", 0;
		DamageFactor "Lava", 0;
		DamageFactor "Slime", 0;
		
		Player.DisplayName "Mogdread";
		Player.SoundClass "Mogdread";
		Player.CrouchSprite "MOGC";
		//Player.ScoreIcon "MOGFACE";
		Player.ColorRange 0, 0;
		Player.DamageScreenColor "ff 55 00";
		
		Player.ForwardMove 1, 1;
		Player.SideMove 1, 1;
		Player.JumpZ 8.0;
		Player.HexenArmor 10, 10, 25, 5, 20;
		Player.InvulnerabilityMode "Reflective";
		Player.HealRadiusType "Health";
		
		Player.FlechetteType "ArtiPoisonBag3";
		Player.StartItem "FirestoneCane";
		Player.StartItem "FirestoneCharge", 100;
		
		Player.WeaponSlot 1, "Chainsaw", "Gauntlets"; // Melee
		Player.WeaponSlot 2, "FirestoneCane"; // Primary
		Player.WeaponSlot 3, "Shotgun", "SuperShotgun", "Crossbow", "StrifeCrossbow", "CWeapStaff", "LavaCannon"; // Secondary
		Player.WeaponSlot 4, "Chaingun", "Blaster", "AssaultGun", "CWeapFlame", "AlchemicalRaygun"; // Tertiary
		Player.WeaponSlot 5, "RocketLauncher", "Mace", "CWeapWraithverge", "MiniMissileLauncher"; // Powerful
		Player.WeaponSlot 6, "PlasmaRifle", "SkullRod", "FlameThrower", "Mauler"; // Special
		Player.WeaponSlot 7, "BFG9000", "PhoenixRod", "StrifeGrenadeLauncher"; // Ultimate

		SchismPlayerPawn.BaseName "Mogdread";
		SchismPlayerPawn.HexenClass "Cleric";
		SchismPlayerPawn.AbilityCooldownMax 10 * TICRATE;
		SchismPlayerPawn.AbilityBCooldownMax 10 * TICRATE;
	}
	
	override void Tick() {
		super.Tick();
		
		// Ablity Particles:
		if (self.abilityCooldown > 7 * TICRATE) {
			int particleRange = self.radius / 4;
			int particleVert = self.height / 4;
			self.SpawnParticle(
				"Smoke", TICRATE, 0.5, (
					FRandom(-particleRange, particleRange),
					-10,
					(self.height / 2) + FRandom(-particleVert, particleVert)
				),
				Random(0, 359), Random(-45, 45),
				1.7
			);
		}
		if (self.abilityBCooldown > 50 * TICRATE) {
			int particleRange = self.radius / 4;
			int particleVert = self.height / 4;
			self.SpawnParticle(
				"FireEmber", TICRATE, 0.5, (
					FRandom(-particleRange, particleRange),
					-10,
					(self.height / 2) + FRandom(-particleVert, particleVert)
				),
				Random(0, 359), Random(-45, 45),
				1.7, 0xFF2222
			);
		}
	}
	
	override int DamageMobj(Actor inflictor, Actor source, int damage, Name damageType, int flags, double angle) {
		if (self.passiveEnabled) {
			if (damageType == "Fire" || damageType == "Lava" || damageType == "Slime") {
				return 0;
			}
		}
		return super.DamageMobj(inflictor, source, damage, damageType, flags, angle);
	}
	
	override string GetAmmoClass(int category, int size) {
		string sizeString = "";
		if (size == 1) {
			sizeString = "Large";
		}

		switch(category) {
			case -1:
				return "InfernalCharge" .. sizeString;
			case 0:
				return "FirestoneCharge" .. sizeString;
			case 1:
				return "MogLavaCharge" .. sizeString;
			case 2:
				return "LightCharge" .. sizeString;
			default:
				return super.GetAmmoClass(category, size);
		}
	}
	
	override class<Inventory> GetWeaponClass(int weaponCategory, int index) {
		switch(weaponCategory) {
			case 0:
				switch(index) {
					case 0:
						return "FirestoneCane";
					case 1:
						return "InfernoSpiralGem";
					case 2:
						return "FlameCombustionGem";
					case 3:
						return "DelphicNovaeGem";
					case 4:
						return "GlasmaShowerGem";
					case 5:
						return "IgnitionGem";
					default:
						return "";
				}

			case 1:
				switch(index) {
					case 0:
						return "LavaCannon";
					case 1:
						return "OverheatModule";
					case 2:
						return "EruptionModule";
					case 3:
						return "VolcanoModule";
					case 4:
						return "MineModule";
					case 5:
						return "ExhaustModule";
					default:
						return "";
				}

			case 2:
				switch(index) {
					case 0:
						return "AlchemicalRaygun";
					case 1:
						return "ReactionModule";
					case 2:
						return "CombustionModule";
					case 3:
						return "LaserModule";
					case 4:
						return "ExperimentalGlasmaModule";
					case 5:
						return "RadiationModule";
					default:
						return "";
				}

			default:
				return "";
		}
	}

	override class<Inventory> GetSigilClass() {
		return "StarStone";
	}
	
	override int GetWeaponPassiveLevel(int category) {
		switch (category) {
			case 0:
				return min(CountInv("IgnitionGem"), 3);
			case 1:
				return min(CountInv("ExhaustModule"), 3);
			case 2:
				return min(CountInv("RadiationModule"), 3);
			default:
				return super.GetWeaponPassiveLevel(category);
		}
	}
	
	override bool SchismSpecial(int specialId) {
		if (!super.SchismSpecial(specialId)) {
			return false;
		}
		
		if (specialId == 1) {
			Smokescreen();
		}
		else if (specialId == 2) {
			Overclock();
		}
		
		return true;
	}
	
	virtual void Smokescreen() {
		A_StartSound("Mogdread/Smokescreen", CHAN_5);
		SchismStatusEffects.InflictEffect("SchismCloakEffect", self, self, self, 3 * TICRATE, "", 3);
		for (int i = 0; i < 40; i++) {
			self.SpawnParticle("Smoke", 2 * TICRATE, 1, (FRandom(-60, 60), FRandom(-60, 60), FRandom(-4, 4)), Random(0, 359), Random(-10, 10));
		}
	}
	
	virtual void Overclock() {
		A_StartSound("Mogdread/Overclock", CHAN_5);
		SchismHasteBuff hasteEffect = SchismHasteBuff(SchismStatusEffects.InflictEffect("SchismHasteBuff", self, self, self, 10 * TICRATE, "", 3));
		if (hasteEffect) {
			hasteEffect.ammoCostMultiplier = 2;
		}
		for (int i = 0; i < 20; i++) {
			self.SpawnParticle("FireEmber", 2 * TICRATE, 1, (FRandom(-40, 40), FRandom(-40, 40), FRandom(-4, 4)), Random(0, 359), Random(-10, 10));
			self.SpawnParticle("FireBlast", 2 * TICRATE, 1, (FRandom(-40, 40), FRandom(-40, 40), FRandom(-4, 4)), Random(0, 359), Random(-10, 10));
		}
	}
	
	States {
		Spawn:
			MGDR A -1;
			Loop;

		See:
			MGDR ABCD 4;
			Loop;

		Missile:
			MGDR E 8;
			Goto Spawn;
			
		Melee:
			MGDR F 8;
			Goto Spawn;

		Pain:
			MGDR G 4;
			MGDR G 4 A_Pain();
			Goto Spawn;

		Death:
			MGDR G 6;
			MGDR H 6 A_PlayerScream();
			MGDR IJ 6;
			MGDR K 6 A_NoBlocking();
			MGDR LM 6;
			MGDR N -1;
			Stop;

		XDeath:
		Burn:
		Ice:
		Disintegrate:
			MGDR O 5 A_PlayerScream();
			MGDR P 0 A_NoBlocking();
			MGDR Q 5 A_SkullPop();
			MGDR RSTUV 5;
			MGDR W -1;
			Stop;
    }
}