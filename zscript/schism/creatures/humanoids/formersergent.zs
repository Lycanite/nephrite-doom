class FormerSergent : FormerPrivate {
	Default {
		Tag "Former Sergent";
		Health 30;
		PainChance 170;

        DropItem "Shotgun";

		AttackSound "FormerSergent/Fire";
		Obituary "%o was put out of commision by a Former Sergent!";
    }

    States {
        Spawn:
			FSGT AB 10 A_Look();
			Loop;
			
		See:
			FSGT AABBCCDD 4 A_Chase();
			Loop;
			
		Missile:
			FSGT E 10 A_FaceTarget();
			FSGT F 8 A_CreatureHitscan(5, 15, 3);
            FSGT E 8;
			Goto See;
			
		Pain:
			FSGT G 3;
			FSGT G 3 A_Pain();
			Goto See;
			
		Death:
			FSGT H 5;
			FSGT I 5 A_Scream();
			FSGT J 5 A_CreatureDie();
			FSGT KLM 5;
			FSGT N -1;
			Stop;
			
		XDeath:
			FSGT O 5;
			FSGT P 5 A_Scream();
			FSGT Q 5 A_CreatureDie();
			FSGT RSTUV 5;
			FSGT W -1;
			Stop;
			
		Raise:
			FSGT MLKJIH 5;
			Goto See;
    }
}