class FormerCaptain : FormerMajor {
	Default {
		Tag "Former Captain";

		AttackSound "FormerCaptain/Fire";
		Obituary "%o was gunned down by a Former Captain!";
    }

    States {
        Spawn:
			FCPT AB 10 A_Look();
			Loop;
			
		See:
			FCPT AABBCCDD 4 A_Chase();
			Loop;
			
		Missile:
			FCPT E 10 A_FaceTarget();
			FCPT FEFE 4 A_CreatureHitscan(5, 15, 3);
            FCPT F 1 A_MonsterRefire(40, "See");
			Loop;
			
		Pain:
			FCPT G 3;
			FCPT G 3 A_Pain();
			Goto See;
			
		Death:
			FCPT H 5;
			FCPT I 5 A_Scream();
			FCPT J 5 A_CreatureDie();
			FCPT K 5;
			FCPT L -1;
			Stop;
			
		XDeath:
			FCPT Q 5;
			FCPT R 5 A_Scream();
			FCPT S 5 A_CreatureDie();
			FCPT ST 5;
			FCPT U -1;
			Stop;
			
		Raise:
			FCPT KJIH 5;
			Goto See;
    }
}