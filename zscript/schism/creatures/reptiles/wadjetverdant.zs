class WadjetVerdant : Wadjet {
	Default {
		Tag "Verdant Wadjet";
		
		Obituary "%o was evicerated by a Verdant Wadjet!";
		HitObituary "%o was evicerated by a Verdant Wadjet!";
	}
	
	States {
		Spawn:
			WADV AABBCCBB 4 {
				return A_MinionIdle();
			}
			Loop;
			
		Guard:
			WADV AABBCCBB 4 {
				return A_MinionGuard();
			}
			Loop;
			
		Follow:
			WADV DEFE 4 {
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			WADV DEFE 4 {
				return A_MinionChase();
			}
			Loop;
			
		Portal:
			WADV GHI 8 {
				return A_WadjetPortal();
			}
			Loop;
			
		Melee:
			WADV J 8 A_FaceTarget();
			WADV K 4 A_CreatureMelee(25, 45);
			WADV L 4;
			Goto See;
			
		Pain:
			WADV M 6;
			WADV M 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			WADV N 8;
			WADV O 8 A_Scream();
			WADV PQ 8;
			WADV R 8 A_CreatureDie();
			WADV STUVW 8;
			WADV X -1;
			Stop;
	}
}
