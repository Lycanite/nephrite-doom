class WeaponUpgrade : Inventory {
	int ExtraAmmoAmount;
	class<Inventory> ExtraAmmoClass;
	
	property ExtraAmmoAmount: ExtraAmmoAmount;
	property ExtraAmmoClass: ExtraAmmoClass;
	
	Default {
		Inventory.PickupMessage "You have found a weapon upgrade!";
		Inventory.Amount 1;
		Inventory.MaxAmount 1;
		WeaponUpgrade.ExtraAmmoAmount 100;
		WeaponUpgrade.ExtraAmmoClass "";
		
		RenderStyle "Normal";
		
		+WEAPONSPAWN;
		+INVENTORY.RESTRICTABSOLUTELY;
		+INVENTORY.PERSISTENTPOWER;
		+INVENTORY.ALWAYSPICKUP;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		extraAmmoAmount = 100;
	}
	
	override bool CanPickup(Actor toucher) {
		if (!super.CanPickup(toucher)) {
			return false;
		}
		if (toucher.CountInv(self.GetClassName()) >= self.MaxAmount) {
			if (self.ExtraAmmoClass) {
				toucher.A_GiveInventory(self.ExtraAmmoClass, self.ExtraAmmoAmount);
			}
		}
		return true;
	}

	States {
		Spawn:
			REIS A 1 bright;
			Loop;
	}
}
