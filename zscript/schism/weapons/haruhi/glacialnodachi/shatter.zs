class ShatterFractal : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Glacial Nodachi can now cast Shattering Strike!";
		
		Inventory.Icon "GNODX0";
		Inventory.PickupSound "GlacialNodachi/Pickup";
		Inventory.RestrictedTo "Haruhi";
		WeaponUpgrade.ExtraAmmoClass "GlacialCharge";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}
	
	States {
		Spawn:
			GNOD X 1 bright;
			Loop;
	}
}
