class FoulMenagerie : ReilodosSpell {
	int wadjetLimit;
	Array<Actor> foulBurstSources;
	
	Default {
		Radius 20;
		Height 16;
		Scale 0.5;
		
		Inventory.PickupSound "FoulMenagerie/Pickup";
		Weapon.UpSound "FoulMenagerie/Up";
		Weapon.ReadySound "";
		SchismWeapon.FailSound "FoulMenagerie/Fire/Fail";
		
		SchismWeapon.PrimaryCost 1;
		SchismWeapon.SecondaryCost 1;
		SchismWeapon.SpecialACost 50;
		SchismWeapon.SpecialBCost 50;
		SchismWeapon.SpecialCCost 50;
		SchismWeapon.SpecialDCost 50;
		
		SchismWeapon.SpecialItemA "ArgusScroll";
		SchismWeapon.SpecialItemB "EntropicBoltScroll";
		SchismWeapon.SpecialItemC "BedlamFiendScroll";
		SchismWeapon.SpecialItemD "WarpWadjetsScroll";
		SchismWeapon.PassiveItem "ChaoticChupacabraScroll";
		
		Inventory.PickupMessage "The menacing powers of the Foul Menagerie have been bestowed upon you!";
		Obituary "%o was torn to pieces by %k's Foul Menagerie!";
		SchismWeapon.InsufficientAmmoAMessage "\caNot enough Chaos Energy to cast this spell.";
		SchismWeapon.InsufficientAmmoBMessage "\caNot enough Havoc Energy to cast this spell.";
		SchismWeapon.NoSpecialAMessage "\caYou need an Argus Scroll to cast this spell.";
		SchismWeapon.NoSpecialBMessage "\caYou need an Entropic Bolt Scroll to cast this spell.";
		SchismWeapon.NoSpecialCMessage "\caYou need a Bedlam Fiend Scroll to cast this spell.";
		SchismWeapon.NoSpecialDMessage "\caYou need a Warp Wadjets Scroll to cast this spell.";
		SchismWeapon.IdleBob 30;
	}
	
	
	override void BeginPlay() {
		super.BeginPlay();
		self.wadjetLimit = 3;
	}
	
	virtual void AddFoulBurstSource(Actor burstSource) {
		if (!burstSource) {
			return;
		}
		self.foulBurstSources.Push(burstSource);
	}
	
	virtual void RemoveFoulBurstSource(Actor burstSource) {
		if (!burstSource) {
			return;
		}
		int burstIndex = self.foulBurstSources.Find(burstSource);
		if (burstIndex < self.foulBurstSources.Size()) {
			self.foulBurstSources.Delete(burstIndex, 1);
		}
	}
	
	
	// Primary Chaos Orb:
	action void A_ChaosOrb() {
		A_FireProjectile("FoulMenageriePortalBolt", 0, false);
	}
	
	// Secondary Foul Burst:
	action state A_FoulBurst() {
		if (!A_CheckAmmo(2)) {
			return ResolveState("Ready");
		}
		
		bool anyBursts = false;

		// Burst Chaos Minions:
		int minionCount = invoker.GetSchismPlayerInfo().GetMinionCountAll(invoker.PlayerNumber());
		for (int i = 0; i < minionCount; i++) {
			ChaosMinion minion = ChaosMinion(invoker.GetSchismPlayerInfo().GetMinion(invoker.PlayerNumber(), "", i));
			if (minion) {
				string foulBurstProjectileClass = "FoulBurstProjectile";
				if (minion is "ChupacabraPhosphor" && invoker.GetPassiveLevel() < 3) {
					continue;
				}
				if (minion is "WadjetVerdant") {
					foulBurstProjectileClass = "LightFoulBurstProjectile";
				}
				minion.EmitFollowProjectile(foulBurstProjectileClass, minionCount);
				anyBursts = true;
			}
		}
		
		// Burst Entropic Charge:
		int extraBurstCount = invoker.foulBurstSources.Size();
		for (int i = 0; i < extraBurstCount; i++) {
			Actor burstSource = invoker.foulBurstSources[i];
			if (burstSource) {
				FoulBurstProjectile foulBurstProjectile = FoulBurstProjectile(SchismAttacks.EmitFollowProjectile("FoulBurstProjectile", burstSource, self));
				if (foulBurstProjectile) {
					foulBurstProjectile.havoc = 1;
					anyBursts = true;
				}
			}
		}
		
		if (anyBursts) {
			A_SpendAmmo(2);
		}
		
		return null;
	}
	
	// Special 1 Argusbolt:
	action state A_Argusbolt() {
		if (!A_CheckSpecial(1) || !A_SpendAmmo(3)) {
			return ResolveState("Ready");
		}
		A_FireProjectile("FoulMenagerieArgusbolt", 0, false);
		
		return null;
	}
	
	// Special 2 Entropic Bolt:
	action state A_Entropicbolt() {
		if (!A_CheckSpecial(2) || !A_CheckAmmo(4)) {
			return ResolveState("Ready");
		}
		FoulMenagerieEntropicBolt entropicBoltProjectile = FoulMenagerieEntropicBolt(A_FireProjectile("FoulMenagerieEntropicbolt", 0, false, 0, 0));
		if (entropicBoltProjectile) {
			entropicBoltProjectile.primaryCost = invoker.PrimaryCost;
			entropicBoltProjectile.secondaryCost = invoker.SpecialBCost;
			entropicBoltProjectile.foulMenagerie = invoker;
		}
		
		return null;
	}
	
	// Special 3 Bedlam Fiend:
	action state A_BedlamFiend() {
		if (!A_CheckSpecial(3) || !A_CheckAmmo(5)) {
			return ResolveState("Ready");
		}
		A_FireProjectile("FoulMenagerieNomadrumBolt", 0, false);
		return null;
	}
	
	// Special 4 Warp Wadjets:
	action void A_WarpWadjets() {
		if (!A_CheckSpecial(4)) {
			return;
		}

		// Attempt to Spawn:
		string wadjetClass = "WadjetVerdant";
		bool spawned = false;
		Actor wadjetActor = null;
		[spawned, wadjetActor] = invoker.owner.A_SpawnItemEx(wadjetClass, invoker.owner.radius + 64, 0, 0, 0, 0, 0, 0, SXF_NOCHECKPOSITION|SXF_SETMASTER|SXF_NOPOINTERS);
		WadjetVerdant wadjet = WadjetVerdant(wadjetActor);
		if (wadjet) {
			wadjet.master = invoker.owner;
			wadjet.angle = wadjet.angle + 180;
			wadjet.permanentSummon = true;
			wadjet.summonPos = invoker.owner.pos;
		}
		
		// Spawn Fail:
		if (!wadjet) {
			A_StartSound("FoulMenagerie/Fire/Fail", CHAN_WEAPON);
			A_Print("\caThe Wadjet's spawn location is blocked.", 2);
			return;
		}
		
		// Spawn Success:
		A_StartSound("Wadjet/See", CHAN_WEAPON);
		A_SpendAmmo(6);
		
		// Remove Excess Wadjets:
		int wadjetMinionCount = invoker.GetSchismPlayerInfo().GetMinionCount(PlayerNumber(), wadjetClass); // Note: Spawned Wadjets don't get updated until they tick, destroyed do immediately.
		int missingWadjets = invoker.wadjetLimit - wadjetMinionCount;
		while (missingWadjets <= 0) {
			WadjetVerdant excessWadjet = WadjetVerdant(invoker.GetSchismPlayerInfo().GetMinion(PlayerNumber(), wadjetClass, 0));
			if (excessWadjet) {
				excessWadjet.Destroy();
			}
			missingWadjets++;
		}
	}
	
	// Passive Chaotic Chupacabra:
	action void A_ChaoticChupacabra() {
		int passiveLevel = invoker.GetPassiveLevel();
		if (passiveLevel <= 0 || --invoker.passiveCooldown > 0) {
			return;
		}
		invoker.passiveCooldown = 10 * TICRATE;
		
		int activeSummonCount = invoker.GetSchismPlayerInfo().GetMinionCount(PlayerNumber(), "ChupacabraPhosphor");
		int summonCap = 2;
		int summonsDue = min(passiveLevel, 2) - activeSummonCount;
		
		for (int i = 0; i < summonsDue; i++) {
			double projectileAngle = 90;
			if ((i + 1) % 2 == 0) {
				projectileAngle = -projectileAngle;
			}
			A_FireProjectile("FoulMenagerieChaoticChupacabraBolt", projectileAngle, false, 0, 0, FPF_NOAUTOAIM, 10);
		}
	}
	
	// Ultimate Havoc Serpent:
	action void A_HavocSerpent() {
		A_StartSound("FoulMenagerie/Fire/HavocSerpent");
		A_GiveInventory("HavocSerpentMorph", 1);
	}
	
	
	States {
		Spawn:
			REIB A 4 bright;
			Loop;
			
		MainHand.Select:
			REH1 A 1 A_Raise(12);
			Loop;
			
		MainHand.Deselect:
			REH1 A 1 A_Lower(12);
			Loop;
			
		MainHand.Ready:
			REH1 BBCCDD 3 {
				A_ChaoticChupacabra();
				return A_SchismWeaponReady();
			}
			Loop;
			
		MainHand.Fire:
			REH1 A 0 A_ClearBobbing();
			REH1 EFG 4 A_Light2();
		Hold:
			REH1 G 4;
			REH1 H 4 A_ChaosOrb();
			REH1 I 4;
			REH1 I 0 A_ReFire("Hold");
			REH1 I 0 A_Light0();
			Goto Ready;
			
		MainHand.AltFire:
			REH1 A 0 A_ClearBobbing();
			REH1 JKLS 2 A_Light2();
		AltHold:
			REH1 T 4 {
				return A_FoulBurst();
			}
			REH1 U 4;
			REH1 V 4 A_ReFire("AltHold");
			REH1 N 4 A_Light0();
			Goto Ready;
		
		MainHand.User1:
			REH1 A 0 A_ClearBobbing();
			REH1 G 4;
			REH1 H 4 A_Argusbolt();
			REH1 I 4;
			Goto Ready;
			
		MainHand.User2:
			REH1 A 0 A_ClearBobbing();
			REH1 EFGH 4;
			REH1 I 4  {
				return A_Entropicbolt();
			}
			REH1 GF 4;
			REH1 E 4 A_Light0();
			Goto Ready;
		
		MainHand.User3:
			REH1 A 0 A_ClearBobbing();
			REH1 JKLM 4;
			REH1 T 4  {
				return A_BedlamFiend();
			}
			REH1 UV 4;
			REH1 N 4 A_Light0();
			Goto Ready;
		
		MainHand.User4:
			REH1 A 0 A_ClearBobbing();
			REH1 G 4;
			REH1 H 4 A_WarpWadjets();
			REH1 I 4 A_Light0();
			Goto Ready;
			
		MainHand.Ultimate:
			REH1 NSRN 4;
			REH1 N 0 A_HavocSerpent();
			Goto Ready;
			
		OffHand.Select:
		OffHand.Deselect:
		OffHand.Ready:
			REFM ABCD 4;
			Loop;
		
		OffHand.Fire:
		OffHand.AltFire:
		OffHand.User1:
		OffHand.User2:
		OffHand.User3:
		OffHand.User4:
		OffHand.Reload:
			REFM EFGH 4 bright;
			Loop;
			
		OffHand.Ultimate:
			REH1 N 0 A_OverlayFlags(10, PSPF_FLIP|PSPF_MIRROR, true);
			REH1 NSRN 4;
			Loop;
	}
}

#include "zscript/schism/weapons/reilodos/foulmenagerie/portalbolt.zs"
#include "zscript/schism/weapons/reilodos/foulmenagerie/foulburst.zs"
#include "zscript/schism/weapons/reilodos/foulmenagerie/argusbolt.zs"
#include "zscript/schism/weapons/reilodos/foulmenagerie/entropicbolt.zs"
#include "zscript/schism/weapons/reilodos/foulmenagerie/bedlamfiend.zs"
#include "zscript/schism/weapons/reilodos/foulmenagerie/warpwadjets.zs"
#include "zscript/schism/weapons/reilodos/foulmenagerie/havocserpent.zs"
#include "zscript/schism/weapons/reilodos/foulmenagerie/chaoticchupacabra.zs"
