class Prototenet : MinionCreature {
	Default {
		Tag "Prototenet";

		Health 1000;
		Radius 28;
		Height 72;
		Mass 1000;
		Speed 8;
		Scale 0.75;
		PainChance 50;
		Damage 2;
		BloodColor "99 99 99";

		MinionCreature.SummonTime 25 * 35;
		
		SeeSound "Prototenet/See";
		PainSound "Prototenet/Pain";
		DeathSound "Prototenet/Death";
		ActiveSound "Prototenet/See";
		MeleeSound "Prototenet/Melee";
		Obituary "%o was crushed by a Prototenet!";
		HitObituary "%o was crushed by a Prototenet!";

		+DONTTHRUST;
	}
	
	States {
		Spawn:
			PRTN ABCB 8 {
				A_Look();
				return A_StayWithMaster();
			}
			Loop;
			
		Follow:
			PRTN DEFE 4 {
				A_Look();
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			PRTN DEFE 4 {
				return A_MinionChase();
			}
			Loop;
			
		Melee:
			PRTN G 8 A_FaceTarget();
			PRTN H 4 A_CreatureMelee(12, 20);
			PRTN I 4;
			Goto See;
			
		Pain:
			PRTN J 6;
			PRTN J 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			PRTN J 8;
			PRTN K 8 A_Scream();
			PRTN LM 8;
			PRTN M 8 A_CreatureDie();
			PRTN NO 8;
			PRTN P -1;
			Stop;
	}
}
