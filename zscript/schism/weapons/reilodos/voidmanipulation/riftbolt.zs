class RiftboltScroll : WeaponPassive {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Void Manipulation Splint projectiles are now empowered when they pass through Rifts!";
		
		Inventory.Icon "REISO0";
		Inventory.PickupSound "VoidManipulation/Pickup";
		Inventory.RestrictedTo "Reilodos", "ReilodosHavocSerpent";
		WeaponUpgrade.ExtraAmmoClass "VoidEnergy";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			REIS O 1 bright;
			Loop;
	}
}