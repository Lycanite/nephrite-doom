class Grimreaver : MinionCreature {
	LichScepter lichScepter;

	Default {
		Tag "Grimreaver";
		
		Health 300;
		Radius 20;
		Height 56;
		Speed 8;
		FloatSpeed 8;
		Scale 1;
		Gravity 0;
		PainChance 120;
		Damage 1;
		Alpha 0.8;
		BloodColor "44 00 00";
		
		MinionCreature.SummonTime 60 * 35;
		MinionCreature.Stance 1; // Follow
		
		SeeSound "Grimreaver/See";
		PainSound "Grimreaver/Pain";
		DeathSound "Grimreaver/Death";
		ActiveSound "Grimreaver/See";
		MeleeSound "Grimreaver/Melee";
		Obituary "%o was decapitated by a Grimreaver!";
		HitObituary "%o was decapitated by a Grimreaver!";
		
		+FLOAT;
		+THRUACTORS;
	}
	
	action void A_DeathNova() {
		LichScepterProjectile lichScepterProjectile = LichScepterProjectile(A_SpawnProjectile("LichScepterProjectile", 64));
		if (lichScepterProjectile) {
			lichScepterProjectile.summonMaster = invoker.GetFinalMaster();
			lichScepterProjectile.lichScepter = invoker.lichScepter;
		}
		if (!invoker.GetFinalMaster()) {
			return;
		}
		lichScepterProjectile = LichScepterProjectile(A_SpawnProjectile("LichScepterProjectile", 64));
		if (lichScepterProjectile) {
			lichScepterProjectile.summonMaster = invoker.GetFinalMaster();
			lichScepterProjectile.lichScepter = invoker.lichScepter;
			lichScepterProjectile.weaveDistance = 4;
			lichScepterProjectile.scale = lichScepterProjectile.scale * 0.75;
		}
		lichScepterProjectile = LichScepterProjectile(A_SpawnProjectile("LichScepterProjectile", 64));
		if (lichScepterProjectile) {
			lichScepterProjectile.summonMaster = invoker.GetFinalMaster();
			lichScepterProjectile.lichScepter = invoker.lichScepter;
			lichScepterProjectile.weaveDistance = -4;
			lichScepterProjectile.scale = lichScepterProjectile.scale * 0.75;
		}
	}
	
	action void A_BoneBurst() {
		for (int i = 0; i < 20; i += 20) {
			invoker.A_SpawnItemEx("SchismGoreBone", 0, 0, 10, random(-6, 6), random(-6, 6), random(10, 40), i, SXF_CLIENTSIDE);
		}
	}

	action state A_GrimreaverRefire() {
		if (!invoker.GetFinalMaster()) {
			return ResolveState("See");
		}
		return A_MonsterRefire(10, "See");
	}
	
	States {
		Spawn:
			GRVE AA 4 {
				return A_MinionIdle();
			}
			Loop;
			
		Guard:
			GRVE AA 4 {
				return A_MinionGuard();
			}
			Loop;
			
		Follow:
			GRVE AA 4 {
				return A_FollowMaster();
			}
			Loop;
			
		See:
			GRVE AA 4 {
				return A_MinionChase();
			}
			Loop;
		
		Missile:
			GRVE B 20 A_FaceTarget();
			GRVE B 10 A_FaceTarget();
			GRVE C 4 bright A_DeathNova();
			GRVE D 4 bright A_GrimreaverRefire();
			Goto Missile + 1;
			
		Pain:
			GRVE E 6;
			GRVE E 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			GRVE E 5;
			GRVE F 5 A_Scream();
			GRVE G 5;
			GRVE H 5 A_CreatureDie();
			GRVE IJKLM 5;
			GRVE N -1 A_BoneBurst();
			Stop;
	}
}
