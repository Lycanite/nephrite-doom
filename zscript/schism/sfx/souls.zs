class HellfireSoul : SchismSFX {   
	Default {
		Radius 1;
		Height 1;
		Speed 3;
		RenderStyle "Add";
		Alpha 0.8;

		SchismSFX.RandomRoll false;
	}
	
	States {
		Spawn:
			NSHF ABCD 6 bright;
			Stop;
	}
}