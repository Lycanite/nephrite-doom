class Malwrath : Creature {	
	Default {
		Tag "Malwrath";
		Health 400;
		Radius 31;
		Height 56;
		Mass 400;
		Speed 8;
		PainChance 128;
		BloodColor "Blue";
		
		SeeSound "Malwrath/See";
		ActiveSound "Malwrath/Idle";
		PainSound "Malwrath/Pain";
		DeathSound "Malwrath/Death";
		MeleeSound "Malwrath/Melee";
		Obituary "%o was blasted away by a greedy Malwrath.";
		HitObituary "%o was devoured by a greedy Malwrath.";
		
		+FLOAT;
		+NOGRAVITY;
		+CREATURE.HOVERBOB;
	}
	
	States {
		Spawn:
			MALW A 10 A_Look();
			Loop;
			
		See:
			MALW A 3 A_Chase();
			Loop;
			
		Melee:
			MALW BC 5 A_FaceTarget();
			MALW D 5 bright A_CreatureMelee(20, 40, "Nether", "Nether");
			Goto See;
			
		Missile:
			MALW B 5 {
				A_FaceTarget();
				A_StartSound(invoker.meleeSound);
			}
			MALW C 5 A_FaceTarget();
			MALW D 5 bright A_SpawnProjectile("GreedyBlastProjectile", invoker.height / 2);
			Goto See;
			
		Pain:
			MALW E 3;
			MALW E 3 A_Pain();
			MALW F 6;
			Goto See;
			
		Death:
			MALW G 8 ;
			MALW H 8 A_Scream();
			MALW IJ 8;
			MALW K 8 A_CreatureDie();
			MALW L -1 A_SetFloorClip();
			Stop;
			
		Raise:
			MALW L 8 A_UnSetFloorClip();
			MALW KJIHG 8;
			Goto See;
	}
}