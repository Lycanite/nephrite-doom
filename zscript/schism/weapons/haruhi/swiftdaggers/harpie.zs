class HarpieSash : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Swift Daggers can now cast Harpie Strike!";
		
		Inventory.Icon "SWDGW0";
		Inventory.PickupSound "SwiftDaggers/Pickup";
		Inventory.RestrictedTo "Haruhi";
		WeaponUpgrade.ExtraAmmoClass "WhirlwindCharge";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}
	
	States {
		Spawn:
			SWDG W 1 bright;
			Loop;
	}
}

class SwiftDaggersHarpieProjectile : SwiftDaggersProjectile {
	Default {
		Radius 16;
		Height 10;
		Scale 1;
		Speed 32;
		DamageFunction 10;
		
		SeeSound "SwiftDaggers/Projectile/Harpie";
		DeathSound "SwiftDaggers/Projectile/Harpie/Explode";
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.generateSpecialAmmo = false;
	}
	
	States {
		Spawn:
			SDP1 ABCDEFGHIJ 2;
			
		Death:
			SDP1 K 2 {
				SchismAttacks.Explode(invoker, invoker.target, 30, 100, 0, 1, false, false, "Air");
			}
			SDP1 L 2;
			Stop;
	}
}

class SwiftDaggersHarpieStrike : Powerup {
	bool strikeComplete;
	int powerupTime;
	
	Default {
		Powerup.Duration -3;
	}
	
	override void Tick() {
		super.Tick();
		
		if (self.owner && !self.strikeComplete && self.owner.vel.z == 0 && self.powerupTime++ > 10)
		{
			self.strikeComplete = true;
			for (int i = 10; i < 360; i += 10) {
				self.owner.A_SpawnProjectile("SwiftDaggersProjectile", 32, 0, i, CMF_AIMDIRECTION|CMF_ABSOLUTEPITCH|CMF_ABSOLUTEANGLE, 0);
			}
		}
	}
}
