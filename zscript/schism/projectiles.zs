#include "zscript/schism/projectiles/schismprojectile.zs"

// Fire:
#include "zscript/schism/projectiles/flamethrower.zs"
#include "zscript/schism/projectiles/fireball.zs"

// Earth:
#include "zscript/schism/projectiles/throwingscythe.zs"
#include "zscript/schism/projectiles/chromabolt.zs"

// Air:
#include "zscript/schism/projectiles/wind.zs"

// Order:
#include "zscript/schism/projectiles/hierobolt.zs"

// Chaos:
#include "zscript/schism/projectiles/entrostar.zs"
#include "zscript/schism/projectiles/maelsphere.zs"
#include "zscript/schism/projectiles/ruinerblast.zs"

// Acid
#include "zscript/schism/projectiles/acidbarb.zs"

// Lava
#include "zscript/schism/projectiles/magma.zs"

// Lightning
#include "zscript/schism/projectiles/staticshock.zs"

// Shadow
#include "zscript/schism/projectiles/duskflare.zs"

// Frost:
#include "zscript/schism/projectiles/frostshard.zs"

// Fae:
#include "zscript/schism/projectiles/lifestar.zs"
#include "zscript/schism/projectiles/animashard.zs"
#include "zscript/schism/projectiles/bloodbelch.zs"
#include "zscript/schism/projectiles/ichorspit.zs"

// Poison:
#include "zscript/schism/projectiles/poisonspatter.zs"
#include "zscript/schism/projectiles/toxicrocket.zs"
#include "zscript/schism/projectiles/noxiousdart.zs"

// Aether:
#include "zscript/schism/projectiles/smite.zs"

// Nether:
#include "zscript/schism/projectiles/doomfireball.zs"
#include "zscript/schism/projectiles/hellfireball.zs"
#include "zscript/schism/projectiles/greedyblast.zs"
#include "zscript/schism/projectiles/wrathorb.zs"
#include "zscript/schism/projectiles/vainhellcannon.zs"
#include "zscript/schism/projectiles/gluttonglobe.zs"
#include "zscript/schism/projectiles/netherstorm.zs"
#include "zscript/schism/projectiles/hadessphere.zs"

// Void:
#include "zscript/schism/projectiles/tendrilbolt.zs"
#include "zscript/schism/projectiles/voidgrazer.zs"