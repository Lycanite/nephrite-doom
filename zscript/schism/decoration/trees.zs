class SchismTree : Actor {
	bool user_skybox; // If true, forces the tree to always produce particles.
	int user_variant; // If non-zero, forces a variation.
	string user_season; // If set, overrides the season from the level default.

	int springVariants;
	property SpringVariants: springVariants; // The number of random spring season variants.
	int summerVariants;
	property SummerVariants: summerVariants; // The number of random summer season variants.
	int autumnVariants;
	property AutumnVariants: autumnVariants; // The number of random autumn season variants.
	int winterVariants;
	property WinterVariants: winterVariants; // The number of random winter season variants.

	vector2 baseScale; // The scale captured before random scaling is applied.

	Default {
		Tag "Tree";
		
		Health 100;
		Radius 24;
		Height 120;
		ProjectilePassHeight -16;
		
		+SOLID;
	}

	override void PostBeginPlay() {
		super.PostBeginPlay();
		self.baseScale = self.scale;
	}

	virtual void ReviveTree() {
		self.Revive();
		self.SetStateLabel("Spawn");
	}

	virtual void SetSeason(int seasonId) {
		switch (seasonId) {
			case 1:
				self.user_season = "spring";
				break;
			case 2:
				self.user_season = "summer";
				break;
			case 3:
				self.user_season = "autumn";
				break;
			case 4:
				self.user_season = "winter";
				break;
			default:
				self.user_season = ""; // Map default
				break;
		}
		self.SetStateLabel("Spawn");
	}

	virtual string GetSeason() {
		if (self.user_season) {
			return self.user_season;
		}
		int mapSeasonId = ACS_NamedExecuteWithResult("Get Season", 0, 0, 0, 0);
		switch (mapSeasonId) {
			case 1:
				return "summer";
			case 2:
				return "autumn";
			case 3:
				return "winter";
			default:
				return "spring";
		}
	}

	action state A_RandomVariant() {
		// Scale Randomisation:
		float scale = SchismUtil.PositionalVarition(invoker.pos, 0.9, 1.1);
		invoker.scale.x = invoker.baseScale.x * scale;
		invoker.scale.y = invoker.baseScale.y * scale;

		// Season Variants:
		string season = invoker.GetSeason();
		float variants = 0;
		if (season == "summer") {
			variants = invoker.summerVariants;
		}
		else if (season == "autumn") {
			variants = invoker.autumnVariants;
		}
		else if (season == "winter") {
			variants = invoker.winterVariants;
		}
		else {
			variants = invoker.springVariants;
		}

		// Variation:
		if (!variants) {
			return null;
		}

		// State Variation:
		int stateVariation = invoker.user_variant ? invoker.user_variant : Round(SchismUtil.PositionalVarition(invoker.pos, 1, variants));
		switch (stateVariation) {
			case 1:
				if (invoker.health <= 0) {
					if (season == "summer") {
						return FindState("Death.Summer.1");
					}
					if (season == "autumn") {
						return FindState("Death.Autumn.1");
					}
					if (season == "winter") {
						return FindState("Death.Winter.1");
					}
					return FindState("Death.Spring.1");
				}
				if (season == "summer") {
					return FindState("Summer.1");
				}
				if (season == "autumn") {
					return FindState("Autumn.1");
				}
				if (season == "winter") {
					return FindState("Winter.1");
				}
				return FindState("Spring.1");
			case 2:
				if (invoker.health <= 0) {
					if (season == "summer") {
						return FindState("Death.Summer.2");
					}
					if (season == "autumn") {
						return FindState("Death.Autumn.2");
					}
					if (season == "winter") {
						return FindState("Death.Winter.2");
					}
					return FindState("Death.Spring.2");
				}
				if (season == "summer") {
					return FindState("Summer.2");
				}
				if (season == "autumn") {
					return FindState("Autumn.2");
				}
				if (season == "winter") {
					return FindState("Winter.2");
				}
				return FindState("Spring.2");
			case 3:
				if (invoker.health <= 0) {
					if (season == "summer") {
						return FindState("Death.Summer.3");
					}
					if (season == "autumn") {
						return FindState("Death.Autumn.3");
					}
					if (season == "winter") {
						return FindState("Death.Winter.3");
					}
					return FindState("Death.Spring.3");
				}
				if (season == "summer") {
					return FindState("Summer.3");
				}
				if (season == "autumn") {
					return FindState("Autumn.3");
				}
				if (season == "winter") {
					return FindState("Winter.3");
				}
				return FindState("Spring.3");
			case 4:
				if (invoker.health <= 0) {
					if (season == "summer") {
						return FindState("Death.Summer.4");
					}
					if (season == "autumn") {
						return FindState("Death.Autumn.4");
					}
					if (season == "winter") {
						return FindState("Death.Winter.4");
					}
					return FindState("Death.Spring.4");
				}
				if (season == "summer") {
					return FindState("Summer.4");
				}
				if (season == "autumn") {
					return FindState("Autumn.4");
				}
				if (season == "winter") {
					return FindState("Winter.4");
				}
				return FindState("Spring.4");
		}
		return null;
	}

	action void A_DropLeaves(string leafClass) {
		if (invoker.user_skybox || !CheckRange(5120, true)) {
			invoker.A_SpawnItemEx(
				leafClass,
				Random(-invoker.Radius * 0.5, invoker.Radius * 0.5),
				Random(-invoker.Radius * 0.5, invoker.Radius * 0.5),
				Random(invoker.height * 0.5, invoker.height),
				0, 0, 0, 0, SXF_CLIENTSIDE
			);
		}
	}
	
	States {
		Spawn:
			TNT1 A 0 NoDelay {
				return A_RandomVariant();
			}
			Stop;

		Death:
			TNT1 A 0 {
				return A_RandomVariant();
			}
			Stop;
	}
}

// Conifers:
#include "zscript/schism/decoration/trees/redwood.zs"
#include "zscript/schism/decoration/trees/fir.zs"
#include "zscript/schism/decoration/trees/pine.zs"
#include "zscript/schism/decoration/trees/spruce.zs"
#include "zscript/schism/decoration/trees/sequoia.zs"

// Temperate:
#include "zscript/schism/decoration/trees/linden.zs"
#include "zscript/schism/decoration/trees/alder.zs"
#include "zscript/schism/decoration/trees/walnut.zs"
#include "zscript/schism/decoration/trees/maple.zs"
#include "zscript/schism/decoration/trees/ash.zs"
#include "zscript/schism/decoration/trees/birch.zs"
#include "zscript/schism/decoration/trees/oak.zs"
#include "zscript/schism/decoration/trees/sycamore.zs"
#include "zscript/schism/decoration/trees/willow.zs"
#include "zscript/schism/decoration/trees/mangrove.zs"
#include "zscript/schism/decoration/trees/elm.zs"
#include "zscript/schism/decoration/trees/bamboo.zs"
#include "zscript/schism/decoration/trees/chestnut.zs"
#include "zscript/schism/decoration/trees/magnolia.zs"
#include "zscript/schism/decoration/trees/sakura.zs"

// Palms:
#include "zscript/schism/decoration/trees/sabal.zs"
#include "zscript/schism/decoration/trees/date.zs"
#include "zscript/schism/decoration/trees/caranday.zs"
#include "zscript/schism/decoration/trees/chusan.zs"

// Abstract:
#include "zscript/schism/decoration/trees/socotra.zs"
#include "zscript/schism/decoration/trees/baobab.zs"
#include "zscript/schism/decoration/trees/spine.zs"
#include "zscript/schism/decoration/trees/lumpy.zs"