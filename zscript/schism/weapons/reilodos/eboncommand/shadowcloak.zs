class ShadowCloakScroll : WeaponPassive {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Ebon Command will now cloak you in shadows when stood still or in darkness!";
		
		Inventory.Icon "REISN0";
		Inventory.PickupSound "EbonCommand/Up";
		Inventory.RestrictedTo "Reilodos", "ReilodosHavocSerpent";
		WeaponUpgrade.ExtraAmmoClass "ShadowEnergy";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}
	
	States {
		Spawn:
			REIS N 1 bright;
			Loop;
	}
}