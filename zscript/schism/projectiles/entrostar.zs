class EntrostarProjectile : SchismProjectile {
	Default {
		Radius 13;
		Height 8;
		Scale 1;
		Speed 25;
		FastSpeed 30;
		Damage 5;
		Alpha 1;
		RenderStyle "Add";
		
		DamageType "Chaos";
		
		SeeSound "Projectile/Entrostar/Fire";
		DeathSound "Projectile/Entrostar/Explode";

		SchismProjectile.ParticleName "ChaosSpark";
		SchismProjectile.ParticleCount 4;
	}
	
	States {
		Spawn:
			NPES AB 5 bright;
			Loop;
			
		Death:
			NPES CDEFG 5 bright;
			Stop;
	}
}