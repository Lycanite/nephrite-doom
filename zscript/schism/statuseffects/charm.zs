class SchismCharmEffect : SchismStatusEffect {
	Actor lastMaster; // The owner's master to reset to after this effect.
	
	Default {
		PowerUp.Duration -10;
		
		SchismStatusEffect.Detrimental true;
		SchismStatusEffect.ParticleName "Nether";
		SchismStatusEffect.ParticleCount 1;
		SchismStatusEffect.ParticleSpeed 2;
		SchismStatusEffect.ParticleDuration TICRATE;
		SchismStatusEffect.ParticleColor 0xDD00FF;
	}

	override void RefreshEffect(Actor effectInflictor, Actor effectSource, int effectTime, Class<SchismStatusEffect> spread, int strength) {
		super.RefreshEffect(effectInflictor, effectSource, effectTime, spread, strength);
		if (!self.owner) {
			return;
		}

		self.owner.target = null;
		self.lastMaster = self.owner.master;
		self.owner.master = self.target;
		self.owner.SetStateLabel("See");
	}
	
	override void EndEffect() {
		super.EndEffect();
		if (!self.owner) {
			return;
		}

		self.owner.master = self.lastMaster;
		self.owner.SetStateLabel("See");
	}

	override void DoEffect() {
		super.DoEffect();
		if (!self.owner || self.owner.Health <= 0 || self.owner.bCorpse) {
			return;
		}

		// Keep Current Target if Not the Charmer:
		if (self.owner.target && self.owner.target != self.effectInflictor) {
			return;
		}
		if (self.owner.target == self.effectInflictor) {
			self.owner.target = null;
		}

		// Get New Target Every Second:
		if (self.effectTics % TICRATE != 0) {
			return;
		}
		BlockThingsIterator iter = BlockThingsIterator.Create(self.owner, 1000, false);
		while (iter.Next()) {
			Actor target = iter.thing;
			if (!target || target == self.owner) {
				continue;
			}
			if (!target.bIsMonster || target.bCorpse) {
				continue;
			}

			if (!self.owner.CheckSight(target)) {
				continue;
			}

			if (!SchismInteraction.CanHarmTarget(self.effectInflictor, target)) {
				continue;
			}

			self.owner.target = target;
			self.owner.SetStateLabel("See");
			break;
		}
	}
}