class EbonCommandProjectile : SchismProjectile {
	Default {
		Radius 5;
		Height 7;
		Speed 30;
		Gravity 0;
		DamageFunction Random(10, 20);
		Alpha 1.0;
		RenderStyle "Translucent";
		Scale 1;
		
		DamageType "Shadow";
		
		SeeSound "EbonCommand/Projectile";
		DeathSound "EbonCommand/Projectile/Explode";
		Obituary "%o was consumed by the shadows of %k's Ebon Command!";

		SchismProjectile.ParticleName "Shadow";
		
		-THRUGHOST;
	}
	
	States {
		Spawn:
			EBCM ABCD 4;
			Loop;
			
		Death:
			EBCM EFGH 4;
			Stop;
	}
}