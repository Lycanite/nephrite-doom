class ScourgeDartScroll : WeaponPassive {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Scourge Darts have been added to your Malady Quiver.";
		
		Inventory.Icon "VESWP0";
		Inventory.PickupSound "MaladyQuiver/Pickup";
		Inventory.RestrictedTo "Vess";
		WeaponUpgrade.ExtraAmmoClass "ToxicDrupe";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			VESW P 1 bright;
			Loop;
	}
}

extend class MaladyQuiver {
	action state A_ScourgeDart() {
		if (!A_CheckSpecial(3) || !A_SpendAmmo(5)) {
			return ResolveState("Ready");
		}
        A_FireProjectile("ScourgeDartProjectile", 0, false);
		return null;
	}

    States {
		MainHand.User3:
			VSH5 K 4 {
				A_ClearBobbing();
				A_Light2();
			}
			VSH5 L 4;
			VSH5 M 4 A_ScourgeDart();
			VSH5 N 4;
			VSH5 OPQL 4;
			VSH5 C 0 A_MirrorHands();
			VSH5 C 0 A_Light0();
			Goto Ready;

		OffHand.User3:
			VSH5 ABCDEFGH 2;
			Loop;
    }
}

class ScourgeDartProjectile : MaladyDartProjectile {
	Default {
        Scale 1.5;
		Radius 2;
		Height 2;
		Speed 50;
		FastSpeed 50;

		SchismProjectile.ParticleName "SporeBubble";
		SchismProjectile.ParticleColor 0xF700AAFF;
		SchismProjectile.StatusEffectClass "SchismPlagueDebuff";
		SchismProjectile.StatusEffectDuration 30 * 35;
		SchismProjectile.StatusEffectStrength 2;

		SchismProjectile.PoolTics 10 * TICRATE;
		SchismProjectile.PoolDamage 1;
		SchismProjectile.PoolDamageRadius 280;
		SchismProjectile.PoolEffectTime 30 * TICRATE;
		SchismProjectile.PoolEffectClass "SchismPlagueDebuff";
		SchismProjectile.PoolClass "ScourgePool";
		SchismProjectile.PoolScale 2;
		
		SeeSound "MaladyQuiver/Fire/ScourgeDart";
		DeathSound "MaladyQuiver/Explode/ScourgeDart";

		+RIPPER;
	}
	
	States {
		Spawn:
			VMQ3 ABC 4;
			Loop;
			
		Death:
			VMQ3 DEFGH 4;
			VMQ3 I 4 {
				invoker.bFloorHugger = true;
				invoker.particleCount = 20;
			}
		Pool:
			0000 A 1 A_PoolUpdate(0.04);
			0000 A 15;
			Loop;
	}
}

class ScourgePool : ProjectilePool {
	Default {
		Radius 16;
		Height 1;
		Scale 1;
		Gravity 0.25;
		Alpha 1;
		Renderstyle "Translucent";

		ProjectilePool.ScaleBase 1;
		ProjectilePool.RemainingTics 10 * TICRATE;

		ProjectilePool.particleName "SporeBubble";
		ProjectilePool.particleCount 1;
		ProjectilePool.particleOffsetRandom 60;
		ProjectilePool.particleDuration TICRATE;
		ProjectilePool.particleSpeed 0.5;
		ProjectilePool.particlePitchMin -45;
		ProjectilePool.particlePitchMax 45;
	}
	
	States {
		Spawn:
			VMQ5 ABCDEFGHIJ 4 {
				return A_EffectUpdate(4);
			}
			Loop;
	}
}