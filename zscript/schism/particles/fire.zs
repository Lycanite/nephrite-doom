struct SchismFireParticle {
	static void apply(out FSpawnParticleParams particleParams) {
		particleParams.texture = TexMan.CheckForTexture("particles/fire/Fire01.png");
		particleParams.style = STYLE_Add;
		particleParams.flags = SPF_ROLL|SPF_FULLBRIGHT;

		particleParams.accel = (0, 0, 0);
		particleParams.startRoll = Random(0, 359);
		particleParams.rollVel = Random(-20, 20);
		particleParams.rollAcc = 0;

		particleParams.size = 16.0;
		particleParams.sizeStep = 2;
		particleParams.startAlpha = 0.8;
		particleParams.fadeStep = particleParams.startAlpha / particleParams.lifetime;
	}
}

struct SchismFireEmberParticle {
	static void apply(out FSpawnParticleParams particleParams) {
		particleParams.texture = TexMan.CheckForTexture("particles/fire/FireEmber01.png");
		particleParams.style = STYLE_Add;
		particleParams.flags = SPF_ROLL|SPF_FULLBRIGHT;

		particleParams.accel = (0, 0, 0);
		particleParams.startRoll = Random(0, 359);
		particleParams.rollVel = Random(-5, 5);
		particleParams.rollAcc = 0;

		particleParams.size = 8.0;
		particleParams.sizeStep = 0.2;
		particleParams.startAlpha = 1.0;
		particleParams.fadeStep = particleParams.startAlpha / particleParams.lifetime;
	}
}

struct SchismFireBlastParticle {
	static void apply(out FSpawnParticleParams particleParams) {
		particleParams.texture = TexMan.CheckForTexture("particles/fire/FireBlast01.png");
		particleParams.style = STYLE_Add;
		particleParams.flags = SPF_ROLL|SPF_FULLBRIGHT;

		particleParams.accel = (0, 0, 0);
		particleParams.startRoll = 0;
		particleParams.rollVel = 0;
		particleParams.rollAcc = 0;

		particleParams.size = Random(16, 32);
		particleParams.sizeStep = 0.1;
		particleParams.startAlpha = 1.0;
		particleParams.fadeStep = particleParams.startAlpha / particleParams.lifetime;
	}
}

struct SchismFlameParticle {
	static void apply(out FSpawnParticleParams particleParams) {
		particleParams.texture = TexMan.CheckForTexture("particles/fire/Flame01.png");
		particleParams.style = STYLE_Add;
		particleParams.flags = SPF_ROLL|SPF_FULLBRIGHT;

		particleParams.accel = (0, 0, 0);
		particleParams.startRoll = Random(0, 359);
		particleParams.rollVel = Random(-20, 20);
		particleParams.rollAcc = 0;

		particleParams.size = 16.0;
		particleParams.sizeStep = 2;
		particleParams.startAlpha = 0.8;
		particleParams.fadeStep = particleParams.startAlpha / particleParams.lifetime;
	}
}

struct SchismFumeParticle {
	static void apply(out FSpawnParticleParams particleParams) {
		particleParams.texture = TexMan.CheckForTexture("particles/fire/Fume01.png");
		particleParams.style = STYLE_Translucent;
		particleParams.flags = SPF_ROLL;

		particleParams.accel = (0, 0, 0);
		particleParams.startRoll = Random(0, 359);
		particleParams.rollVel = Random(-10, 10);
		particleParams.rollAcc = 0;

		particleParams.size = 32.0;
		particleParams.sizeStep = 1;
		particleParams.startAlpha = 0.8;
		particleParams.fadeStep = particleParams.startAlpha / particleParams.lifetime;
	}
}

struct SchismSmokeParticle {
	static void apply(out FSpawnParticleParams particleParams) {
		particleParams.texture = TexMan.CheckForTexture("particles/fire/Smoke01.png");
		particleParams.style = STYLE_Translucent;
		particleParams.flags = SPF_ROLL;

		particleParams.accel = (0, 0, 0);
		particleParams.startRoll = Random(0, 359);
		particleParams.rollVel = Random(-10, 10);
		particleParams.rollAcc = 0;

		particleParams.size = 32.0;
		particleParams.sizeStep = 1;
		particleParams.startAlpha = 0.8;
		particleParams.fadeStep = particleParams.startAlpha / particleParams.lifetime;
	}
}