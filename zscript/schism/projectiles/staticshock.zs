class StaticShockProjectile : SchismProjectile {
	Default {
		Radius 5;
		Height 7;
		Scale 0.5;
		Speed 20;
		DamageFunction 2;
		Alpha 1;
		RenderStyle "Add";
		DamageType "Lightning";
		
		SchismProjectile.ParticleName "Lightning";
		SchismProjectile.ParticleInterval 1;
		SchismProjectile.ParticleScale 2;
		
		SeeSound "Projectile/StaticShock/Fire";
		DeathSound "Projectile/StaticShock/Explode";
		
		+NODAMAGETHRUST;
		+RIPPER;
	}
	
	States {
		Spawn:
			NPSS ABCDEF 2;
			Loop;
			
		Death:
			NPSS GHIJK 4;
			Stop;
	}
}