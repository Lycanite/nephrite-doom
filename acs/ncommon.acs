#library "ncommon"
#include "zcommon.acs"

// Constants:
#libdefine ELEMENT_FIRE 1
#libdefine ELEMENT_WATER 2
#libdefine ELEMENT_EARTH 3
#libdefine ELEMENT_AIR 4
#libdefine ELEMENT_ORDER 5
#libdefine ELEMENT_CHAOS 6
#libdefine ELEMENT_ACID 7
#libdefine ELEMENT_LAVA 8
#libdefine ELEMENT_LIGHTNING 9
#libdefine ELEMENT_LIGHT 10
#libdefine ELEMENT_SHADOW 11
#libdefine ELEMENT_ARBOUR 12
#libdefine ELEMENT_FROST 13
#libdefine ELEMENT_FAE 14
#libdefine ELEMENT_POISON 15
#libdefine ELEMENT_PHASE 16
#libdefine ELEMENT_ARCANE 17
#libdefine ELEMENT_QUAKE 18
#libdefine ELEMENT_AETHER 19
#libdefine ELEMENT_NETHER 20
#libdefine ELEMENT_VOID 21
#libdefine ELEMENT_NOVA 22
#libdefine ELEMENT_FLUX 23
#libdefine ELEMENT_GRAVITY 24
#libdefine ELEMENT_VORTEX 25
#libdefine ELEMENT_CHRONO 26
#libdefine ELEMENT_XENO 27

// Info:
function int GetDistance(int originX, int originY, int targetX, int targetY) {
    int distanceX = targetX - originX;
    int distanceY = targetY - originY;
    int distanceSq = distanceX * distanceX + distanceY * distanceY;
    return Sqrt(distanceSq);
}

// Custom Teleporting:
function void TeleportCustomSound(int destinationTID, int useAngle, int destinationSectorTag, bool keepHeight, str sound) {
    Teleport_NoFog(destinationTID, useAngle, destinationSectorTag, keepHeight);
    PlaySound(destinationTID, sound);
}

str elementTeleportSounds[9] = {
    "Teleport/Order",
    "Teleport/Chaos",
    "Teleport/Phase",
    "Teleport/Arcane",
    "Teleport/Aether",
    "Teleport/Nether",
    "Teleport/Void",
    "Teleport/Chrono"
};

/**
 * Teleports with a element based sound and effect.
 */
script "Teleport Element" (int destinationTID, int destinationSectorTag, int elementId) {
    str sound = elementTeleportSounds[elementId];
    TeleportCustomSound(destinationTID, 3, destinationSectorTag, false, sound);
}

/**
 * Plays an element based teleport sound.
 */
script "Teleport Sound" (int elementId) {
    str sound = elementTeleportSounds[elementId];
    PlaySound(0, sound);
}

/**
 * Plays the complete alert sound and lowers the tagged sector to lowest floor.
 */
script "Complete Zone Lower" (int tag, int speed) {
    PlaySound(0, "Alert/Complete");
    Floor_LowerToLowest(tag, speed);
}

/**
 * Used for falling into a void, teleports back to a checkpoint location or causes instant death on skill 3+.
 */
script "Fall Off" (int destinationTID, int destinationSectorTag) {
    if (GameSkill() >= SKILL_HARD || CheckActorProperty(0, APROP_Friendly, false)) {
        DamageThing(1024, MOD_FALLING);
        terminate;
    }
    Teleport_NoFog(destinationTID, 3, destinationSectorTag, false);
}


// Sectors:
bool movingSectors[1024]; // Checked by any sector lower/raise by value scripts.

/**
 * Lowers the floor by speed to the lowest neighbor then copies the texture and damage. Then raises back up and reverts after a delay.
 * @param floorTag The tag of the floor sector.
 * @param speed The drop speed and the recover speed / 8 (raises back up at 8x this value).
 * @param recoverDelay The delay in seconds before the floor raises back up.
 * @param initialAmount If above 0, the floor will lower by this amount x 8 first using the speed and will then lower the rest of the way at 32 speed. Note: if this value is greater that the lowest neighboring floor it will lower past it.
*/
script "Floor Drop Recover" (int floorTag, int speed, int recoverDelay, int initialAmount) {
    // Moving Lock:
    if (movingSectors[floorTag]) {
        terminate;
    }
    movingSectors[floorTag] = true;

    // Drop:
    TagWait(floorTag);
    int startHeight = GetSectorFloorZ(floorTag, 0, 0) >> 16;
    str startTexture = GetActorFloorTexture(floorTag);
    ScriptCall("SchismLevel", "SnapshotFloorTexture", floorTag);
    if (initialAmount > 0) {
        Floor_LowerByValue(floorTag, speed, initialAmount * 8);
        speed = 32;
        TagWait(floorTag);
    }
    Generic_Floor(floorTag, speed, 0, 2, 3 + 4); // 2 = Move to lowest neighbor, 3 = Copy texture and specials, +4 = Neighbor texture taget
    TagWait(floorTag);

    // Recover:
    Delay(recoverDelay * 35);
    Floor_MoveToValue(floorTag, 32, startHeight, 0);
    TagWait(floorTag);
    SetSectorDamage(floorTag, 0);
    ScriptCall("SchismLevel", "RevertFloorTexture", floorTag);

    // Unlock:
    movingSectors[floorTag] = false;
}

int toggleElevators[65536] = {}; // 0 for undetermined, 1 for ground floor, 2 for top floor.
script "Toggle Elevator" (int tag, int speed, int startingPosition) {
    int position = toggleElevators[tag] > 0 ? toggleElevators[tag] : startingPosition;
    if (position > 1) {
        Floor_LowerToLowest(tag, speed);
        position = 1;
    } else {
        Floor_RaiseToNearest(tag, speed);
        position = 2;
    }
    TagWait(tag);
    toggleElevators[tag] = position;
}

bool toggleCeilings[65536] = {}; // False for lowered, true for raised, reverse tag uses the opposite.
script "Toggle Ceiling Raise Lower" (int tag, int reverseTag, int speed) {
    if (tag > 0) {
        TagWait(tag);
        int position = toggleCeilings[tag];
        if (!position) {
            Ceiling_RaiseToHighest(tag, speed);
        } else {
            Ceiling_LowerToHighestFloor(tag, speed);
        }
        toggleCeilings[tag] = !position;
    }
    if (reverseTag > 0) {
        TagWait(reverseTag);
        int reversePosition = toggleCeilings[reverseTag];
        if (reversePosition) {
            Ceiling_RaiseToHighest(reverseTag, speed);
        } else {
            Ceiling_LowerToHighestFloor(reverseTag, speed);
        }
        toggleCeilings[reverseTag] = !reversePosition;
    }
}

script "Raise Floor Then Lower Floor" (int raiseTag, int lowerTag, int speed) {
    TagWait(raiseTag);
    TagWait(lowerTag);
    Floor_RaiseToLowestCeiling(raiseTag, speed);
    TagWait(raiseTag);
    Floor_LowerToLowest(lowerTag, speed);
}

script "Raise Floor Then Lower Ceiling" (int floorTag, int ceilingTag, int speed) {
    TagWait(floorTag);
    TagWait(ceilingTag);
    Floor_RaiseToLowestCeiling(floorTag, speed);
    TagWait(floorTag);
    Ceiling_LowerToHighestFloor(ceilingTag, speed);
}

script "Lower Floor Then Raise Ceiling" (int floorTag, int ceilingTag, int speed) {
    TagWait(floorTag);
    TagWait(ceilingTag);
    Floor_LowerToLowest(floorTag, speed);
    TagWait(floorTag);
    Ceiling_RaiseToHighest(ceilingTag, speed);
}

script "Lower Floor Then Lower Ceiling" (int floorTag, int ceilingTag, int speed) {
    TagWait(floorTag);
    TagWait(ceilingTag);
    Floor_LowerToLowest(floorTag, speed);
    TagWait(floorTag);
    Ceiling_LowerToHighestFloor(ceilingTag, speed);
}

script "Raise Floor Then Raise Ceiling" (int floorTag, int ceilingTag, int speed) {
    TagWait(floorTag);
    TagWait(ceilingTag);
    Floor_RaiseToLowestCeiling(floorTag, speed);
    TagWait(floorTag);
    Ceiling_RaiseToHighest(ceilingTag, speed);
}

script "Raise Ceiling Then Lower Ceiling" (int raiseTag, int lowerTag, int speed) {
    TagWait(raiseTag);
    TagWait(lowerTag);
    Ceiling_RaiseToHighest(raiseTag, speed);
    TagWait(raiseTag);
    Ceiling_LowerToHighestFloor(lowerTag, speed);
}

script "Raise Ceiling Then Lower Floor" (int floorTag, int ceilingTag, int speed) {
    TagWait(floorTag);
    TagWait(ceilingTag);
    Ceiling_RaiseToHighest(ceilingTag, speed);
    TagWait(ceilingTag);
    Floor_LowerToLowest(floorTag, speed);
}

script "Lower Ceiling Then Raise Floor" (int floorTag, int ceilingTag, int speed) {
    TagWait(floorTag);
    TagWait(ceilingTag);
    Ceiling_LowerToHighestFloor(ceilingTag, speed);
    TagWait(ceilingTag);
    Floor_RaiseToLowestCeiling(floorTag, speed);
}

script "Lower Ceiling Then Lower Floor" (int floorTag, int ceilingTag, int speed) {
    TagWait(floorTag);
    TagWait(ceilingTag);
    Ceiling_LowerToHighestFloor(ceilingTag, speed);
    TagWait(ceilingTag);
    Floor_LowerToLowest(floorTag, speed);
}

script "Raise Ceiling Then Raise Floor" (int floorTag, int ceilingTag, int speed) {
    TagWait(floorTag);
    TagWait(ceilingTag);
    Ceiling_RaiseToHighest(ceilingTag, speed);
    TagWait(ceilingTag);
    Floor_RaiseToLowestCeiling(floorTag, speed);
}

script "Raise Floor and Ceiling" (int floorTag, int ceilingTag, int speed) {
    TagWait(floorTag);
    TagWait(ceilingTag);
    Ceiling_RaiseToHighest(ceilingTag, speed);
    Floor_RaiseToHighest(floorTag, speed);
}

script "Lower Floor and Ceiling" (int floorTag, int ceilingTag, int speed) {
    TagWait(floorTag);
    TagWait(ceilingTag);
    Ceiling_LowerToHighestFloor(ceilingTag, speed);
    Floor_LowerToLowest(floorTag, speed);
}

script "Raise Floor To Highest Floor and Lower Floor Lowest Floor" (int raiseTag, int lowerTag, int speed) {
    TagWait(raiseTag);
    TagWait(lowerTag);
    Floor_RaiseToHighest(raiseTag, speed);
    Floor_LowerToLowest(lowerTag, speed);
}

script "Floor and Ceiling Lower To Lowest" (int floorTag, int ceilingTag, int speed) {
    TagWait(floorTag);
    TagWait(ceilingTag);
    Ceiling_LowerToLowest(ceilingTag, speed);
    Floor_LowerToLowest(floorTag, speed);
}

script "Lower Floor To Lowest and Raise Ceiling To Highest" (int floorTag, int ceilingTag, int speed) {
    TagWait(floorTag);
    TagWait(ceilingTag);
    Ceiling_RaiseToHighest(ceilingTag, speed);
    Floor_LowerToLowest(floorTag, speed);
}

script "Floor and Ceiling Raise To Nearest" (int floorTag, int ceilingTag, int speed) {
    TagWait(floorTag);
    TagWait(ceilingTag);
    Ceiling_RaiseToNearest(ceilingTag, speed);
    Floor_RaiseToNearest(floorTag, speed);
}

script "FC Lower" (int floorTag, int ceilingTag, int value, int speed) {
    if (movingSectors[floorTag] || movingSectors[ceilingTag]) {
        terminate;
    }
    movingSectors[floorTag] = true;
    movingSectors[ceilingTag] = true;

    int amount = value * 8;

    TagWait(floorTag);
    TagWait(ceilingTag);
    Floor_LowerByValue(floorTag, speed ? speed : 16, amount);
    Ceiling_LowerByValue(ceilingTag, speed ? speed : 16, amount);

    TagWait(floorTag);
    TagWait(ceilingTag);
    movingSectors[floorTag] = false;
    movingSectors[ceilingTag] = false;
}

script "FC Raise" (int floorTag, int ceilingTag, int value, int speed) {
    if (movingSectors[floorTag] || movingSectors[ceilingTag]) {
        terminate;
    }
    movingSectors[floorTag] = true;
    movingSectors[ceilingTag] = true;

    int amount = value * 8;

    TagWait(floorTag);
    TagWait(ceilingTag);
    Floor_RaiseByValue(floorTag, speed ? speed : 16, amount);
    Ceiling_RaiseByValue(ceilingTag, speed ? speed : 16, amount);

    TagWait(floorTag);
    TagWait(ceilingTag);
    movingSectors[floorTag] = false;
    movingSectors[ceilingTag] = false;
}

script "FC Lower Wait Raise x8" (int floorTag, int ceilingTag, int value, int delaySecs) {
    if (movingSectors[floorTag] || movingSectors[ceilingTag]) {
        terminate;
    }
    movingSectors[floorTag] = true;
    movingSectors[ceilingTag] = true;

    int speed = 16;
    int amount = value * 8;

    TagWait(floorTag);
    TagWait(ceilingTag);
    Floor_LowerByValue(floorTag, speed, amount);
    Ceiling_LowerByValue(ceilingTag, speed, amount);

    TagWait(floorTag);
    TagWait(ceilingTag);
    Delay(delaySecs * 35);
    Floor_RaiseByValue(floorTag, speed, amount);
    Ceiling_RaiseByValue(ceilingTag, speed, amount);

    TagWait(floorTag);
    TagWait(ceilingTag);
    movingSectors[floorTag] = false;
    movingSectors[ceilingTag] = false;
}

script "FC Raise Wait Lower x8" (int floorTag, int ceilingTag, int value, int delaySecs) {
    if (movingSectors[floorTag] || movingSectors[ceilingTag]) {
        terminate;
    }
    movingSectors[floorTag] = true;
    movingSectors[ceilingTag] = true;

    int speed = 16;
    int amount = value * 8;

    TagWait(floorTag);
    TagWait(ceilingTag);
    Floor_RaiseByValue(floorTag, speed, amount);
    Ceiling_RaiseByValue(ceilingTag, speed, amount);

    TagWait(floorTag);
    TagWait(ceilingTag);
    Delay(delaySecs * 35);
    Floor_LowerByValue(floorTag, speed, amount);
    Ceiling_LowerByValue(ceilingTag, speed, amount);

    TagWait(floorTag);
    TagWait(ceilingTag);
    movingSectors[floorTag] = false;
    movingSectors[ceilingTag] = false;
}

script "Ceiling Alternate" (int sectorTag, int speed, int height, int delaySecs) {
    // Raise:
    TagWait(sectorTag);
    Ceiling_RaiseByValue(sectorTag, speed, height);

    // Lower:
    TagWait(sectorTag);
    Delay(delaySecs * 35);
    Ceiling_LowerByValue(sectorTag , speed, height);
    
    // Loop:
    TagWait(sectorTag);
    Delay(delaySecs * 35);
    ACS_NamedExecuteWithResult("Ceiling Alternate", sectorTag, speed, height, delaySecs);
}

script "Floor Alternate" (int sectorTag, int speed, int height, int delaySecs) {
    // Raise:
    TagWait(sectorTag);
    Floor_RaiseByValue(sectorTag, speed, height);

    // Lower:
    TagWait(sectorTag);
    Delay(delaySecs * 35);
    Floor_LowerByValue(sectorTag , speed, height);
    
    // Loop:
    TagWait(sectorTag);
    Delay(delaySecs * 35);
    ACS_NamedExecuteWithResult("Floor Alternate", sectorTag, speed, height, delaySecs);
}


// Elevators (3D Floors):
script "Elevator Alternate" (int sectorTag, int speed, int height, int delaySecs) {
    // Raise:
    TagWait(sectorTag);
    Floor_RaiseByValue(sectorTag, speed, height);
    Ceiling_RaiseByValue(sectorTag, speed, height);

    // Lower:
    TagWait(sectorTag);
    Delay(delaySecs * 35);
    Floor_LowerByValue(sectorTag, speed, height);
    Ceiling_LowerByValue(sectorTag, speed, height);
    
    // Loop:
    TagWait(sectorTag);
    Delay(delaySecs * 35);
    ACS_NamedExecuteWithResult("Elevator Alternate", sectorTag, speed, height, delaySecs);
}

script "3D Platform Alterate" (int sectorTag, int speed, int height, int delaySecs) { // TODO Alias
    ACS_NamedExecuteWithResult("Elevator Alternate", sectorTag, speed, height, delaySecs);
}

script "3D Water Waves" (int sectorTag, int speed, int height, int delaySecs) { // TODO Alias
    ACS_NamedExecuteWithResult("Ceiling Alternate", sectorTag, speed, height, delaySecs);
}

script "Elevator Move x8" (int sectorTag, int speed, int value) {
    if (movingSectors[sectorTag]) {
        terminate;
    }
    movingSectors[sectorTag] = true;
    TagWait(sectorTag);

    int amount = value * 8;
    if (amount > 0) {
        Ceiling_RaiseByValue(sectorTag, speed, amount);
        Floor_RaiseByValue(sectorTag, speed, amount);
    } else {
        Ceiling_LowerByValue(sectorTag, speed, amount);
        Floor_LowerByValue(sectorTag, speed, amount);
    }

    TagWait(sectorTag);
    movingSectors[sectorTag] = false;
}


// Poly Objects:
script "Poly Alternate" (int polyNum, int speed, int angle, int distance) {
    PolyWait(polyNum);
    Polyobj_OR_Move(polyNum, speed, angle, distance);
    PolyWait(polyNum);
    Delay((16 / speed) * 35);
    Polyobj_OR_Move(polyNum, speed, angle + 128, distance);
    PolyWait(polyNum);
    Delay((16 / speed) * 35);
    ACS_NamedExecuteWithResult("Poly Alternate", polyNum, speed, angle, distance);
}

script "Poly Sweeps" (int polyNum, int speed, int fromThingTag, int toThingTag) {
    PolyWait(polyNum);
    Polyobj_OR_MoveToSpot(polyNum, ThingDistance(fromThingTag, toThingTag) * 8, fromThingTag);
    PolyWait(polyNum);
    Polyobj_OR_MoveToSpot(polyNum, speed, toThingTag);
    PolyWait(polyNum);
    ACS_NamedExecuteWithResult("Poly Sweeps", polyNum, speed, fromThingTag, toThingTag);
}


// Things:
function int ThingDistance(int fromThingTag, int toThingTag) {
    return GetDistance(GetActorX(fromThingTag) >> 16, GetActorY(fromThingTag) >> 16, GetActorX(toThingTag) >> 16, GetActorY(toThingTag) >> 16);
}

function int AngleToThing(int fromThingTag, int toThingTag) {
    int distanceX = GetActorX(toThingTag) - GetActorX(fromThingTag);
    int distanceY = GetActorY(toThingTag) - GetActorY(fromThingTag);
    return VectorAngle(distanceX, distanceY);
}

script "Activate and Light" (int thingTag, int sectorTag, int lightLevel) {
    Thing_Activate(thingTag);
    Light_ChangeToValue(sectorTag, lightLevel);
}