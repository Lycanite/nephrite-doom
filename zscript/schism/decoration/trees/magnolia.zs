class SchismTreeMagnolia : SchismTree {
	Default {
		Tag "Magnolia Tree";
		SchismTree.SpringVariants 2;
		SchismTree.SummerVariants 2;
		SchismTree.AutumnVariants 2;
		SchismTree.WinterVariants 2;
	}
	
	States {
		Spring.1:
			TR22 A 4 A_DropLeaves("SchismLeafSakura");
			Loop;

		Spring.2:
			TR22 B 4 A_DropLeaves("SchismLeafSakura");
			Loop;

		Summer.1:
			TR22 E -1;
			Stop;

		Summer.2:
			TR22 F -1;
			Stop;

		Autumn.1:
			TR22 I -1;
			Stop;

		Autumn.2:
			TR22 J -1;
			Stop;

		Winter.1:
			TD22 M -1;
			Stop;

		Winter.2:
			TD22 N -1;
			Stop;

		Death.Spring.1:
			TD22 A -1;
			Stop;

		Death.Spring.2:
			TD22 B -1;
			Stop;

		Death.Summer.1:
			TD22 E -1;
			Stop;

		Death.Summer.2:
			TD22 F -1;
			Stop;

		Death.Autumn.1:
			TD22 I -1;
			Stop;

		Death.Autumn.2:
			TD22 J -1;
			Stop;

		Death.Winter.1:
			TD22 M -1;
			Stop;

		Death.Winter.2:
			TD22 N -1;
			Stop;
	}
}