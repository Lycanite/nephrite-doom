class Uvhash : Actor {
	Default {
		Tag "Uvhash The Void God";
		Health 5000000;
		Radius 80;
		Height 256;
		Mass 100000;
		Speed 32;
		FloatSpeed 28;
		Scale 2;
		Gravity 0;
		PainChance 0;
		DamageFunction 50;
		BloodColor "77 00 99";

		MONSTER;
		+FLOORCLIP;
		+FLOAT;
		
		SeeSound "Uvhash/See";
		PainSound "Uvhash/Pain";
		DeathSound "Uvhash/Death";
		ActiveSound "Uvhash/See";
		MeleeSound "Uvhash/Melee";
		Obituary "%o was torn from reality by Uvhash The Void God!";
		HitObituary "%o was torn from reality by Uvhash The Void God!";
	}
	
	States {
		Spawn:
			NUVH ABCD 16 A_Look();
			Loop;
			
		See:
			NUVH ABCD 16 A_Chase("Melee", "Missile");
			Loop;
			
		Melee:
		Missile:
			NUVH A 4 A_FaceTarget();
			NUVH B 2 bright A_SpawnProjectile("VoidSplintProjectile", 16);
			NUVH B 2 bright A_SpawnProjectile("VoidSplintProjectile", 16);
			NUVH B 2 bright A_SpawnProjectile("VoidSplintProjectile", 16);
			NUVH B 2 bright A_SpawnProjectile("VoidSplintProjectile", 16);
			NUVH B 2 bright A_SpawnProjectile("VoidSplintProjectile", 16);
			NUVH B 2 bright A_SpawnProjectile("VoidSplintProjectile", 16);
			NUVH B 2 bright A_SpawnProjectile("VoidSplintProjectile", 16);
			NUVH B 2 bright A_SpawnProjectile("VoidSplintProjectile", 16);
			NUVH B 2 bright A_SpawnProjectile("VoidSplintProjectile", 16);
			NUVH B 2 bright A_SpawnProjectile("VoidSplintProjectile", 16);
			NUVH B 2 bright A_SpawnProjectile("VoidSplintProjectile", 16);
			NUVH B 2 bright A_SpawnProjectile("VoidSplintProjectile", 16);
			NUVH B 2 bright A_SpawnProjectile("VoidSplintProjectile", 16);
			Goto See;
			
		Pain:
			NUVH C 3;
			NUVH C 3 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			NUVH M -1;
			Stop;
	}
}