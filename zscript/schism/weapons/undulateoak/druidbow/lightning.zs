class LightningDruidCrest : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Druid Bow can use Lightning Arrows to call fourth a mighty storm!";
		
		Inventory.Icon "DBW1D0";
		Inventory.PickupSound "DruidBow/Pickup";
		Inventory.RestrictedTo "UndulateOak", "UndulateOakCockatrice";
		WeaponUpgrade.ExtraAmmoClass "DruidArrow";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			DBW1 D 1 bright;
			Loop;
	}
}

class DruidBowLightningArrow : EctoplasmProjectile {
	Default {
		Radius 10;
		Height 10;
		Speed 15;
		DamageFunction 5;
		Gravity 0.25;
		Alpha 1;
		Renderstyle "Normal";
		Projectile;
		
		DamageType "Electric";
		
		SeeSound "DruidBow/Lightning/Fire";
		DeathSound "DruidBow/Lightning/Explode";
		
		+RANDOMIZE;
		-NOGRAVITY;
		+SKYEXPLODE;
		-THRUGHOST;
	}
	
	States {
		Spawn:
			DBWL A 6 bright;
			Loop;
			
		Death:
			DBWL C 0 bright A_SpawnItemEx("UOakLightningStrike", 0, 0);
			DBWL CDEFG 4 bright {SchismAttacks.Explode(invoker, invoker.target, 6, 50, 0, 1, false, false, "Electric");}
			Stop;
	}
}

class DruidBowLightningArrowFar : DruidBowLightningArrow {
	Default {
		Speed 25;
		Gravity 0.1;
		
		SeeSound "DruidBow/Lightning/FireFar";
	}
}

class DruidBowLightningArrowCharged : DruidBowLightningArrowFar {
	Default {
		SeeSound "DruidBow/Lightning/FireCharged";
		DeathSound "DruidBow/Lightning/Explode";
	}
	
	States {
		Spawn:
			DBWL B 2 bright A_SpawnItemEx("DruidBowLightningArrowTrail",random(-2,-8),random(8,-8),random(8,-8),0,0,frandom(0,0.6),0,SXF_NOCHECKPOSITION|SXF_CLIENTSIDE);
			Loop;
			
		Death:
			DBWL M 0 bright {
				A_SpawnItemEx("UOakLightningStrike", 0, 0);
				A_SpawnItemEx("UOakLightningStrike", 120);
				A_SpawnItemEx("UOakLightningStrike", -120);
				A_SpawnItemEx("UOakLightningStrike", 0, 120);
				A_SpawnItemEx("UOakLightningStrike", 0, -120);
				A_SpawnItemEx("UOakLightningStrike", 90, 90);
				A_SpawnItemEx("UOakLightningStrike", -90, 90);
				A_SpawnItemEx("UOakLightningStrike", 90, -90);
				A_SpawnItemEx("UOakLightningStrike", -90, -90);
			}
			DBWL MNOPQR 4 bright {SchismAttacks.Explode(invoker, invoker.target, 6, 50, 0, 1, false, false, "Electric");}
			Stop;
	}
}

class DruidBowLightningArrowTrail : Actor  {
	Default  {
		RenderStyle "Add";
		Alpha 0.5;
		
		+FORCEXYBILLBOARD
		+NOINTERACTION
	}
	
	States {
		Spawn:
			DBWL H 0 ThrustThingZ(0, Random(5, 10), 0, 0);
			DBWL H 0 ThrustThing(Random(0, 255), Random(0, 10), 0, 0);
		Active:
			DBWL HIJKL 1 bright A_FadeOut(0.05);
			Loop;
	}
}
