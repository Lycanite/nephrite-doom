class QuackleEgg : WeaponPassive {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Primordial Staff now comes with a free Quackle!";
		
		Inventory.Icon "WCCKW0";
		Inventory.PickupSound "Cockatrice/See";
		Inventory.RestrictedTo "UndulateOak", "UndulateOakCockatrice";
		WeaponUpgrade.ExtraAmmoClass "LifeSeed";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			WCCK W 1 bright;
			Loop;
	}
}