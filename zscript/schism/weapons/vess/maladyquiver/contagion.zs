class ContagionScroll : WeaponPassive {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "The Contagion of your Malady Quiver has been strengthened.";
		
		Inventory.Icon "VESWR0";
		Inventory.PickupSound "MaladyQuiver/Pickup";
		Inventory.RestrictedTo "Vess";
		WeaponUpgrade.ExtraAmmoClass "LustDrupe";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			VESW R 1 bright;
			Loop;
	}
}