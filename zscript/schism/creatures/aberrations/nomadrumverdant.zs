class NomadrumVerdant : Nomadrum {
	bool turretQueued;
	int menagerieTick;

	Default {
		Tag "Verdant Nomadrum";
	}

	override state CheckMinionState() {
		if (turretQueued) {
			return self.ResolveState("Turret");
		}
		return null;
	}
	
	override FollowProjectile EmitFollowProjectile(String projectileClass, int minionCount) {
		FollowProjectile followProjectile = super.EmitFollowProjectile(projectileClass, minionCount);
		self.turretQueued = true;
		return followProjectile;
	}
	
	States {
		Spawn:
			NMDV AABBCCBB 4 {
				return A_MinionIdle();
			}
			Loop;
			
		Guard:
			NMDV AABBCCBB 4 {
				return A_MinionGuard();
			}
			Loop;
			
		Follow:
			NMDV DDEEFFEE 4 {
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			NMDV DDEEFFEE 4 {
				return A_MinionChase();
			}
			Loop;
			
		Melee:
			NMDV GH 5 A_FaceTarget();
			NMDV I 7 A_CreatureMelee(15, 25, "BedlamSmoke", "Chaos");
			Goto See;

		Missile:
			NMDV J 8 A_FaceTarget();
			NMDV KL 8 {
				A_FaceTarget();
				for (int i = 0; i < 3; i++) {
					A_SpawnProjectile("MaelsphereProjectile", invoker.height / 2);
				}
			}
			NMDV J 0 A_MonsterRefire(10, "See");
			Goto Missile + 2;

		Turret:
			NMDV J 8 {
				invoker.turretQueued = false;
				A_FaceTarget();
				if (self.menagerieTick++ >= 4) {
					self.menagerieTick = 0;
					A_SpawnProjectile("FoulMenageriePortalBolt", invoker.height / 2, Random(-6, 6), Random(-45, 45), CMF_AIMDIRECTION, Random(-10, 45));
				} else {
					A_SpawnProjectile("MaelsphereProjectile", invoker.height / 2, Random(-6, 6), Random(-45, 45), CMF_AIMDIRECTION, Random(-10, 45));
				}
			}
			NMDV KLK 8 {
				A_FaceTarget();
				for (int i = 0; i < 3; i++) {
					A_SpawnProjectile("MaelsphereProjectile", invoker.height / 2, Random(-6, 6), Random(-45, 45), CMF_AIMDIRECTION, Random(-10, 45));
				}
			}
			NMDV K 0 {
				return invoker.CheckMinionState();
			}
			Goto Guard;
			
		Pain:
			NMDV M 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			NMDV MN 8;
			NMDV O 8 A_Scream();
			NMDV P 8;
			NMDV Q 8 A_NoBlocking();
			NMDV RSTU 8;
			NMDV V 8;
			Stop;
	}
}