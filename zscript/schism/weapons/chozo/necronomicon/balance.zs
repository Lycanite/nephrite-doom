class NecronomiconBalancePage : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "You found the Necronomicon Page of Balance!";
		
		Inventory.Icon "NCRM[0";
		Inventory.PickupSound "Necronomicon/Pickup";
		Inventory.RestrictedTo "Chozo";
		WeaponUpgrade.ExtraAmmoClass "NecroticRune";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			NCRM Y 1 bright;
			Loop;
	}
}

class NecronomiconNetherLightningProjectile : SoulEssenceProjectile {
	string clusterProjectile;
	Actor sourceTarget;
	Actor lastClusterTarget;
	int clusterCount;
	int clusterCountMax;
	
	Default {
		Radius 5;
		Height 7;
		Scale 0.5;
		Speed 60;
		DamageFunction 1;
		DamageType "Electric";
		Alpha 1;
		RenderStyle "Add";
		
		SchismProjectile.ParticleName "Nether";
		SchismProjectile.ParticleInterval 8;
		SchismProjectile.ParticleScale 0.5;
		
		SoulEssenceProjectile.SoulEssenceMin 1;
		SoulEssenceProjectile.SoulEssenceMin 3;
		
		SeeSound "Necronomicon/Fire/NetherLightning";
		DeathSound "Necronomicon/Explode/NetherLightning";
		
		+NODAMAGETHRUST;
		+RIPPER;
		-BLOODSPLATTER;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		clusterProjectile = "NecronomiconNetherLightningProjectile";
		clusterCount = 0;
		clusterCountMax = 2;
	}
	
	virtual void SetSourceTarget(Actor newSourcetarget) {
		sourceTarget = newSourcetarget;
	}
	
	virtual void SetClusterCount(int setCount) {
		self.clusterCount = setCount;
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		if (sourceTarget && sourceTarget == targetActor)
		{
			return 0;
		}
		
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		if (sourceTarget && specialDamage > 0 && clusterCount < clusterCountMax && targetActor && targetActor.bIsMonster && targetActor != lastClusterTarget && clusterProjectile != "")
		{
			Actor projectileActor = A_SpawnProjectile(clusterProjectile, 0, 0, Random(1, 360), CMF_AIMDIRECTION, 0);
			NecronomiconNetherLightningProjectile clusterActor = NecronomiconNetherLightningProjectile(projectileActor);
			if (clusterActor)
			{
				clusterActor.SetSourceTarget(targetActor);
				clusterActor.SetClusterCount(clusterCount + 1);
			}
			lastClusterTarget = targetActor;
		}
		
		return specialDamage;
	}
	
	States {
		Spawn:
			NCR4 A 1;
		Spawn.Loop:
			NCR4 BCD 1;
			Loop;
			
		Death:
			NCR4 EFGHIJ 4;
			Stop;
	}
}

class NecronomiconAetherLightningProjectile : NecronomiconNetherLightningProjectile {
	int remainingTics;
	
	Default {
		Scale 1;
		Speed 10;
		DamageFunction 4;
		
		SchismProjectile.ParticleName "Aether";
		SchismProjectile.ParticleInterval 1;
		SchismProjectile.ParticleScale 2;
		
		SoulEssenceProjectile.SoulEssenceMin 0;
		SoulEssenceProjectile.SoulEssenceMin 0;
		
		SeeSound "Necronomicon/Fire/AetherLightning";
		DeathSound "Necronomicon/Explode/AetherLightning";
		
		+FLOORHUGGER;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		clusterProjectile = "NecronomiconAetherLightningProjectile";
		clusterCount = 0;
		clusterCountMax = 1;
		remainingTics = 80;
	}
	
	action state A_AetherUpdate() {
		A_StartSound("Necronomicon/Fire/AetherLightning", CHAN_AUTO, CHANF_LOOPING);
		SchismAttacks.Explode(invoker, invoker.target, 1, 100);
		if (invoker.remainingTics-- <= 0) {
			return invoker.ResolveState("Death");
		}
		return null;
	}
	
	States {
		Spawn:
			NCR4 KL 2;
		Spawn.Loop:
			NCR4 MNOP 2 A_AetherUpdate();
			Loop;
			
		Death:
			NCR4 Q 2 {
				A_StopSound(CHAN_AUTO);
				A_Scream();
			}
			NCR4 R 2;
			Stop;
	}
}
