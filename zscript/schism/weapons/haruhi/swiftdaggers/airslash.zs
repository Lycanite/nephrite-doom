class SwiftDaggersProjectile : SchismProjectile {
	bool vertical;
	
	Default {
		Radius 16;
		Height 10;
		Scale 1;
		Speed 12;
		DamageFunction 3;
		Gravity 0;
		Alpha 0.6;
		Renderstyle "Add";
		
		DamageType "Air";
		
		SeeSound "SwiftDaggers/Projectile";
		DeathSound "SwiftDaggers/Projectile/Explode";
		
		+CANNOTPUSH;
		+NODAMAGETHRUST;
		+THRUGHOST;
	}
	
	action state A_RandomAngle() {
		if (random(0, 1) == 1) {
			invoker.vertical = true;
			return ResolveState("Spawn.Vertical");
		}
		return null;
	}
	
	action state A_DeathAngle() {
		if (invoker.vertical) {
			return ResolveState("Death.Vertical");
		}
		return null;
	}
	
	States {
		Spawn:
			SWDP A 0 {
				return A_RandomAngle();
			}
			SWDP ABCDEF 1;
		Spawn.Vertical:
			SDP2 ABCDEF 1;
			
		Death:
			SWDP G 0 {
				return A_DeathAngle();
			}
			SWDP G 2 {
				SchismAttacks.Explode(invoker, invoker.target, 2, 100, 0, 1, false, false, "Air");
			}
			SWDP HIJK 2;
			Stop;
		Death.Vertical:
			SDP2 G 2 {
				SchismAttacks.Explode(invoker, invoker.target, 2, 100, 0, 1, false, false, "Air");
			}
			SDP2 HIJK 2;
			Stop;
	}
}

class SwiftDaggersWhirlingProjectile : SwiftDaggersProjectile {
	Default {
		Radius 16;
		Height 10;
		Scale 0.75;
		Speed 16;
		DamageFunction 5;
		
		SeeSound "SwiftDaggers/Projectile/Whirling";
		DeathSound "SwiftDaggers/Projectile/Whirling/Explode";
		
		+CANNOTPUSH;
		+NODAMAGETHRUST;
		+THRUGHOST;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.pitch = 0;
	}
	
	States {
		Spawn:
			SWDP LMNO 2;
			
		Death:
			SWDP P 2 {
				SchismAttacks.Explode(invoker, invoker.target, 10, 100, 0, 1, false, false, "Air");
			}
			SWDP QRS 2;
			Stop;
	}
}