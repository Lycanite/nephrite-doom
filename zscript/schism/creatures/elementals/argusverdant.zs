class ArgusVerdant : Argus {
	Default {
		Tag "Verdant Argus";
	}
	
	States {
		Spawn:
			ARGV ABCDE 4 {
				return A_MinionIdle();
			}
			Loop;
			
		Guard:
			ARGV ABCDE 4 {
				return A_MinionGuard();
			}
			Loop;
			
		Follow:
			ARGV ABCDE 4 {
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			ARGV ABCDE 4 {
				A_MinionChase();
			}
			ARGV GGH 4 {
				A_MinionChase();
				A_FaceTarget();
			}
			ARGV H 4 bright {
				A_MinionChase();
				A_SpawnProjectile("FoulMenageriePortalBolt", 32);
			}
			Loop;
			
		Melee:
		Missile:
			ARGV G 8 A_FaceTarget();
			ARGV H 8 bright A_SpawnProjectile("FoulMenageriePortalBolt", 32);
			Goto See;
			
		Pain:
			ARGV F 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			ARGV F 5;
			ARGV I 5 A_Scream();
			ARGV J 5;
			ARGV K 5 A_NoBlocking();
			ARGV LMNOPQRST 5;
			ARGV U 5;
			Stop;
	}
}