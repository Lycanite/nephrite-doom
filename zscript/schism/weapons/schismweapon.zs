class SchismWeapon : Weapon {
	mixin QueryPlayerInfoMixin;
	mixin SpawnParticleMixin;

	bool ultimatePending;
	
	int primaryCost;
	int secondaryCost;
	int specialACost;
	int specialBCost;
	int specialCCost;
	int specialDCost;
	
	string insufficientAmmoAMessage;
	string insufficientAmmoBMessage;
	string noSpecialAMessage;
	string noSpecialBMessage;
	string noSpecialCMessage;
	string noSpecialDMessage;
	
	class<Inventory> specialItemA;
	class<Inventory> specialItemB;
	class<Inventory> specialItemC;
	class<Inventory> specialItemD;
	class<Inventory> passiveItem;
	
	sound failSound;
	
	int idleBob, currentIdleBob;
	
	int lifetime;
	int passiveCooldown;
	
	property PrimaryCost: primaryCost;
	property SecondaryCost: secondaryCost;
	property SpecialACost: specialACost;
	property SpecialBCost: specialBCost;
	property SpecialCCost: specialCCost;
	property SpecialDCost: specialDCost;
	
	property SpecialItemA: specialItemA;
	property SpecialItemB: specialItemB;
	property SpecialItemC: specialItemC;
	property SpecialItemD: specialItemD;
	property PassiveItem: passiveItem;
	
	property InsufficientAmmoAMessage: insufficientAmmoAMessage;
	property InsufficientAmmoBMessage: insufficientAmmoBMessage;
	property NoSpecialAMessage: noSpecialAMessage;
	property NoSpecialBMessage: noSpecialBMessage;
	property NoSpecialCMessage: noSpecialCMessage;
	property NoSpecialDMessage: noSpecialDMessage;
	
	property FailSound: failSound;
	
	property IdleBob: idleBob;
	
	Default {
		Weapon.SlotPriority 1.0;
		
		Weapon.AmmoUse1 1;
		Weapon.AmmoUse2 1;
		Weapon.AmmoGive 100;
		Weapon.AmmoGive2 100;
		
		SchismWeapon.PrimaryCost 1;
		SchismWeapon.SecondaryCost 1;
		SchismWeapon.SpecialACost 50;
		SchismWeapon.SpecialBCost 50;
		SchismWeapon.SpecialCCost 50;
		SchismWeapon.SpecialDCost 50;
		
		SchismWeapon.SpecialItemA "";
		SchismWeapon.SpecialItemB "";
		SchismWeapon.SpecialItemC "";
		SchismWeapon.SpecialItemD "";
		SchismWeapon.PassiveItem "";
		
		SchismWeapon.InsufficientAmmoAMessage "";
		SchismWeapon.InsufficientAmmoBMessage "";
		SchismWeapon.NoSpecialAMessage "";
		SchismWeapon.NoSpecialBMessage "";
		SchismWeapon.NoSpecialCMessage "";
		SchismWeapon.NoSpecialDMessage "";
		
		SchismWeapon.FailSound "";

		SchismWeapon.IdleBob 0;
		
		+Weapon.AMMO_OPTIONAL;
		+Weapon.ALT_AMMO_OPTIONAL;
	}
	
	override void Tick() {
		super.Tick();
		self.lifetime++;
		self.PassiveTick(self.GetPassiveLevel());
	}
	
	
	// Weapon Ultimate
	virtual void ActivateUltimateAbility() {
		self.ultimatePending = true;
	}
	
	
	// Weapon Passive
	virtual int GetPassiveLevel() {
		if (!self.passiveItem || !self.owner) {
			return 0;
		}
		return min(self.owner.CountInv(self.passiveItem), 3);
	}
	
	virtual void PassiveTick(int passiveLevel) {
		if (self.passiveCooldown > 0) {
			self.passiveCooldown--;
		}
	}
	
	
	// Gets an ammo cost based on an action number.
	// Action numbers 1 and 2 are primary and secondary, 3-6 are specials a, b, c and d.
	action int A_GetAmmoCost(int actionNumber) {
		int cost = invoker.primaryCost;
		if (actionNumber == 2) {
			cost = invoker.secondaryCost;
		}
		if (actionNumber == 3) {
			cost = invoker.specialACost;
		}
		if (actionNumber == 4) {
			cost = invoker.specialBCost;
		}
		if (actionNumber == 5) {
			cost = invoker.specialCCost;
		}
		if (actionNumber == 6) {
			cost = invoker.specialDCost;
		}

		if (invoker.owner) {
			SchismHasteBuff hasteEffect = SchismHasteBuff(invoker.owner.FindInventory("SchismHasteBuff"));
			if (hasteEffect) {
				cost = Ceil(hasteEffect.ammoCostMultiplier * cost);
			}
		}
		
		return cost;
	}
	
	// Gets an ammo class based on an action number.
	action class<Inventory> A_GetAmmoClass(int actionNumber) {
		if (actionNumber >= 3) {
			return invoker.ammoType2;
		}
		return invoker.ammoType1;
	}
	
	// Gets an ammo insufficient message based on an action number.
	action string A_GetAmmoInsufficientMessage(int actionNumber) {
		if (actionNumber >= 3) {
			return invoker.insufficientAmmoBMessage;
		}
		return invoker.insufficientAmmoAMessage;
	}
	
	// Checks ammo automatically, returns false and prints a message and plays a fail sound if there is not enough ammo.
	action int A_CheckAmmo(int actionNumber, int overrideAmount = 0, bool showWarning = true) {
		if (actionNumber >= 3 && !A_CheckAmmo(1, 1)) { // All special actions also cost 1 of the primary ammo.
			return false;
		}
		
		int amount = overrideAmount != 0 ? overrideAmount : A_GetAmmoCost(actionNumber);
		class<Inventory> ammoClass = A_GetAmmoClass(actionNumber);
		
		if ((amount == 0 && invoker.owner.CountInv(ammoClass) == 0) || invoker.owner.CountInv(ammoClass) < amount) {
			if (showWarning) {
				string insufficientMessage = A_GetAmmoInsufficientMessage(actionNumber);
				A_Print(insufficientMessage);
				A_StartSound(invoker.FailSound, CHAN_WEAPON);
			}
			return false;
		}
		
		return true;
	}
	
	/**
	 * Spends ammo automatically, returns false and prints a message if there is not enough ammo.
	 * @param actionNumber The number of the action being used. 1/2 for Primary/Secondary, 3-6 for Specials 1-4.
	 * @param overrideAmount Optionally overrides the amount of ammo spent to a specific amount.
	 */
	action bool A_SpendAmmo(int actionNumber, int overrideAmount = 0) {
		if (actionNumber > 2 && !A_SpendAmmo(1, 1)) { // All special actions also cost 1 of the primary ammo.
			return false;
		}
		
		int amount = overrideAmount != 0 ? overrideAmount : A_GetAmmoCost(actionNumber);
		class<Inventory> ammoClass = A_GetAmmoClass(actionNumber);
		
		if (!A_CheckAmmo(actionNumber, overrideAmount)) {
			return false;
		}
		
		A_TakeInventory(ammoClass, amount);
		return true;
	}
	
	// Checks if ammo is available, and returns an appropriate provided state to jump to.
	action state A_CheckAmmoJump(int actionNumber, StateLabel insufficientState, StateLabel sufficientState = null) {
		if (!A_CheckAmmo(actionNumber)) {
			if (!insufficientState) {
				return null;
			}
			return ResolveState(insufficientState);
		}
		if (!sufficientState) {
			return null;
		}
		return ResolveState(sufficientState);
	}
	
	// Checks if a special action is available, and returns an appropriate provided state to jump to.
	action state A_CheckSpecialJump(int specialNumber, StateLabel insufficientState, StateLabel sufficientState = null) {
		if (!A_CheckSpecial(specialNumber)) {
			if (!insufficientState) {
				return null;
			}
			return ResolveState(insufficientState);
		}
		if (!sufficientState) {
			return null;
		}
		return ResolveState(sufficientState);
	}
	
	// Gets a special attack item class based on a special number.
	action class<Inventory> A_GetSpecialClass(int specialNumber) {
		if (specialNumber == 2) {
			return invoker.specialItemB;
		}
		if (specialNumber == 3) {
			return invoker.specialItemC;
		}
		if (specialNumber == 4) {
			return invoker.specialItemD;
		}
		return invoker.specialItemA;
	}
	
	// Gets a special attack missing item warning message based on a special number.
	action string A_GetNoSpecialMessage(int specialNumber) {
		if (specialNumber == 2) {
			return invoker.noSpecialBMessage;
		}
		if (specialNumber == 3) {
			return invoker.noSpecialCMessage;
		}
		if (specialNumber == 4) {
			return invoker.noSpecialDMessage;
		}
		return invoker.noSpecialAMessage;
	}
	
	// Checks if a special attack can be used and returns true or false, if it can't it also plays a sound and prints a warning message.
	action bool A_CheckSpecial(int specialNumber, bool checkAmmo = true) {
		class<Inventory> specialItemClass = A_GetSpecialClass(specialNumber);
		if (invoker.owner.CountInv(specialItemClass) < 1) {
			A_StartSound(invoker.failSound, CHAN_WEAPON);
			A_Print(A_GetNoSpecialMessage(specialNumber));
			return false;
		}
		return !checkAmmo || A_CheckAmmo(specialNumber + 2);
	}
	
	
	// Refire for user1 button.
	action state A_User1ReFire() {
		if (GetPlayerInput(MODINPUT_BUTTONS) & BT_USER1) {
			return ResolveState("User1Hold");
		}
		return null;
	}
	
	// Refire for user2 button.
	action state A_User2ReFire() {
		if (GetPlayerInput(MODINPUT_BUTTONS) & BT_USER2) {
			return ResolveState("User2Hold");
		}
		return null;
	}
	
	// Refire for user3 button.
	action state A_User3ReFire() {
		if (GetPlayerInput(MODINPUT_BUTTONS) & BT_USER3) {
			return ResolveState("User3Hold");
		}
		return null;
	}
	
	// Refire for user4 button.
	action state A_User4ReFire() {
		if (GetPlayerInput(MODINPUT_BUTTONS) & BT_USER4) {
			return ResolveState("User4Hold");
		}
		return null;
	}
	
	
	// Calls weapon ready with all 4 user buttons enabled also checks for ultimate, also bobs the weapon if enabled.
	action state A_SchismWeaponReady() {
		if (invoker.ultimatePending) {
			invoker.ultimatePending = false;
			return ResolveState("Ultimate");
		}
		
		A_WeaponReady(WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4);
		
		if (invoker.idleBob != 0) {
			invoker.currentIdleBob += invoker.idleBob;
			A_OverlayOffset(1, sin(invoker.currentIdleBob) * 4, 32 + cos(invoker.currentIdleBob) * 2);
			A_OverlayOffset(10, -sin(invoker.currentIdleBob) * 5, -cos(invoker.currentIdleBob) * 3);
		}
		
		return null;
	}
	
	// Clears all weapon bobbing.
	action void A_ClearBobbing() {
		invoker.currentIdleBob = 0;
		A_OverlayOffset(1, 0, 32);
		A_OverlayOffset(10, 0, 0);
	}


	/**
	 * Fires projectiles in a cross shape.
	 * radius The number of additional projectiles to fire off of the center in each direction. 0 will result in no cross.
	 * spread The distance between each projectile.
	 */
	action void A_FireProjectileCross(Class<Actor> projectileClass, int radius = 5, float spread = 30, Array<Actor> projectiles = null) {
		// Horizontal:
		for (int i = -radius; i <= radius; i++) {
			Actor projectile = A_FireProjectile(projectileClass, 0, false, i * spread, 0);
			if (projectile && projectiles) {
				projectiles.Push(projectile);
			}
		}

		// Vertical:
		for (int i = -radius; i <= radius; i++) {
            if (i == 0) { 
                continue;
            }
			float offsetZ = float(i) * spread;
			Actor projectile = A_FireProjectile(projectileClass, 0, false, 0, offsetZ);
			if (projectile) {
				projectile.SetOrigin((projectile.pos.x, projectile.pos.y, projectile.pos.z + (offsetZ * 0.1)), false);
				if (projectiles) {
					projectiles.Push(projectile);
				}
			}
		}
	}
}
