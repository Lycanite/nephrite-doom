class SchismStump : Actor {
	Default {
		Tag "Stump";
		
		Radius 24;
		Height 38;
		ProjectilePassHeight -16;

        +SOLID;
	}
	
	States {
		Spawn:
			NSTM A -1;
			Stop;
	}
}

class SchismLog : SchismStump {
	Default {
		Tag "Log";
	}
	
	States {
		Spawn:
			NSTM B -1;
			Stop;
	}
}