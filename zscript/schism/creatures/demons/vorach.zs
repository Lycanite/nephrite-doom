class Vorach : Creature {
	Default {
		Tag "Vorach";
		Health 400;
		Speed 7;
		Radius 30;
		Height 56;
		Mass 1000;
		
		PainChance 28;
		
		Obituary "%o couldn't escape a Vorach.";
		SeeSound "Vorach/See";
		ActiveSound "Vorach/Idle";
		PainSound "Vorach/Pain";
		DeathSound "Vorach/Death";
	}
	
	States {
		Spawn:
			VORA D 10 A_Look();
			Loop;
			
		See:
			VORA AA 3 A_Chase();
			VORA B 7 A_Chase();
			VORA CC 3 A_Chase();
			VORA D 7 A_Chase();
			VORA EE 3 A_Chase();
			VORA F 7 A_Chase();
			Loop;
			
		Melee:
		Missile:
			VORA G 0 bright A_StartSound("Vorach/Melee");
			VORA GHHHH 4 bright A_FaceTarget();
			VORA II 4 bright A_FaceTarget();
			VORA J 8 bright A_SpawnProjectile("VoidGrazerProjectile", invoker.height / 2, 0, 0);
			VORA JJ 4 bright A_FaceTarget();
			Goto See;
			
		Pain:
			VORA K 4;
			VORA K 4 A_Pain();
			Goto See;
			
		Death:
			VORA L 6;
			VORA M 6 A_Scream();
			VORA N 6;
			VORA O 6 A_CreatureDie();
			VORA P 8;
			VORA Q -1;
			Stop;
			
		Raise:
			VORA QPONML 6;
			Goto See;
	}
}