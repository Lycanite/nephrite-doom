class GreatDrakeScale : WeaponUltimate {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "You have obtained the Draconic Scale of a Great Drake!";
		
		Inventory.Icon "AVESV0";
		Inventory.PickupSound "Vess/Taunt";
		Inventory.RestrictedTo "Vess";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}
	
	
	States {
		Spawn:
			AVES V 1 bright;
			Loop;
	}
}
