class BloodfallsScroll : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "You can now launch your Blood Globe forwards for a blood soaking attack!";
		
		Inventory.Icon "VESWD0";
		Inventory.PickupSound "BloodGlobe/Pickup";
		Inventory.RestrictedTo "Vess";
		WeaponUpgrade.ExtraAmmoClass "BloodDrupe";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			VESW D 1 bright;
			Loop;
	}
}

extend class BloodGlobe {
	action void A_Bloodfalls() {
		int level = invoker.AnimaLevel(invoker.specialACost);
		if (!A_CheckSpecial(1) || !A_SpendAmmo(3) || !invoker.sphere || !invoker.sphereTarget) {
			return;
		}
		invoker.sphereTarget.locked = true;
		// vector3 bloodfallsTargetPos = invoker.owner.Vec3Angle(invoker.owner.radius + 600, invoker.owner.angle, invoker.owner.height); // None raytracing absolute target.
		FLineTraceData lineTraceData;
		invoker.owner.LineTrace(invoker.owner.angle, invoker.owner.radius + 600, 0, TRF_THRUACTORS, invoker.owner.height / 2, 0, 0, lineTraceData);
		vector3 bloodfallsTargetPos = lineTraceData.HitLocation;
		invoker.sphereTarget.SetOrigin(bloodfallsTargetPos, false);
		invoker.sphere.Bloodfalls(level);
	}

	States {
		MainHand.User1:
			VSH1 Q 0 A_ClearBobbing();
			VSH1 QR 4;
			VSH1 S 4 A_Bloodfalls();
			VSH1 TU 4 A_Light0();
			Goto Ready;
	}
}

extend class BloodGlobeSphere {
	float bloodfallsSpeed;
	int bloodfallsLevel;
	int bloodfallsLifetime;

	virtual void Bloodfalls(int level) {
		if (!self.bloodGlobe) {
			return;
		}
		self.specialAction = "Bloodfalls";
		self.bloodfallsLifetime = 0;
		float distance = self.Distance3D(self.bloodGlobe.sphereTarget);
		self.bloodfallsSpeed = distance / (TICRATE * 3);
		self.bloodfallsLevel = level;
		self.SetState(self.ResolveState("Bloodfalls"));
	}

	action state A_Bloodfalls() {
		if (!invoker.bloodGlobe) {
			return invoker.ResolveState("Death");
		}

		// Timeout:
		if (invoker.bloodfallsLifetime++ >= 3 * TICRATE) {
			A_EndSpecial();
			return invoker.ResolveState("Spawn");
		}

		// Float Forwards:
		A_FloatToTarget(invoker.bloodfallsSpeed);

		// Fire Projectiles:
		for (int i = 0; i < invoker.bloodfallsLevel; i++) {
			Actor projectile = A_SpawnProjectile("BloodGlobeBloodfallsProjectile", 0, 0, Random(0, 360), CMF_AIMDIRECTION|CMF_TRACKOWNER, Random(0, 90));
			if (projectile && invoker.bloodGlobe) {
				projectile.target = invoker.bloodGlobe.owner;
				projectile.A_SetScale(invoker.scale.x);
			}
		}

		// Distance Check:
		if (invoker.bloodGlobe.sphereTarget) {
			float distance = invoker.Distance3D(invoker.bloodGlobe.sphereTarget);
			if (distance <= 0.1) {
				A_EndSpecial();
				return ResolveState("Spawn");
			}
		}

		return null;
	}

	States {
		Bloodfalls:
			VBLO MMMMNNNNOOOO 1 A_Bloodfalls();
			Loop;
	}
}

class BloodGlobeBloodfallsProjectile : BloodBelchProjectile {
	Default {
		Speed 10;
		FastSpeed 10;
		DamageFunction 10;
		Alpha 0.5;

		SchismProjectile.LifetimeMax TICRATE;
		SchismProjectile.KnockbackStrength 4;
		SchismProjectile.StatusEffectClass "SchismSoakDebuff";
		SchismProjectile.StatusEffectDuration 10 * TICRATE;
		SchismProjectile.ParticleInterval TICRATE;
		SchismProjectile.ParticleColor 0x96FF2222;
		
		SeeSound "BloodGlobe/Fire/Bloodfalls";
	}
}