class Nymph : MinionCreature {
	Default {
		Tag "Nymph";
		
		Health 150;
		Radius 10;
		Height 70;
		Speed 15;
		FloatSpeed 15;
		Scale 0.5;
		Gravity 0;
		PainChance 120;
		Damage 1;
		Alpha 1;
		BloodColor "ff 00 ff";
		MaxTargetRange 1000;

		DamageFactor "Fae", 0.1;
		
		SeeSound "Nymph/See";
		PainSound "Nymph/Pain";
		DeathSound "Nymph/Death";
		ActiveSound "Nymph/Idle";
		MeleeSound "Nymph/Melee";
		Obituary "%o was unhealed by a Nymph!";
		HitObituary "%o was unhealed by a Nymph!";
		
		+FLOAT;
		+Creature.HOVERBOB;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		self.SpawnParticle("Anima", 8, 1, (FRandom(-8, 8), FRandom(-8, 8), FRandom(10, 40)), Random(45, 315), Random(-10, 10), 0.25);
		int leafColor = Random(1, 10) >= 5 ? 0xFFDD99 : 0xFF4455;
		self.SpawnParticle("Leaf", 2 * 8, 1, (FRandom(-8, 8), FRandom(-8, 8), FRandom(10, 40)), Random(45, 315), Random(-10, 10), 0.5, leafColor);
	}
	
	action void A_Burst() {
		for (int i = 0; i < 20; i += 20) {
			invoker.SpawnParticle("Anima", 8, 3, (FRandom(-6, 6), FRandom(-6, 6), FRandom(10, 40)), i, Random(-10, 10));
		int leafColor = Random(1, 10) >= 5 ? 0xFFDD99 : 0xFF4455;
			invoker.SpawnParticle("Leaf", 2 * 8, 4, (FRandom(-6, 6), FRandom(-6, 6), FRandom(10, 40)), i, Random(-10, 10), 1.0, leafColor);
		}
	}
	
	States {
		Spawn:
			NYMP AABBCCBB 4 {
				A_Look();
				return A_StayWithMaster();
			}
			Loop;
			
		Follow:
			NYMP DEFE 4 {
				A_Look();
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			NYMP DEFE 4 {
				return A_MinionChase();
			}
			Loop;
		
		Missile:
			NYMP G 20 A_FaceTarget();
			NYMP H 4 bright A_SpawnProjectile("NymphProjectile", 64);
			NYMP I 4 bright;
			Goto See;
			
		Pain:
			NYMP J 6 A_Pain();
			Goto See;
			
		Pain.SelfDamage:
			Goto See;
			
		Death:
		XDeath:
			NYMP J 5;
			NYMP K 5 A_Scream();
			NYMP L 5;
			NYMP M 5 A_NoBlocking();
			NYMP NOPQRSTUV 5;
			NYMP W 5 A_Burst();
		Fade:
			NYMP X 1 bright A_FadeOut(0.1);
			Loop;
	}
}

class NymphProjectile : LifestarProjectile {
	Default {
		Scale 1.5;
		Speed 10;
		FastSpeed 10;
		Damage 2;
		Alpha 1;
		RenderStyle "Add";
		
		DamageType "Fae";

		SchismProjectile.ParticleName "Anima";
		SchismProjectile.ParticleCount 4;
		SchismProjectile.ParticleScale 0.5;
	}

	action state A_NymphProjectileUpdate() {
		A_SeekerMissile(0, 15, SMF_LOOK|SMF_PRECISE);
		if (invoker.lifetime > 10 * TICRATE) {
			return ResolveState("Death");
		}
		return null;
	}
	
	States {
		Spawn:
			NPLS AB 5 bright A_NymphProjectileUpdate();
			Loop;
			
		Death:
			NPLS CDEFG 5 bright;
			Stop;
	}
}