class MolochKathoga : Kathoga {
	Default {
		Tag "Moloch Kathoga";
		Health 130;
		Speed 14;
		
		Obituary "%o was devoured by a Moloch Kathoga.";
		SeeSound "MolochKathoga/See";
		ActiveSound "MolochKathoga/Idle";
		PainSound "MolochKathoga/Pain";
		DeathSound "MolochKathoga/Death";
		AttackSound "MolochKathoga/Melee";
	}
		
	States {
		Spawn:
			MOLC AB 10 A_Look();
			Loop;
			
		See:
			MOLC AABBCCDD 2 fast A_Chase();
			Loop;
		
		Melee:
			MOLC EF 8 fast A_FaceTarget();
			MOLC G 8 fast A_CreatureMelee(4, 40, "Chaos", "Chaos");
			Goto See;
		
		Pain:
			MOLC H 2 fast;
			MOLC H 2 fast A_Pain();
			Goto See;
		
		Death:
			MOLC I 8;
			MOLC J 8 A_Scream();
			MOLC K 8;
			MOLC L 8 A_CreatureDie();
			MOLC M 8;
			MOLC N -1;
			Stop;
			
		Raise:
			MOLC N 5;
			MOLC MLKJI 5;
			Goto See;
	}
}