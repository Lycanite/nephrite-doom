class SchismTreeMaple : SchismTree {
	Default {
		Tag "Maple Tree";
		SchismTree.SpringVariants 2;
		SchismTree.SummerVariants 2;
		SchismTree.AutumnVariants 2;
		SchismTree.WinterVariants 2;
	}
	
	States {
		Spring.1:
			TR08 A -1;
			Stop;

		Spring.2:
			TR08 B -1;
			Stop;

		Summer.1:
			TR08 E -1;
			Stop;

		Summer.2:
			TR08 F -1;
			Stop;

		Autumn.1:
			TR08 I -1;
			Stop;

		Autumn.2:
			TR08 J -1;
			Stop;

		Winter.1:
			TD08 M -1;
			Stop;

		Winter.2:
			TD08 N -1;
			Stop;

		Death.Spring.1:
			TD08 A -1;
			Stop;

		Death.Spring.2:
			TD08 B -1;
			Stop;

		Death.Summer.1:
			TD08 E -1;
			Stop;

		Death.Summer.2:
			TD08 F -1;
			Stop;

		Death.Autumn.1:
			TD08 I -1;
			Stop;

		Death.Autumn.2:
			TD08 J -1;
			Stop;

		Death.Winter.1:
			TD08 M -1;
			Stop;

		Death.Winter.2:
			TD08 N -1;
			Stop;
	}
}