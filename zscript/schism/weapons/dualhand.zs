const PSP_WEAPON_OFFHAND = 10;

class DualHand : SchismWeapon {
	bool mirrorMainHand; // If true, the off hand sprites will be mirrored.
	bool mirrorOffHand; // If true, the off hand sprites will be mirrored.

	property MirrorMainHand: mirrorMainHand;
	property MirrorOffHand: mirrorOffHand;
	
	action state A_SetMainHandState(StateLabel targetState) {
		A_OverlayFlags(PSP_WEAPON, PSPF_FLIP|PSPF_MIRROR, invoker.mirrorMainHand);
		return ResolveState(targetState);
	}
	
	action void A_SetOffHandState(StateLabel targetState) {
		A_Overlay(PSP_WEAPON_OFFHAND, targetState);
		A_OverlayFlags(PSP_WEAPON_OFFHAND, PSPF_FLIP|PSPF_MIRROR, invoker.mirrorOffHand);
		A_OverlayFlags(PSP_WEAPON_OFFHAND, PSPF_RENDERSTYLE|PSPF_ALPHA, true);
	}

	action void A_MirrorHands() {
		invoker.mirrorMainHand = !invoker.mirrorMainHand;
		A_OverlayFlags(PSP_WEAPON, PSPF_FLIP|PSPF_MIRROR, invoker.mirrorMainHand);
		
		invoker.mirrorOffHand = !invoker.mirrorOffHand;
		A_OverlayFlags(PSP_WEAPON_OFFHAND, PSPF_FLIP|PSPF_MIRROR, invoker.mirrorOffHand);
	}
	
	States {
		Select:
			TNT1 A 0 {
				A_SetOffHandState("OffHand.Select");
				return A_SetMainHandState("MainHand.Select");
			}
			
		Deselect:
			TNT1 A 0 {
				A_SetOffHandState("OffHand.Deselect");
				return A_SetMainHandState("MainHand.Deselect");
			}
			
		Ready:
			TNT1 A 0 {
				A_SetOffHandState("OffHand.Ready");
				return A_SetMainHandState("MainHand.Ready");
			}
		
		Fire:
			TNT1 A 0 {
				A_SetOffHandState("OffHand.Fire");
				return A_SetMainHandState("MainHand.Fire");
			}
		MainHand.Fire:
			Goto Ready;
		
		AltFire:
			TNT1 A 0 {
				A_SetOffHandState("OffHand.AltFire");
				return A_SetMainHandState("MainHand.AltFire");
			}
		
		User1:
			TNT1 A 0 {
				A_SetOffHandState("OffHand.User1");
				return A_SetMainHandState("MainHand.User1");
			}
		User1Hold:
			TNT1 A 0 {
				A_SetOffHandState("OffHand.User1Hold");
				return A_SetMainHandState("MainHand.User1Hold");
			}
		
		User2:
			TNT1 A 0 {
				A_SetOffHandState("OffHand.User2");
				return A_SetMainHandState("MainHand.User2");
			}
		User2Hold:
			TNT1 A 0 {
				A_SetOffHandState("OffHand.User2Hold");
				return A_SetMainHandState("MainHand.User2Hold");
			}
		
		User3:
			TNT1 A 0 {
				A_SetOffHandState("OffHand.User3");
				return A_SetMainHandState("MainHand.User3");
			}
		User3Hold:
			TNT1 A 0 {
				A_SetOffHandState("OffHand.User3Hold");
				return A_SetMainHandState("MainHand.User3Hold");
			}
		
		User4:
			TNT1 A 0 {
				A_SetOffHandState("OffHand.User4");
				return A_SetMainHandState("MainHand.User4");
			}
		User4Hold:
			TNT1 A 0 {
				A_SetOffHandState("OffHand.User4Hold");
				return A_SetMainHandState("MainHand.User4Hold");
			}
		
		Reload:
			TNT1 A 0 {
				A_SetOffHandState("OffHand.Reload");
				return A_SetMainHandState("MainHand.Reload");
			}
		
		Ultimate:
			TNT1 A 0 {
				A_SetOffHandState("OffHand.Ultimate");
				return A_SetMainHandState("MainHand.Ultimate");
			}
			
			
		// Main Hand States:
		MainHand.Select:
			TNT1 A 1 A_Raise(12);
			Loop;
			
		MainHand.Deselect:
			TNT1 A 1 A_Lower(12);
			Loop;
			
		MainHand.Ready:
			TNT1 A 1;
			Loop;
			
		MainHand.AltFire:
			Goto Ready;
		MainHand.User1:
		MainHand.User1Hold:
			Goto Ready;
		MainHand.User2:
		MainHand.User2Hold:
			Goto Ready;
		MainHand.User3:
		MainHand.User3Hold:
			Goto Ready;
		MainHand.User4:
		MainHand.User4Hold:
			Goto Ready;
		MainHand.Reload:
			Goto Ready;
		MainHand.Ultimate:
			Goto Ready;
		
		
		// Off Hand States:			
		OffHand.Select:
		OffHand.Deselect:
		OffHand.Ready:
			TNT1 A 1;
			Loop;
		
		OffHand.Reload:
		OffHand.Ultimate:
		OffHand.User4:
		OffHand.User4Hold:
		OffHand.User3:
		OffHand.User3Hold:
		OffHand.User2:
		OffHand.User2Hold:
		OffHand.User1:
		OffHand.User1Hold:
		OffHand.AltFire:
		OffHand.Fire:
			TNT1 A 1 bright;
			Loop;
			
		OffHand.Hidden:
			Stop;
	}
}
