class TearRiftProjectile : SchismProjectile {
	VoidManipulation voidManipulation;
	VoidManipulationRift targetRift;
	bool expired;
	int empowerEnergy;
	string riftClass;
	
	Default {
		Radius 5;
		Height 7;
		Speed 200;
		Gravity 0;
		DamageFunction random(0, 1);
		Alpha 1.0;
		RenderStyle "Translucent";
		Scale 0.75;
		
		SeeSound "VoidManipulation/Fire/Rift";
		DeathSound "VoidManipulation/Fire/Rift";
		Obituary "%o was sent to the void via %k's Void Manipulation!";
		
		+NODAMAGETHRUST;
		+PAINLESS;
		+SEEKERMISSILE;
		+SKYEXPLODE;
		+GHOST;
		+THRUGHOST;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		expired = false;
		empowerEnergy = 20;
		riftClass = "VoidManipulationRift";
	}
	
	
	action void A_UpdateTargetRift() {
		if (invoker.targetRift) {
			return;
		}
		if (invoker.voidManipulation) {
			invoker.targetRift = invoker.voidManipulation.targetRift;
		}
	}
	
	action state A_RiftProjectileUpdate() {
		A_UpdateTargetRift();
		
		if (invoker.expired) {
			if (invoker.targetRift) { // Expire if the target wasn't reached due to the range limit.
				return ResolveState("Expired");
			}
			return ResolveState("Death"); // Create a new rift if the range limit was reached and there is no target rift.
		}
		
		// Do nothing else if there is no target.
		if (!invoker.targetRift) {
			return null;
		}
		
		// Rift Impact or Expired:
		if (Distance3D(invoker.targetRift) <= invoker.speed + 10) {
			return ResolveState("RiftImpact");
		}
		
		// Follow Target Rift:
		invoker.bNoClip = true;
		invoker.tracer = invoker.targetRift;
		invoker.speed = 20;
		A_FaceTracer(360, 360, 0, 0, FAF_MIDDLE);
		A_SeekerMissile(0, 90, !SMF_LOOK|SMF_PRECISE);
		
		return null;
	}
	
	action void A_CreateRift() {
		if (invoker.targetRift || !invoker.voidManipulation) {
			A_RiftImpact();
			return;
		}
		invoker.voidManipulation.CreateRift(invoker.riftClass, invoker.Pos, invoker.empowerEnergy);
	}
	
	action void A_RiftImpact() {
		if (!invoker.targetRift) {
			return;
		}
		invoker.targetRift.Empower(invoker.empowerEnergy);
	}
	
	
	States {
		Spawn:
			VDPT ABCDEFGHI 1 A_RiftProjectileUpdate();
			Loop;
			
		Death:
			VDPT A 0 {
				A_UpdateTargetRift();
				A_CreateRift();
			}
		Expired:
			VDPT ABCDEFGHI 1 A_FadeOut(0.05);
			Loop;
			
		RiftImpact:
			VDPT A 0 A_RiftImpact();
			Stop;
	}
}
