class Ent : MinionCreature {
	Default {
		Health 300;
		Radius 18;
		Height 52;
		Speed 8;
		Scale 0.75;
		PainChance 80;
		Damage 25;
		BloodColor "00 aa 00";
		
		SeeSound "Ent/See";
		PainSound "Ent/Pain";
		DeathSound "Ent/Death";
		ActiveSound "Ent/Idle";
		MeleeSound "Ent/Melee";
		Obituary "An Ent wouldn't leave %o alone!";
		HitObituary "An Ent wouldn't leave %o alone!";
	}
	
	States {
		Spawn:
			ENTT AABBCCBB 4 {
				A_Look();
				return A_StayWithMaster();
			}
			Loop;
			
		Follow:
			ENTT DEFE 4 {
				A_Look();
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			ENTT DEF 4 {
				return A_MinionChase();
			}
			ENTT E 4 {
				return A_MinionChase();
			}
			Loop;
			
		Melee:
			ENTT G 4 A_FaceTarget();
			ENTT H 4 A_CreatureMelee(25);
			ENTT I 4;
			Goto See;
			
		Pain:
			ENTT J 6;
			ENTT J 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			ENTT KL 5;
			ENTT M 5 A_Scream();
			ENTT N 5;
			ENTT P 5 A_CreatureDie();
			ENTT PQRSTUVW 5;
			ENTT X -1;
			Stop;
	}
}
