class UndulateOak : SchismPlayerPawn {
	int blinkTime;
	int blinkBackTime;
	
	int activeEctoplasm;
	
	Default {
		Speed 1;
		Health 150;
		Radius 16;
		Height 56;
		Mass 100;
		Scale 0.8;
		PainChance 255;
		RadiusDamageFactor 0.25;
		
		Player.DisplayName "UndulateOak";
		Player.SoundClass "UndulateOak";
		//Player.CrouchSprite "UOAC";
		//Player.ScoreIcon "OAKFACE";
		Player.ColorRange 0, 0;
		Player.DamageScreenColor "ff 00 00";
		
		Player.MaxHealth 150;
		Player.ForwardMove 1, 1;
		Player.SideMove 1, 1;
		Player.JumpZ 8.0;
		Player.HexenArmor 20, 20, 50, 10, 40;
		Player.InvulnerabilityMode "Ghost";
		Player.HealRadiusType "Health";
		
		Player.FlechetteType "ArtiPoisonBag1";
		Player.StartItem "PrimordialStaff";
		Player.StartItem "LifeSeed", 100;
		
		Player.WeaponSlot 1, "Chainsaw", "Gauntlets"; // Melee
		Player.WeaponSlot 2, "PrimordialStaff"; // Primary
		Player.WeaponSlot 3, "Shotgun", "SuperShotgun", "Crossbow", "StrifeCrossbow", "FWeapAxe", "DruidBow"; // Secondary
		Player.WeaponSlot 4, "Chaingun", "Blaster", "AssaultGun", "FWeapHammer", "RaijinsBattleaxe"; // Tertiary
		Player.WeaponSlot 5, "RocketLauncher", "Mace", "FWeapQuietus", "MiniMissileLauncher"; // Powerful
		Player.WeaponSlot 6, "PlasmaRifle", "SkullRod", "FlameThrower", "Mauler"; // Special
		Player.WeaponSlot 7, "BFG9000", "PhoenixRod", "StrifeGrenadeLauncher"; // Ultimate

		SchismPlayerPawn.BaseName "UndulateOak";
		SchismPlayerPawn.HexenClass "Cleric";
		SchismPlayerPawn.AbilityCooldownMax 2 * TICRATE;
		SchismPlayerPawn.AbilityBCooldownMax 2 * TICRATE;
		
	}
	
	override void Tick() {
		// Voodoo Doll:
		if (!self.player || !self.player.mo || self.player.mo != self) {
			super.Tick();
			return;
		}
		
		super.Tick();
		
		// Blink Timing:
		if (self.blinkTime > 0) {
			self.blinkTime--;
			if (self.blinkTime <= 0) {
				self.EnigmaBlinkFinish();
			}
		}
		
		// Passive - Time Dilation:
		if (self.passiveEnabled) {
			if (self.UpdateTic % TICRATE == 0) {
				BlockThingsIterator iter = BlockThingsIterator.Create(self, 100, false);
				while (iter.Next()) {
					Actor target = iter.thing;
					if (SchismInteraction.CanHarmTarget(self, target) && !PlayerPawn(target)) {
						SchismDelayDebuff debuff = SchismDelayDebuff(target.GiveInventoryType("SchismDelayDebuff"));
						if (debuff) {
							debuff.ApplyEffect(self, self);
						}
					}
				}
			}
		}
	}
	
	override int DamageMobj(Actor inflictor, Actor source, int damage, Name mod, int flags, double angle) {
		double damageTaken = super.DamageMobj(inflictor, source, damage, mod, flags, angle);
		int reducedDamage = damageTaken;
		reducedDamage = Ceil(damageTaken / 2);
		return reducedDamage;
	}
	
	override string GetAmmoClass(int category, int size) {
		string sizeString = "";
		if (size == 1) {
			sizeString = "Large";
		}

		switch(category) {
			case -1:
				return "Ectoplasm" .. sizeString;
			case 0:
				return "LifeSeed" .. sizeString;
			case 1:
				return "DruidArrow" .. sizeString;
			case 2:
				return "StormSphere" .. sizeString;
			default:
				return super.GetAmmoClass(category, size);
		}
	}
	
	override class<Inventory> GetWeaponClass(int weaponCategory, int index) {
		switch(weaponCategory) {
			case 0:
				switch(index) {
					case 0:
						return "PrimordialStaff";
					case 1:
						return "BombEgg";
					case 2:
						return "TornadoEgg";
					case 3:
						return "PolymorphEgg";
					case 4:
						return "ThornsEgg";
					default:
						return "QuackleEgg";
				}

			case 1:
				switch(index) {
					case 0:
						return "DruidBow";
					case 1:
						return "DruidBow"; // Initial
					case 2:
						return "LightningDruidCrest";
					case 3:
						return "WaterDruidCrest";
					case 4:
						return "FaeDruidCrest";
					default:
						return "QuackleDruidCrest";
				}

			case 2:
				switch(index) {
					case 0:
						return "RaijinsBattleaxe";
					case 1:
						return "PhantomTalisman";
					case 2:
						return "EctoTalisman";
					case 3:
						return "TwisterTalisman";
					case 4:
						return "DreadTalisman";
					default:
						return "QuackleTalisman";
				}

			default:
				return "";
		}
	}

	override class<Inventory> GetSigilClass() {
		return "DimensionalFragment";
	}
	
	override bool SchismSpecial(int specialId) {
		if (!super.SchismSpecial(specialId)) {
			return false;
		}
		
		if (specialId == 1) {
			EnigmaBlinkStart(500);
		}
		else if (specialId == 2) {
			EnigmaBlinkStart(-500);
		}
		
		return true;
	}
	
	virtual void EnigmaBlinkStart(int force) {
		A_SpawnItemEx("OakBlink", 0, 0, 0);
		A_ChangeVelocity(force, 0, 0, CVF_RELATIVE|CVF_REPLACE);
		blinkTime = 2;
	}
	
	virtual void EnigmaBlinkFinish() {
		A_ChangeVelocity(0, 0, 0, CVF_RELATIVE|CVF_REPLACE);
		A_SpawnItemEx("OakBlink", 0, 0, 0);
		A_Explode(100, 200, !XF_HURTSOURCE|XF_NOTMISSILE);
	}
	
	States {
		Spawn:
			UOAK A -1;
			Loop;
		See:
			UOAK ABCD 4;
			Loop;
		Missile:
			UOAK E 8;
			Goto Spawn;
		Melee:
			UOAK F 8;
			Goto Spawn;
		Pain:
			UOAK G 4;
			UOAK G 4 A_Pain;
			Goto Spawn;
		Death:
			UOAK G 6;
			UOAK H 6 A_PlayerScream;
			UOAK IJ 6;
			UOAK K 6 A_NoBlocking;
			UOAK LM 6;
			UOAK N -1;
			Stop;
		XDeath:
		Burn:
		Ice:
		Disintegrate:
			UOAK O 5 A_PlayerScream;
			UOAK P 0 A_NoBlocking;
			UOAK Q 5 A_SkullPop;
			UOAK RSTUV 5;
			UOAK W -1;
			Stop;
    }
}


#include "zscript/schism/classes/undulateoak/cockatrice.zs"