class DreadWallSigil : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Grim Scythe can now summon a Dread Wall at the cost of Soul Essence!";
		
		Inventory.Icon "GSCID0";
		Inventory.PickupSound "GrimScythe/Pickup";
		Inventory.RestrictedTo "Chozo";
		WeaponUpgrade.ExtraAmmoClass "GrimRune";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			GSCI D 1 bright;
			Loop;
	}
}

class GrimScytheDreadWall : Actor {
	int lifeTime;
	
	Default {
		Scale 1;
		Health 5000;
		Radius 64;
		Height 100;
		Mass 10000000;
		Gravity 1;
		
		+SOLID;
		+SHOOTABLE;
		+NOBLOOD;
		+DONTGIB;
		+NOICEDEATH;
		+FLOORHUGGER
		-USESPECIAL;
		
		SeeSound "GrimScythe/Fire/Wall";
		DeathSound "GrimScythe/Explode/Wall";
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		lifeTime = 10 * 35;
	}
	
	override bool Used(Actor user) {
		bool isUsed = super.Used(user);
		if (PlayerPawn(user)) {
			lifeTime = 0;
		}
		return isUsed;
	}
	
	action state WallUpdate() {
		invoker.lifeTime -= 4;
		if (invoker.lifeTime <= 0) {
			return ResolveState("Death");
		}
		return null;
	}

	States {
		Spawn:
			GSCP TUVW 4 {
				return WallUpdate();
			}
			Loop;
		
		Death:
			GSCP XYZ 4;
			Stop;
	}
}

class GrimScytheDreadRoof : GrimScytheDreadWall {
	Default {
		Gravity 0;
		
		+NOGRAVITY
		-FLOORHUGGER
	}
}
