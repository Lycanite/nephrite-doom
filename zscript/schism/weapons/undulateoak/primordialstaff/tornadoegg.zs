class TornadoEgg : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Primordial Staff can now unleash a deadly Eggnado!";
		
		Inventory.Icon "WCCKX0";
		Inventory.PickupSound "PrimordialStaff/Pickup";
		Inventory.RestrictedTo "UndulateOak", "UndulateOakCockatrice";
		WeaponUpgrade.ExtraAmmoClass "LifeSeed";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			WCCK X 1 bright;
			Loop;
	}
}

class PrimordialStaffEggnadoProjectile : EctoplasmProjectile {
	Default {
		Radius 16;
		Height 10;
		Scale 1;
		Speed 12;
		DamageFunction 20;
		Gravity 0.125;
		Alpha 1;
		Renderstyle "Normal";
		Projectile;
		
		SeeSound "PrimordialStaff/Tornado";
		DeathSound "PrimordialStaff/Tornado/Explode";
		
		-NOGRAVITY;
		+EXPLODEONWATER;
	}
	
	States {
		Spawn:
			WCEG PQRS 4;
			Loop;
			
		Death:
			WCEG Y 4 bright {
				Gravity = 0;
				SchismAttacks.Explode(invoker, invoker.target, 2, 40);
				A_SpawnItemEx("PrimordialStaffEggnado", 0, 0, 0, 0, 0, 0, 0, SXF_TRANSFERPOINTERS);
			}
			WCEG ZWX 4 bright;
			Stop;
	}
}

class PrimordialStaffEggnado : SchismAttackActor {
	Default {
		Radius 16;
		Height 10;
		Scale 1.5;
		Speed 12;
		DamageFunction 0;
		Gravity 0.125;
		Alpha 0.8;
		Renderstyle "Translucent";

		SchismAttackActor.Lifespan 10 * TICRATE;
		SchismAttackActor.GenerateSpecialAmmo true;
		
		SeeSound "PrimordialStaff/Tornado";
		DeathSound "PrimordialStaff/Tornado/Explode";
		
		+RANDOMIZE;
		-NOGRAVITY;
	}
	
	action state A_TornadoUpdate() {
		if (!target) {
			return invoker.ResolveState("Death");
		}

		A_StartSound(invoker.seeSound, CHAN_BODY, CHANF_LOOPING);
		A_Wander();
		SchismAttacks.Explode(invoker, invoker.target, 0, 100, 4, 8);
		
		int offsetXY = random(-10, 10);
		int offsetZ = random(0, 40);
		PrimordialStaffProjectile projectileActor = PrimordialStaffProjectile(A_SpawnProjectile("PrimordialStaffProjectile", offsetZ, offsetXY, Random(-180, 180), CMF_AIMDIRECTION|CMF_TRACKOWNER, Random(0, -35)));
		if (projectileActor) {
			projectileActor.target = target;
		}
		
		if (invoker.lifespan <= 0) {
			return invoker.ResolveState("Death");
		}
		return null;
	}
	
	States {
		Spawn:
			WCET DEFG 4 bright;
			
		See:
			WCET ABC 4 bright {
				return A_TornadoUpdate();
			}
			Loop;
			
		Death:
			WCET G 4 bright A_StopSound(CHAN_BODY);
			WCET FED 4 bright;
			Stop;
	}
}
