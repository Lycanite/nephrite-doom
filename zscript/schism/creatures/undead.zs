#include "zscript/schism/creatures/undead/draugr.zs"
#include "zscript/schism/creatures/undead/draugrkeppel.zs"

#include "zscript/schism/creatures/undead/ghoul.zs"
#include "zscript/schism/creatures/undead/ghoulkeppel.zs"

#include "zscript/schism/creatures/undead/ossifex.zs"
#include "zscript/schism/creatures/undead/ossifexkeppel.zs"
#include "zscript/schism/creatures/undead/anzuossifex.zs"

#include "zscript/schism/creatures/undead/grimreaver.zs"
#include "zscript/schism/creatures/undead/grimreaverkeppel.zs"