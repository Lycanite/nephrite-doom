class JousteAlpha : Jouste {
	Default {
		Health 300;
		Radius 20;
		Height 64;
		Speed 28;
		Scale 0.75;
		PainChance 100;
		Damage 4;
		BloodColor "ff 88 22";
		
		SeeSound "JousteAlpha/See";
		PainSound "JousteAlpha/Pain";
		DeathSound "JousteAlpha/Death";
		ActiveSound "JousteAlpha/Idle";
		MeleeSound "JousteAlpha/Melee";
		Obituary "%o was pierced by a Jouste Alpha!";
		HitObituary "%o was pierced by a Jouste Alpha!";

		+HARMFRIENDS;
		+DOHARMSPECIES;
		+FORCEINFIGHTING;
		-NOINFIGHTSPECIES;
	}

	/**
	* Checks for nearby monsters of the same class and targets them for attack if in sight.
	*/
	action state A_LookAlpha() {
		BlockThingsIterator iter = BlockThingsIterator.Create(self, 5000, false);
		while (iter.Next()) {
			JousteAlpha rival = JousteAlpha(iter.thing);
			if (!rival || rival == invoker || rival.health <= 0 || rival.bDormant || !invoker.CheckSight(rival)) {
				continue;
			}
			invoker.target = rival;
			return ResolveState("See");
		}
		return null;
	}
	
	States {
		Spawn:
			JOUA A 4 {
				A_Look();
				state alphaState = A_LookAlpha();
				if (alphaState) {
					return alphaState;
				}
				return A_StayWithMaster();
			}
			JOUA ABBCCBB 4 {
				A_Look();
				return A_StayWithMaster();
			}
			Loop;
			
		Follow:
			JOUA DEFE 4 {
				A_Look();
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			JOUA D 4 {
				A_LookAlpha();
				return A_MinionChase();
			}
			JOUA EF 4 {
				return A_MinionChase();
			}
			JOUA E 4 {
				A_StartSound("JousteAlpha/Step");
				return A_MinionChase();
			}
			Loop;
			
		Melee:
			JOUA G 4 A_FaceTarget();
			JOUA H 4 A_CreatureMelee(8);
			JOUA I 4;
			Goto See;
			
		Pain:
			JOUA J 6;
			JOUA J 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			JOUA KL 5;
			JOUA M 5 A_Scream();
			JOUA N 5;
			JOUA P 5 A_CreatureDie();
			JOUA QRSTUVW 5;
			JOUA X -1;
			Stop;
	}
}
