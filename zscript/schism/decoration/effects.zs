class SchismWaterSpout : Actor {
	Default {
		Tag "Water Spout";
		
		Alpha 0.75;
		RenderStyle "Translucent";
	}
	
	States {
		Spawn:
			NWTR ABCDEF 4;
			Loop;
	}
}

class SchismVoidTendril : Actor {
	Default {
		Tag "Void Tendril";
		
		Scale 1;
		Radius 24;
		Height 40;
		ProjectilePassHeight -16;
		
		+SOLID;
		+RANDOMIZE;
	}
	
	States {
		Spawn:
			NVTN A 8 {
				invoker.scale.x = FRandom(0.9, 1.1);
				invoker.scale.y = FRandom(0.9, 1.1);
			}
			NVTN BCD 8;
		Idle:
			NVTN EFGH 8;
			Loop;
	}
}

class SchismVoidTendrilEye : SchismVoidTendril {
	Default {
		Tag "Void Eye Tendril";
	}
	
	States {
		Spawn:
			NVTN I 8 {
				invoker.scale.x = FRandom(0.9, 1.1);
				invoker.scale.y = FRandom(0.9, 1.1);
			}
			NVTN JKL 8;
		Idle:
			NVTN MNOP 8;
			Loop;
	}
}

class SchismFleshTendril : Actor {
	Default {
		Tag "Flesh Tendril";

		Scale 1;
		Radius 24;
		Height 40;
		ProjectilePassHeight -16;

		+SOLID;
		+RANDOMIZE;
	}

	States {
		Spawn:
			NFTN A 8 {
				invoker.scale.x = FRandom(0.9, 1.1);
				invoker.scale.y = FRandom(0.9, 1.1);
			}
			NFTN BCD 8;
		Idle:
			NFTN EFGH 8;
			Loop;
	}
}

class SchismFleshTendrilEye : Actor {
	Default {
		Tag "Flesh Eye Tendril";

		Scale 1;
		Radius 24;
		Height 40;
		ProjectilePassHeight -16;

		+SOLID;
		+RANDOMIZE;
	}

	States {
		Spawn:
			NFTN I 8 {
				invoker.scale.x = FRandom(0.9, 1.1);
				invoker.scale.y = FRandom(0.9, 1.1);
			}
			NFTN JKL 8;
		Idle:
			NFTN MNOP 8;
			Loop;
	}
}

class HeartOfIxOrder : Actor {
	mixin SpawnParticleMixin;
	
	Default {
		Tag "Heart of Ix";
		
		Scale 1.5;
		Radius 24;
		Height 64;
		ProjectilePassHeight -16;
		Gravity 0;
		
		+SOLID;
		+RANDOMIZE;
	}
	
	action void OrderHeartUpdate() {
		A_SpawnProjectile("HeartOfIxOrderChainPlanter", 50, 0, Random(0, 359), CMF_AIMDIRECTION, Random(-70, 20));
	}
	
	States {
		Spawn:
			NFX1 A 4 OrderHeartUpdate();
			NFX1 BC 4 OrderHeartUpdate();
			Loop;
	}
}

class HeartOfIxOrderChainPlanter : Actor {
	Default {
		Speed 18;
		DamageFunction 0;
		Gravity 0;
		
		PROJECTILE;
		+RIPPER;
	}
	
	States {
		Spawn:
			TNT1 A 1 A_SpawnItemEx("HeartOfIxOrderChain");
			Loop;
	}
}

class HeartOfIxOrderChain : Actor {
	int lifeTime;
	
	Default {
		Gravity 0;
		Alpha 0.75;
		RenderStyle "Add";
		
		+CLIENTSIDEONLY;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.lifeTime = 3 * 35;
	}
	
	action state ChainUpdate() {
		if (--invoker.lifeTime <= 0)
		{
			return ResolveState("Fade");
		}
		return null;
	}
	
	States {
		Spawn:
			NFX1 D 1 { return ChainUpdate(); }
			Loop;
		Fade:
			NFX1 D 1 A_FadeOut(0.075);
			Loop;
		Death:
			NFX1 D 1;
			Stop;
	}
}

class HeartOfIxVoid : HeartOfIxOrder {
	Default {
		Tag "Void Heart of Ix";
	}
	
	action void A_VoidHeartUpdate() {
		invoker.SpawnParticle("Shadow", TICRATE, 4);
	}
	
	States {
		Spawn:
			NFX1 EFGH 4 A_VoidHeartUpdate();
			Loop;
	}
}