extend class WickedCavort {
	action void A_WrathCross() {
		if (!A_SpendAmmo(1)) {
			return;
		}

        A_StartSound("WickedCavort/Fire", CHAN_WEAPON);
		Array<Actor> projectiles;
		int radius = 1 + invoker.GetPassiveLevel() + (invoker.damageBoost > 0 ? 2: 0);
        A_FireProjectileCross("WrathCrossProjectile", radius, 30, projectiles);
		for (int i = 0; i < projectiles.Size(); i++) {
			WrathCrossProjectile waltzProjectile = WrathCrossProjectile(projectiles[i]);
			if (waltzProjectile) {
				waltzProjectile.Boost(invoker.damageBoost);
			}
		}
		invoker.damageBoost = Max(0, invoker.damageBoost - 100);
	}

    States {
		MainHand.Fire:
			VSH1 W 2 {
                A_ClearBobbing();
                A_Light2();
            }
			VSH1 X 2;
			VSH1 Y 2 A_WrathCross();
            VSH1 Z 2;
            VSH3 ABC 2;
			VSH3 C 0 A_MirrorHands();
			VSH3 C 0 A_ReFire("Fire");
			VSH3 C 0 A_Light0();
			Goto Ready;

        OffHand.Fire:
			VSH3 DEF 2;
			VSH3 MNO 2;
			Loop;
    }
}

class WrathCrossProjectile : SchismProjectile {
	int boostLevel;

	Default {
		Radius 5;
		Height 7;
		Speed 20;
		Gravity 0;
		DamageFunction GetDamage();
		Alpha 0.5;
		RenderStyle "Add";
		Scale 1;
		
		DamageType "Nether";
		
		SeeSound "WickedCavort/Fire";
		DeathSound "WickedCavort/Explode";
		Obituary "%o had their soul cooked by %k's Wrath Cross!";

		SchismProjectile.LifetimeMax TICRATE;
		SchismProjectile.ParticleName "Nether";
		SchismProjectile.ParticleInterval 4;
		
		-THRUGHOST;
	}

    virtual void Boost(int boostLevel) {
		self.boostLevel = boostLevel;
		if (boostLevel) {
			self.particleInterval = 1;
			self.particleColor = 0x4400EE;
			self.SetShade(0x4400EE);
			self.A_SetRenderStyle(1, STYLE_Shaded);
		}
    }

	virtual int GetDamage() {
		if (self.boostLevel == 0) {
			return Random(10, 20);
		}
		return 20 + (self.boostLevel * 4);
	}
	
	States {
		Spawn:
			VWC1 ABCDE 4;
			Loop;
			
		Death:
			VWC1 EFGH 4;
			Stop;
	}
}