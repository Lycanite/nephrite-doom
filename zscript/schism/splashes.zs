class SchismSplashFloor : Actor {
	Default {
		Gravity 0;
		Alpha 0.7;
		RenderStyle	"Translucent";
		
		+NOBLOCKMAP;
		+NOCLIP;
		+DONTSPLASH;
		+DONTBLAST;
		+NOTELEPORT;
	}
	
	States {
		Spawn:
			SPSH EFGHIJK 5;
			Stop;
	}
}

class SchismSplashFloorLava : SchismSplashFloor {
	Default {
		RenderStyle	"Add";
	}
	
	States {
		Spawn:
			LVAS ABCDEFGHI 5;
			Stop;
	}
}

class SchismSplashFloorOoze : SchismSplashFloor {
	Default {
		RenderStyle	"Add";
	}
	
	States {
		Spawn:
			OOZE EFGHI 5;
			Stop;
	}
}

class SchismSplashFloorSludge : SchismSplashFloor {
	States {
		Spawn:
			SPSH EFGH 8;
			Stop;
	}
}

class SchismSplashFloorAcid : SchismSplashFloor {
	States {
		Spawn:
			LVAS ABCD 8;
			Stop;
	}
}

class SchismSplashFloorBlood : SchismSplashFloor {
	States {
		Spawn:
			BSPH EFGH 8;
			Stop;
	}
}

class SchismSplashWall : Actor {
	Default {
		Radius 4;
		Height 4;
		Gravity 0.5;
		Alpha 0.7;
		RenderStyle	"Translucent";
		
		PROJECTILE;
		+NOBLOCKMAP;
		+DONTSPLASH;
		+DONTBLAST;
		+NOTELEPORT;
		+CANNOTPUSH;
		+DROPOFF;
		-NOGRAVITY;
	}
	
	States {
		Spawn:
			SPSH ABCD 8;
			Stop;
			
		Death:
			SPSH D 10;
			Stop;
	}
}

class SchismSplashWallOoze : SchismSplashWall {
	
	States {
		Spawn:
			OOZE ABCD 8;
			Stop;
			
		Death:
			OOZE D 10;
			Stop;
	}
}

class SchismSplashWallSludge : SchismSplashWall {
	
	States {
		Spawn:
			SLDG ABCD 8;
			Stop;
			
		Death:
			SLDG D 10;
			Stop;
	}
}

class SchismSplashWallBlood : SchismSplashWall {
	
	States {
		Spawn:
			BSPH ABCD 8;
			Stop;
			
		Death:
			BSPH D 10;
			Stop;
	}
}