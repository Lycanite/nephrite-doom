class AnimashardProjectile : SchismProjectile {
	Default {
		Radius 3;
		Height 5;
		Scale 0.3;
		Speed 30;
		FastSpeed 60;
		DamageFunction 6;
		Alpha 1;
		RenderStyle "Translucent";
		BounceType "Grenade";
		BounceCount 1;
		
		DamageType "Fae";
		SchismProjectile.KnockbackStrength 0.25;
		SchismProjectile.ParticleName "Anima";
		SchismProjectile.ParticleCount 1;
		SchismProjectile.ParticleInterval 8;
		SchismProjectile.ParticleScale 0.25;
		SchismProjectile.ParticleColor 0xAAFF55FF;
		
		SeeSound "Projectile/Animashard/Fire";
		DeathSound "Projectile/Animashard/Explode";
	}
	
	States {
		Spawn:
			NPAS ABCDEFGH 2;
			Loop;
			
		Death:
			NPAS ABC 2;
			Stop;
	}
}