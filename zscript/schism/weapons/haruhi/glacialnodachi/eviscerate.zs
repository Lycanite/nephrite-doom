class EviscerateFractal : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Glacial Nodachi can now cast Eviscerate!";
		
		Inventory.Icon "GNODY0";
		Inventory.PickupSound "GlacialNodachi/Pickup";
		Inventory.RestrictedTo "Haruhi";
		WeaponUpgrade.ExtraAmmoClass "GlacialCharge";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}
	
	States {
		Spawn:
			GNOD Y 1 bright;
			Loop;
	}
}

class GlacialNodachiEviscerateProjectile : GlacialNodachiProjectile {
	Default {
		Scale 1;
		DamageFunction 1;
		
		SeeSound "GlacialNodachi/Projectile/Eviscerate";
		DeathSound "GlacialNodachi/Projectile/Eviscerate/Explode";
		
		+RIPPER;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.generateSpecialAmmo = false;
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		if (specialDamage > 0)
		{
			for (int i = 0; i < 360; i += 60)
			{
				Actor projectileActor = A_SpawnProjectile("GlacialNodachiEviscerateClusterProjectile", 0, targetActor.Radius + 4, i, CMF_AIMDIRECTION, 0);
			}
		}
		return specialDamage;
	}
	
	States {
		Spawn:
			GNP4 ABCDEFGDEFGDEFG 2;
			
		Death:
			GNP4 H 2 {
				SchismAttacks.Explode(invoker, invoker.target, 4, 100, 0, 1, false, false, "Ice");
			}
			GNP4 IJKL 2 A_FadeOut(0.2);
			Stop;
	}
}

class GlacialNodachiEviscerateClusterProjectile : GlacialNodachiProjectile {
	override void BeginPlay() {
		super.BeginPlay();
		self.generateSpecialAmmo = false;
	}
}
