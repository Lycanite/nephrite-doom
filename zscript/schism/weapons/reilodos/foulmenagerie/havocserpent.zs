class HavocSerpentMorph : PowerupGiver {
	Default {
		Scale 1;
		Inventory.MaxAmount 0;
		Powerup.Type "PowerHavocSerpent";
		Powerup.Duration 525;
		
		Inventory.Icon "REISD0";
		Inventory.PickupSound "HavocSerpent/See";
		Inventory.RestrictedTo "Reilodos", "ReilodosHavocSerpent";
		
		+INVENTORY.RESTRICTABSOLUTELY;
		+INVENTORY.AUTOACTIVATE;
	}

	States {
		Spawn:
			REIB A 1 bright;
			Loop;
	}
}

class PowerHavocSerpent : PowerMorph {
	int transformHealth;
	
	Default {
		PowerMorph.PlayerClass "ReilodosHavocSerpent";
		PowerMorph.MorphStyle(
			MRF_FULLHEALTH|
			MRF_ADDSTAMINA|
			MRF_FAILNOTELEFRAG|
			MRF_WHENINVULNERABLE|
			MRF_UNDOBYDEATH|
			MRF_UNDOBYDEATHFORCED|
			MRF_UNDOBYDEATHSAVES|
			MRF_UNDOALWAYS|
			MRF_TRANSFERTRANSLATION
		);
	}
	
	override void InitEffect() {
		if (owner) {
			transformHealth = owner.Health;
		}
		super.InitEffect();		
	}
	
	override void EndEffect() {
		super.EndEffect();
		if (owner) {
			owner.Health = transformHealth;
		}
	}
}

class WeaponHavocSerpent : Weapon {
	mixin QueryPlayerInfoMixin;

	int primaryCost;
	int secondayCost;
	int argusCost;
	int bedlamFiendCost;
	int collisionCost;
	int havocSerpentCost;
	
	Default {
		Radius 20;
		Height 16;
		Scale 0.5;
		
		Weapon.AmmoType "ChaosEnergy";
		Weapon.AmmoUse 0;
		Weapon.AmmoGive 0;
		Weapon.AmmoType2 "HavocEnergy";
		Weapon.AmmoUse2 0;
		Weapon.AmmoGive2 0;
		Weapon.SlotPriority 0;
		
		Weapon.UpSound "HavocSerpent/See";
		Weapon.ReadySound "";
		Obituary "%o was devoured by %k's Havoc Serpent!";
		
		+WEAPON.AMMO_OPTIONAL;
		+WEAPON.ALT_AMMO_OPTIONAL;
		+WEAPON.MELEEWEAPON;
		-WEAPON.DONTBOB;
		+WEAPON.CHEATNOTWEAPON;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		
		primaryCost = 1;
		secondayCost = 1;
		argusCost = 50;
		bedlamFiendCost = 50;
		collisionCost = 2;
		havocSerpentCost = 50;
	}
	
	action state User2ReFire() {
		if (GetPlayerInput(MODINPUT_BUTTONS) & BT_USER2) {
			return ResolveState("User2Hold");
		}
		return null;
	}
	
	
	// Primary Havoc Serpent Attack:
	action void HavocSerpentAttack(void) {
		int minionCount = 0;
		if (invoker.GetSchismPlayerInfo()) {
			minionCount = invoker.GetSchismPlayerInfo().stats[PlayerNumber()].minions.Size();
		}
		A_CustomPunch(20, true, CPF_PULLIN, "BulletPuff", 96);
		if (CountInv("ChaosEnergy") < 1) {
			return;
		}
		A_TakeInventory("ChaosEnergy", 1);
		Actor firedProjectile = A_FireProjectile("FoulMenageriePortalBoltUnstable", random(-40, 40), false, random(-5, 5), random(-10, 10));
		FoulMenageriePortalBolt foulMenagerieProjectile = FoulMenageriePortalBolt(firedProjectile);
		if (foulMenagerieProjectile && minionCount >= 50) {
			foulMenagerieProjectile.summonType = null;
		}
	}
	
	
	// Secondary Foul Burst:
	action void FoulBurst(void) {
		if (CountInv("ChaosEnergy") < invoker.secondayCost) {
			A_StartSound("FoulMenagerie/Fire/Fail", CHAN_WEAPON);
			A_Print("\caYou need more Chaos to cast Foul Burst.", 2);
			return;
		}
		
		bool anyMinions = false;
		if (invoker.GetSchismPlayerInfo()) {
			int minionCount = invoker.GetSchismPlayerInfo().stats[PlayerNumber()].minions.Size();
			for (int i = 0; i < minionCount; i++)
			{
				ChaosMinion minion = ChaosMinion(invoker.GetSchismPlayerInfo().stats[PlayerNumber()].minions[i]);
				if (minion)
				{
					minion.EmitFollowProjectile("FoulBurstProjectile", minionCount);
					anyMinions = true;
				}
			}
		}
		
		if (anyMinions) {
			A_TakeInventory("ChaosEnergy", invoker.secondayCost);
		}
	}
	
	
	// Special 1 Summon Argus:
	action void A_SummonArgus(void) {
		if (CountInv("ArgusScroll") < 1) {
			A_StartSound("FoulMenagerie/Fire/Fail", CHAN_WEAPON);
			A_Print("\caYou need an Argus Scroll to use this spell.");
			return;
		}
		if (CountInv("ChaosEnergy") < invoker.primaryCost) {
			A_StartSound("FoulMenagerie/Fire/Fail", CHAN_WEAPON);
			A_Print("\caYou need more Chaos to summon an Argus.", 2);
			return;
		}
		if (CountInv("HavocEnergy") < invoker.argusCost) {
			A_StartSound("FoulMenagerie/Fire/Fail", CHAN_WEAPON);
			A_Print("\caYou need more Havoc to summon an Argus.", 2);
			return;
		}
		
		A_TakeInventory("ChaosEnergy", invoker.primaryCost);
		A_TakeInventory("HavocEnergy", invoker.argusCost);
		A_FireProjectile("FoulMenagerieArgusBolt");
	}
	
	
	// Special 2 Entropic Bolt:
	action void A_EntropicBolt(void) {
		if (CountInv("EntropicBoltScroll") < 1) {
			A_StartSound("FoulMenagerie/Fire/Fail", CHAN_WEAPON);
			A_Print("\caYou need a Entropic Bolt Scroll to use this spell.");
			return;
		}
		if (CountInv("ChaosEnergy") < invoker.primaryCost) {
			A_StartSound("FoulMenagerie/Fire/Fail", CHAN_WEAPON);
			A_Print("\caYou need more Chaos to cast Entropic Bolt.", 2);
			return;
		}
		if (CountInv("HavocEnergy") < invoker.collisionCost) {
			A_StartSound("FoulMenagerie/Fire/Fail", CHAN_WEAPON);
			A_Print("\caYou need more Havoc to cast Entropic Bolt.", 2);
			return;
		}
		
		FoulMenagerieEntropicBolt entropicBoltProjectile = FoulMenagerieEntropicBolt(A_FireProjectile("FoulMenagerieEntropicBolt", 0, false, 0, 0));
		if (entropicBoltProjectile) {
			entropicBoltProjectile.primaryCost = invoker.primaryCost;
			entropicBoltProjectile.secondaryCost = invoker.collisionCost;
		}
	}
	
	
	// Special 3 Bedlam Fiend:
	action state A_BedlamFiend() {
		if (CountInv("BedlamFiendScroll") < 1) {
			A_StartSound("FoulMenagerie/Fire/Fail", CHAN_WEAPON);
			A_Print("\caYou need a Bedlam Fiend Scroll to use this spell.");
			return null;
		}
		if (CountInv("ChaosEnergy") < invoker.primaryCost) {
			A_StartSound("FoulMenagerie/Fire/Fail", CHAN_WEAPON);
			A_Print("\caYou need more Chaos to summon a Nomadrum.", 2);
			return null;
		}
		if (CountInv("HavocEnergy") < invoker.bedlamFiendCost) {
			A_StartSound("FoulMenagerie/Fire/Fail", CHAN_WEAPON);
			A_Print("\caYou need more Havoc to summon a Nomadrum.", 2);
			return null;
		}
		
		A_TakeInventory("ChaosEnergy", invoker.primaryCost);
		A_TakeInventory("HavocEnergy", invoker.bedlamFiendCost);
		A_FireProjectile("FoulMenagerieNomadrumBolt");
		return null;
	}
	
	
	// Special 4 Dismount:
	action void A_HavocSerpentDismount(void) {
		A_TakeInventory("PowerHavocSerpent", 1);
	}
	
	
	States {
		Select:
			REHS A 1 A_Raise(12);
			Loop;
			
		Deselect:
			REHS A 1 A_Lower(12);
			Loop;
			
		Ready:
			REHS ABCD 6 A_WeaponReady(WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3);
		Mounted:
			REHS ABCD 6 A_WeaponReady(WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4);
			Loop;
			
		Fire:
			REHS EF 4 HavocSerpentAttack();
			REHS G 4 A_ReFire;
			Goto Mounted;
			
		AltFire:
			REHS HI 4 A_Light2();
		AltHold:
			REHS JK 4 FoulBurst();
			REHS I 4 A_ReFire("AltHold");
			REHS H 4 A_Light0();
			Goto Mounted;
		
		User1:
			REHS E 0 A_JumpIfInventory("HavocEnergy", invoker.argusCost, 2);
			REHS E 0 A_JumpIfInventory("ArgusScroll", 1, 1);
			Goto Mounted;
			REHS E 4;
			REHS F 4 A_SummonArgus();
			REHS G 4;
			Goto Mounted;
		
		User2:
			REHS HI 4;
			REHS J 4  {
				return A_EntropicBolt();
			}
			REHS K 4;
			REHS I 4 A_Light0();
			Goto Mounted;
			
		User3:
			REHS HI 4;
			REHS J 4  {
				return A_BedlamFiend();
			}
			REHS K 4;
			REHS I 4 A_Light0();
			Goto Mounted;
		
		User4:
			REHS A 0 A_HavocSerpentDismount();
			Goto Mounted;
	}
}
