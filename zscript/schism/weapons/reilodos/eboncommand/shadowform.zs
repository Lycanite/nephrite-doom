class ShadowformScroll : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Ebon Command can now put you into Shadowform!";
		
		Inventory.Icon "REISF0";
		Inventory.PickupSound "EbonCommand/Up";
		Inventory.RestrictedTo "Reilodos", "ReilodosHavocSerpent";
		WeaponUpgrade.ExtraAmmoClass "ShadowEnergy";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			REIS F 1 bright;
			Loop;
	}
}

class EbonCommandShadowFormGiver : PowerupGiver {
	Default {
		Scale 1;
		Inventory.MaxAmount 0;
		Powerup.Duration -6000;
		Powerup.Type "PowerEbonCommandShadowForm";
		
		Inventory.Icon "REECA0";
		Inventory.PickupSound "";
		Inventory.RestrictedTo "Reilodos", "ReilodosHavocSerpent";
		
		+INVENTORY.RESTRICTABSOLUTELY;
		+INVENTORY.AUTOACTIVATE;
	}
}

class PowerEbonCommandShadowForm : PowerInvisibility {
	Default {
		Powerup.Mode "Fuzzy";
		
		+SHADOW;
		+GHOST;
		+CANTSEEK;
	}
	
	override void DoEffect() {
		super.DoEffect();
		
		owner.A_ChangeVelocity(0, 0, 0, CVF_RELATIVE|CVF_REPLACE);
	}
}

class EbonCommandShadowFormInvulnerabilityGiver : EbonCommandShadowFormGiver {
	Default {
		Powerup.Type "PowerEbonCommandShadowFormInvulnerability";
	}
}

class PowerEbonCommandShadowFormInvulnerability : PowerInvulnerable {
	
}
