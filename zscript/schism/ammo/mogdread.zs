class FirestoneCharge : Ammo {
	Default {
		//$Category "Ammunition"
		
		Tag "Firestone Charge";
		Scale 0.35;
		Inventory.PickupMessage "A Firestone Charge!";
		Inventory.Amount 20;
		Inventory.MaxAmount 200;
		Ammo.BackpackAmount 300;
		Ammo.BackpackMaxAmount 300;
		
		Inventory.Icon "AMOGA0";
		Inventory.PickupSound "Mogdread/Ammo";
		Inventory.RestrictedTo "Mogdread";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			AMOG ABC 4 bright;
			Loop;
	}
}


class FirestoneChargeLarge : FirestoneCharge {
	Default {
		//$Category "Ammunition"
		
		Tag "Large Firestone Charge";
		Scale 0.4;
		Inventory.PickupMessage "A Greater Firestone Charge!";
		Inventory.Amount 100;
	}

	States {
		Spawn:
			AMOG DEF 4 bright;
			Loop;
	}
}


class MoglavaCharge : Ammo {
	Default {
		//$Category "Ammunition"
		
		Tag "Moglava Charge";
		Scale 0.5;
		Inventory.PickupMessage "A Moglava Charge! A special blend of blood, tears and of course, lava!";
		Inventory.Amount 20;
		Inventory.MaxAmount 200;
		Ammo.BackpackAmount 300;
		Ammo.BackpackMaxAmount 300;
		
		Inventory.Icon "AMOGG0";
		Inventory.PickupSound "Mogdread/Ammo";
		Inventory.RestrictedTo "Mogdread";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			AMOG GHI 4 bright;
			Loop;
	}
}


class MoglavaChargeLarge : MoglavaCharge {
	Default {
		//$Category "Ammunition"
		
		Tag "Large Moglava Charge";
		Scale 0.5;
		Inventory.PickupMessage "An extra large Moglava Charge, with a free side of destruction!";
		Inventory.Amount 100;
	}

	States {
		Spawn:
			AMOG JKL 4 bright;
			Loop;
	}
}


class LightCharge : Ammo {
	Default {
		//$Category "Ammunition"
		
		Tag "Light Charge";
		Scale 0.5;
		Inventory.PickupMessage "A Light Charge!";
		Inventory.Amount 20;
		Inventory.MaxAmount 200;
		Ammo.BackpackAmount 300;
		Ammo.BackpackMaxAmount 300;
		
		Inventory.Icon "AMOGM0";
		Inventory.PickupSound "Mogdread/Ammo";
		Inventory.RestrictedTo "Mogdread";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			AMOG MNON 4 bright;
			Loop;
	}
}


class LightChargeLarge : LightCharge {
	Default {
		//$Category "Ammunition"
		
		Tag "Large Light Charge";
		Scale 0.5;
		Inventory.PickupMessage "A Greater Light Charge!";
		Inventory.Amount 100;
	}

	States {
		Spawn:
			AMOG PQRQ 4 bright;
			Loop;
	}
}


class InfernalCharge : Ammo {
	Default {
		//$Category "Ammunition"
		
		Tag "Infernal Charge";
		Scale 0.5;
		Inventory.PickupMessage "An Infernal Charge!";
		Inventory.Amount 20;
		Inventory.MaxAmount 200;
		Ammo.BackpackAmount 300;
		Ammo.BackpackMaxAmount 300;
		
		Inventory.Icon "AMOGS0";
		Inventory.PickupSound "Mogdread/Ammo";
		Inventory.RestrictedTo "Mogdread";
		
		+FLOATBOB;
		+INVENTORY.IGNORESKILL;
	}

	States {
		Spawn:
			AMOG STU 4 bright;
			Loop;
	}
}