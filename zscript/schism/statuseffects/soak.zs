class SchismSoakDebuff : SchismStatusEffect {	
	double speedOriginal;
	double frictionOriginal;
	
	Default {
		PowerUp.Duration -10;
		
		SchismStatusEffect.Detrimental true;
		SchismStatusEffect.ParticleName "Water";
	}
	
	override string GetFollowEffectClass() {
		return "SchismSoakFollowEffect";
	}
	
	override void InitEffect() {
		super.InitEffect();
		if (!self.owner) {
			self.EffectInvalid = true;
			return;
		}
		
		self.speedOriginal = self.owner.speed;
		self.owner.speed = self.owner.speed * 0.75;

		self.frictionOriginal = self.owner.friction;
		self.owner.friction = self.owner.friction * 1.5;
	}
	
	override void EndEffect() {
		super.EndEffect();
		if (self.EffectInvalid || !self.owner) {
			return;
		}
		
		self.owner.speed = self.speedOriginal;
		self.owner.friction = self.frictionOriginal;
	}
	
	override void OnOwnerDamaged(int damage, string damageType, actor inflictor) {
		super.OnOwnerDamaged(damage, damageType, inflictor);
		if (damageType == "Electric" || damageType == "Lightning") {
			self.SoakSpark();
		}
		else if (damageType == "Ice" || damageType == "Frost" || damageType == "Air") {
			self.FastFreeze();
		}
		else if (damageType == "Chaos" && inflictor && self.owner.PoisonDurationReceived <= 0) {
			self.owner.PoisonMobj(self.effectInflictor, self.effectInflictor, 10, 350, 10, "poison");
		}
	}
	
	bool SoakSpark() {
		if (!self.owner.bBoss) {
			self.owner.A_ChangeVelocity(0, 0, 0, CVF_RELATIVE|CVF_REPLACE);
		}
		SchismSoakFollowEffect soakProjectile = SchismSoakFollowEffect(self.followEffect);
		if (soakProjectile && owner && owner.Health > 0) {
			return soakProjectile.SoakSpark(effectInflictor);
		}
		return false;
	}
	
	bool FastFreeze() {
		// Remove Soak and Inflict Chill:
		Actor soakedTarget = self.owner;
		if (!soakedTarget) {
			return false;
		}
		self.RemoveEffect();
		SchismStatusEffects.InflictEffect("SchismChillDebuff", self.effectInflictor, self.effectInflictor, soakedTarget, 15 * 35);
		return true;
	}
}

class SchismSoakFollowEffect : SchismFollowEffect {
	Default {
		SeeSound "Debuff/Soak";
		DeathSound "Debuff/Soak";
		Obituary "%o was drowned by %k's water magic!";
	}
	
	bool SoakSpark(Actor triggerActor) {
		A_StartSound("Debuff/Soak/Spark");
		A_QuakeEx(1, 1, 1, 10, 0, 500, "world/quake", 0, 1, 1, 0, 0, 1, 1);
		A_SpawnItemEx("SoakSparkEffect", 0, 0, 0, 0, random(-4, 4), random(-4, 4), 0, SXF_CLIENTSIDE);
		SchismAttacks.Explode(self, triggerActor, 2, 100);
		return true;
	}
}

class SoakSparkEffect : Actor  {
	Default  {
		RenderStyle "Add";
		Alpha 1;
		Scale 2.0;
		
		+MISSILE;
		+FORCEXYBILLBOARD;
		+CLIENTSIDEONLY;
		+NOCLIP;
		+NOBLOCKMAP;
		+NOINTERACTION;
	}
	
	States {
		Spawn:
			DBWL H 0 {
				ThrustThingZ(0, Random(5, 10), 0, 0);
				ThrustThing(Random(0, 255), Random(0, 10), 0, 0);
			}
		Active:
			DBWL HIJKL 1 bright A_FadeOut(0.05);
			Loop;
	}
}
