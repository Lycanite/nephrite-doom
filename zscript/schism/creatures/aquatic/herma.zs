class Herma : Creature {
	Default {
		Tag "Herma";
		
		Health 100;
		Radius 14;
		Height 40;
		Speed 14;
		Mass 500;
		FloatSpeed 14;
		Scale 1;
		Gravity 1;
		PainChance 60;
		Damage 1;
		Alpha 1;
		BloodColor "ff 66 00";
		
		SeeSound "Herma/See";
		PainSound "Herma/Pain";
		DeathSound "Herma/Death";
		ActiveSound "Herma/Idle";
		MeleeSound "Herma/Melee";
		Obituary "%o was snipped by a Herma!";
		HitObituary "%o was snipped by a Herma!";
	}
	
	States {
	Spawn:
		HERM ABCB 12 A_Look();
		Loop;

	See:
		HERM DEFE 6 A_Chase();
		Loop;

	Melee:
		HERM G 4 A_FaceTarget();
		HERM H 4 A_FaceTarget();
		HERM I 6 A_CreatureMelee(4, 8);
		Goto See;

	Pain:
		HERM J 3;
		HERM J 3 A_Pain();
		Goto See;

	Death:
	XDeath:
		HERM JK 6;
		HERM L 5 A_Scream();
		HERM M 5 A_CreatureDie();
		HERM NO 5;
		HERM P -1;
		Stop;
	}
}