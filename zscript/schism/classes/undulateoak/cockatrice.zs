class UndulateOakCockatrice : PlayerPawn {
	Default {
		Speed 1;
		Health 500;
		Radius 16;
		Height 64;
		Mass 220;
		
		Scale 0.7;
		PainChance 50;
		RadiusDamageFactor 0.125;
		
		Player.DisplayName "Undulate Oak";
		Player.SoundClass "UndulateOakCockatrice";
		//Player.CrouchSprite "UOAC";
		//Player.ScoreIcon "OAKFACE";
		Player.ColorRange 0, 0;
		Player.DamageScreenColor "ff 00 00";
		Player.ViewHeight 58.0;
		Player.ViewBob 1.5;
		
		Player.ForwardMove 1.25, 1.25;
		Player.SideMove 0.875, 0.925;
		Player.JumpZ 10.0;
		Player.HexenArmor 25, 25, 30, 40, 50;
		Player.InvulnerabilityMode "Ghost";
		Player.HealRadiusType "Health";
		Player.MaxHealth 250;
		
		Player.FlechetteType "ArtiPoisonBag1";
		
		Player.MorphWeapon "WeaponCockatrice";
		
		-PLAYERPAWN.CANSUPERMORPH;
		-PLAYERPAWN.CROUCHABLEMORPH;
		+NOSKIN;
	}
	
	States
    {
		Spawn:
			UOKM A -1;
			Loop;
		See:
			UOKM CDEF 4;
			Loop;
		Missile:
			UOKM G 8;
			Goto Spawn;
		Melee:
			UOKM H 8;
			Goto Spawn;
		Pain:
			UOKM I 4;
			UOKM I 4 A_Pain;
			Goto Spawn;
		Death:
		XDeath:
		Burn:
		Ice:
		Disintegrate:
			UOKM J 6;
			UOKM K 6 A_Scream;
			UOKM LM 6;
			UOKM M 6 A_NoBlocking;
			UOKM O -1;
			Stop;
    }
}