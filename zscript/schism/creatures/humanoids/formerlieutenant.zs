class FormerLieutenant : FormerSergent {
	Default {
		Tag "Former Lieutenant";

		AttackSound "FormerLieutenant/Fire";
		Obituary "%o was gunned down by a Former Lieutenant!";
    }

    States {
        Spawn:
			FLIU AB 10 A_Look();
			Loop;
			
		See:
			FLIU AABBCCDD 4 A_Chase();
			Loop;
			
		Missile:
			FLIU E 10 A_FaceTarget();
			FLIU FE 2 A_CreatureHitscan(1, 5, 1, 43);
			FLIU FE 2 A_CreatureHitscan(1, 5, 1, 43);
			FLIU FE 2 A_CreatureHitscan(1, 5, 1, 43);
            FLIU E 8;
			Goto See;
			
		Pain:
			FLIU G 3;
			FLIU G 3 A_Pain();
			Goto See;
			
		Death:
			FLIU H 5;
			FLIU I 5 A_Scream();
			FLIU J 5 A_CreatureDie();
			FLIU K 5;
			FLIU L -1;
			Stop;
			
		XDeath:
			FLIU Q 5;
			FLIU R 5 A_Scream();
			FLIU S 5 A_CreatureDie();
			FLIU ST 5;
			FLIU U -1;
			Stop;
			
		Raise:
			FLIU KJIH 5;
			Goto See;
    }
}