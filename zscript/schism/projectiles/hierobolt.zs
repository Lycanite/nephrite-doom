class HieroboltProjectile : SchismProjectile {
	Default {
		Radius 5;
		Height 7;
		Speed 20;
		Gravity 0;
		DamageFunction random(20, 40);
		Alpha 1.0;
		RenderStyle "Add";
		Scale 2;
		
		SeeSound "Projectile/Hierobolt/Fire";
		DeathSound "Projectile/Hierobolt/Explode";
		Obituary "%o was put to justice by %k!";

		SchismProjectile.ParticleName "Order";
	}
	
	States {
		Spawn:
			HIEP AB 4;
			Loop;
			
		Death:
			HIEP CDEFG 4;
			Stop;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
	}
}