class PrimordialStaff : SchismWeapon {
	Default {
		Radius 20;
		Height 16;
		
		Inventory.RestrictedTo "UndulateOak", "UndulateOakCockatrice";
		
		Inventory.PickupSound "PrimordialStaff/Pickup";
		Weapon.UpSound "PrimordialStaff/Up";
		Weapon.ReadySound "";
		SchismWeapon.FailSound "PrimordialStaff/Fire/Fail";
		
		Weapon.AmmoType1 "LifeSeed";
		Weapon.AmmoType2 "Ectoplasm";
		
		SchismWeapon.PrimaryCost 1;
		SchismWeapon.SecondaryCost 2;
		SchismWeapon.SpecialACost 25;
		SchismWeapon.SpecialBCost 50;
		SchismWeapon.SpecialCCost 50;
		SchismWeapon.SpecialDCost 50;
		
		SchismWeapon.SpecialItemA "BombEgg";
		SchismWeapon.SpecialItemB "TornadoEgg";
		SchismWeapon.SpecialItemC "PolymorphEgg";
		SchismWeapon.SpecialItemD "ThornsEgg";
		SchismWeapon.PassiveItem "QuackleEgg";
		
		Inventory.PickupMessage "You found the ancient Primordial Staff!";
		Obituary "%o was put down for future excavation by %k's Primordial Staff!";
		SchismWeapon.InsufficientAmmoAMessage "\caNot enough Life Seeds to use this ability.";
		SchismWeapon.InsufficientAmmoBMessage "\caNot enough Ectoplasm to use this ability.";
		SchismWeapon.NoSpecialAMessage "\caYou need a Bomb Egg to use this ability.";
		SchismWeapon.NoSpecialBMessage "\caYou need a Tornado Egg to use this ability.";
		SchismWeapon.NoSpecialCMessage "\caYou need a Polymorph Egg to use this ability.";
		SchismWeapon.NoSpecialDMessage "\caYou need an Arbour Egg to use this ability.";

		SchismWeapon.IdleBob 30;
		
		+FLOATBOB;
		+NOGRAVITY;
		+INVENTORY.RESTRICTABSOLUTELY;
		+WEAPON.AMMO_OPTIONAL;
		+WEAPON.ALT_AMMO_OPTIONAL;
	}
	
	// Primary Launch Egg:
	action void A_LaunchEgg() {
		A_StartSound("PrimordialStaff/Fire");
		A_FireProjectile("PrimordialStaffProjectileWeak", 0, false);
	}
	
	// Secondary Egg Burst:
	action void A_EggBurst() {
		if (!A_SpendAmmo(2)) {
			A_Light0();
			return;
		}
		A_StartSound("PrimordialStaff/Fire/Burst");
		for (int i = 0; i <= 5; i++) {
			A_FireProjectile("PrimordialStaffProjectile", random(-15, 15), false, random(-5, 5), random(-10, 10));
		}
	}
	
	// Special 3 Polymorph:
	action void A_FirePolymorph() {
		A_SpendAmmo(5);
		A_FireProjectile("PrimordialStaffPolymorphProjectile", 0, false);
		A_FireProjectile("PrimordialStaffPolymorphProjectile", 10, false, -10);
		A_FireProjectile("PrimordialStaffPolymorphProjectile", 20, false, -20);
		A_FireProjectile("PrimordialStaffPolymorphProjectile", -10, false, 10);
		A_FireProjectile("PrimordialStaffPolymorphProjectile", -20, false, 20);
	}
	
	// Ultimate Transform Cockatrice:
	action void A_TransformCockatrice() {
		A_GiveInventory("CockatriceMorph", 1);
	}
	
	States {
		Ready:
			WCCK ABCDCB 4 {
				return A_SchismWeaponReady();
			}
			Loop;
			
		Deselect:
			WCCK A 1 A_Lower(12);
			Loop;
			
		Select:
			WCCK A 1 A_Raise(12);
			Loop;
			
		Fire:
			WCCK E 4 {
				A_ClearBobbing();
				A_Light2();
			}
			WCCK F 4 A_LaunchEgg();
			WCCK F 0 A_Light0();
			Goto Ready;
			
		AltFire:
			WCCK E 8 A_Light2();
			WCCK F 4 {
				return A_EggBurst();
			}
			WCCK F 4 A_Light1();
			WCCK E 4 A_Light0();
			Goto Ready;
			
		User1:
			WCCK E 4 {
				return A_CheckSpecialJump(1, "Ready");
			}
			WCCK F 4 {
				A_ClearBobbing();
				A_Light2();
				A_StartSound("PrimordialStaff/Fire/Bomb");
			}
			WCCK G 4 {
				A_SpendAmmo(3);
				A_FireProjectile("PrimordialStaffBomb");
			}
			WCCK H 4 A_Light0();
			WCCK DCB 4;
			Goto Ready;
			
		User2:
			WCCK E 4 {
				return A_CheckSpecialJump(2, "Ready");
			}
			WCCK F 4 {
				A_ClearBobbing();
				A_Light2();
				A_StartSound("PrimordialStaff/Fire/Tornado");
			}
			WCCK I 4 {
				A_SpendAmmo(4);
				A_FireProjectile("PrimordialStaffEggnadoProjectile");
			}
			WCCK J 4 A_Light0();
			WCCK DCB 4;
			Goto Ready;
			
		User3:
			WCCK E 4 {
				return A_CheckSpecialJump(3, "Ready");
			}
			WCCK F 4 {
				A_ClearBobbing();
				A_Light2();
				A_StartSound("PrimordialStaff/Fire/Polymorph");
			}
			WCCK K 4 {
				A_FirePolymorph();
			}
			WCCK L 4 A_Light0();
			WCCK DCB 4;
			Goto Ready;
		
		User4:
			WCCK E 4 {
				return A_CheckSpecialJump(4, "Ready");
			}
			WCCK F 4 {
				A_ClearBobbing();
				A_Light2();
				A_StartSound("PrimordialStaff/Fire/Polymorph");
			}
			WCCK M 4 {
				A_SpendAmmo(6);
				A_FireProjectile("PrimordialStaffEggnadoProjectile"); // TODO: Fire thorns.
			}
			WCCK N 4 A_Light0();
			WCCK DCB 4;
			Goto Ready;
		
		Ultimate:
			WCCK MNDC 4;
			WCCK B 4 A_TransformCockatrice();
			Goto Ready;
			
		Spawn:
			CHKN B -1;
			Stop;
	}
}

class PrimordialStaffProjectile : EctoplasmProjectile {
	Default {
		Radius 16;
		Height 10;
		Scale 0.25;
		Speed 15;
		DamageFunction 20;
		Gravity 0.25;
		Alpha 1;
		Renderstyle "Normal";
		BounceType "Grenade";
		BounceCount 2;
		
		SeeSound "PrimordialStaff/Egg";
		DeathSound "PrimordialStaff/Egg/Explode";
		
		-NOGRAVITY;
		+CANBOUNCEWATER;
		+CANNOTPUSH;
		+NODAMAGETHRUST;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		ectoplasmMin = 1;
		ectoplasmMax = 3;
	}
	
	States {
		Spawn:
			WCEG A 6;
			Loop;
			
		Death:
			WCEG B 4 bright {
				Gravity = 0;
				SchismAttacks.Explode(invoker, invoker.target, 2, 40);
			}
			WCEG CDE 4 bright;
			Stop;
	}
}

class PrimordialStaffProjectileWeak : PrimordialStaffProjectile {
	override void BeginPlay() {
		super.BeginPlay();
		ectoplasmMin = 0;
		ectoplasmMax = 0;
	}
}

class PrimordialStaffProjectileStrong : PrimordialStaffProjectile {
	Default {
		Radius 16;
		Height 10;
		Scale 0.5;
		Speed 20;
		DamageFunction 40;
		Gravity 0.125;
		Alpha 1;
		Renderstyle "Normal";
		BounceType "Grenade";
		BounceCount 5;
		
		-CANNOTPUSH;
		-NODAMAGETHRUST;
	}
}

#include "zscript/schism/weapons/undulateoak/primordialstaff/bombegg.zs"
#include "zscript/schism/weapons/undulateoak/primordialstaff/tornadoegg.zs"
#include "zscript/schism/weapons/undulateoak/primordialstaff/polymorphegg.zs"
#include "zscript/schism/weapons/undulateoak/primordialstaff/thornsegg.zs"
#include "zscript/schism/weapons/undulateoak/primordialstaff/quackleegg.zs"
#include "zscript/schism/weapons/undulateoak/primordialstaff/transformcockatrice.zs"
