class Buer : Creature {
	Default {
		Tag "Buer";
		Health 100;
		Radius 16;
		Height 56;
		Mass 50;
		Speed 8;
		PainChance 256;
		Gravity 0;
		
		Obituary "%o got spitted by a Buer.";
		HitObituary "%o was consumed by a Buer.";
		
		SeeSound "Buer/See";
		ActiveSound "Buer/Idle";
		PainSound "Buer/Pain";
		DeathSound "Buer/Death";
		MeleeSound "Buer/Melee";
		
		+FLOAT;
		+NOGRAVITY;
		+DONTFALL;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		self.SpawnParticle("BloodDrip", 8, 1, (FRandom(-8, 8), FRandom(-8, 8), FRandom(10, 40)), 0, 0, 1);
	}
	
	States {
		Spawn:
			BUER A 4 A_Look();
			Loop;
			
		See:
			BUER A 4 A_Chase();
			Loop;
			
		Missile:
			BUER A 2 A_FaceTarget();
			BUER B 2 A_SpawnProjectile("IchorspitProjectile", 12, 0, 0, 0, 0);
			BUER BA 2;
			Goto See;
			
		Pain:
			BUER A 2 A_Pain;
			Goto See;
			
		Death:
			BUER C 6 A_CreatureDie();
			BUER D 5 bright {
				A_Scream();
				A_SetTranslucent(0.8, 0);
			}
			BUER EFGH 5 Bright;
			Stop;
	}
}