class GreenCardN : DoomKey {
	Default {
		Inventory.PickupMessage "You got the green keycard.";
		Inventory.Icon "STKEYS6";
	}
	
	States {
		Spawn:
			GKEY A 10;
			GKEY B 10 Bright;
			Loop;
	}
}

class MagentaCardN : DoomKey {
	Default {
		Inventory.PickupMessage "You got the magenta keycard.";
		Inventory.Icon "STKEYS7";
	}
	
	States {
		Spawn:
			MKEY A 10;
			MKEY B 10 Bright;
			Loop;
	}
}

class CyanCardN : DoomKey {
	Default {
		Inventory.PickupMessage "You got the cyan keycard.";
		Inventory.Icon "STKEYS8";
	}
	
	States {
		Spawn:
			CKEY A 10;
			CKEY B 10 Bright;
			Loop;
	}
}

class GreenSkullN : DoomKey {
	Default {
		Inventory.PickupMessage "You got the green skull.";
		Inventory.Icon "STKEYS9";
	}
	
	States {
		Spawn:
			GSKU A 10;
			GSKU B 10 Bright;
			Loop;
	}
}

class MagentaSkullN : DoomKey {
	Default {
		Inventory.PickupMessage "You got the magenta skull.";
		Inventory.Icon "STKEYSA";
	}
	
	States {
		Spawn:
			MSKU A 10;
			MSKU B 10 Bright;
			Loop;
	}
}

class CyanSkullN : DoomKey {
	Default {
		Inventory.PickupMessage "You got the cyan skull.";
		Inventory.Icon "STKEYSB";
	}
	
	States {
		Spawn:
			CSKU A 10;
			CSKU B 10 Bright;
			Loop;
	}
}