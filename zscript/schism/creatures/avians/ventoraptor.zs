class Ventoraptor : MinionCreature {
	Default {
		Tag "Ventoraptor";

		Health 200;
		Radius 26;
		Height 72;
		Speed 30;
		Scale 0.5;
		PainChance 250;
		Damage 2;
		Gravity 0.5;
		BloodColor "ff 00 00";

		MinionCreature.SummonTime 5 * 35;

		+JUMPDOWN;
		
		SeeSound "Ventoraptor/See";
		PainSound "Ventoraptor/Pain";
		DeathSound "Ventoraptor/Death";
		ActiveSound "Ventoraptor/See";
		MeleeSound "Ventoraptor/Melee";
		Obituary "%o was ripped apart by a Ventoraptor!";
		HitObituary "%o was ripped apart by a Ventoraptor!";
	}
	
	States {
		Spawn:
			VRAP AABBCCBB 4 {
				A_Look();
				return A_StayWithMaster();
			}
			Loop;
			
		Follow:
			VRAP DEFE 4 {
				A_Look();
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			VRAP DEF 4 {
				return A_MinionChase();
			}
			VRAP E 4 {
				if (FRandom(0, 1) >= 0.75) {
					A_Leap(Random(10, 20), Random(30, 50));
				}
				return A_MinionChase();
			}
			Loop;
			
		Melee:
			VRAP G 4 A_FaceTarget();
			VRAP H 4 A_CreatureMelee(4);
			VRAP I 4;
			Goto See;
			
		Pain:
			VRAP J 6;
			VRAP J 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			VRAP KL 5;
			VRAP M 5 A_Scream();
			VRAP N 5;
			VRAP P 5 A_CreatureDie();
			VRAP P -1;
			Stop;
	}
}
