class FleshbenderScroll : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "You can now weave flesh into tendrils to help feed your Blood Globe!";
		
		Inventory.Icon "VESWG0";
		Inventory.PickupSound "BloodGlobe/Pickup";
		Inventory.RestrictedTo "Vess";
		WeaponUpgrade.ExtraAmmoClass "BloodDrupe";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			VESW G 1 bright;
			Loop;
	}
}

extend class BloodGlobe {
	action void A_Fleshbender() {
		int level = invoker.AnimaLevel(invoker.specialBCost);
		if (!A_CheckSpecial(4) || !A_SpendAmmo(6) || !invoker.sphere) {
			return;
		}
		A_StartSound("BloodGlobe/Fire/Fleshbender", CHAN_WEAPON);
		invoker.sphereTarget.locked = true;
		invoker.sphere.Fleshbender(level);
	}

	States {
		MainHand.User4:
			VSH1 K 0 A_ClearBobbing();
			VSH1 LM 4;
			VSH1 Q 4 A_Fleshbender();
			VSH1 RJ 4 A_Light0();
			Goto Ready;
	}
}

extend class BloodGlobeSphere {
	int fleshbenderLevel;

	virtual void Fleshbender(int level) {
		if (!self.bloodGlobe) {
			return;
		}
		self.specialAction = "Fleshbender";
		self.fleshbenderLevel = level;
		
		float scale = 0.1 + (float(level) / 8);
		self.A_SetScale(scale);
		self.SetState(self.ResolveState("Fleshbender"));
	}

	action BloodGlobeFleshTendril A_Fleshbender() { 
		bool spawned;
		Actor actor;
		[spawned, actor] = invoker.A_SpawnItemEx("BloodGlobeFleshTendril", 0, 0);
		BloodGlobeFleshTendril tendril = BloodGlobeFleshTendril(actor);
		if (!tendril) {
			return null;
		}
		tendril.master = invoker.bloodGlobe.owner;
		tendril.summonTime = tendril.summonTime * invoker.fleshbenderLevel;
		return tendril;
	}

	States {
		Fleshbender:
			VBLO P 0 A_Fleshbender();
			VBLO PPPPQQQQRRRRQQQQPPPP 1 A_BloodGlobeUpdate(false);
			VBLO P 0 A_EndSpecial();
			Goto Spawn;
	}
}

class BloodGlobeFleshTendril : MinionCreature {
	Default {
		Tag "Blood Globe Flesh Tendril";

		Health 100;
		Radius 8;
		Height 128;
		Speed 0;
		FloatSpeed 0;
		Scale 1;
		Gravity 1;
		PainChance 0;
		Damage 2;
		BloodColor "ff 00 00";
		ProjectilePassHeight -16;

		DamageFactor "Fae", 0.1;

		MinionCreature.SummonTime 10 * 35;
		MinionCreature.Stance 2; // Guard
		
		SeeSound "BloodGlobe/Fleshbender/See";
		PainSound "";
		DeathSound "BloodGlobe/Fleshbender/Death";
		ActiveSound "";
		MeleeSound "BloodGlobe/Fleshbender/Melee";
		Obituary "%o was leeched by a Blood Globe Flesh Tendril!";
		HitObituary "%o was leeched by a Blood Globe Flesh Tendril!";

		-COUNTKILL;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		if (self.health > 0) {
			self.SpawnParticle("Fae", 16, 2, (FRandom(-16, 16), FRandom(-16, 16), FRandom(16, 32)), Random(0, 259));
		}
	}

	States {
		Spawn:
			VBLT ABCD 8;
		Idle:
			VBLT EFGH 8 A_MinionIdle();
			Loop;
		Guard:
			VBLT EFGH 8 A_MinionGuard();
			Loop;
		Follow:
			VBLT EFGH 8 A_FollowMaster(1000);
			Loop;
		See:
			VBLT EFGH 8 A_MinionChase();
			Loop;
		Melee:
			VBLT EFG 5 A_FaceTarget();
			VBLT H 7 A_CreatureMelee(10, 20, "Fae", "Fae");
			Goto Idle;
		Missile:
			VBLT H 8 A_FaceTarget();
			VBLT EEFFGGHH 2 {
				A_FaceTarget();
				A_SpawnProjectile("AnimashardProjectile", invoker.height * 0.8);
			}
			VBLT H 0 A_MonsterRefire(10, "Idle");
			Goto Missile + 2;
		Death:
			VBLT A 8 A_Scream();
			VBLT B 8 A_NoBlocking();
			VBLT CD 8;
			Stop;
	}
}