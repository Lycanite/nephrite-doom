class PandemicDartScroll : WeaponPassive {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Pandemic Darts have been added to your Malady Quiver.";
		
		Inventory.Icon "VESWQ0";
		Inventory.PickupSound "MaladyQuiver/Pickup";
		Inventory.RestrictedTo "Vess";
		WeaponUpgrade.ExtraAmmoClass "ToxicDrupe";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			VESW Q 1 bright;
			Loop;
	}
}

extend class MaladyQuiver {
	action state A_PandemicDart() {
		if (!A_CheckSpecial(4) || !A_SpendAmmo(6)) {
			return ResolveState("Ready");
		}
        A_FireProjectile("PandemicDartProjectile", 0, false);
		return null;
	}

    States {
		MainHand.User4:
			VSH5 K 4 {
				A_ClearBobbing();
				A_Light2();
			}
			VSH5 L 4;
			VSH5 M 4 A_PandemicDart();
			VSH5 N 4;
			VSH5 OPQL 4;
			VSH5 C 0 A_MirrorHands();
			VSH5 C 0 A_Light0();
			Goto Ready;

		OffHand.User4:
			VSH5 ABCDEFGH 2;
			Loop;
    }
}

class PandemicDartProjectile : MaladyDartProjectile {
	Default {
        Scale 1;
		Radius 2;
		Height 2;
		Speed 20;
		FastSpeed 20;
		
		SeeSound "MaladyQuiver/Fire/PandemicDart";
		DeathSound "MaladyQuiver/Explode/PandemicDart";

		SchismProjectile.ParticleName "Anima";
		SchismProjectile.ParticleColor 0xF7DD00FF;
		SchismProjectile.StatusEffectClass "SchismPandemicEffect";
		SchismProjectile.StatusEffectDuration 10 * 35;
		SchismProjectile.StatusEffectStrength 1;
	}
	
	States {
		Spawn:
			VMQ4 ABCDEFG 4;
			Loop;
			
		Death:
			VMQ4 HIJKLMNO 4;
			Stop;
	}
}