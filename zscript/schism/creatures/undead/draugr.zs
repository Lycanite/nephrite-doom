class Draugr : MinionCreature {
	Default {
		Tag "Draugr";
		
		Health 100;
		Radius 10;
		Height 50;
		Speed 15;
		Scale 0.75;
		Gravity 1;
		PainChance 120;
		Damage 1;
		BloodColor "44 00 00";

		MinionCreature.SummonTime 60 * 35;
		
		SeeSound "Draugr/See";
		PainSound "Draugr/Pain";
		DeathSound "Draugr/Death";
		ActiveSound "Draugr/See";
		MeleeSound "Draugr/Melee";
		Obituary "%o was devoured by a Draugr!";
		HitObituary "%o was devoured by a Draugr!";
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		if (Health <= 0) {
			return;
		}
	}
	
	action void A_DraugrAttack() {
		A_CreatureMelee(10, 30);
	}
	
	action void A_BoneBurst() {
		for (int i = 0; i < 20; i += 20) {
			A_SpawnItemEx("SchismGoreBone", 0, 0, 10, random(-6, 6), random(-6, 6), random(10, 40), i, SXF_CLIENTSIDE);
		}
	}
	
	States {
		Spawn:
			DRGR B 4 {
				return A_MinionIdle();
			}
			Loop;
			
		Guard:
			DRGR B 4 {
				return A_MinionGuard();
			}
			Loop;
			
		Follow:
			DRGR ABCDEF 4 {
				return A_FollowMaster();
			}
			Loop;
			
		See:
			DRGR ABCDEF 4 {
				return A_MinionChase();
			}
			Loop;
			
		Melee:
			DRGR GH 4 A_FaceTarget();
			DRGR I 4 A_DraugrAttack();
			Goto See;
			
		Pain:
			DRGR J 6 A_Pain();
			Goto See;
			
		Pain.SelfDamage:
			Goto See;
			
		Death:
		XDeath:
			DRGR J 5;
			DRGR K 5 A_Scream();
			DRGR L 5;
			DRGR M 5 A_CreatureDie();
			DRGR N 5;
			DRGR O -1 A_BoneBurst();
			Stop;
	}
}