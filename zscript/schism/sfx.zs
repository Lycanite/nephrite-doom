class SchismSFX : Actor {
	float thrustLatMin;
	float thrustLatMax;
	float thrustLonMin;
	float thrustLonMax;
	float thrustVertMin;
	float thrustVertMax;
	float rollMin;
	float rollMax;
	float growthMin;
	float growthMax;
	float fadeOut;
	bool randomRoll;

	float alphaBase;
	float thrustRoll;
	float growth;

	property ThrustLatMin: thrustLatMin;
	property ThrustLatMax: thrustLatMax;
	property ThrustLonMin: thrustLonMin;
	property ThrustLonMax: thrustLonMax;
	property ThrustVertMin: thrustVertMin;
	property ThrustVertMax: thrustVertMax;
	property RollMin: rollMin;
	property RollMax: rollMax;
	property GrowthMin: growthMin;
	property GrowthMax: growthMax;
	property FadeOut: fadeOut;
	property RandomRoll: randomRoll;

	bool initAlpha; // True when this special effect has set its base (starting) alpha.
	bool initThrust; // True when this special effect has set its initial thrust.
	bool initRoll; // True when this special effect has set its initial roll.
	bool initGrow; // True when this special effect has set its initial growth.
	bool inSkybox; // If true, prevent this special effect from being range culled.
	
	Default {
		Radius 1;
		Height 2;
		Scale 1;
		Speed 0;
		Gravity 0;
		Alpha 1;
		RenderStyle "Translucent";

		SchismSFX.ThrustLatMin -1;
		SchismSFX.ThrustLatMax 1;
		SchismSFX.ThrustLonMin -1;
		SchismSFX.ThrustLonMax 1;
		SchismSFX.ThrustVertMin 2;
		SchismSFX.ThrustVertMax 6;
		SchismSFX.RollMin -2;
		SchismSFX.RollMax 2;
		SchismSFX.FadeOut 0.15;
		SchismSFX.RandomRoll true;
		
		+MISSILE;
		+FORCEXYBILLBOARD;
		+CLIENTSIDEONLY;
		+NOCLIP;
		+NOBLOCKMAP;
		+NOINTERACTION;
		+ROLLSPRITE;
		+DONTSPLASH;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}

		// Initial Alpha:
		if (!self.initAlpha) {
			self.initAlpha = true;
			self.alphaBase = self.alpha;
		}

		// Initial Thrust:
		if (!self.initThrust) {
			self.initThrust = true;
			self.A_SFXThrust();
		}

		// Initial Roll Thrust:
		if (!self.initRoll && self.rollMin < self.rollMax) {
			self.initRoll = true;
			self.thrustRoll = Random(self.rollMin, self.rollMax);
		}

		// Initial Growth:
		if (!self.initGrow && self.growthMin < self.growthMax) {
			self.initGrow = true;
			self.growth = Random(self.growthMin, self.growthMax);
		}

		// Random Roll:
		if (self.randomRoll) {
			self.randomRoll = false;
			self.A_SetRoll(Random(0, 359));
		}

		// Range Destroy:
		if (!self.inSkybox && CheckRange(2560, true)) {
			self.Destroy();
		}
	}

	/**
	 * Sets the thrust of this special effect, only needs to be called on spawn and on change.
	 */
	action void A_SFXThrust() {
		float thrustLat = invoker.thrustLatMin < invoker.thrustLatMax ? Random(invoker.thrustLatMin, invoker.thrustLatMax) : invoker.thrustLatMin;
		if (thrustLat != 0) {
			invoker.Thrust(thrustLat, invoker.angle + 90);
		}

		float thrustLon = invoker.thrustLonMin < invoker.thrustLonMax ? Random(invoker.thrustLonMin, invoker.thrustLonMax) : invoker.thrustLonMin;
		if (thrustLon != 0) {
			invoker.Thrust(thrustLon, invoker.angle);
		}

		float thrustVert = invoker.ThrustVertMin < invoker.ThrustVertMax ? Random(invoker.ThrustVertMin, invoker.ThrustVertMax) : invoker.ThrustVertMin;
		if (thrustVert != 0) {
			ThrustThingZ(0, thrustVert, 0, 0);
		}
	}
	
	/**
	 * Updates special effect, should be called on action ticks, applies, rotations, scaling, etc.
	 * @param The number of ticks the calling update frame took, importing to ensure special effects have a consisent lifespan via fadeout, etc.
	 */
	action void A_SFXUpdate(float ticks) {
		float strength = ticks / 4;

		// Fade Out:
		invoker.A_FadeOut(Max(invoker.fadeOut * strength * Max(invoker.alphaBase, 0.001), 0.0001));

		// Roll:
		if (invoker.thrustRoll != 0) {
			invoker.A_SetRoll((invoker.roll + (invoker.thrustRoll * strength)) % 359, SPF_INTERPOLATE);
		}

		// Growth:
		if (invoker.growth != 0) {
			float growth = Max(invoker.growth * strength, 0);
			A_SetScale(invoker.scale.x + growth, invoker.scale.y + growth);
		}
	}
}

class SchismSFXSolid : SchismSFX {
	Default {
		-NOCLIP;
		-NOBLOCKMAP;
		-NOINTERACTION;
	}
}

class PlasmaPuff : BulletPuff {
	Default {
		Scale 0.5;
		DamageType "Lightning";
	}

	States {
		Spawn:
			PFPL A 4 bright;
			PFPL B 4;
		Melee:
			PFPL CD 4;
			Stop;
	}
}

#include "zscript/schism/sfx/leaf.zs"
#include "zscript/schism/sfx/gore.zs"
#include "zscript/schism/sfx/oakblink.zs"
#include "zscript/schism/sfx/souls.zs"
#include "zscript/schism/sfx/trails.zs"