struct SchismArbourParticle {
	static void apply(out FSpawnParticleParams particleParams) {
		particleParams.texture = TexMan.CheckForTexture("particles/arbour/Arbour01.png");
		particleParams.style = STYLE_Translucent;
		particleParams.flags = SPF_ROLL;

		particleParams.accel = (0, 0, 0);
		particleParams.startRoll = Random(0, 359);
		particleParams.rollVel = Random(-5, 5);
		particleParams.rollAcc = 0;

		particleParams.size = 16.0;
		particleParams.sizeStep = 0;
		particleParams.startAlpha = 1.0;
		particleParams.fadeStep = particleParams.startAlpha / particleParams.lifetime;
	}
}

struct SchismLeafParticle {
	static void apply(out FSpawnParticleParams particleParams) {
		particleParams.texture = TexMan.CheckForTexture("particles/arbour/Leaf01.png");
		particleParams.style = STYLE_Translucent;
		particleParams.flags = SPF_ROLL;

		particleParams.accel = (0, 0, 0);
		particleParams.startRoll = Random(0, 359);
		particleParams.rollVel = Random(-5, 5);
		particleParams.rollAcc = 0;

		particleParams.size = 16.0;
		particleParams.sizeStep = 0;
		particleParams.startAlpha = 1.0;
		particleParams.fadeStep = particleParams.startAlpha / particleParams.lifetime;
	}
}