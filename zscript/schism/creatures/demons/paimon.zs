Class Paimon : Creature {
    int missileIter;
	
    Default {
		Tag "Paimon";
        Health 1000;
        Radius 24;
        Height 60;
        Mass 1000;
        Speed 10;
        PainChance 24;
        Gravity 0;

        SeeSound "Paimon/See";
        ActiveSound "Paimon/Idle";
        PainSound "Paimon/Pain";
        DeathSound "Paimon/Death";
        Obituary "%o was hexed by a Paimon.";
        HitObituary "%o got shredded by a Paimon.";

        +MISSILEMORE;
        +DONTHARMCLASS;
        +BOSSDEATH;
        +Creature.HOVERBOB;
    }

    action void A_BlurChase() {
        A_SpawnItemEx("PaimonBlur", 0, 0, 0, 0, 0, 0, 0, SXF_CLIENTSIDE);
        A_Chase();
    }

    States {
        Spawn:
            PAIM AC 10 A_Look();
            Loop;

        See:
            PAIM A 0 { invoker.bHoverBob = true; }
            PAIM AABBAACCDDCC 4 A_Chase();
            PAIM A 0 A_Jump(64, "Blur");
            Loop;

        Blur:
            PAIM A 1 A_StartSound("monster/blur");
            PAIM AAAAAAAAAAAAAAAAAAAAAAAA 1 A_BlurChase();
            Goto See;

        Missile:
        MissileDecide:
            PAIM A 0 A_Jump(256, "MissileFireball", "MissileFrostshard", "MissileDuskflare");
        MissileFireball:
            PAIM EF 7 Bright A_FaceTarget();
            PAIM G 0 Bright A_SpawnProjectile("FireballProjectile", 26.0, 0, Random(-6, -1));
            PAIM G 7 Bright A_SpawnProjectile("FireballProjectile", 26.0, 0, Random( 1,  6));
            PAIM A 3 Bright;
            PAIM HI 7 Bright A_FaceTarget();
            PAIM J 0 Bright A_SpawnProjectile("FireballProjectile", 26.0, 0, Random(-6, -1));
            PAIM J 7 Bright A_SpawnProjectile("FireballProjectile", 26.0, 0, Random( 1,  6));
            PAIM A 3 A_SpidRefire();
            PAIM A 0 A_Jump(64, "MissileFireball");
            Goto See;
        MissileFrostshard:
            PAIM EF 7 Bright A_FaceTarget();
            PAIM G 7 Bright A_SpawnProjectile("FrostshardProjectile", 26.0);
            PAIM A 3 A_SpidRefire();
            PAIM A 0 A_Jump(128, "MissileFireball", "MissileDuskflare");
            Goto See;
        MissileDuskflare:
            PAIM KLM 5 Bright A_FaceTarget();
        MissileDuskflareLoopPrep:
            PAIM A 0 { invoker.missileIter = 0; }
        MissileDuskflareLoop:
            PAIM N 1 Bright A_SpawnProjectile("DuskflareProjectile");
            PAIM O 1 Bright A_FaceTarget();
            PAIM A 0 {
                if (++invoker.missileIter < 15) {
                    return ResolveState("MissileDuskflareLoop");
                }
                return ResolveState(null);
            }
            PAIM L 5;
            Goto See;

        Melee:
        LeftSlash:
            PAIM EF 8 A_FaceTarget();
            PAIM G 3 A_CreatureMelee(10, 80, "Flame", "Fire");
            PAIM A 3;
            PAIM A 0 A_Jump(128, "RightSlash");
            Goto See;
        RightSlash:
            PAIM HI 8 A_FaceTarget();
            PAIM J 3 A_CreatureMelee(10, 80, "Shadow", "Shadow");
            PAIM A 3;
            Goto See;

        Pain:
            PAIM P 2;
            PAIM P 2 A_Pain();
            Goto See;

        Death:
            PAIM Q 6;
            PAIM R 6 A_Scream();
            PAIM S 6 A_CreatureDie();
            PAIM TUVW 6;
            PAIM X -1 A_BossDeath();
            Stop;
        }
}

Class PaimonBlur : Actor {
    Default {
        RenderStyle "Translucent";
        Alpha 0.8;

        +NOINTERACTION;
    }

    States {
        Spawn:
            TNT1 A 3;
            PAIM A 5 A_FadeOut();
            Wait;
    }
}