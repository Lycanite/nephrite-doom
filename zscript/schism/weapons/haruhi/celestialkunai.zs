class CelestialKunai : SchismWeapon {
	CelestialKunaiGraplingProjectile grapling;
	Array<CelestialKunaiMagnetProjectile> magnetProjectiles;
	Actor lastHitActor;
	
	Default {
		Radius 20;
		Height 16;
		Scale 0.5;
		
		Inventory.RestrictedTo "Haruhi";
		
		Inventory.PickupSound "CelestialKunai/Pickup";
		Weapon.UpSound "CelestialKunai/Up";
		Weapon.ReadySound "";
		SchismWeapon.FailSound "CelestialKunai/Fire/Fail";
		
		Weapon.AmmoType1 "CelestialCharge";
		Weapon.AmmoType2 "PranaEnergy";
		
		SchismWeapon.PrimaryCost 1;
		SchismWeapon.SecondaryCost 1;
		SchismWeapon.SpecialACost 25;
		SchismWeapon.SpecialBCost 50;
		SchismWeapon.SpecialCCost 25;
		SchismWeapon.SpecialDCost 200;
		
		SchismWeapon.SpecialItemA "MagnetBlade";
		SchismWeapon.SpecialItemB "AntigravBlade";
		SchismWeapon.SpecialItemC "SpatialBlade";
		SchismWeapon.SpecialItemD "BlackHoleBlade";
		SchismWeapon.PassiveItem "MagnetBlade";
		// SchismWeapon.PassiveItem "LeylineBlade";
		
		Inventory.PickupMessage "The Celestial Kunai are yours to weild!";
		Obituary "%o was obliterated by %k's Celestial Kunai!";
		SchismWeapon.InsufficientAmmoAMessage "\caNot enough Celestial Charges to use this ability.";
		SchismWeapon.InsufficientAmmoBMessage "\caNot enough Prana Energy to use this ability.";
		SchismWeapon.NoSpecialAMessage "\caYou need a Magnet Blade to use this ability.";
		SchismWeapon.NoSpecialBMessage "\caYou need an Antigrav Blade to use this ability.";
		SchismWeapon.NoSpecialCMessage "\caYou need a Spatial Blade to use this ability.";
		SchismWeapon.NoSpecialDMessage "\caYou need a Black Hole Blade to use this ability.";

		SchismWeapon.IdleBob 30;

		+WEAPON.AMMO_OPTIONAL;
	}
	
	
	// Primary Celestial Blade:
	action void A_CelestialBlade() {
		if (!A_SpendAmmo(1)) {
			return;
		}
		
		CelestialKunaiProjectile ckProjectile = CelestialKunaiProjectile(A_FireProjectile("CelestialKunaiProjectile", 0, false));
		if (ckProjectile)
		{
			ckProjectile.celestialKunai = invoker;
		}
		
		// Spatial Blades:
		ThinkerIterator spatialIterator = ThinkerIterator.Create("CelestialKunaiSpatialEffect");
		CelestialKunaiSpatialEffect spatialEffect;
		Actor angleActor = invoker.owner;
		if (invoker.lastHitActor)
		{
			angleActor = invoker.lastHitActor;
		}
		while (spatialEffect = CelestialKunaiSpatialEffect(spatialIterator.Next())) {
			if (spatialEffect.owner && spatialEffect.owner.health > 0 && spatialEffect.InflictedBy(invoker.owner)) {
				A_SpawnItemEx("CelestialKunaiProjectile",
					spatialEffect.owner.pos.x, spatialEffect.owner.pos.y +(spatialEffect.owner.height / 2), spatialEffect.owner.pos.z,
					1, 0, 0, spatialEffect.owner.AngleTo(angleActor),
					SXF_ABSOLUTEPOSITION|SXF_ABSOLUTEANGLE|SXF_ABSOLUTEVELOCITY|
					SXF_NOCHECKPOSITION|SXF_MULTIPLYSPEED|SXF_SETTARGET|SXF_ISTRACER);
				if (tracer) 
				{
					tracer.SetOrigin(spatialEffect.owner.pos +(0, 0, spatialEffect.owner.height / 2), false);
				}
			}
		}
	}
	
	
	// Secondary Grapling Blade:
	action state A_OnGraplingBlade() {
		if (invoker.grapling)
		{
			invoker.grapling.Detach();
			invoker.grapling = null;
			return ResolveState("AltFire.Detach");
		}
		return null;
	}
	
	action void A_GraplingBlade() {
		if (!A_SpendAmmo(2)) {
			return;
		}
		invoker.grapling = CelestialKunaiGraplingProjectile(A_FireProjectile("CelestialKunaiGraplingProjectile", 0, false, 0, 0, FPF_NOAUTOAIM, 0));
		if (invoker.grapling)
		{
			invoker.grapling.celestialKunai = invoker;
		}
	}
	
	
	// Special 1 Magnet Blade:
	action void A_MagnetBlade() {
		if (!A_CheckSpecial(1) || !A_SpendAmmo(3)) {
			return;
		}
		Actor projectile = A_FireProjectile("CelestialKunaiMagnetProjectile", 0, false);
		CelestialKunaiMagnetProjectile magnetProjectile = CelestialKunaiMagnetProjectile(projectile);
		if (magnetProjectile)
		{
			magnetProjectile.celestialKunai = invoker;
			invoker.magnetProjectiles.Push(magnetProjectile);
		}
	}
	
	
	// Special 2 Antigrav Blade:
	action void A_AntigravBlade() {
		if (!A_CheckSpecial(2) || !A_SpendAmmo(4)) {
			return;
		}
		A_FireProjectile("CelestialKunaiAntigravProjectile", 0, false);
	}
	
	
	// Special 3 Spatial Blade:
	action void A_SpatialBlade() {
		if (!A_CheckSpecial(3) || !A_SpendAmmo(5)) {
			return;
		}
		A_FireProjectile("CelestialKunaiSpatialProjectile", 0, false);
	}
	
	
	// Special 4 Black Hole Blade: TODO Replace
	action void A_BlackHoleBladeOld() {
		if (!A_CheckSpecial(4) || !A_SpendAmmo(6)) {
			return;
		}
		A_FireProjectile("CelestialKunaiBlackHoleProjectile", 0, false);
	}
	
	
	// Ultimate Black Hole Blade:
	action void A_BlackHoleBlade() {
		A_FireProjectile("CelestialKunaiBlackHoleProjectile", 0, false);
	}
	
	
	States {
		Spawn:
			CKNI Z 4 bright;
			Loop;
			
		Select:
			CKNI D 1 A_Raise(12);
			Loop;
			
		Deselect:
			CKNI D 1 A_Lower(12);
			Loop;
			
		Ready:
			CKNI DEF 2 {
				return A_SchismWeaponReady();
			}
		Ready.Loop:
			CKNI AABBCCBB 3 {
				return A_SchismWeaponReady();
			}
			Loop;
			
		Fire:
			CKNI A 0 A_ClearBobbing();
			CKNI GHI 2;
			CKNI J 2 A_CelestialBlade();
			Goto Ready;
			
		AltFire:
			CKNI A 0 {
				A_ClearBobbing();
				return A_OnGraplingBlade();
			}
			CKNI KLM 2;
			CKNI N 2 A_GraplingBlade();
			Goto Ready;
		AltFire.Detach:
			CKNI G 8;
			Goto Ready;
		
		User1:
			CKNI A 0 A_ClearBobbing();
			CKNI KLM 2;
			CKNI N 2 A_MagnetBlade();
			Goto Ready;
			
		User2:
			CKNI A 0 A_ClearBobbing();
			CKNI KLM 2;
			CKNI N 2 A_AntigravBlade();
			Goto Ready;
		
		User3:
			CKNI A 0 A_ClearBobbing();
			CKNI KLM 2;
			CKNI N 2 A_SpatialBlade();
			Goto Ready;
		
		User4:
			CKNI A 0 A_ClearBobbing();
			CKNI GHI 2;
			CKNI J 2 A_BlackHoleBladeOld();
			Goto Ready;
		
		Ultimate:
			CKNI A 0 A_ClearBobbing();
			CKNI GHI 2;
			CKNI J 2 A_BlackHoleBlade();
			Goto Ready;
	}
}

#include "zscript/schism/weapons/haruhi/celestialkunai/celestialblade.zs"
#include "zscript/schism/weapons/haruhi/celestialkunai/magnetblade.zs"
#include "zscript/schism/weapons/haruhi/celestialkunai/antigravblade.zs"
#include "zscript/schism/weapons/haruhi/celestialkunai/spatialblade.zs"
#include "zscript/schism/weapons/haruhi/celestialkunai/blackholeblade.zs"
