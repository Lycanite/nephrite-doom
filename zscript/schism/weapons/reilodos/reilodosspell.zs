class ReilodosSpell : DualHand {	
	Default {
		Weapon.AmmoType "ChaosEnergy";
		Weapon.AmmoType2 "HavocEnergy";
		
		Inventory.PickupSound "Reilodos/Taunt";
		Inventory.RestrictedTo "Reilodos", "ReilodosHavocSerpent";
		
		+FLOATBOB;
		+NOGRAVITY;
		+INVENTORY.RESTRICTABSOLUTELY;
	}
}
