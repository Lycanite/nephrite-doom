class FoulBurstProjectile : FollowProjectile {
	int havoc;
	
	Default {
		Radius 16;
		Height 10;
		Scale 0.75;
		Speed 0;
		Gravity 0;
		DamageFunction 0;
		Alpha 0.5;
		Renderstyle "Add";
		
		DamageType "Chaos";
		
		SeeSound "FoulMenagerie/Burst";
		DeathSound "FoulMenagerie/Burst/Finish";
		Obituary "%o was devoured by %k's Foul Menagerie!";
		
		+NODAMAGETHRUST;
		+PAINLESS;
		+FORCERADIUSDMG;
		-THRUGHOST;
	}
	
	override void BeginPlay() {	
		super.BeginPlay();
		
		self.havoc = 0;
	}
	
	virtual void FoulBurst(bool giveHavoc) {
		int targets = SchismAttacks.Explode(self, self.target, 1, 100, 0, 1, false, false, "Chaos");
		if (giveHavoc && self.havoc > 0 && targets > 0 && self.target && self.target.master) {
			target.A_GiveInventory("HavocEnergy", targets * self.havoc, AAPTR_MASTER);
		}
		FireEffects();
	}
	
	virtual void FireEffects() {
		float offsetX = FRandom(0, 1);
		float offsetY = FRandom(0, 1);
		switch (Random(0, 5)) {
			case 1:
				self.SpawnParticle("ChaosEmber", 2 * TICRATE, 2, (0, 0, 0), Random(0, 359), Random(-90, -60));
				break;
			case 2:
			case 3:
			case 4:
				break;
				self.SpawnParticle("Chaos", 1 * TICRATE, 1, (0, 0, 0), Random(0, 359));
		}
	}
	
	States {
		Spawn:
			Goto Death;
			
		Death:
			FMEN EF 4 bright {
				FoulBurst(false);
			}
			FMEN G 4 bright {
				FoulBurst(true);
			}
			FMEN HIJ 4 bright {
				FoulBurst(false);
			}
			FMEN J 1 bright A_FadeOut(2);
			Loop;
	}
}

class LightFoulBurstProjectile : FoulBurstProjectile {
	Default {
		Alpha 0.05;
	}
}