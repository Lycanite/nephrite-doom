class LichScepterOssifexEffigy : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Lich Scepter can now cast Raise Ossifex at the cost of Soul Essence!";
		
		Inventory.Icon "LSCPV0";
		Inventory.PickupSound "LichScepter/Pickup";
		Inventory.RestrictedTo "Chozo";
		WeaponUpgrade.ExtraAmmoClass "DeathRune";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			LSCP V 1 bright;
			Loop;
	}
}
