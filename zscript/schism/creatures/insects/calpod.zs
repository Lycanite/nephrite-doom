class Calpod : MinionCreature {
	Default {
		Tag "Calpod";

		Health 20;
		Radius 12;
		Height 48;
		Speed 18;
		Scale 0.4;
		PainChance 250;
		Damage 2;
		BloodColor "00 ff 00";

		MinionCreature.SummonTime 10 * 35;

		+JUMPDOWN;
		
		SeeSound "Calpod/See";
		PainSound "Calpod/Pain";
		DeathSound "Calpod/Death";
		ActiveSound "Calpod/See";
		MeleeSound "Calpod/Melee";
		Obituary "%o was munched by a Calpod!";
		HitObituary "%o was munched by a Calpod!";
	}
	
	States {
		Spawn:
			CALP AABBCCBB 4 {
				A_Look();
				return A_StayWithMaster();
			}
			Loop;
			
		Follow:
			CALP DEFG 4 {
				A_Look();
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			CALP DEF 4 {
				return A_MinionChase();
			}
			CALP G 4 {
				if (FRandom(0, 1) >= 0.75) {
					A_FaceTarget();
					A_Leap(Random(10, 20), Random(30, 50));
				}
				return A_MinionChase();
			}
			Loop;
			
		Melee:
			CALP H 4 A_FaceTarget();
			CALP I 4 A_CreatureMelee(4);
			CALP J 4;
			Goto See;
			
		Pain:
			CALP K 6;
			CALP L 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			CALP KL 5;
			CALP M 5 A_Scream();
			CALP N 5;
			CALP P 5 A_CreatureDie();
			CALP QRSTUVW 5;
			CALP X -1;
			Stop;
	}
}
