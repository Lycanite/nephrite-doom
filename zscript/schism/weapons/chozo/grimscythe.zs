class GrimScythe : SchismWeapon {
	bool mirroredScythe;

	Default {
		Radius 20;
		Height 16;
		Scale 0.5;
		
		Inventory.RestrictedTo "Chozo";
		
		Inventory.PickupSound "GrimScythe/Pickup";
		Weapon.UpSound "GrimScythe/Up";
		Weapon.ReadySound "";
		SchismWeapon.FailSound "GrimScythe/Fire/Fail";
		
		Weapon.AmmoType1 "GrimRune";
		Weapon.AmmoType2 "SoulEssence";
		
		SchismWeapon.PrimaryCost 1;
		SchismWeapon.SecondaryCost 2;
		SchismWeapon.SpecialACost 10;
		SchismWeapon.SpecialBCost 2;
		SchismWeapon.SpecialCCost 25;
		SchismWeapon.SpecialDCost 25;
		
		SchismWeapon.SpecialItemA "CorpseExplosionSigil";
		SchismWeapon.SpecialItemB "SoulGraspSigil";
		SchismWeapon.SpecialItemC "DreadWallSigil";
		SchismWeapon.SpecialItemD "DreadValleySigil";
		SchismWeapon.PassiveItem "BoneArmorSigil";
		
		Inventory.PickupMessage "You found the dreaded spectral Grim Scythe.";
		Obituary "%o was reaped by %k's Grim Scythe!";
		SchismWeapon.InsufficientAmmoAMessage "\caNot enough Necrotic Runes to use this ability.";
		SchismWeapon.InsufficientAmmoBMessage "\caNot enough Soul Essence to use this ability.";
		SchismWeapon.NoSpecialAMessage "\caYou need a Corpse Explosion Sigil to use this ability.";
		SchismWeapon.NoSpecialBMessage "\caYou need a Soul Grasp Sigil to use this ability.";
		SchismWeapon.NoSpecialCMessage "\caYou need a Dread Wall Sigil to use this ability.";
		SchismWeapon.NoSpecialDMessage "\caYou need a Dread Valley Sigil to use this ability.";

		SchismWeapon.IdleBob 20;
		
		+INVENTORY.RESTRICTABSOLUTELY;
		+WEAPON.AMMO_OPTIONAL;
		+WEAPON.ALT_AMMO_OPTIONAL;
		+WEAPON.NOALERT;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		mirroredScythe = false;
	}
	
	// Primary Grim Scythe:
	action void A_GrimScythe() {
		for (int i = -2; i <= 2; i++) {
			Actor projectile = A_FireProjectile("GrimScytheProjectile", 15 * i, false, 0, 0);
			GrimScytheProjectile grimScytheProjectile = GrimScytheProjectile(projectile);
			if (grimScytheProjectile) {
				grimScytheProjectile.tracer = invoker.owner;
			}
		}
		int meleeDamage = 20;
		if (CountInv("PowerStrength")) {
			meleeDamage = 40;
		}
		A_CustomPunch(meleeDamage, true, CPF_PULLIN|CPF_NOTURN|CPF_STEALARMOR, "BulletPuff", 96, true, 1, "ArmorBonus", "GrimScythe/Hit");
	}
	
	// Secondary Spectral Scythe:
	action void A_SpectralScythe() {
		A_SpendAmmo(2);
		A_StartSound("GrimScythe/Fire/Spectral");
		for (int i = -2; i <= 2; i++) {
			Actor projectile = A_FireProjectile("SpectralScytheProjectile", 20 * i, false, 0, 0);
			SpectralScytheProjectile spectralScytheProjectile = SpectralScytheProjectile(projectile);
			if (spectralScytheProjectile) {
				spectralScytheProjectile.tracer = invoker.owner;
				spectralScytheProjectile.grimScythe = invoker;
			}
		}
	}
	
	// Special 1 Corpse Explosion:
	action void A_CorpseExplosion() {
		A_SpendAmmo(3);
		A_StartSound("GrimScythe/Fire/Corpse");
		A_SpawnItemEx("GrimScytheCorpseExplosionProjectile", Random(-100, 100), Random(-100, 100), 0, 0, 0, 0, 0, 0);
		ThinkerIterator corpseFinder = ThinkerIterator.Create("Actor");
		Actor possibleCorpse;
		int corpseCount = 0;
		while ((possibleCorpse = Actor(corpseFinder.Next())) && corpseCount < 20) {
			if (possibleCorpse.bCorpse && !possibleCorpse.bNoKillScripts && invoker.owner.Distance3D(possibleCorpse) <= 1200) {
				A_SpawnItemEx("GrimScytheCorpseExplosionProjectile", possibleCorpse.Pos.x - invoker.owner.Pos.x, possibleCorpse.Pos.y - invoker.owner.Pos.y, possibleCorpse.Pos.z - invoker.owner.Pos.z, 0, 0, 0, 0, SXF_SETTRACER|SXF_ABSOLUTEPOSITION);
				possibleCorpse.Destroy();
				corpseCount++;
			}
		}
	}
	
	// Special 2 Soul Grasp:
	action void A_SoulGrasp() {
		A_SpendAmmo(4);
		A_StartSound("GrimScythe/Fire/Grasp");
		A_FireProjectile("GrimScytheSoulGraspProjectile", 0, false, 0, 0);
	}
	
	// Special 3 Dread Wall:
	action void A_DreadWall() {
		A_SpendAmmo(5);
		A_StartSound("GrimScythe/Fire/Wall");
		A_SpawnItemEx("GrimScytheDreadWall", invoker.owner.radius + 60, -120, 0, 0, 0, 0, 0);
		A_SpawnItemEx("GrimScytheDreadWall", invoker.owner.radius + 60, 0, 0, 0, 0, 0, 0);
		A_SpawnItemEx("GrimScytheDreadWall", invoker.owner.radius + 60, 120, 0, 0, 0, 0, 0);
	}
	
	// Special 4 Dread Valley:
	action void A_DreadValley() {
		A_SpendAmmo(6);
		A_StartSound("GrimScythe/Fire/Valley");

		// Start Wall:
		A_SpawnItemEx("GrimScytheDreadWall", invoker.owner.radius - 120, -120, 0, 0, 0, 0, 0);
		A_SpawnItemEx("GrimScytheDreadWall", invoker.owner.radius - 120, 0, 0, 0, 0, 0, 0);
		A_SpawnItemEx("GrimScytheDreadWall", invoker.owner.radius - 120, 120, 0, 0, 0, 0, 0);

		// Left Wall:
		A_SpawnItemEx("GrimScytheDreadWall", invoker.owner.radius - 60, -120, 0, 0, 0, 0, 0);
		A_SpawnItemEx("GrimScytheDreadWall", invoker.owner.radius, -120, 0, 0, 0, 0, 0);
		A_SpawnItemEx("GrimScytheDreadWall", invoker.owner.radius + 60, -120, 0, 0, 0, 0, 0);
		A_SpawnItemEx("GrimScytheDreadWall", invoker.owner.radius + 120, -120, 0, 0, 0, 0, 0);

		// Right Wall:
		A_SpawnItemEx("GrimScytheDreadWall", invoker.owner.radius - 60, 120, 0, 0, 0, 0, 0);
		A_SpawnItemEx("GrimScytheDreadWall", invoker.owner.radius, 120, 0, 0, 0, 0, 0);
		A_SpawnItemEx("GrimScytheDreadWall", invoker.owner.radius + 60, 120, 0, 0, 0, 0, 0);
		A_SpawnItemEx("GrimScytheDreadWall", invoker.owner.radius + 120, 120, 0, 0, 0, 0, 0);

		// End Wall:
		A_SpawnItemEx("GrimScytheDreadWall", invoker.owner.radius + 240, -120, 0, 0, 0, 0, 0);
		A_SpawnItemEx("GrimScytheDreadWall", invoker.owner.radius + 240, 0, 0, 0, 0, 0, 0);
		A_SpawnItemEx("GrimScytheDreadWall", invoker.owner.radius + 240, 120, 0, 0, 0, 0, 0);

		// Roof Walls:
		A_SpawnItemEx("GrimScytheDreadRoof", -120, 0, invoker.owner.height * 2, 0, 0, 0, 0);
		A_SpawnItemEx("GrimScytheDreadRoof", 0, 0, invoker.owner.height * 2, 0, 0, 0, 0);
		A_SpawnItemEx("GrimScytheDreadRoof", 120, 0, invoker.owner.height * 2, 0, 0, 0, 0);
	}
	
	// Ultimate Grim Ascension:
	action void A_GrimAscension() {
		A_StartSound("GrimScythe/Fire/Ascension");
		A_Log("Grim Scythe Ultimate: Ascension not implemented yet!");
	}
	
	States {
		Spawn:
			GSCI A -1;
			Stop;
			
		Ready:
			GSCY AAAABBBBCCCCBBBB 2 {
				return A_SchismWeaponReady();
			}
			Loop;
			
		Deselect:
			GSCY A 1 A_Lower(12);
			Loop;
			
		Select:
			GSCY A 1 A_Raise(12);
			Loop;
			
		Fire:
			GSCY D 2 {
				A_OverlayFlags(0, PSPF_FLIP|PSPF_MIRROR, invoker.mirroredScythe);
				A_ClearBobbing();
			}
			GSCY E 2;
			GSCY F 2 A_GrimScythe();
			GSCY G 4;
			GSCY F 0 {
				A_OverlayFlags(0, PSPF_FLIP|PSPF_MIRROR, !invoker.mirroredScythe);
				invoker.mirroredScythe = !invoker.mirroredScythe;
			}
			goto Ready;
			
		AltFire:
			GSCY H 4 A_ClearBobbing();
			GSCY I 4 {
				return A_CheckAmmoJump(2, "Ready");
			}
			GSCY JK 4;
			GSCY L 4 A_SpectralScythe();
			GSCY M 4 A_ReFire();
			Goto Ready;
		
		User1:
			GSCY B 4 A_ClearBobbing();
		User1Hold:
			GSCY O 4 {
				return A_CheckSpecialJump(1, "Ready");
			}
			GSCY P 4 A_CorpseExplosion();
			GSCY Q 4 A_Light1();
			GSCY R 4 A_Light0();
			GSCY R 0 {
				return A_User1ReFire();
			}
			GSCY B 4;
			goto Ready;
		
		User2:
			GSCY B 2 A_ClearBobbing();
			GSCY S 2 {
				return A_CheckSpecialJump(2, "Ready");
			}
			GSCY T 2 A_Light2();
			GSCY U 2  {
				return A_SoulGrasp();
			}
			GSCY S 2;
			GSCY D 2 A_Light0();
			goto Ready;
		
		User3:
			GSCY B 4 A_ClearBobbing();
			GSCY V 4 {
				return A_CheckSpecialJump(3, "Ready");
			}
			GSCY W 4 A_Light2();
			GSCY X 4  {
				return A_DreadWall();
			}
			GSCY Y 4;
			GSCY Z 4 A_Light0();
			goto Ready;
		
		User4:
			GSCY B 4 A_ClearBobbing();
			GSCY V 4 {
				return A_CheckSpecialJump(4, "Ready");
			}
			GSCY W 4 A_Light2();
			GSCY X 4  {
				return A_DreadValley();
			}
			GSCY Y 4;
			GSCY Z 4 A_Light0();
			goto Ready;
		
		Ultimate:
			GSCY V 4 A_ClearBobbing();
			GSCY Y 4;
			GSCY Z 4 A_GrimAscension();
			Goto Ready;
	}
}

class GrimScytheProjectile : SchismProjectile {
	Default {
		Radius 16;
		Height 10;
		Scale 1;
		Speed 30;
		FastSpeed 20;
		DamageFunction 1;
		Alpha 0.75;
		RenderStyle "Add";
		
		SeeSound "GrimScythe/Fire";
		DeathSound "GrimScythe/Explode";
		
		+RIPPER;
		+NODAMAGETHRUST;
		+PAINLESS;
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		
		// Taunt:
		if (specialDamage > 0 && !targetActor.bFriendly && !PlayerPawn(targetActor)) {
			targetActor.target = target;
		}
		
		return specialDamage;
	}
	
	States {
		Spawn:
			GSCP ABCDEF 4;
			Stop;
			
		Death:
			Stop;
	}
}

class SpectralScytheProjectile : SoulEssenceProjectile {
	int returnTime;
	int remainingTics;
	
	Default {
		Radius 32;
		Height 10;
		Scale 1;
		Speed 20;
		FastSpeed 20;
		DamageFunction 25;
		Alpha 0.8;
		RenderStyle "Translucent";
		BounceType "Grenade";
		BounceCount 5;

		SoulEssenceProjectile.SoulEssenceMin 10;
		SoulEssenceProjectile.SoulEssenceMin 20;
		
		SeeSound "GrimScythe/Fire/Spectral";
		DeathSound "GrimScythe/Explode/Spectral";
		
		+RIPPER;
		+SEEKERMISSILE;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		
		returnTime = 35;
		remainingTics = returnTime * 4;
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		
		// Taunt:
		if (specialDamage > 0 && !targetActor.bFriendly && !PlayerPawn(targetActor)) {
			targetActor.target = target;
		}
		
		return specialDamage;
	}
	
	action state A_SpectralScytheSeek() {
		if (invoker.returnTime-- <= 0) {
			A_SeekerMissile(0, 20, SMF_LOOK|SMF_PRECISE);
			bNoclip = true;
			if (tracer && invoker.Distance3D(tracer) <= 50) {
				return invoker.ResolveState("Death");
			}
		}
		if (invoker.remainingTics-- <= 0) {
			return invoker.ResolveState("Death");
		}
		return null;
	}
	
	States {
		Spawn:
			GSCP GHIJKLMN 1 {
				return A_SpectralScytheSeek();
			}
			Loop;
			
		Death:
			GSCP ABCDEF 2;
			Stop;
	}
}

#include "zscript/schism/weapons/chozo/grimscythe/corpseexplosion.zs"
#include "zscript/schism/weapons/chozo/grimscythe/soulgrasp.zs"
#include "zscript/schism/weapons/chozo/grimscythe/dreadwall.zs"
#include "zscript/schism/weapons/chozo/grimscythe/dreadvalley.zs"
#include "zscript/schism/weapons/chozo/grimscythe/bonearmor.zs"
#include "zscript/schism/weapons/chozo/grimscythe/grimascension.zs"
