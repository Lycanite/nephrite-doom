class GrimreaverKeppel : Grimreaver {
	Default {
		Tag "Keppel Grimreaver";
	}
	
	States {
		Spawn:
			GRVK AA 4 {
				return A_MinionIdle();
			}
			Loop;
			
		Guard:
			GRVK AA 4 {
				return A_MinionGuard();
			}
			Loop;
			
		Follow:
			GRVK AA 4 {
				return A_FollowMaster();
			}
			Loop;
			
		See:
			GRVK AA 4 {
				return A_MinionChase();
			}
			Loop;
		
		Missile:
			GRVK B 20 A_FaceTarget();
			GRVK B 10 A_FaceTarget();
			GRVK C 4 bright A_DeathNova();
			GRVK D 4 bright A_GrimreaverRefire();
			Goto Missile + 1;
			
		Pain:
			GRVK E 6;
			GRVK E 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			GRVK E 5;
			GRVK F 5 A_Scream();
			GRVK G 5;
			GRVK H 5 A_CreatureDie();
			GRVK IJKLM 5;
			GRVK N -1 A_BoneBurst();
			Stop;
	}
}
