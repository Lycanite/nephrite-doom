class ChaosMinion : MinionCreature {
	ChaosMinion youngerChaosMinion;
	ChaosMinion olderChaosMinion;
	
	bool burstHeal;
	Actor detonationTarget;
	String detonationProjectile;
	float detonationRange;
	int burstEnergy; // How much Havoc Energy this Creature gives from Foul Burst damage ticks.
	int burstEnergyCharge; // How many ticks this Creature is havoc Charged for, counts down from max since the last Foul Burst.
	int burstEnergyChargeMax; // The Charge Ticks to set when a Foul Burst starts.

	property BurstHeal: burstHeal;
	property BurstEnergy: burstEnergy;
	property BurstEnergyCharge: burstEnergyCharge;
	property BurstEnergyChargeMax: burstEnergyChargeMax;

	Default {
		MinionCreature.MinionCategory "chaos";
		ChaosMinion.BurstHeal false;
		ChaosMinion.BurstEnergy 1;
		ChaosMinion.BurstEnergyCharge 0;
		ChaosMinion.BurstEnergyChargeMax 20;
	}

	override void PostBeginPlay() {
		super.PostBeginPlay();
		
		// Add To Chaos Minion Collection:
		if (self.HasPlayerMaster()) {
			int minionCount = self.GetSchismPlayerInfo().stats[self.GetPlayerNumber()].minions.Size();
			for (int i = minionCount - 1; i >= 0; i--) {
				ChaosMinion chaosSibling = ChaosMinion(self.GetSchismPlayerInfo().stats[self.GetPlayerNumber()].minions[i]);
				if (chaosSibling && chaosSibling != self) {
					olderChaosMinion = chaosSibling;
					olderChaosMinion.youngerChaosMinion = self;
					break;
				}
			}
		}
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}

		// Foul Burst Charge:
		if (self.burstEnergyCharge > 0) {
			self.burstEnergyCharge--;
		}

		// Detonation:
		if (self.detonationTarget && Distance3D(self.detonationTarget) <= (self.detonationTarget.radius + self.radius) + self.detonationRange) {
			if (self.detonationProjectile) {
				EmitFollowProjectile(self.detonationProjectile);
			}
			self.A_Die();
			self.detonationTarget = null;
		}
	}
	
	// Spawns a Follow Projectile which will follow this actor during its lifetime.
	virtual FollowProjectile EmitFollowProjectile(String projectileClass, int minionCount = 0) {
		FollowProjectile followProjectile = SchismAttacks.EmitFollowProjectile(projectileClass, self, self);
		if (followProjectile) {
			if (burstHeal) {
				A_DamageSelf(-2, "None", DMSS_FOILINVUL|DMSS_NOFACTOR|DMSS_NOPROTECT);
			}
			
			FoulBurstProjectile foulBurstProjectile = FoulBurstProjectile(followProjectile);
			if (foulBurstProjectile) {
				self.burstEnergyCharge = self.burstEnergyChargeMax;
				foulBurstProjectile.havoc = self.burstEnergy;
			}
		}
		return followProjectile;
	}
	
	virtual void SetDetonationTarget(Actor detonationTarget, String detonationProjectile, float detonationRange) {
		self.detonationTarget = detonationTarget;
		self.detonationProjectile = detonationProjectile;
		self.detonationRange = detonationRange;
	}
	
	action void A_MinionHavocMeleeAttack(int damageMin, int damageMax = 0, string damageType = "Melee") {
		invoker.A_CreatureMelee(damageMin, damageMax, damageType);
		if (invoker.master && invoker.burstEnergyCharge > 0) {
			invoker.A_GiveInventory("HavocEnergy", invoker.burstEnergy, AAPTR_MASTER);
		}
	}
}