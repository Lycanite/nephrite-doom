class Trite : MinionCreature {
	Default {
		Tag "Trite";
		Health 50;
		Radius 20;
		Height 25;
		Mass 100;
		Speed 16;
		Scale 0.5;
		MeleeThreshold 64;
		MaxTargetRange 160;
		MinMissileChance 10;
		PainChance 192;

		MinionCreature.SummonTime 30 * 35;

		SeeSound "Trite/See";
		ActiveSound "Trite/Idle";
		PainSound "Trite/Pain";
		DeathSound "Trite/Death";
		MeleeSound "Trite/Melee";
		HitObituary "%o was nipped by a Trite.";
		
		+DONTHARMCLASS;
	}
	
	action void A_SetLeaping(bool isLeaping) {
		if (isLeaping) {
			Gravity = 0.125;
		}
		else {
			Gravity = 1;
		}
		invoker.bNODAMAGE = isLeaping;
		invoker.bNOPAIN = isLeaping;
	}
	
	States {
		Spawn:
			TRIT ABCB 6 A_Look();
			Loop;
			
		See:
			TRIT DEFE 4 {
				A_SetLeaping(false);
				A_StartSound("Trite/Step", CHAN_7);
				A_Chase();
			}
			Loop;
			
		Melee:
			TRIT G 3 A_FaceTarget();
			TRIT H 3 {
				A_FaceTarget();
				A_Jump(192, 2);
				A_StartSound("Trite/Hit");
			}
			TRIT I 6 A_CustomMeleeAttack(3);
			Goto See;
			
		Missile:
			TRIT G 0 A_Jump(128, "See");
			TRIT G 3 A_FaceTarget();
			TRIT H 10 {
				A_SetLeaping(true);
				A_StartSound("Trite/Melee");
				A_SkullAttack();
				ThrustThingZ(0, 6, 0, 1);
			}
			TRIT H 0 A_SetLeaping(false);
			TRIT I 8 A_Stop();
			Goto See;
			
		Pain:
			TRIT JK 3;
			TRIT L 3 A_Pain();
			Goto See;
			
		Death:
			TRIT JKLM 3;
			TRIT N 3 A_Scream();
			TRIT O 3 A_CreatureDie();
			TRIT PQRSTUVWX 3;
			Stop;
	}
}