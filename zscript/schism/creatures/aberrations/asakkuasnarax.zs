class AsakkuAsnarax : Creature {
	Default {
		Tag "Asakku Asnarax";
		Scale 1;
		Health 4000;
		Radius 128;
		Height 100;
		Speed 12;
		Gravity 1;
		Mass 1000;
		PainChance 40;
		
		SeeSound "AsakkuAsnarax/See";
		ActiveSound "AsakkuAsnarax/Active";
		PainSound "AsakkuAsnarax/Pain";
		DeathSound "AsakkuAsnarax/Death";
		AttackSound "AsakkuAsnarax/Fire";
		Obituary "%o was annihilated by an Asakku Asnarax!";
		HitObituary "%o was annihilated by an Asakku Asnarax!";
		
		+BOSS;
		+BOSSDEATH;
		+MISSILEMORE;
		+NORADIUSDMG;
		+DONTMORPH;
	}

	action void A_AsnaraxFire() {
		A_CreatureHitscan(5, 15, 3);
	}
	
	States {
		Spawn:
			AKRX A 10 A_Look();
			Loop;
			
		See:
			AKRX A 3 {
				A_Chase();
				A_StartSound("AsakkuAsnarax/Step");
			}
			AKRX ABB 3 A_Chase();
			AKRX C 3 {
				A_Chase();
				A_StartSound("AsakkuAsnarax/Step");
			}
			AKRX CDD 3 A_Chase();
			AKRX E 3 {
				A_Chase();
				A_StartSound("AsakkuAsnarax/Step");
			}
			AKRX EFF 3 A_Chase();
			Loop;
			
		Missile:
			AKRX A 20 A_FaceTarget();
            AKRX A 0 A_Jump(256, "MissileStar", "MissileRuiner");
		MissileStar:
			AKRX H 4 bright {
				Actor projectile = A_SpawnProjectile("IchorspitProjectile", 52);
				if (projectile) {
					projectile.speed = 16;
				}
				A_AsnaraxFire();
			}
			AKRX G 4 bright A_AsnaraxFire();
            AKRX G 0 A_Jump(24, "MissileRuiner");
			AKRX G 1 A_MonsterRefire(10, "See");
			Goto MissileStar;
		MissileRuiner:
			AKRX GH 8 bright {
				A_DualPainAttack("HadesSphereProjectile");
				A_AsnaraxFire();
			}
			AKRX G 1 A_MonsterRefire(10, "See");
			Goto MissileStar;
			
		Pain:
			AKRX I 3;
			AKRX I 3 A_Pain();
			Goto See;
			
		Death:
			AKRX J 6;
			AKRX K 6 A_Scream();
			AKRX LM 6;
			AKRX N 6 A_CreatureDie();
			AKRX OPQR 6;
			AKRX S -1 A_BossDeath();
			Stop;
	}
}